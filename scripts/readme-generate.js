const fs = require('fs');
const path = require('path');

// Chemins des fichiers Markdown pour les sections
const sections = {
	'#whatIs': path.join('src', 'app', 'docs', 'started', 'what-is', 'index.md'),
	'#peerDependencies': path.join('src', 'app', 'docs', 'started', 'peer-dependency', 'index.md'),
	'#installLib': path.join('src', 'app', 'docs', 'started', 'install', 'index.md'),
	'#usage': path.join('src', 'app', 'docs', 'started', 'usage', 'index.md'),
};

// Chemin du fichier template recap.md
const templateFile = 'recap.md';

// Chemins des fichiers README à générer
const outputFiles = [
	path.join('projects', 'm-ui', 'README.md'),
	'README.md'
];

// Fonction pour lire et remplacer les @include dans le template, tout en filtrant les lignes spécifiques
function generateContentFromTemplate(templatePath, sectionsMap) {
	// Lire le fichier template (recap.md)
	let content = fs.readFileSync(templatePath, 'utf-8');

	Object.entries(sectionsMap).forEach(([tag, filePath]) => {
		if (fs.existsSync(filePath)) {
			let fileContent = fs.readFileSync(filePath, 'utf-8');

			// Supprimer les lignes contenant "{{ NgDocPage.title }}"
			if (typeof fileContent === 'string') {
				fileContent = fileContent
					.split('\n')
					.filter(line => !line.includes('{{ NgDocPage.title }}')) // Filtre les lignes avec "{{ NgDocPage.title }}"
					.map(line => {
						// Ajouter un niveau au titre (# => ##, ## => ###, etc.)
						if (line.startsWith('#')) {
							return `#${line}`; // Ajoute un '#' au début de la ligne
						}
						return line;
					})
					.join('\n');
			}

			// Remplace la balise @include dans le template
			content = content.replace(`@include ${tag}`, fileContent);
		} else {
			console.error(`Fichier non trouvé pour ${tag} : ${filePath}`);
			content = content.replace(`@include ${tag}`, `<!-- Fichier non trouvé : ${filePath} -->`);
		}
	});

	return content;
}

// Fonction pour écrire le contenu dans les fichiers de sortie
function writeToOutputFiles(content, outputPaths) {
	outputPaths.forEach((outputPath) => {
		const outputDirectory = path.dirname(outputPath);

		// Crée le répertoire si nécessaire
		if (!fs.existsSync(outputDirectory)) {
			fs.mkdirSync(outputDirectory, { recursive: true });
		}

		// Écrit le contenu dans le fichier
		fs.writeFileSync(outputPath, content);
		console.log(`Fichier généré avec succès : ${outputPath}`);
	});
}

// Génération des contenus
try {
	const finalContent = generateContentFromTemplate(templateFile, sections);

	// Écriture dans les deux fichiers README
	writeToOutputFiles(finalContent, outputFiles);
} catch (error) {
	console.error('Une erreur est survenue lors de la génération des README :', error);
}
