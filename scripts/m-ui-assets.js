const fs = require('fs');
const path = require('path');
const fse = require('fs-extra'); // Utilisation de fs-extra pour la copie de dossiers

// Utilisez `path.resolve` pour construire les chemins absolus à partir de la racine du projet
const rootDir = path.resolve(__dirname, '..'); // Remonte d'un niveau pour atteindre la racine
const sourceDir = path.join(rootDir, 'projects', 'm-ui', 'src', 'assets', 'icons');
const targetDir = path.join(rootDir, 'src', 'assets', 'icons');

// Vérifiez que le dossier source existe avant de copier
if (!fs.existsSync(sourceDir)) {
	console.error(`Le dossier source n'existe pas : ${sourceDir}`);
	process.exit(1); // Quitte le script avec une erreur
}

// Copie le dossier `assets` de manière récursive
fse.copy(sourceDir, targetDir, (err) => {
	if (err) {
		console.error('Erreur lors de la copie des assets :', err);
	} else {
		console.log(`Dossier 'assets' copié de ${sourceDir} vers ${targetDir}`);
	}
});
