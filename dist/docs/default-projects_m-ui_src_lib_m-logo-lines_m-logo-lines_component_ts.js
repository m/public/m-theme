"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["default-projects_m-ui_src_lib_m-logo-lines_m-logo-lines_component_ts"],{

/***/ 42631:
/*!**********************************************************************!*\
  !*** ./projects/m-ui/src/lib/m-logo-lines/m-logo-lines.component.ts ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MLogoLines": () => (/* binding */ MLogoLines)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/checkbox */ 44792);






function MLogoLines_ng_container_1_ng_container_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](0);
  }
}
function MLogoLines_ng_container_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, MLogoLines_ng_container_1_ng_container_1_Template, 1, 0, "ng-container", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngTemplateOutlet", _r3);
  }
}
function MLogoLines_ng_template_2_ng_container_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](0);
  }
}
function MLogoLines_ng_template_2_Template(rf, ctx) {
  if (rf & 1) {
    const _r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-checkbox", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("change", function MLogoLines_ng_template_2_Template_mat_checkbox_change_0_listener($event) {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r8);
      const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresetView"](ctx_r7.clickLine(ctx_r7.ligne, $event));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, MLogoLines_ng_template_2_ng_container_1_Template, 1, 0, "ng-container", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("description", ctx_r2.description)("hideInput", !ctx_r2.description);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("checked", ctx_r2.checked);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattribute"]("id", ctx_r2.ligne.id);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngTemplateOutlet", _r3);
  }
}
const _c0 = function (a0, a1, a2) {
  return {
    "width.px": a0,
    "height.px": a1,
    "opacity": a2
  };
};
function MLogoLines_ng_template_4__svg_svg_6_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "svg", 9)(1, "defs")(2, "style");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, " .relais-cls-1 {fill: #fff; stroke: #ea541e; stroke-miterlimit: 10;} .relais-cls-2 {fill: #ea541e; stroke-width: 0;} ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "g", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](5, "rect", 11)(6, "path", 12)(7, "path", 13)(8, "path", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
  }
  if (rf & 2) {
    const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction3"](1, _c0, ctx_r9.width, ctx_r9.height, ctx_r9.opacity));
  }
}
const _c1 = function () {
  return ["TRAM", "CHRONO", "NAVETTE", "CHRONO_PERI"];
};
const _c2 = function () {
  return ["CHRONO", "NAVETTE", "TRAM", "CHRONO_PERI"];
};
const _c3 = function (a0, a1, a2, a3, a4, a5) {
  return {
    "width.px": a0,
    "height.px": a1,
    "background": a2,
    "font-size": a3,
    "opacity": a4,
    "checkbox": a5
  };
};
const _c4 = function () {
  return ["NAVETTE"];
};
function MLogoLines_ng_template_4_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnamespaceSVG"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "svg", 6)(2, "title");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "text", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](6, MLogoLines_ng_template_4__svg_svg_6_Template, 9, 5, "svg", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("circle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](13, _c1).includes(ctx_r4.lineType) || ctx_r4.isCircle)("rounded", !_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](14, _c2).includes(ctx_r4.lineType) || ctx_r4.isRounded)("square", ctx_r4.isSquare);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngStyle", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction6"](15, _c3, ctx_r4.width, ctx_r4.height, "#" + ((ctx_r4.ligne == null ? null : ctx_r4.ligne.color) || "999999"), ctx_r4.fontSize, ctx_r4.opacity, ctx_r4.disturbance == null ? null : ctx_r4.disturbance.hasDisturbance))("ngClass", ctx_r4.getClass());
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattribute"]("viewBox", ctx_r4.viewBox);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r4.longName);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵattribute"]("fill", "#" + ((ctx_r4.ligne == null ? null : ctx_r4.ligne.textColor) || "ffffff"));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r4.shortName);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](22, _c4).includes(ctx_r4.lineType));
  }
}
/**
 * @component
 * @name MLogoLines
 * @description
 * A component that displays a logo for a given transport line. It customizes the size, padding,
 * and shape of the logo based on the input properties, and calculates optimal font size
 * for the line's name display.
 *
 * ### Usage example
 * ```html
 * <m-logo-lines [ligne]="lineData" [height]="50" [width]="50"></m-logo-lines>
 * ```
 */
class MLogoLines {
  constructor() {
    /** Padding coefficient for the top of the logo. */
    this.paddingCoefTop = 0.23;
    /** Padding coefficient for the left side of the logo. */
    this.paddingCoefLeft = 0.23;
    /** If `true`, displays the logo in a circular shape */
    this.isCircle = false;
    /** If `true`, applies rounded corners to the logo. */
    this.isRounded = false;
    /** If `true`, displays the logo in a square shape. */
    this.isSquare = false;
    /** If `true` display mat-checkbox behind logo-lines */
    this.isCheckbox = false;
    /** If `true`, displays the line's description. */
    this.description = false;
    /** If `true`, selects the line. */
    this.checked = false;
    /** The opacity of the logo. */
    this.opacity = 1;
    /** Emits when the line is clicked. */
    this.lineChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.EventEmitter();
  }
  /**
   * @method onClick
   * @description
   * Emits the `lineClicked` event when the line is clicked.
   *
   * @returns {void}
   */
  clickLine(ligne, evt) {
    this.lineChange.emit({
      line: ligne,
      checked: evt.checked
    });
  }
  /**
   * @method ngOnChanges
   * @description
   * Updates the component when input properties change. Calculates viewBox, font size, and other
   * properties based on the input line data and dimensions.
   *
   * @param {SimpleChanges} changes - The changes in input properties.
   * @returns {void}
   */
  ngOnChanges(changes) {
    if (!changes.ligne) return;
    this.shortName = this.ligne.shortName;
    this.longName = this.ligne.longName;
    this.lineType = this.ligne.type;
    if (this.ligne.type === 'SNC') this.shortName = 'TER';
    if (this.ligne.type === 'NAVETTE') this.shortName = this.ligne.shortName.substring(this.ligne.shortName.length - 1);
    if (changes.width || changes.height) {
      this.viewBox = `0 0 ${this.width} ${this.height}`;
      const fontHeight = this.height - this.height * this.paddingCoefTop;
      const maxTextWidth = this.width - this.width * this.paddingCoefLeft;
      const fzh = 0.8 / 16 * fontHeight;
      const fzw = 0.8 / 8 * (maxTextWidth / this.shortName.length);
      this.fontSize = `${Math.min(fzw, fzh)}em`;
    }
  }
  /**
   * Attribute class for any disturbances
   * @returns {string}
   */
  getClass() {
    if (this.disturbance && this.disturbance.hasDisturbance) {
      const hasOuter = this.disturbance.outer ? 'outer' : '';
      const hasDisturbance = this.disturbance.hasDisturbance ? 'has-disturbance' : '';
      let nsv = '';
      if (this.disturbance.nsv) {
        nsv = `disturbance-${this.disturbance.nsv}`;
      }
      return `${hasOuter} ${hasDisturbance} ${nsv}`;
    }
  }
  static #_ = this.ɵfac = function MLogoLines_Factory(t) {
    return new (t || MLogoLines)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
    type: MLogoLines,
    selectors: [["m-logo-lines"]],
    hostVars: 4,
    hostBindings: function MLogoLines_HostBindings(rf, ctx) {
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵstyleProp"]("height", ctx.height, "px")("width", ctx.width, "px");
      }
    },
    inputs: {
      ligne: "ligne",
      paddingCoefTop: "paddingCoefTop",
      paddingCoefLeft: "paddingCoefLeft",
      height: "height",
      width: "width",
      isCircle: "isCircle",
      isRounded: "isRounded",
      isSquare: "isSquare",
      isCheckbox: "isCheckbox",
      description: "description",
      checked: "checked",
      opacity: "opacity",
      disturbance: "disturbance"
    },
    outputs: {
      lineChange: "lineChange"
    },
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵNgOnChangesFeature"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵStandaloneFeature"]],
    decls: 6,
    vars: 2,
    consts: [[4, "ngIf", "ngIfElse"], ["logoCheckbox", ""], ["logolines", ""], [4, "ngTemplateOutlet"], ["labelPosition", "before", "type", "checkbox", 1, "m-logo-line-checkbox", 3, "checked", "change"], [1, "m-logo-line-icons"], [1, "m-logo-line", 3, "ngStyle", "ngClass"], ["x", "51%", "y", "50%", "text-anchor", "middle", "dy", "0.34em"], ["viewBox", "0 0 59.3 40.1", "class", "relais", 3, "ngStyle", 4, "ngIf"], ["viewBox", "0 0 59.3 40.1", 1, "relais", 3, "ngStyle"], ["transform", "translate(0 16)"], ["x", "3", "y", "7.9", "width", "53.8", "height", "31.5", "rx", "1.7", "ry", "1.7", "transform", "translate(-1.8 0) rotate(-5.5)", 1, "relais-cls-1"], ["d", "M17.5,19.6c.9,0,1.7,0,2.5.2.8.2,1.3.6,1.8,1.2s.9,1.1,1.2,1.9.4,1.5.7,2.2c0,.8.1,1.5,0,2.1,0,.8-.3,1.4-.7,1.9s-1.1,1.3-1.8,1.8-1.4.7-2.4.8-1.7,0-2.5-.2-1.4-.8-2.1-1.5h-.2s0,.2,0,.3v.8c.1.3,0,.5,0,.8l-.4.5c-.3.2-.6,0-.9,0-.6,0-1.1-.4-1.4-.8-.3-.4-.5-.9-.7-1.3-.2-.4-.4-.9-.4-1.5s-.2-.9-.3-1l-.2-2.6c0-.8,0-1.7,0-2.4,0-.8,0-1.5.2-2.5s.3-1.7.4-2.6c0-.8.3-1.4.4-2,.2-.6.3-1.2.7-1.7.2-.6.7-1.1,1.1-1.6s.8-1,1.5-1.5c.6-.5,1.3-.7,2.1-.8s1.5,0,2.2.4c.6.2,1.3.8,1.7,1.4s.6,1.3.5,2.1c0,.6-.2,1.2-.4,1.7-.1.5-.4,1.1-.6,1.6s-.5,1-.8,1.4c-.3.2-.6.5-.8.8M20.8,26.7c.2-.8.3-1.4,0-2.1,0-.8-.3-1.3-.6-1.9s-.7-1-1.2-1.3c-.5-.3-1-.5-1.3-.5-.5-.1-1.1,0-1.4.1-.5,0-.7.2-1.2.4l-.9.5c-.1.2-.4.3-.4.6-.1.3-.2.8-.3,1.2s0,.9,0,1.5l.9-.5c.3-.2.6-.2.9-.4.3,0,.6-.2.9,0,.3,0,.5.1.5.1q.2.1,0,.3c-.1.2-.3.2-.6.4s-.6.4-.8.7c-.3.3-.5.7-.6,1.3-.1.5,0,.8.1,1.1s.4.6.8.8c.5.1.9.2,1.4.2s1-.3,1.5-.4c.4-.2.9-.5,1.3-.9.6-.4.8-.7,1-1.2M17.3,12.9c-.3-.1-.6,0-.9.2-.3.3-.7.7-1,1.2-.3.5-.5,1.1-.7,1.7-.2.6-.5,1.3-.6,1.7s-.2.9-.2,1.4,0,.5.2.4c.6,0,1.2-.4,1.6-.8.4-.3.8-.8,1.2-1.3.3-.5.5-1,.8-1.6.1-.5.2-1.1.3-1.5v-1.2c-.3,0-.4-.3-.7-.2", 1, "relais-cls-2"], ["d", "M28.2,18.7v1.1c.1.5.2,1.1.3,1.7l.2,2c0,.6.3,1.2.3,1.8.2.6.4,1,.6,1.3.2.3.5.4.8.4.5,0,.9-.2,1.2-.6.3-.3.5-.8.8-1.4.2-.6.3-1.2.4-1.9,0-.6.2-1.4.1-2,0-.6,0-1.2,0-1.8s-.1-1.1,0-1.4c0-.3,0-.6.2-.8.1-.2.4-.3.7-.4s.6,0,.9,0l.9.4c.3.1.5.4.8.5.2.3.3.4.4.7v1.4c.2.6,0,1.2.2,2l.2,2.3c0,.8.1,1.5.4,2.1s.4,1.2.6,1.6c.3.4.7.7,1.1.8s.6.2.8.4.2.3,0,.5-.3.3-.6.4c-.3.2-.8,0-1.4.1-.9,0-1.7-.3-2.4-.8s-1-1-1.3-1.4c-.4-.6-.6-1.3-.7-2.1-.2.8-.5,1.6-.9,2.2-.2.6-.8,1.1-1.2,1.5-.6.5-1.3.6-2.1.7-.9,0-1.5-.3-2.2-.6-.6-.4-1.1-.8-1.5-1.4-.4-.6-.7-1.1-.9-1.9s-.4-1.3-.5-2.1-.3-1.2-.3-1.6l-.2-1.7c0-.5,0-.9,0-1.2.1-.3,0-.6.2-.8.1-.2.4-.2.7-.4.4-.2.7-.2,1.2-.3s.8,0,1.1,0c.6,0,.8.2.8.7", 1, "relais-cls-2"], ["d", "M47.5,13.8c.2.1,0,.3,0,.5-.1.2-.3.2-.4.3s-.4.2-.6.4c-.3.2-.4.3-.7.7-.3.3-.4.6-.5,1-.1.3,0,.8,0,1.4s.3,1.2.6,1.6c.4.6.7,1,1.2,1.6.5.4,1,1,1.5,1.4.5.4,1,1,1.5,1.4.5.4.9,1,1.1,1.6.4.6.4,1,.5,1.6,0,.6-.4,1.1-.8,1.6-.6.5-1.1.9-1.9,1.1s-1.6.5-2.5.5-1.8.2-2.6,0c-.8,0-1.5-.3-2.2-.6s-1-.8-1.2-1.3c-.4-.9-.4-1.5-.4-2.1,0-.6.2-1.1.3-1.5.3-.5.4-.8.8-1,.3-.2.6-.5.7-.5.4-.3.7-.4.9,0s0,.8-.2,1.2c-.1.3,0,.6,0,.9.2.3.4.7.7,1s.8.5,1.3.6,1.1.2,1.7.1.8,0,1-.3c.4-.2.7-.4.9-.5.3-.2.4-.5.4-.8s0-.6-.4-.9-.8-.7-1.5-1.2-1.5-.9-2.1-1.6c-.8-.5-1.5-1.2-2-1.9s-.9-1.4-1-2.2c-.1-1.5,0-2.6.4-3.2.4-.8,1.3-1.2,2.4-1.6.6-.2,1-.3,1.3-.3s.6,0,.9,0c.2.1.3.1.5.3,0,.6.2.7.2.9", 1, "relais-cls-2"]],
    template: function MLogoLines_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, MLogoLines_ng_container_1_Template, 2, 1, "ng-container", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, MLogoLines_ng_template_2_Template, 2, 7, "ng-template", null, 1, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, MLogoLines_ng_template_4_Template, 7, 23, "ng-template", null, 2, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
      }
      if (rf & 2) {
        const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx.isCheckbox)("ngIfElse", _r1);
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule, _angular_common__WEBPACK_IMPORTED_MODULE_1__.NgClass, _angular_common__WEBPACK_IMPORTED_MODULE_1__.NgIf, _angular_common__WEBPACK_IMPORTED_MODULE_1__.NgTemplateOutlet, _angular_common__WEBPACK_IMPORTED_MODULE_1__.NgStyle, _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_2__.MatCheckboxModule, _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_2__.MatCheckbox],
    styles: ["[_nghost-%COMP%] {\n  position: relative;\n}\n[_nghost-%COMP%]   div.m-logo-line-icons[_ngcontent-%COMP%] {\n  position: relative;\n  display: flex;\n}\n[_nghost-%COMP%]   div.m-logo-line-icons[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%] {\n  box-sizing: border-box;\n  font-family: Arial, serif;\n  font-weight: bold;\n  -webkit-user-select: none;\n          user-select: none;\n}\n[_nghost-%COMP%]   div.m-logo-line-icons[_ngcontent-%COMP%]   svg[_ngcontent-%COMP%]   a.relais[_ngcontent-%COMP%] {\n  border: none !important;\n}\n[_nghost-%COMP%]   div.m-logo-line-icons[_ngcontent-%COMP%]   svg.checkbox[_ngcontent-%COMP%] {\n  border: 4px solid transparent;\n  transition: all 250ms;\n}\n[_nghost-%COMP%]   div.m-logo-line-icons[_ngcontent-%COMP%]   svg.relais[_ngcontent-%COMP%] {\n  position: absolute;\n  top: 4px;\n  left: 0;\n}\n[_nghost-%COMP%]   div.m-logo-line-icons[_ngcontent-%COMP%]   svg.square[_ngcontent-%COMP%] {\n  border-radius: unset;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3Byb2plY3RzL20tdWkvc3JjL3RoZW1lL2dsb2JhbC9fdmFyaWFibGVzLnNjc3MiLCJ3ZWJwYWNrOi8vLi9wcm9qZWN0cy9tLXVpL3NyYy90aGVtZS9nbG9iYWwvX2hlbHBlcnMuc2NzcyIsIndlYnBhY2s6Ly8uL3Byb2plY3RzL20tdWkvc3JjL3RoZW1lL3RoZW1lcy9faW5kZXguc2NzcyIsIndlYnBhY2s6Ly8uL3Byb2plY3RzL20tdWkvc3JjL2xpYi9tLWxvZ28tbGluZXMvbS1sb2dvLWxpbmVzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQWdCQSxrQkFBQTtBQUlBLGVBQUE7QUFHQSxvQkFBQTtBQU9BLGdCQUFBO0FBR0Esb0JBQUE7QUFHQSxpQkFBQTtBQUlBLGdCQUFBO0FBR0Esb0JBQUE7QUNwQ0E7Ozs7OztHQUFBO0FBV0E7Ozs7OztHQUFBO0FBNkJBOztFQUFBO0FBWUE7O0VBQUE7QUFzQ0E7O0dBQUE7QUN0RkE7O0VBQUE7QUNDQTtFQUNDLGtCQUFBO0FBdUJEO0FBckJDO0VBQ0Msa0JBQUE7RUFDQSxhQUFBO0FBdUJGO0FBckJFO0VBVUMsc0JBQUE7RUFDQSx5QkFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7VUFBQSxpQkFBQTtBQWNIO0FBMUJHO0VBQ0MsdUJBQUE7QUE0Qko7QUF6Qkc7RUFmRiw2QkFBQTtFQWlCRyxxQkFBQTtBQTJCSjtBQW5CRztFQUNDLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLE9BQUE7QUFxQko7QUFsQkc7RUFDQyxvQkFBQTtBQW9CSiIsInNvdXJjZXNDb250ZW50IjpbIiRhc3NldHMtcGF0aDogXCJeLi9hc3NldHNcIiAhZGVmYXVsdDtcclxuJHNwYWNpbmc6IDhweCAhZGVmYXVsdDtcclxuXHJcbiRlbGV2YXRpb24tY29sb3JzOiAoXHJcbiAgMDogMCUsXHJcbiAgMTogNSUsXHJcbiAgMjogNyUsXHJcbiAgMzogOCUsXHJcbiAgNDogOSUsXHJcbiAgNjogMTElLFxyXG4gIDg6IDEyJSxcclxuICAxMjogMTQlLFxyXG4gIDE2OiAxNSUsXHJcbiAgMjQ6IDE2JSxcclxuKSAhZGVmYXVsdDtcclxuXHJcbi8qKioqIHRvb2xiYXIgKioqKi9cclxuJHRvb2xiYXItaGVpZ2h0OiA1NnB4ICFkZWZhdWx0O1xyXG4kdG9vbGJhci1tYWluLW5hdi1oZWlnaHQ6ICR0b29sYmFyLWhlaWdodCArICRzcGFjaW5nICFkZWZhdWx0O1xyXG5cclxuLyoqKiogZm9udCAqKioqL1xyXG4kZm9udC1mYW1pbHk6IFJvYm90bywgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBzYW5zLXNlcmlmO1xyXG5cclxuLyoqKiogZm9udCBzaXplICoqKiovXHJcbiRkZWZhdWx0LWZ6OiAxLjRyZW0gIWRlZmF1bHQ7XHJcbiRkZWZhdWx0LWZ6LXNtYWxsOiAxLjJyZW0gIWRlZmF1bHQ7XHJcbiRkZWZhdWx0LWZ6LWJpZzogMS44cmVtICFkZWZhdWx0O1xyXG4kaGVhZGVyLWZ6OiAkZGVmYXVsdC1meiArIDAuMnJlbSAhZGVmYXVsdDtcclxuJGhlYWRlci1mei1iaWc6ICRoZWFkZXItZnogKyAwLjRyZW0gIWRlZmF1bHQ7XHJcblxyXG4vKioqKiBJY29ucyAqKioqL1xyXG4kaWNvbi1sYXJnZTogNDBweCAhZGVmYXVsdDtcclxuXHJcbi8qKioqIENvbnRhaW5lciAqKioqL1xyXG4kZW5kLWJnLXNpemU6IDE0M3B4ICsgJHNwYWNpbmcgKiAyICFkZWZhdWx0O1xyXG5cclxuLyoqKiogc2hhZG93ICoqKiovXHJcbiRsaWdodC1ib3gtc2hhZG93OiAwIDRweCA4cHggcmdiYShibGFjaywgMC41KSAhZGVmYXVsdDtcclxuJGRhcmstYm94LXNoYWRvdzogMCA0cHggOHB4IHJnYmEoYmxhY2ssIDAuNykgIWRlZmF1bHQ7XHJcblxyXG4vKioqKiBTaGFwZSAqKioqL1xyXG4kc2hhcGUtcmFkaXVzOiA4cHggIWRlZmF1bHQ7XHJcblxyXG4vKioqIEJyZWFrcG9pbnQgKioqKi9cclxuJGJyZWFrcG9pbnQ6IDEwNzlweDtcclxuIiwiQHVzZSBcInNhc3M6bWFwXCI7XHJcbkB1c2UgXCJzYXNzOm1ldGFcIjtcclxuQHVzZSBcInNhc3M6bGlzdFwiO1xyXG5AdXNlIFwic2FzczptYXRoXCI7XHJcbkB1c2UgXCJzYXNzOnNlbGVjdG9yXCI7XHJcbkB1c2UgXCJ2YXJpYWJsZXNcIiBhcyB2YXI7XHJcblxyXG4vKipcclxuICogU2xpZ2h0bHkgbGlnaHRlbiBhIGNvbG9yXHJcbiAqIEBhY2Nlc3MgcHVibGljXHJcbiAqIEBwYXJhbSB7Q29sb3J9ICRjb2xvciAtIGNvbG9yIHRvIHRpbnRcclxuICogQHBhcmFtIHtOdW1iZXJ9ICRwZXJjZW50YWdlIC0gcGVyY2VudGFnZSBvZiBgJGNvbG9yYCBpbiByZXR1cm5lZCBjb2xvclxyXG4gKiBAcmV0dXJuIHtDb2xvcn1cclxuICoqL1xyXG5AZnVuY3Rpb24gdGludCgkY29sb3IsICRwZXJjZW50YWdlKSB7XHJcblx0QHJldHVybiBtaXgod2hpdGUsICRjb2xvciwgJHBlcmNlbnRhZ2UpO1xyXG59XHJcblxyXG4vKipcclxuICogU2xpZ2h0bHkgZGFya2VuIGEgY29sb3JcclxuICogQGFjY2VzcyBwdWJsaWNcclxuICogQHBhcmFtIHtDb2xvcn0gJGNvbG9yIC0gY29sb3IgdG8gc2hhZGVcclxuICogQHBhcmFtIHtOdW1iZXJ9ICRwZXJjZW50YWdlIC0gcGVyY2VudGFnZSBvZiBgJGNvbG9yYCBpbiByZXR1cm5lZCBjb2xvclxyXG4gKiBAcmV0dXJuIHtDb2xvcn1cclxuICoqL1xyXG5AZnVuY3Rpb24gc2hhZGUoJGNvbG9yLCAkcGVyY2VudGFnZSkge1xyXG5cdEByZXR1cm4gbWl4KGJsYWNrLCAkY29sb3IsICRwZXJjZW50YWdlKTtcclxufVxyXG5cclxuQG1peGluIGZ1bGwtaGVpZ2h0KCkge1xyXG5cdGhlaWdodDogY2FsYygxMDB2aCAtICN7dmFyLiR0b29sYmFyLW1haW4tbmF2LWhlaWdodH0pO1xyXG59XHJcblxyXG5AbWl4aW4gaWNvbi1tYXJnaW4oJGRpcmVjdGlvbikge1xyXG5cdG1hcmdpbi0jeyRkaXJlY3Rpb259OiB2YXIuJHNwYWNpbmc7XHJcblxyXG5cdCYtbGFyZ2Uge1xyXG5cdFx0bWFyZ2luLSN7JGRpcmVjdGlvbn06IHZhci4kc3BhY2luZyAqIDI7XHJcblx0fVxyXG5cdCYtdmVyeS1sYXJnZSB7XHJcblx0XHRtYXJnaW4tI3skZGlyZWN0aW9ufTogdmFyLiRzcGFjaW5nICogNDtcclxuXHR9XHJcblx0Ji1zbWFsbCB7XHJcblx0XHRtYXJnaW4tI3skZGlyZWN0aW9ufTogbWF0aC5kaXYodmFyLiRzcGFjaW5nLCAyKTtcclxuXHR9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBNdXN0IGJlIHVzZWQgb25seSBpbnNpZGUgYSBvdmVycmlkZS1YIG1peGluLlxyXG4gKi9cclxuQG1peGluIGRyb3Atc2hhZG93KCkge1xyXG5cdEBhdC1yb290ICN7c2VsZWN0b3IucmVwbGFjZSgmLCBcImJvZHlcIiwgXCJib2R5LmRhcmstdGhlbWVcIil9IHtcclxuXHRcdGJveC1zaGFkb3c6IHZhci4kZGFyay1ib3gtc2hhZG93O1xyXG5cdH1cclxuXHRAYXQtcm9vdCAje3NlbGVjdG9yLnJlcGxhY2UoJiwgXCJib2R5XCIsIFwiYm9keS5saWdodC10aGVtZVwiKX0ge1xyXG5cdFx0Ym94LXNoYWRvdzogdmFyLiRsaWdodC1ib3gtc2hhZG93O1xyXG5cdH1cclxufVxyXG5cclxuLyoqXHJcbiAqIE11c3QgYmUgdXNlZCBvbmx5IGluc2lkZSBhIG92ZXJyaWRlLVggbWl4aW4uXHJcbiAqL1xyXG5AbWl4aW4gYm90dG9tLXBpY3R1cmUoKSB7XHJcblx0Jjo6YWZ0ZXIge1xyXG5cdFx0Y29udGVudDogXCJcIjtcclxuXHRcdGRpc3BsYXk6IGJsb2NrO1xyXG5cdFx0aGVpZ2h0OiB2YXIuJGVuZC1iZy1zaXplO1xyXG5cdFx0YmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuXHRcdGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcblx0XHRAYXQtcm9vdCAje3NlbGVjdG9yLnJlcGxhY2UoJiwgXCJib2R5XCIsIFwiYm9keS5kYXJrLXRoZW1lXCIpfSB7XHJcblx0XHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybCh2YXIuJGFzc2V0cy1wYXRoICsgXCIvaW1hZ2VzL2JvdHRvbV9waWN0dXJlX21ldHJvbW9iX2RhcmsvYm90dG9tX3BpY3R1cmVfbWV0cm9tb2JfZGFyay5wbmdcIik7XHJcblxyXG5cdFx0XHRAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tcmVzb2x1dGlvbjogMjAwZHBpKSwgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDEwMjRweCkge1xyXG5cdFx0XHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybCh2YXIuJGFzc2V0cy1wYXRoICsgXCIvaW1hZ2VzL2JvdHRvbV9waWN0dXJlX21ldHJvbW9iX2RhcmsvYm90dG9tX3BpY3R1cmVfbWV0cm9tb2JfZGFya0AyeC5wbmdcIik7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdEBhdC1yb290ICN7c2VsZWN0b3IucmVwbGFjZSgmLCBcImJvZHlcIiwgXCJib2R5LmxpZ2h0LXRoZW1lXCIpfSB7XHJcblx0XHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybCh2YXIuJGFzc2V0cy1wYXRoICsgXCIvaW1hZ2VzL2JvdHRvbV9waWN0dXJlX21ldHJvbW9iX2xpZ2h0L2JvdHRvbV9waWN0dXJlX21ldHJvbW9iX2xpZ2h0LnBuZ1wiKTtcclxuXHJcblx0XHRcdEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1yZXNvbHV0aW9uOiAyMDBkcGkpLCBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMTAyNHB4KSB7XHJcblx0XHRcdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKHZhci4kYXNzZXRzLXBhdGggKyBcIi9pbWFnZXMvYm90dG9tX3BpY3R1cmVfbWV0cm9tb2JfbGlnaHQvYm90dG9tX3BpY3R1cmVfbWV0cm9tb2JfbGlnaHRAMngucG5nXCIpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG5AZnVuY3Rpb24gc3dpdGNoKCR0ZXN0LCAkYXJncy4uLikge1xyXG5cdCRjYXNlczogbWV0YS5rZXl3b3JkcygkYXJncyk7XHJcblx0QGlmIChtYXAuaGFzLWtleSgkY2FzZXMsICR0ZXN0KSkge1xyXG5cdFx0QHJldHVybiBtYXAuZ2V0KCRjYXNlcywgJHRlc3QpO1xyXG5cdH0gQGVsc2UgaWYgbWFwLmhhcy1rZXkoJGNhc2VzLCBcImRlZmF1bHRcIikge1xyXG5cdFx0QHJldHVybiBtYXAuZ2V0KCRjYXNlcywgXCJkZWZhdWx0XCIpO1xyXG5cdH0gQGVsc2Uge1xyXG5cdFx0QHJldHVybiBudWxsO1xyXG5cdH1cclxufVxyXG5cclxuLyoqXHJcbiAqIFNhc3MgZGVidWcgZnVuY3Rpb24gZnJvbSBodHRwczovL2NvZGVwZW4uaW8vS2l0dHlHaXJhdWRlbC9wZW4vdW55QkhcclxuICoqL1xyXG5AZnVuY3Rpb24gZGVidWctbWFwKCRsaXN0LCAkcHJlOiB0cnVlLCAkbGV2ZWw6IDEpIHtcclxuXHQkdGFiOiBcIiAgICBcIjtcclxuXHQkaW5kZW50OiBcIlwiO1xyXG5cdCRicmVhazogaWYoJHByZSwgXCJcXEEgXCIsIFwiXCIpO1xyXG5cclxuXHRAaWYgbGVuZ3RoKCRsaXN0KSA9PSAwIHtcclxuXHRcdEByZXR1cm4gXCIoIClcIjtcclxuXHR9XHJcblxyXG5cdEBpZiBsZW5ndGgoJGxpc3QpID09IDEge1xyXG5cdFx0QHJldHVybiBpZigkcHJlLCBcIihcIiArIHR5cGUtb2YoJGxpc3QpICsgXCIpIFwiLCBcIlwiKSArICRsaXN0O1xyXG5cdH1cclxuXHJcblx0QGZvciAkaSBmcm9tIDEgdG8gJGxldmVsIHtcclxuXHRcdCRpbmRlbnQ6ICRpbmRlbnQgKyAkdGFiO1xyXG5cdH1cclxuXHJcblx0JHJlc3VsdDogXCJbXCIgKyAkYnJlYWs7XHJcblxyXG5cdEBmb3IgJGkgZnJvbSAxIHRocm91Z2ggbGVuZ3RoKCRsaXN0KSB7XHJcblx0XHQkaXRlbTogbnRoKCRsaXN0LCAkaSk7XHJcblx0XHQkcmVzdWx0OiAkcmVzdWx0ICsgaWYoJHByZSwgJGluZGVudCArICR0YWIsIFwiIFwiKTtcclxuXHJcblx0XHRAaWYgbGVuZ3RoKCRpdGVtKSA+IDEge1xyXG5cdFx0XHQkcmVzdWx0OiAkcmVzdWx0ICsgaWYoJHByZSwgXCIobGlzdDogXCIgKyBsZW5ndGgoJGl0ZW0pICsgXCIpIFwiLCBcIlwiKSArIGRlYnVnLW1hcCgkaXRlbSwgJHByZSwgJGxldmVsICsgMSk7XHJcblx0XHR9IEBlbHNlIHtcclxuXHRcdFx0QGlmICRwcmUge1xyXG5cdFx0XHRcdCRyZXN1bHQ6ICRyZXN1bHQgKyBcIihcIiArIHR5cGUtb2YoJGl0ZW0pICsgXCIpIFwiO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRAaWYgbGVuZ3RoKCRpdGVtKSA9PSAwIHtcclxuXHRcdFx0XHQkcmVzdWx0OiAkcmVzdWx0ICsgXCIoIClcIjtcclxuXHRcdFx0fSBAZWxzZSBpZiB0eXBlLW9mKCRpdGVtKSA9PSBzdHJpbmcge1xyXG5cdFx0XHRcdCRyZXN1bHQ6ICRyZXN1bHQgKyBxdW90ZSgkaXRlbSk7XHJcblx0XHRcdH0gQGVsc2UgaWYgJGl0ZW0gPT0gbnVsbCB7XHJcblx0XHRcdFx0JHJlc3VsdDogJHJlc3VsdCArIFwibnVsbFwiO1xyXG5cdFx0XHR9IEBlbHNlIHtcclxuXHRcdFx0XHQkcmVzdWx0OiAkcmVzdWx0ICsgJGl0ZW07XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHJcblx0XHRAaWYgJGkgIT0gbGVuZ3RoKCRsaXN0KSB7XHJcblx0XHRcdCRyZXN1bHQ6ICRyZXN1bHQgKyBcIixcIiArICRicmVhaztcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdCRyZXN1bHQ6ICRyZXN1bHQgKyAkYnJlYWsgKyBpZigkcHJlLCBpZigkbGV2ZWwgPiAxLCAkaW5kZW50LCBcIlwiKSwgXCIgXCIpICsgXCJdXCI7XHJcblxyXG5cdEByZXR1cm4gcXVvdGUoJHJlc3VsdCk7XHJcbn1cclxuIiwiQHVzZSBcInNhc3M6bWFwXCI7XHJcbkB1c2UgXCJzYXNzOm1ldGFcIjtcclxuQHVzZSBcIi4vZGFyay9pbmRleFwiIGFzIGRhcms7XHJcbkB1c2UgXCIuL2xpZ2h0L2luZGV4XCIgYXMgbGlnaHQ7XHJcblxyXG4kZGFyay1vdmVycmlkZTogKCkgIWRlZmF1bHQ7XHJcbiRsaWdodC1vdmVycmlkZTogKCkgIWRlZmF1bHQ7XHJcblxyXG4kZGFyay10aGVtZTogKCkgIWRlZmF1bHQ7XHJcbiRsaWdodC10aGVtZTogKCkgIWRlZmF1bHQ7XHJcblxyXG4vKipcclxuICogSGVscGVyIGZ1bmN0aW9uIHRvIGdldCB0aGUgcmlnaHQgY29sb3Igb3ZlcmxheSBmdW5jdGlvbiBhY2NvcmRpbmcgdG8gdGhlIHRoZW1lLlxyXG4gKi9cclxuQGZ1bmN0aW9uIGdldC1vdmVybGF5LWZ1bmN0aW9uKCR0aGVtZSkge1xyXG4gICQtdGhlbWU6IG1hcC5nZXQoJHRoZW1lLCB0aGVtZSk7XHJcbiAgQHJldHVybiBtZXRhLmdldC1mdW5jdGlvbihcImNvbG9yLW92ZXJsYXlcIiwgZmFsc2UsIGlmKCQtdGhlbWUgPT0gXCJsaWdodFwiLCBcImxpZ2h0XCIsIFwiZGFya1wiKSk7XHJcbn1cclxuXHJcbkBmdW5jdGlvbiBkZWZpbmUoKSB7XHJcbiAgJGRhcmstdGhlbWU6IGRhcmsuZGVmaW5lKCRkYXJrLW92ZXJyaWRlKSAhZ2xvYmFsO1xyXG4gICRsaWdodC10aGVtZTogbGlnaHQuZGVmaW5lKCRsaWdodC1vdmVycmlkZSkgIWdsb2JhbDtcclxuICBAcmV0dXJuIChkYXJrLXRoZW1lOiAkZGFyay10aGVtZSwgbGlnaHQtdGhlbWU6ICRsaWdodC10aGVtZSk7XHJcbn1cclxuXHJcbkBtaXhpbiBvdmVycmlkZS10aGVtZXMoKSB7XHJcbiAgYm9keS5kYXJrLXRoZW1lICYge1xyXG4gICAgQGNvbnRlbnQgKCRkYXJrLXRoZW1lKTtcclxuICB9XHJcbiAgYm9keS5saWdodC10aGVtZSAmIHtcclxuICAgIEBjb250ZW50ICgkbGlnaHQtdGhlbWUpO1xyXG4gIH1cclxufVxyXG5cclxuQGZ1bmN0aW9uIGRhcmstY29sb3Itb3ZlcmxheSgkZWxldmF0aW9uKSB7XHJcbiAgQHJldHVybiBkYXJrLmNvbG9yLW92ZXJsYXkoJGVsZXZhdGlvbik7XHJcbn1cclxuXHJcbkBmdW5jdGlvbiBsaWdodC1jb2xvci1vdmVybGF5KCRlbGV2YXRpb24pIHtcclxuICBAcmV0dXJuIGxpZ2h0LmNvbG9yLW92ZXJsYXkoJGVsZXZhdGlvbik7XHJcbn1cclxuXHJcbkBmdW5jdGlvbiBkYXJrLWJhY2tncm91bmQtcGFsZXR0ZSgpIHtcclxuICBAcmV0dXJuIG1hcC5nZXQoJGRhcmstdGhlbWUsIGJhY2tncm91bmQpO1xyXG59XHJcblxyXG5AZnVuY3Rpb24gZGFyay1mb3JlZ3JvdW5kLXBhbGV0dGUoKSB7XHJcbiAgQHJldHVybiBtYXAuZ2V0KCRkYXJrLXRoZW1lLCBmb3JlZ3JvdW5kKTtcclxufVxyXG5cclxuQGZ1bmN0aW9uIGxpZ2h0LWJhY2tncm91bmQtcGFsZXR0ZSgpIHtcclxuICBAcmV0dXJuIG1hcC5nZXQoJGxpZ2h0LXRoZW1lLCBiYWNrZ3JvdW5kKTtcclxufVxyXG5cclxuQGZ1bmN0aW9uIGxpZ2h0LWZvcmVncm91bmQtcGFsZXR0ZSgpIHtcclxuICBAcmV0dXJuIG1hcC5nZXQoJGxpZ2h0LXRoZW1lLCBmb3JlZ3JvdW5kKTtcclxufVxyXG4iLCJAdXNlIFwic2FzczptYXBcIjtcclxuQHVzZSBcInNhc3M6bWF0aFwiO1xyXG5AdXNlIFwiQGFuZ3VsYXIvbWF0ZXJpYWxcIiBhcyBtYXQ7XHJcbkB1c2UgXCIuLi8uLi90aGVtZS9nbG9iYWxcIiBhcyBnbG9iYWw7XHJcbkB1c2UgXCIuLi8uLi90aGVtZS9jb21wb25lbnRzL2J1dHRvblwiO1xyXG5cclxuJG9wYWNpdHk6IDAuNTQ7XHJcblxyXG5AbWl4aW4gbG9nb0JvcmRlcigkY29sb3IpIHtcclxuXHRib3JkZXI6IDRweCBzb2xpZCAkY29sb3I7XHJcbn1cclxuXHJcbjpob3N0IHtcclxuXHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG5cdGRpdi5tLWxvZ28tbGluZS1pY29uc3tcclxuXHRcdHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHRcdGRpc3BsYXk6IGZsZXg7XHJcblxyXG5cdFx0c3ZnIHtcclxuXHRcdFx0YS5yZWxhaXN7XHJcblx0XHRcdFx0Ym9yZGVyOiBub25lIWltcG9ydGFudDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0Ji5jaGVja2JveHtcclxuXHRcdFx0XHRAaW5jbHVkZSBsb2dvQm9yZGVyKHRyYW5zcGFyZW50KTtcclxuXHRcdFx0XHR0cmFuc2l0aW9uOiBhbGwgMjUwbXM7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGJveC1zaXppbmc6IGJvcmRlci1ib3g7XHJcblx0XHRcdGZvbnQtZmFtaWx5OiBBcmlhbCwgc2VyaWY7XHJcblx0XHRcdGZvbnQtd2VpZ2h0OmJvbGQ7XHJcblx0XHRcdHVzZXItc2VsZWN0OiBub25lO1xyXG5cclxuXHRcdFx0Ji5yZWxhaXMge1xyXG5cdFx0XHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdFx0XHR0b3A6IDRweDtcclxuXHRcdFx0XHRsZWZ0OiAwO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHQmLnNxdWFyZXtcclxuXHRcdFx0XHRib3JkZXItcmFkaXVzOiB1bnNldDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcbn1cclxuXHJcblxyXG5AbWl4aW4gb3ZlcnJpZGUtY29sb3IoJHRoZW1lKSB7XHJcblx0JC10aGVtZTogbWFwLmdldCgkdGhlbWUsIHRoZW1lKTtcclxuXHQkY29sb3ItY29uZmlnOiBtYXQuZ2V0LWNvbG9yLWNvbmZpZygkdGhlbWUpO1xyXG5cdCRwcmltYXJ5LXBhbGV0dGU6IG1hcC5nZXQoJGNvbG9yLWNvbmZpZywgXCJwcmltYXJ5XCIpO1xyXG5cdCRhY2NlbnQtcGFsZXR0ZTogbWFwLmdldCgkY29sb3ItY29uZmlnLCBcImFjY2VudFwiKTtcclxuXHJcblx0Lmhhcy1kaXN0dXJiYW5jZSB7XHJcblxyXG5cdFx0ZGl2Lm0tbG9nby1saW5lLWljb25zOjphZnRlciB7XHJcblx0XHRcdGNvbnRlbnQ6IFwiXCI7XHJcblx0XHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uL2Fzc2V0cy9pY29ucy9pY19wZXJ0dXJiX1wiICsgJC10aGVtZSArIFwiLnN2Z1wiKTtcclxuXHRcdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0XHRyaWdodDogLTRweDtcclxuXHRcdFx0dG9wOiAtMnB4O1xyXG5cdFx0XHR3aWR0aDogMTZweDtcclxuXHRcdFx0aGVpZ2h0OiAxNnB4O1xyXG5cdFx0XHRiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XHJcblx0XHRcdHotaW5kZXg6IDE7XHJcblx0XHR9XHJcblxyXG5cdFx0Ji5kaXN0dXJiYW5jZS00IHtcclxuXHJcblx0XHRcdGRpdi5tLWxvZ28tbGluZS1pY29ucyB7XHJcblx0XHRcdFx0b3BhY2l0eTogJG9wYWNpdHk7XHJcblxyXG5cdFx0XHRcdCY6OmFmdGVyIHtcclxuXHRcdFx0XHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uL2Fzc2V0cy9pY29ucy9pY19ob3JzX3NlcnZpY2VfXCIgKyAkLXRoZW1lICsgXCIuc3ZnXCIpO1xyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0Lm0tbG9nby1saW5lLWNoZWNrYm94e1xyXG5cclxuXHRcdGRpdjo6YWZ0ZXIsIC5yZWxhaXN7XHJcblx0XHRcdG9wYWNpdHk6ICRvcGFjaXR5O1xyXG5cdFx0fVxyXG5cclxuXHRcdCYubWF0LW1kYy1jaGVja2JveC1jaGVja2Vke1xyXG5cclxuXHRcdFx0LmNpcmNsZXtcclxuXHRcdFx0XHRib3JkZXItcmFkaXVzOiA1MCU7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdC5yb3VuZGVke1xyXG5cdFx0XHRcdGJvcmRlci1yYWRpdXM6IDhweDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0LnNxdWFyZXtcclxuXHRcdFx0XHRib3JkZXItcmFkaXVzOiB1bnNldDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0ZGl2OjphZnRlcntcclxuXHRcdFx0XHRvcGFjaXR5OiAxO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRzdmd7XHJcblx0XHRcdFx0dHJhbnNpdGlvbjogYWxsIDIwMG1zIGxpbmVhcjtcclxuXHJcblx0XHRcdFx0QGluY2x1ZGUgbG9nb0JvcmRlcihtYXAuZ2V0KCRwcmltYXJ5LXBhbGV0dGUsIGRlZmF1bHQpICFpbXBvcnRhbnQpO1xyXG5cclxuXHRcdFx0XHQmLnJlbGFpc3tcclxuXHRcdFx0XHRcdGJvcmRlcjogbm9uZSFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHQmLmNpcmNsZSwgJi5yb3VuZGVkLCAmLnNxdWFyZSwgJi5yZWxhaXN7XHJcblx0XHRcdFx0XHRvcGFjaXR5OiAxIWltcG9ydGFudDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdC5tLWNoZWNrbGluZXMtZmF2b3JpcyBsYWJlbDo6YWZ0ZXJ7XHJcblx0XHRcdFx0b3BhY2l0eTogMSFpbXBvcnRhbnQ7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHJcblx0XHQubWRjLWNoZWNrYm94e1xyXG5cdFx0XHRkaXNwbGF5OiBub25lO1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9 */"],
    changeDetection: 0
  });
}

/***/ })

}]);
//# sourceMappingURL=default-projects_m-ui_src_lib_m-logo-lines_m-logo-lines_component_ts.js.map