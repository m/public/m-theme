"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_guides_m-theme_module_ts"],{

/***/ 93204:
/*!*************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-theme/module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);



class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule.forChild([{
      path: '',
      redirectTo: 'theming',
      pathMatch: 'full'
    }, {
      path: '',
      title: 'M-theme',
      children: [{
        path: 'theming',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-kind-icon_mjs-node_modules_ng--95dcfb"), __webpack_require__.e("_ng-doc_m-docs_guides_m-theme_theming_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/m-theme/theming/module */ 64665)).then(m => m.DynamicModule)
      }]
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](DynamicModule, {
    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule]
  });
})();

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_guides_m-theme_module_ts.js.map