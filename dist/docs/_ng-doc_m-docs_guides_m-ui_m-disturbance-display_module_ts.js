"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_guides_m-ui_m-disturbance-display_module_ts"],{

/***/ 96228:
/*!******************************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-ui/m-disturbance-display/component-assets.ts ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   "demoAssets": () => (/* binding */ demoAssets)
/* harmony export */ });
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_ui_m_disturbance_display_assets_MDisturbanceDisplayDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-ui/m-disturbance-display/assets/MDisturbanceDisplayDemo/TypeScript/Asset0.html */ 3977);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_ui_m_disturbance_display_assets_MDisturbanceDisplayDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-ui/m-disturbance-display/assets/MDisturbanceDisplayDemo/HTML/Asset1.html */ 40724);


const demoAssets = {
  MDisturbanceDisplayDemo: [{
    title: 'TypeScript',
    codeType: 'TypeScript',
    code: _raw_loader_ng_doc_m_docs_guides_m_ui_m_disturbance_display_assets_MDisturbanceDisplayDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__["default"]
  }, {
    title: 'HTML',
    codeType: 'HTML',
    code: _raw_loader_ng_doc_m_docs_guides_m_ui_m_disturbance_display_assets_MDisturbanceDisplayDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__["default"]
  }]
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (demoAssets);

/***/ }),

/***/ 91564:
/*!********************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-ui/m-disturbance-display/module.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_ui_m_disturbance_display_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-ui/m-disturbance-display/index.html */ 6886);
/* harmony import */ var _playgrounds__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./playgrounds */ 57593);
/* harmony import */ var src_app_docs_m_ui_m_disturbance_display_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/docs/m-ui/m-disturbance-display/ng-doc.dependencies */ 80675);
/* harmony import */ var _ng_doc_m_docs_guides_m_ui_m_disturbance_display_component_assets__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! .ng-doc/m-docs/guides/m-ui/m-disturbance-display/component-assets */ 96228);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _src_app_docs_m_ui_m_disturbance_display_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../src/app/docs/m-ui/m-disturbance-display/ng-doc.module */ 92982);




// noinspection ES6UnusedImports


// noinspection ES6UnusedImports





class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = '';
    this.pageType = 'guide';
    this.pageContent = _raw_loader_ng_doc_m_docs_guides_m_ui_m_disturbance_display_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
    this.dependencies = src_app_docs_m_ui_m_disturbance_display_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"];
    this.demoAssets = _ng_doc_m_docs_guides_m_ui_m_disturbance_display_component_assets__WEBPACK_IMPORTED_MODULE_3__["default"];
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, src_app_docs_m_ui_m_disturbance_display_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"].module, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'M-disturbance-display'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, _src_app_docs_m_ui_m_disturbance_display_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__.MDisturbanceDisplayPageModule, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
})();

/***/ }),

/***/ 57593:
/*!*************************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-ui/m-disturbance-display/playgrounds.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PlaygroundsModule": () => (/* binding */ PlaygroundsModule)
/* harmony export */ });
/* harmony import */ var src_app_docs_m_ui_m_disturbance_display_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/docs/m-ui/m-disturbance-display/ng-doc.dependencies */ 80675);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _src_app_docs_m_ui_m_disturbance_display_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../src/app/docs/m-ui/m-disturbance-display/ng-doc.module */ 92982);
// noinspection ES6UnusedImports




class PlaygroundsModule {
  static #_ = this.ɵfac = function PlaygroundsModule_Factory(t) {
    return new (t || PlaygroundsModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: PlaygroundsModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, src_app_docs_m_ui_m_disturbance_display_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__["default"].module]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](PlaygroundsModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, _src_app_docs_m_ui_m_disturbance_display_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__.MDisturbanceDisplayPageModule]
  });
})();

/***/ }),

/***/ 82404:
/*!****************************************************************************************!*\
  !*** ./projects/m-ui/src/lib/m-disturbance-display/m-disturbance-display.component.ts ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MDisturbanceDisplay": () => (/* binding */ MDisturbanceDisplay)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/button */ 84522);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _m_icons_m_icons_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../m-icons/m-icons.component */ 73029);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);







function MDisturbanceDisplay_ng_container_0_p_1_ng_container_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](0);
  }
}
const _c0 = function (a0) {
  return {
    $implicit: a0,
    isLink: false
  };
};
function MDisturbanceDisplay_ng_container_0_p_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "p", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, MDisturbanceDisplay_ng_container_0_p_1_ng_container_1_Template, 1, 0, "ng-container", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", ctx_r5.getDynamicClasses(ctx_r5.disturbance));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngTemplateOutlet", _r1)("ngTemplateOutletContext", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](3, _c0, ctx_r5.disturbance));
  }
}
function MDisturbanceDisplay_ng_container_0_ng_template_2_ng_container_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](0);
  }
}
const _c1 = function (a0) {
  return {
    $implicit: a0,
    isLink: true
  };
};
function MDisturbanceDisplay_ng_container_0_ng_template_2_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, MDisturbanceDisplay_ng_container_0_ng_template_2_ng_container_1_Template, 1, 0, "ng-container", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    const _r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", ctx_r7.link)("ngClass", ctx_r7.getDynamicClasses(ctx_r7.disturbance));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngTemplateOutlet", _r1)("ngTemplateOutletContext", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](4, _c1, ctx_r7.disturbance));
  }
}
function MDisturbanceDisplay_ng_container_0_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, MDisturbanceDisplay_ng_container_0_p_1_Template, 2, 5, "p", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, MDisturbanceDisplay_ng_container_0_ng_template_2_Template, 2, 6, "ng-template", null, 4, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](3);
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !ctx_r0.disturbance.hasDisturbance && ctx_r0.disturbanceCollection.length === 0)("ngIfElse", _r6);
  }
}
function MDisturbanceDisplay_ng_template_1_div_1_ng_container_5_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](0);
  }
}
const _c2 = function (a0) {
  return [a0];
};
const _c3 = function (a0) {
  return {
    $implicit: a0
  };
};
function MDisturbanceDisplay_ng_template_1_div_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "m-icons", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span", 13)(3, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Service l\u00E9g\u00E8rement perturb\u00E9");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, MDisturbanceDisplay_ng_template_1_div_1_ng_container_5_Template, 1, 0, "ng-container", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
  }
  if (rf & 2) {
    const disturbance_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    const ctx_r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](3, _c2, disturbance_r10.calculatedClasses));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngTemplateOutlet", _r3)("ngTemplateOutletContext", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](5, _c3, ctx_r12.disturbanceCollection.length));
  }
}
function MDisturbanceDisplay_ng_template_1_div_2_ng_container_5_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](0);
  }
}
function MDisturbanceDisplay_ng_template_1_div_2_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "m-icons", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span", 13)(3, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Service tr\u00E8s perturb\u00E9");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, MDisturbanceDisplay_ng_template_1_div_2_ng_container_5_Template, 1, 0, "ng-container", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
  }
  if (rf & 2) {
    const disturbance_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    const ctx_r13 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](3, _c2, disturbance_r10.calculatedClasses));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngTemplateOutlet", _r3)("ngTemplateOutletContext", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](5, _c3, ctx_r13.disturbanceCollection.length));
  }
}
function MDisturbanceDisplay_ng_template_1_div_3_ng_container_5_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](0);
  }
}
function MDisturbanceDisplay_ng_template_1_div_3_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "m-icons", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span", 13)(3, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Hors service");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, MDisturbanceDisplay_ng_template_1_div_3_ng_container_5_Template, 1, 0, "ng-container", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
  }
  if (rf & 2) {
    const disturbance_r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](3, _c2, disturbance_r10.calculatedClasses));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngTemplateOutlet", _r3)("ngTemplateOutletContext", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](5, _c3, ctx_r14.disturbanceCollection.length));
  }
}
function MDisturbanceDisplay_ng_template_1_div_4_ng_container_5_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](0);
  }
}
function MDisturbanceDisplay_ng_template_1_div_4_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "m-icons", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "span", 13)(3, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4, "Service normal");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, MDisturbanceDisplay_ng_template_1_div_4_ng_container_5_Template, 1, 0, "ng-container", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
  }
  if (rf & 2) {
    const ctx_r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngTemplateOutlet", _r3)("ngTemplateOutletContext", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](2, _c3, ctx_r15.disturbanceCollection.length));
  }
}
function MDisturbanceDisplay_ng_template_1_m_icons_5_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "m-icons", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "chevron_right");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
}
function MDisturbanceDisplay_ng_template_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0, 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, MDisturbanceDisplay_ng_template_1_div_1_Template, 6, 7, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, MDisturbanceDisplay_ng_template_1_div_2_Template, 6, 7, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, MDisturbanceDisplay_ng_template_1_div_3_Template, 6, 7, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, MDisturbanceDisplay_ng_template_1_div_4_Template, 6, 4, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, MDisturbanceDisplay_ng_template_1_m_icons_5_Template, 2, 0, "m-icons", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    const disturbance_r10 = ctx.$implicit;
    const isLink_r11 = ctx.isLink;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngSwitch", disturbance_r10.nsv);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngSwitchCase", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngSwitchCase", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngSwitchCase", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", isLink_r11);
  }
}
function MDisturbanceDisplay_ng_template_3_ng_template_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](0, "Aucun \u00E9v\u00E9nement");
  }
}
function MDisturbanceDisplay_ng_template_3_ng_template_2_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](0, "1 \u00E9v\u00E9nement");
  }
}
function MDisturbanceDisplay_ng_template_3_ng_template_3_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](0);
  }
  if (rf & 2) {
    const ctx_r26 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"]("", ctx_r26.disturbanceCollection.length, " \u00E9v\u00E9nements");
  }
}
function MDisturbanceDisplay_ng_template_3_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, MDisturbanceDisplay_ng_template_3_ng_template_1_Template, 1, 0, "ng-template", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, MDisturbanceDisplay_ng_template_3_ng_template_2_Template, 1, 0, "ng-template", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, MDisturbanceDisplay_ng_template_3_ng_template_3_Template, 1, 1, "ng-template", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngPlural", ctx_r4.disturbanceCollection.length);
  }
}
class MDisturbanceDisplay {
  /**
   * Get the dynamic classes for the disturbance
   * @param disturbance any
   * @return string[]
   */
  getDynamicClasses(disturbance) {
    let classes = ['line-nsv', 'm-disturbance-display'];
    if (disturbance && disturbance.nsv && disturbance.nsv != 0 && disturbance.nsv != 1 && disturbance.nsv != 5) {
      classes.push(`has-disturbance disturbance-${disturbance.nsv}`);
    }
    return classes;
  }
  static #_ = this.ɵfac = function MDisturbanceDisplay_Factory(t) {
    return new (t || MDisturbanceDisplay)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
    type: MDisturbanceDisplay,
    selectors: [["m-disturbance-display"]],
    inputs: {
      disturbance: "disturbance",
      disturbanceCollection: "disturbanceCollection",
      link: "link"
    },
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵStandaloneFeature"]],
    decls: 5,
    vars: 1,
    consts: [[4, "ngIf"], ["disturbanceContentTpl", ""], ["disturbancesCountTpl", ""], [3, "ngClass", 4, "ngIf", "ngIfElse"], ["hasDisturbances", ""], [3, "ngClass"], [4, "ngTemplateOutlet", "ngTemplateOutletContext"], ["mat-button", "", 3, "routerLink", "ngClass"], [3, "ngSwitch"], [4, "ngSwitchCase"], ["class", "default-display", 4, "ngSwitchDefault"], ["class", "icon-right", 4, "ngIf"], ["type", "perturbation_low", 1, "icon-left-large", 3, "ngClass"], [1, "layout", "column"], ["type", "perturbation_hight", 1, "icon-left-large", 3, "ngClass"], ["type", "perturbation_over", 1, "icon-left-large", 3, "ngClass"], [1, "default-display"], ["type", "perturbation_low", 1, "icon-left-large"], [1, "icon-right"], [1, "m-disturbance-display-text-secondary", 3, "ngPlural"], ["ngPluralCase", "=0"], ["ngPluralCase", "=1"], ["ngPluralCase", "other"]],
    template: function MDisturbanceDisplay_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, MDisturbanceDisplay_ng_container_0_Template, 4, 2, "ng-container", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, MDisturbanceDisplay_ng_template_1_Template, 6, 5, "ng-template", null, 1, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, MDisturbanceDisplay_ng_template_3_Template, 4, 1, "ng-template", null, 2, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.disturbance);
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule, _angular_common__WEBPACK_IMPORTED_MODULE_1__.NgClass, _angular_common__WEBPACK_IMPORTED_MODULE_1__.NgIf, _angular_common__WEBPACK_IMPORTED_MODULE_1__.NgTemplateOutlet, _angular_common__WEBPACK_IMPORTED_MODULE_1__.NgSwitch, _angular_common__WEBPACK_IMPORTED_MODULE_1__.NgSwitchCase, _angular_common__WEBPACK_IMPORTED_MODULE_1__.NgSwitchDefault, _angular_common__WEBPACK_IMPORTED_MODULE_1__.NgPlural, _angular_common__WEBPACK_IMPORTED_MODULE_1__.NgPluralCase, _m_icons_m_icons_component__WEBPACK_IMPORTED_MODULE_2__.MIcons, _angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterLink, _angular_material_button__WEBPACK_IMPORTED_MODULE_4__.MatButtonModule, _angular_material_button__WEBPACK_IMPORTED_MODULE_4__.MatAnchor],
    styles: [".has-disturbance[_ngcontent-%COMP%]::after {\n  top: -4px;\n  right: -4px;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3Byb2plY3RzL20tdWkvc3JjL3RoZW1lL2dsb2JhbC9fdmFyaWFibGVzLnNjc3MiLCJ3ZWJwYWNrOi8vLi9wcm9qZWN0cy9tLXVpL3NyYy90aGVtZS9nbG9iYWwvX2hlbHBlcnMuc2NzcyIsIndlYnBhY2s6Ly8uL3Byb2plY3RzL20tdWkvc3JjL2xpYi9tLWRpc3R1cmJhbmNlLWRpc3BsYXkvbS1kaXN0dXJiYW5jZS1kaXNwbGF5LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQWdCQSxrQkFBQTtBQUlBLGVBQUE7QUFHQSxvQkFBQTtBQU9BLGdCQUFBO0FBR0Esb0JBQUE7QUFHQSxpQkFBQTtBQUlBLGdCQUFBO0FBR0Esb0JBQUE7QUNwQ0E7Ozs7OztHQUFBO0FBV0E7Ozs7OztHQUFBO0FBNkJBOztFQUFBO0FBWUE7O0VBQUE7QUFzQ0E7O0dBQUE7QUM1RkE7RUFDQyxTQUFBO0VBQ0EsV0FBQTtBQTJCRCIsInNvdXJjZXNDb250ZW50IjpbIiRhc3NldHMtcGF0aDogXCJeLi9hc3NldHNcIiAhZGVmYXVsdDtcclxuJHNwYWNpbmc6IDhweCAhZGVmYXVsdDtcclxuXHJcbiRlbGV2YXRpb24tY29sb3JzOiAoXHJcbiAgMDogMCUsXHJcbiAgMTogNSUsXHJcbiAgMjogNyUsXHJcbiAgMzogOCUsXHJcbiAgNDogOSUsXHJcbiAgNjogMTElLFxyXG4gIDg6IDEyJSxcclxuICAxMjogMTQlLFxyXG4gIDE2OiAxNSUsXHJcbiAgMjQ6IDE2JSxcclxuKSAhZGVmYXVsdDtcclxuXHJcbi8qKioqIHRvb2xiYXIgKioqKi9cclxuJHRvb2xiYXItaGVpZ2h0OiA1NnB4ICFkZWZhdWx0O1xyXG4kdG9vbGJhci1tYWluLW5hdi1oZWlnaHQ6ICR0b29sYmFyLWhlaWdodCArICRzcGFjaW5nICFkZWZhdWx0O1xyXG5cclxuLyoqKiogZm9udCAqKioqL1xyXG4kZm9udC1mYW1pbHk6IFJvYm90bywgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBzYW5zLXNlcmlmO1xyXG5cclxuLyoqKiogZm9udCBzaXplICoqKiovXHJcbiRkZWZhdWx0LWZ6OiAxLjRyZW0gIWRlZmF1bHQ7XHJcbiRkZWZhdWx0LWZ6LXNtYWxsOiAxLjJyZW0gIWRlZmF1bHQ7XHJcbiRkZWZhdWx0LWZ6LWJpZzogMS44cmVtICFkZWZhdWx0O1xyXG4kaGVhZGVyLWZ6OiAkZGVmYXVsdC1meiArIDAuMnJlbSAhZGVmYXVsdDtcclxuJGhlYWRlci1mei1iaWc6ICRoZWFkZXItZnogKyAwLjRyZW0gIWRlZmF1bHQ7XHJcblxyXG4vKioqKiBJY29ucyAqKioqL1xyXG4kaWNvbi1sYXJnZTogNDBweCAhZGVmYXVsdDtcclxuXHJcbi8qKioqIENvbnRhaW5lciAqKioqL1xyXG4kZW5kLWJnLXNpemU6IDE0M3B4ICsgJHNwYWNpbmcgKiAyICFkZWZhdWx0O1xyXG5cclxuLyoqKiogc2hhZG93ICoqKiovXHJcbiRsaWdodC1ib3gtc2hhZG93OiAwIDRweCA4cHggcmdiYShibGFjaywgMC41KSAhZGVmYXVsdDtcclxuJGRhcmstYm94LXNoYWRvdzogMCA0cHggOHB4IHJnYmEoYmxhY2ssIDAuNykgIWRlZmF1bHQ7XHJcblxyXG4vKioqKiBTaGFwZSAqKioqL1xyXG4kc2hhcGUtcmFkaXVzOiA4cHggIWRlZmF1bHQ7XHJcblxyXG4vKioqIEJyZWFrcG9pbnQgKioqKi9cclxuJGJyZWFrcG9pbnQ6IDEwNzlweDtcclxuIiwiQHVzZSBcInNhc3M6bWFwXCI7XHJcbkB1c2UgXCJzYXNzOm1ldGFcIjtcclxuQHVzZSBcInNhc3M6bGlzdFwiO1xyXG5AdXNlIFwic2FzczptYXRoXCI7XHJcbkB1c2UgXCJzYXNzOnNlbGVjdG9yXCI7XHJcbkB1c2UgXCJ2YXJpYWJsZXNcIiBhcyB2YXI7XHJcblxyXG4vKipcclxuICogU2xpZ2h0bHkgbGlnaHRlbiBhIGNvbG9yXHJcbiAqIEBhY2Nlc3MgcHVibGljXHJcbiAqIEBwYXJhbSB7Q29sb3J9ICRjb2xvciAtIGNvbG9yIHRvIHRpbnRcclxuICogQHBhcmFtIHtOdW1iZXJ9ICRwZXJjZW50YWdlIC0gcGVyY2VudGFnZSBvZiBgJGNvbG9yYCBpbiByZXR1cm5lZCBjb2xvclxyXG4gKiBAcmV0dXJuIHtDb2xvcn1cclxuICoqL1xyXG5AZnVuY3Rpb24gdGludCgkY29sb3IsICRwZXJjZW50YWdlKSB7XHJcblx0QHJldHVybiBtaXgod2hpdGUsICRjb2xvciwgJHBlcmNlbnRhZ2UpO1xyXG59XHJcblxyXG4vKipcclxuICogU2xpZ2h0bHkgZGFya2VuIGEgY29sb3JcclxuICogQGFjY2VzcyBwdWJsaWNcclxuICogQHBhcmFtIHtDb2xvcn0gJGNvbG9yIC0gY29sb3IgdG8gc2hhZGVcclxuICogQHBhcmFtIHtOdW1iZXJ9ICRwZXJjZW50YWdlIC0gcGVyY2VudGFnZSBvZiBgJGNvbG9yYCBpbiByZXR1cm5lZCBjb2xvclxyXG4gKiBAcmV0dXJuIHtDb2xvcn1cclxuICoqL1xyXG5AZnVuY3Rpb24gc2hhZGUoJGNvbG9yLCAkcGVyY2VudGFnZSkge1xyXG5cdEByZXR1cm4gbWl4KGJsYWNrLCAkY29sb3IsICRwZXJjZW50YWdlKTtcclxufVxyXG5cclxuQG1peGluIGZ1bGwtaGVpZ2h0KCkge1xyXG5cdGhlaWdodDogY2FsYygxMDB2aCAtICN7dmFyLiR0b29sYmFyLW1haW4tbmF2LWhlaWdodH0pO1xyXG59XHJcblxyXG5AbWl4aW4gaWNvbi1tYXJnaW4oJGRpcmVjdGlvbikge1xyXG5cdG1hcmdpbi0jeyRkaXJlY3Rpb259OiB2YXIuJHNwYWNpbmc7XHJcblxyXG5cdCYtbGFyZ2Uge1xyXG5cdFx0bWFyZ2luLSN7JGRpcmVjdGlvbn06IHZhci4kc3BhY2luZyAqIDI7XHJcblx0fVxyXG5cdCYtdmVyeS1sYXJnZSB7XHJcblx0XHRtYXJnaW4tI3skZGlyZWN0aW9ufTogdmFyLiRzcGFjaW5nICogNDtcclxuXHR9XHJcblx0Ji1zbWFsbCB7XHJcblx0XHRtYXJnaW4tI3skZGlyZWN0aW9ufTogbWF0aC5kaXYodmFyLiRzcGFjaW5nLCAyKTtcclxuXHR9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBNdXN0IGJlIHVzZWQgb25seSBpbnNpZGUgYSBvdmVycmlkZS1YIG1peGluLlxyXG4gKi9cclxuQG1peGluIGRyb3Atc2hhZG93KCkge1xyXG5cdEBhdC1yb290ICN7c2VsZWN0b3IucmVwbGFjZSgmLCBcImJvZHlcIiwgXCJib2R5LmRhcmstdGhlbWVcIil9IHtcclxuXHRcdGJveC1zaGFkb3c6IHZhci4kZGFyay1ib3gtc2hhZG93O1xyXG5cdH1cclxuXHRAYXQtcm9vdCAje3NlbGVjdG9yLnJlcGxhY2UoJiwgXCJib2R5XCIsIFwiYm9keS5saWdodC10aGVtZVwiKX0ge1xyXG5cdFx0Ym94LXNoYWRvdzogdmFyLiRsaWdodC1ib3gtc2hhZG93O1xyXG5cdH1cclxufVxyXG5cclxuLyoqXHJcbiAqIE11c3QgYmUgdXNlZCBvbmx5IGluc2lkZSBhIG92ZXJyaWRlLVggbWl4aW4uXHJcbiAqL1xyXG5AbWl4aW4gYm90dG9tLXBpY3R1cmUoKSB7XHJcblx0Jjo6YWZ0ZXIge1xyXG5cdFx0Y29udGVudDogXCJcIjtcclxuXHRcdGRpc3BsYXk6IGJsb2NrO1xyXG5cdFx0aGVpZ2h0OiB2YXIuJGVuZC1iZy1zaXplO1xyXG5cdFx0YmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuXHRcdGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcblx0XHRAYXQtcm9vdCAje3NlbGVjdG9yLnJlcGxhY2UoJiwgXCJib2R5XCIsIFwiYm9keS5kYXJrLXRoZW1lXCIpfSB7XHJcblx0XHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybCh2YXIuJGFzc2V0cy1wYXRoICsgXCIvaW1hZ2VzL2JvdHRvbV9waWN0dXJlX21ldHJvbW9iX2RhcmsvYm90dG9tX3BpY3R1cmVfbWV0cm9tb2JfZGFyay5wbmdcIik7XHJcblxyXG5cdFx0XHRAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tcmVzb2x1dGlvbjogMjAwZHBpKSwgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDEwMjRweCkge1xyXG5cdFx0XHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybCh2YXIuJGFzc2V0cy1wYXRoICsgXCIvaW1hZ2VzL2JvdHRvbV9waWN0dXJlX21ldHJvbW9iX2RhcmsvYm90dG9tX3BpY3R1cmVfbWV0cm9tb2JfZGFya0AyeC5wbmdcIik7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdEBhdC1yb290ICN7c2VsZWN0b3IucmVwbGFjZSgmLCBcImJvZHlcIiwgXCJib2R5LmxpZ2h0LXRoZW1lXCIpfSB7XHJcblx0XHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybCh2YXIuJGFzc2V0cy1wYXRoICsgXCIvaW1hZ2VzL2JvdHRvbV9waWN0dXJlX21ldHJvbW9iX2xpZ2h0L2JvdHRvbV9waWN0dXJlX21ldHJvbW9iX2xpZ2h0LnBuZ1wiKTtcclxuXHJcblx0XHRcdEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1yZXNvbHV0aW9uOiAyMDBkcGkpLCBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMTAyNHB4KSB7XHJcblx0XHRcdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKHZhci4kYXNzZXRzLXBhdGggKyBcIi9pbWFnZXMvYm90dG9tX3BpY3R1cmVfbWV0cm9tb2JfbGlnaHQvYm90dG9tX3BpY3R1cmVfbWV0cm9tb2JfbGlnaHRAMngucG5nXCIpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG59XHJcblxyXG5AZnVuY3Rpb24gc3dpdGNoKCR0ZXN0LCAkYXJncy4uLikge1xyXG5cdCRjYXNlczogbWV0YS5rZXl3b3JkcygkYXJncyk7XHJcblx0QGlmIChtYXAuaGFzLWtleSgkY2FzZXMsICR0ZXN0KSkge1xyXG5cdFx0QHJldHVybiBtYXAuZ2V0KCRjYXNlcywgJHRlc3QpO1xyXG5cdH0gQGVsc2UgaWYgbWFwLmhhcy1rZXkoJGNhc2VzLCBcImRlZmF1bHRcIikge1xyXG5cdFx0QHJldHVybiBtYXAuZ2V0KCRjYXNlcywgXCJkZWZhdWx0XCIpO1xyXG5cdH0gQGVsc2Uge1xyXG5cdFx0QHJldHVybiBudWxsO1xyXG5cdH1cclxufVxyXG5cclxuLyoqXHJcbiAqIFNhc3MgZGVidWcgZnVuY3Rpb24gZnJvbSBodHRwczovL2NvZGVwZW4uaW8vS2l0dHlHaXJhdWRlbC9wZW4vdW55QkhcclxuICoqL1xyXG5AZnVuY3Rpb24gZGVidWctbWFwKCRsaXN0LCAkcHJlOiB0cnVlLCAkbGV2ZWw6IDEpIHtcclxuXHQkdGFiOiBcIiAgICBcIjtcclxuXHQkaW5kZW50OiBcIlwiO1xyXG5cdCRicmVhazogaWYoJHByZSwgXCJcXEEgXCIsIFwiXCIpO1xyXG5cclxuXHRAaWYgbGVuZ3RoKCRsaXN0KSA9PSAwIHtcclxuXHRcdEByZXR1cm4gXCIoIClcIjtcclxuXHR9XHJcblxyXG5cdEBpZiBsZW5ndGgoJGxpc3QpID09IDEge1xyXG5cdFx0QHJldHVybiBpZigkcHJlLCBcIihcIiArIHR5cGUtb2YoJGxpc3QpICsgXCIpIFwiLCBcIlwiKSArICRsaXN0O1xyXG5cdH1cclxuXHJcblx0QGZvciAkaSBmcm9tIDEgdG8gJGxldmVsIHtcclxuXHRcdCRpbmRlbnQ6ICRpbmRlbnQgKyAkdGFiO1xyXG5cdH1cclxuXHJcblx0JHJlc3VsdDogXCJbXCIgKyAkYnJlYWs7XHJcblxyXG5cdEBmb3IgJGkgZnJvbSAxIHRocm91Z2ggbGVuZ3RoKCRsaXN0KSB7XHJcblx0XHQkaXRlbTogbnRoKCRsaXN0LCAkaSk7XHJcblx0XHQkcmVzdWx0OiAkcmVzdWx0ICsgaWYoJHByZSwgJGluZGVudCArICR0YWIsIFwiIFwiKTtcclxuXHJcblx0XHRAaWYgbGVuZ3RoKCRpdGVtKSA+IDEge1xyXG5cdFx0XHQkcmVzdWx0OiAkcmVzdWx0ICsgaWYoJHByZSwgXCIobGlzdDogXCIgKyBsZW5ndGgoJGl0ZW0pICsgXCIpIFwiLCBcIlwiKSArIGRlYnVnLW1hcCgkaXRlbSwgJHByZSwgJGxldmVsICsgMSk7XHJcblx0XHR9IEBlbHNlIHtcclxuXHRcdFx0QGlmICRwcmUge1xyXG5cdFx0XHRcdCRyZXN1bHQ6ICRyZXN1bHQgKyBcIihcIiArIHR5cGUtb2YoJGl0ZW0pICsgXCIpIFwiO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRAaWYgbGVuZ3RoKCRpdGVtKSA9PSAwIHtcclxuXHRcdFx0XHQkcmVzdWx0OiAkcmVzdWx0ICsgXCIoIClcIjtcclxuXHRcdFx0fSBAZWxzZSBpZiB0eXBlLW9mKCRpdGVtKSA9PSBzdHJpbmcge1xyXG5cdFx0XHRcdCRyZXN1bHQ6ICRyZXN1bHQgKyBxdW90ZSgkaXRlbSk7XHJcblx0XHRcdH0gQGVsc2UgaWYgJGl0ZW0gPT0gbnVsbCB7XHJcblx0XHRcdFx0JHJlc3VsdDogJHJlc3VsdCArIFwibnVsbFwiO1xyXG5cdFx0XHR9IEBlbHNlIHtcclxuXHRcdFx0XHQkcmVzdWx0OiAkcmVzdWx0ICsgJGl0ZW07XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHJcblx0XHRAaWYgJGkgIT0gbGVuZ3RoKCRsaXN0KSB7XHJcblx0XHRcdCRyZXN1bHQ6ICRyZXN1bHQgKyBcIixcIiArICRicmVhaztcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdCRyZXN1bHQ6ICRyZXN1bHQgKyAkYnJlYWsgKyBpZigkcHJlLCBpZigkbGV2ZWwgPiAxLCAkaW5kZW50LCBcIlwiKSwgXCIgXCIpICsgXCJdXCI7XHJcblxyXG5cdEByZXR1cm4gcXVvdGUoJHJlc3VsdCk7XHJcbn1cclxuIiwiQHVzZSBcInNhc3M6bWFwXCI7XHJcbkB1c2UgXCJzYXNzOm1hdGhcIjtcclxuQHVzZSBcIkBhbmd1bGFyL21hdGVyaWFsXCIgYXMgbWF0O1xyXG5AdXNlIFwiLi4vLi4vdGhlbWUvZ2xvYmFsXCIgYXMgZ2xvYmFsO1xyXG5cclxuLmhhcy1kaXN0dXJiYW5jZTo6YWZ0ZXIge1xyXG5cdHRvcDogZ2xvYmFsLiRzcGFjaW5nIC8gLTI7XHJcblx0cmlnaHQ6IGdsb2JhbC4kc3BhY2luZyAqIC0wLjU7XHJcbn1cclxuXHJcblxyXG5AbWl4aW4gb3ZlcnJpZGUtY29sb3IoJHRoZW1lKSB7XHJcblx0JC10aGVtZTogbWFwLmdldCgkdGhlbWUsIHRoZW1lKTtcclxuXHQkY29sb3ItY29uZmlnOiBtYXQuZ2V0LWNvbG9yLWNvbmZpZygkdGhlbWUpO1xyXG5cdCRwcmltYXJ5LXBhbGV0dGU6IG1hcC5nZXQoJGNvbG9yLWNvbmZpZywgXCJwcmltYXJ5XCIpO1xyXG5cdCRmb3JlZ3JvdW5kOiBtYXAuZ2V0KCR0aGVtZSwgZm9yZWdyb3VuZCk7XHJcblxyXG5cdC5tLWRpc3R1cmJhbmNlLWRpc3BsYXkge1xyXG5cdFx0Ji5oYXMtZGlzdHVyYmFuY2Uge1xyXG5cdFx0XHRwb3NpdGlvbjogcmVsYXRpdmU7XHJcblxyXG5cdFx0XHQmLmRpc3R1cmJhbmNlLXBvc2l0aW9uLW91dGVyOjphZnRlciB7XHJcblx0XHRcdFx0dG9wOiBtYXRoLmRpdihnbG9iYWwuJHNwYWNpbmcsIC0yKTtcclxuXHRcdFx0XHRyaWdodDogbWF0aC5kaXYoZ2xvYmFsLiRzcGFjaW5nLCAtMik7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdCY6OmFmdGVyIHtcclxuXHRcdFx0XHRjb250ZW50OiBcIlwiO1xyXG5cdFx0XHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdFx0XHR3aWR0aDogZ2xvYmFsLiRzcGFjaW5nICogMjtcclxuXHRcdFx0XHRoZWlnaHQ6IGdsb2JhbC4kc3BhY2luZyAqIDI7XHJcblx0XHRcdFx0YmFja2dyb3VuZC1zaXplOiBjb250YWluO1xyXG5cdFx0XHRcdHotaW5kZXg6IDE7XHJcblx0XHRcdFx0dG9wOiBnbG9iYWwuJHNwYWNpbmcgKyAycHg7XHJcblx0XHRcdFx0bGVmdDogZ2xvYmFsLiRzcGFjaW5nICogNS43NTtcclxuXHRcdFx0XHRyaWdodDogdW5zZXQ7XHJcblx0XHRcdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vYXNzZXRzL2ljb25zL2ljX3BlcnR1cmJfXCIgKyAkLXRoZW1lICsgXCIuc3ZnXCIpO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHQmLmRpc3R1cmJhbmNlLTQge1xyXG5cdFx0XHRcdG0tbG9nby1saW5lcyB7XHJcblx0XHRcdFx0XHRvcGFjaXR5OiAwLjU0O1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0Jjo6YWZ0ZXIge1xyXG5cdFx0XHRcdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vYXNzZXRzL2ljb25zL2ljX2hvcnNfc2VydmljZV9cIiArICQtdGhlbWUgKyBcIi5zdmdcIik7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQubGluZS1uc3Yge1xyXG5cdFx0QGlmICQtdGhlbWUgPT0gXCJkYXJrXCIge1xyXG5cdFx0XHRiYWNrZ3JvdW5kOiAjMjIxZDI2O1xyXG5cclxuXHRcdFx0Ji5kaXN0dXJiYW5jZS0yIHtcclxuXHRcdFx0XHRiYWNrZ3JvdW5kOiAjNzE0YTRhO1xyXG5cdFx0XHR9XHJcblx0XHRcdCYuZGlzdHVyYmFuY2UtMyB7XHJcblx0XHRcdFx0YmFja2dyb3VuZDogIzk5MmUyZTtcclxuXHRcdFx0XHRjb2xvcjogI2ZmZiFpbXBvcnRhbnQ7XHJcblx0XHRcdH1cclxuXHRcdFx0Ji5kaXN0dXJiYW5jZS00IHtcclxuXHRcdFx0XHRiYWNrZ3JvdW5kOiAjNWM1OTVmO1xyXG5cdFx0XHR9XHJcblx0XHR9IEBlbHNlIHtcclxuXHRcdFx0YmFja2dyb3VuZDogI2VlZWVlZTtcclxuXHJcblx0XHRcdCYuZGlzdHVyYmFuY2UtMiB7XHJcblx0XHRcdFx0YmFja2dyb3VuZDogI2Y3ZTVlMDtcclxuXHRcdFx0fVxyXG5cdFx0XHQmLmRpc3R1cmJhbmNlLTMge1xyXG5cdFx0XHRcdGJhY2tncm91bmQ6ICNiOTJkMDA7XHJcblx0XHRcdFx0Y29sb3I6ICNmZmYhaW1wb3J0YW50O1xyXG5cdFx0XHR9XHJcblx0XHRcdCYuZGlzdHVyYmFuY2UtNCB7XHJcblx0XHRcdFx0YmFja2dyb3VuZDogIzIxMjEyMTtcclxuXHRcdFx0XHRjb2xvcjogI2ZmZiFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0LnRleHQtc2Vjb25kYXJ5IHtcclxuXHRcdFx0XHRcdGNvbG9yOiBpbmhlcml0IWltcG9ydGFudDtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHJcblx0XHRoZWlnaHQ6IGF1dG8haW1wb3J0YW50O1xyXG5cdFx0Ym9yZGVyLXJhZGl1czogZ2xvYmFsLiRzaGFwZS1yYWRpdXMhaW1wb3J0YW50O1xyXG5cclxuXHRcdC5tZGMtYnV0dG9uX19sYWJlbHtcclxuXHRcdFx0ZGlzcGxheTogZmxleDtcclxuXHRcdFx0ZmxleC1kaXJlY3Rpb246IHJvdztcclxuXHRcdFx0d2lkdGg6IDEwMCU7XHJcblxyXG5cdFx0XHQuaWNvbi1yaWdodHtcclxuXHRcdFx0XHRtYXJnaW4tbGVmdDogYXV0bztcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHRcdCZ7XHJcblx0XHRcdGJvcmRlci1yYWRpdXM6IGdsb2JhbC4kc2hhcGUtcmFkaXVzO1xyXG5cdFx0XHRwYWRkaW5nOiBnbG9iYWwuJHNwYWNpbmcgKiAyIGdsb2JhbC4kc3BhY2luZyAqIDM7XHJcblx0XHRcdG1hcmdpbjogZ2xvYmFsLiRzcGFjaW5nICogMjtcclxuXHRcdFx0ZGlzcGxheTogZmxleDtcclxuXHRcdFx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHRcdH1cclxuXHJcblx0XHQuZGVmYXVsdC1kaXNwbGF5e1xyXG5cdFx0XHRkaXNwbGF5OiBmbGV4O1xyXG5cdFx0XHRmb250LXNpemU6IDE0cHg7XHJcblx0XHR9XHJcblxyXG5cdFx0bS1pY29uc3tcclxuXHRcdFx0bWFyZ2luLXRvcDogYXV0bztcclxuXHRcdFx0bWFyZ2luLWJvdHRvbTogYXV0bztcclxuXHJcblx0XHRcdG1hdC1pY29ue1xyXG5cdFx0XHRcdHdpZHRoOiBnbG9iYWwuJHNwYWNpbmcqNCFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0aGVpZ2h0OiBnbG9iYWwuJHNwYWNpbmcqNCFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0dmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHRcdGRpdntcclxuXHRcdFx0ZGlzcGxheTogZmxleDtcclxuXHRcdFx0ZmxleC1kaXJlY3Rpb246IHJvdztcclxuXHJcblx0XHRcdC5sYXlvdXR7XHJcblx0XHRcdFx0bGluZS1oZWlnaHQ6IDEuM2VtO1xyXG5cdFx0XHRcdHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG5cdFx0XHRcdGZvbnQtc2l6ZTogMTVweDtcclxuXHRcdFx0XHRmb250LXdlaWdodDogbm9ybWFsO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdFx0QG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogMzIwcHgpIHtcclxuXHRcdFx0bWF0LWljb246Zmlyc3Qtb2YtdHlwZSB7XHJcblx0XHRcdFx0ZGlzcGxheTogbm9uZTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHRcdC5tLWRpc3R1cmJhbmNlLWRpc3BsYXktdGV4dC1zZWNvbmRhcnl7XHJcblx0XHRcdG9wYWNpdHk6IDAuNztcclxuXHRcdH1cclxuXHR9XHJcblxyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0= */"]
  });
}

/***/ }),

/***/ 73409:
/*!********************************************************************************************************************!*\
  !*** ./src/app/docs/m-ui/m-disturbance-display/m-disturbance-display-demo/m-disturbance-display-demo.component.ts ***!
  \********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MDisturbanceDisplayDemo": () => (/* binding */ MDisturbanceDisplayDemo)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _metromobilite_m_ui_lib_m_disturbance_display__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @metromobilite/m-ui/lib/m-disturbance-display */ 82404);
/* harmony import */ var src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/features/preview/preview.component */ 55355);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);




const _c0 = function () {
  return [];
};
class MDisturbanceDisplayDemo {
  constructor() {
    this.disturbance = {
      activateHostbinding: true,
      calculatedClasses: "",
      disturbanceOptions: {
        lineOrNsv: 1
      },
      hasDisturbance: false,
      hostClass: "",
      nsv: 1,
      outer: true
    };
    this.disturbanceCollection = {
      disturbances: [{
        'type': 'restriction_ltc',
        'code': 'SEM_1396148590774867315',
        'dateDebut': '08/01/2025 09:00',
        'dateFin': '08/01/2025 16:30',
        'heureDebut': '00:00:00',
        'heureFin': '00:00:00',
        'latitude': -1,
        'longitude': -1,
        'weekEnd': '2',
        'texte': 'A : travaux|jusqu\'au 08/01/2025 16:30|La ligne A ne circule pas entre les stations Polesud - Alpexpo et Malherbe en raison de travaux.\nDes bus relais effectuent la liaison entre ces stations. \n \nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.',
        'titre': 'travaux',
        'description': 'jusqu\'au 08/01/2025 16:30\r\nLa ligne A ne circule pas entre les stations Polesud - Alpexpo et Malherbe en raison de travaux.\nDes bus relais effectuent la liaison entre ces stations. \n \nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.',
        'plan': 'https://www.tag.fr/include/downloadInfoCompPerturbation.php?f=Info coupure A _Malherbe Pole Sud.pdf',
        'visibleTC': true,
        'visibleVoiture': false,
        'visibleVelo': false,
        'visibleVenteTitres': false,
        'visibleBandeauSite': false,
        'visibleBandeauAppli': false,
        'listeLigne': 'SEM_A',
        'listeLigneArret': 'SEM_A',
        'nsv_id': 2,
        'computed': {
          'title': 'travaux',
          'content': 'jusqu\'au 08/01/2025 16:30<br>La ligne A ne circule pas entre les stations Polesud - Alpexpo et Malherbe en raison de travaux.\nDes bus relais effectuent la liaison entre ces stations. \n \nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.',
          'line': {
            'id': 'SEM:A',
            'gtfsId': 'SEM:A',
            'shortName': 'A',
            'longName': 'FONTAINE La Poya / PONT DE CLAIX L\'Etoile',
            'color': '3376B8',
            'textColor': 'FFFFFF',
            'mode': 'TRAM',
            'type': 'TRAM',
            'pdf': 'https://data-pp.mobilites-m.fr/api/planligne/pdf?route=SEM:A'
          }
        }
      }, {
        'type': 'restriction_ltc',
        'code': 'SEM_5828443584937855085',
        'dateDebut': '09/01/2025 22:00',
        'dateFin': '10/01/2025 03:00',
        'heureDebut': '22:00',
        'heureFin': '02:00',
        'latitude': -1,
        'longitude': -1,
        'weekEnd': '0',
        'texte': 'A : travaux de nuit Grand\'Place|à partir du 09/01/2025 22:00|jusqu\'au 10/01/2025 03:00|La ligne A ne circule pas entre les stations L\'Etoile et Chavant en raison de travaux de nuit secteur Grand\'Place.\nDes bus relais effectuent la liaison entre ces stations.\n \nÀ noter que la station Echirolles Gare n\'est pas desservie par les bus relais.\n \nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.',
        'titre': 'travaux de nuit Grand\'Place',
        'description': 'à partir du 09/01/2025 22:00|jusqu\'au 10/01/2025 03:00\r\nLa ligne A ne circule pas entre les stations L\'Etoile et Chavant en raison de travaux de nuit secteur Grand\'Place.\nDes bus relais effectuent la liaison entre ces stations.\n \nÀ noter que la station Echirolles Gare n\'est pas desservie par les bus relais.\n \nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.',
        'plan': 'https://www.tag.fr/include/downloadInfoCompPerturbation.php?f=Info Coupure A LEtoile-Chavant.pdf',
        'visibleTC': true,
        'visibleVoiture': false,
        'visibleVelo': false,
        'visibleVenteTitres': false,
        'visibleBandeauSite': false,
        'visibleBandeauAppli': false,
        'listeLigne': 'SEM_A',
        'listeLigneArret': 'SEM_A',
        'listeArret': 'SEM:2207,SEM:2208',
        'nsv_id': 2,
        'computed': {
          'title': 'travaux de nuit Grand\'Place',
          'content': 'à partir du 09/01/2025 22:00<br>jusqu\'au 10/01/2025 03:00<br>La ligne A ne circule pas entre les stations L\'Etoile et Chavant en raison de travaux de nuit secteur Grand\'Place.\nDes bus relais effectuent la liaison entre ces stations.\n \nÀ noter que la station Echirolles Gare n\'est pas desservie par les bus relais.\n \nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.',
          'line': {
            'id': 'SEM:A',
            'gtfsId': 'SEM:A',
            'shortName': 'A',
            'longName': 'FONTAINE La Poya / PONT DE CLAIX L\'Etoile',
            'color': '3376B8',
            'textColor': 'FFFFFF',
            'mode': 'TRAM',
            'type': 'TRAM',
            'pdf': 'https://data-pp.mobilites-m.fr/api/planligne/pdf?route=SEM:A'
          }
        }
      }],
      dis: {
        hasDisturbance: true,
        nsv: 2,
        calculatedClasses: 'has-disturbance'
      }
    };
  }
  updateDisturbance(nsv) {
    const disturbance = JSON.parse(JSON.stringify(this.disturbance));
    disturbance.nsv = nsv;
    disturbance.disturbanceOptions.lineOrNsv = nsv;
    return disturbance;
  }
  static #_ = this.ɵfac = function MDisturbanceDisplayDemo_Factory(t) {
    return new (t || MDisturbanceDisplayDemo)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
    type: MDisturbanceDisplayDemo,
    selectors: [["app-disturbance-display-demo"]],
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵStandaloneFeature"]],
    decls: 10,
    vars: 16,
    consts: [["title", "Nsv 1", "fullWidth", "true", 3, "nextElement"], [3, "disturbance", "disturbanceCollection"], ["title", "Nsv 2", "fullWidth", "true", 3, "nextElement"], ["title", "Nsv 3", "fullWidth", "true", 3, "nextElement"], ["title", "Nsv 4", "fullWidth", "true", 3, "nextElement"], ["title", "Nsv 5", "fullWidth", "true", 3, "nextElement"]],
    template: function MDisturbanceDisplayDemo_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "app-preview", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "m-disturbance-display", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "app-preview", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](3, "m-disturbance-display", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "app-preview", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](5, "m-disturbance-display", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "app-preview", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](7, "m-disturbance-display", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "app-preview", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](9, "m-disturbance-display", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("nextElement", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disturbance", ctx.updateDisturbance(1))("disturbanceCollection", ctx.disturbanceCollection.disturbances);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("nextElement", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disturbance", ctx.updateDisturbance(2))("disturbanceCollection", ctx.disturbanceCollection.disturbances);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("nextElement", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disturbance", ctx.updateDisturbance(3))("disturbanceCollection", ctx.disturbanceCollection.disturbances);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("nextElement", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disturbance", ctx.updateDisturbance(4))("disturbanceCollection", ctx.disturbanceCollection.disturbances);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("nextElement", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disturbance", ctx.updateDisturbance(5))("disturbanceCollection", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpureFunction0"](15, _c0));
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, _metromobilite_m_ui_lib_m_disturbance_display__WEBPACK_IMPORTED_MODULE_3__.MDisturbanceDisplay, src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__.Preview],
    styles: ["\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZVJvb3QiOiIifQ== */"]
  });
}

/***/ }),

/***/ 80675:
/*!************************************************************************!*\
  !*** ./src/app/docs/m-ui/m-disturbance-display/ng-doc.dependencies.ts ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _m_ui_m_disturbance_display_m_disturbance_display_demo_m_disturbance_display_demo_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @m-ui/m-disturbance-display/m-disturbance-display-demo/m-disturbance-display-demo.component */ 73409);
/* harmony import */ var _ng_doc_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ng-doc.module */ 92982);


const MDisturbanceDisplayPageDependencies = {
  module: _ng_doc_module__WEBPACK_IMPORTED_MODULE_1__.MDisturbanceDisplayPageModule,
  // Add your demos that you are going to use in the page here
  demo: {
    MDisturbanceDisplayDemo: _m_ui_m_disturbance_display_m_disturbance_display_demo_m_disturbance_display_demo_component__WEBPACK_IMPORTED_MODULE_0__.MDisturbanceDisplayDemo
  }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MDisturbanceDisplayPageDependencies);

/***/ }),

/***/ 92982:
/*!******************************************************************!*\
  !*** ./src/app/docs/m-ui/m-disturbance-display/ng-doc.module.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MDisturbanceDisplayPageModule": () => (/* binding */ MDisturbanceDisplayPageModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);


class MDisturbanceDisplayPageModule {
  static #_ = this.ɵfac = function MDisturbanceDisplayPageModule_Factory(t) {
    return new (t || MDisturbanceDisplayPageModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
    type: MDisturbanceDisplayPageModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](MDisturbanceDisplayPageModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
})();

/***/ }),

/***/ 40724:
/*!************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-ui/m-disturbance-display/assets/MDisturbanceDisplayDemo/HTML/Asset1.html ***!
  \************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"html\" class=\"hljs language-html ngde\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Nsv 1\"</span> [<span class=\"hljs-attr ngde\">nextElement</span>]=<span class=\"hljs-string ngde\">\"true\"</span> <span class=\"hljs-attr ngde\">fullWidth</span>=<span class=\"hljs-string ngde\">\"true\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">m-disturbance-display</span>\n    [<span class=\"hljs-attr ngde\">disturbance</span>]=<span class=\"hljs-string ngde\">\"updateDisturbance(1)\"</span>\n    [<span class=\"hljs-attr ngde\">disturbanceCollection</span>]=<span class=\"hljs-string ngde\">\"disturbanceCollection.disturbances\"</span>\n  ></span><span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">m-disturbance-display</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Nsv 2\"</span> [<span class=\"hljs-attr ngde\">nextElement</span>]=<span class=\"hljs-string ngde\">\"true\"</span> <span class=\"hljs-attr ngde\">fullWidth</span>=<span class=\"hljs-string ngde\">\"true\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">m-disturbance-display</span>\n    [<span class=\"hljs-attr ngde\">disturbance</span>]=<span class=\"hljs-string ngde\">\"updateDisturbance(2)\"</span>\n    [<span class=\"hljs-attr ngde\">disturbanceCollection</span>]=<span class=\"hljs-string ngde\">\"disturbanceCollection.disturbances\"</span>\n  ></span><span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">m-disturbance-display</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Nsv 3\"</span> [<span class=\"hljs-attr ngde\">nextElement</span>]=<span class=\"hljs-string ngde\">\"true\"</span> <span class=\"hljs-attr ngde\">fullWidth</span>=<span class=\"hljs-string ngde\">\"true\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">m-disturbance-display</span>\n    [<span class=\"hljs-attr ngde\">disturbance</span>]=<span class=\"hljs-string ngde\">\"updateDisturbance(3)\"</span>\n    [<span class=\"hljs-attr ngde\">disturbanceCollection</span>]=<span class=\"hljs-string ngde\">\"disturbanceCollection.disturbances\"</span>\n  ></span><span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">m-disturbance-display</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Nsv 4\"</span> [<span class=\"hljs-attr ngde\">nextElement</span>]=<span class=\"hljs-string ngde\">\"true\"</span> <span class=\"hljs-attr ngde\">fullWidth</span>=<span class=\"hljs-string ngde\">\"true\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">m-disturbance-display</span>\n    [<span class=\"hljs-attr ngde\">disturbance</span>]=<span class=\"hljs-string ngde\">\"updateDisturbance(4)\"</span>\n    [<span class=\"hljs-attr ngde\">disturbanceCollection</span>]=<span class=\"hljs-string ngde\">\"disturbanceCollection.disturbances\"</span>\n  ></span><span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">m-disturbance-display</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Nsv 5\"</span> [<span class=\"hljs-attr ngde\">nextElement</span>]=<span class=\"hljs-string ngde\">\"false\"</span> <span class=\"hljs-attr ngde\">fullWidth</span>=<span class=\"hljs-string ngde\">\"true\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">m-disturbance-display</span>\n    [<span class=\"hljs-attr ngde\">disturbance</span>]=<span class=\"hljs-string ngde\">\"updateDisturbance(5)\"</span>\n    [<span class=\"hljs-attr ngde\">disturbanceCollection</span>]=<span class=\"hljs-string ngde\">\"[]\"</span>\n  ></span><span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">m-disturbance-display</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n</code></pre>");

/***/ }),

/***/ 3977:
/*!******************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-ui/m-disturbance-display/assets/MDisturbanceDisplayDemo/TypeScript/Asset0.html ***!
  \******************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">CommonModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/common\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Component</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/core\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\"><a href=\"/api/m-ui/classes/MDisturbanceDisplay\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MDisturbanceDisplay</a></span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@metromobilite/m-ui/lib/m-disturbance-display\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Preview</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"src/app/features/preview/preview.component\"</span>;\n\n<span class=\"hljs-meta ngde\">@Component</span>({\n  <span class=\"hljs-attr ngde\">selector</span>: <span class=\"hljs-string ngde\">\"app-disturbance-display-demo\"</span>,\n  <span class=\"hljs-attr ngde\">standalone</span>: <span class=\"hljs-literal ngde\">true</span>,\n  <span class=\"hljs-attr ngde\">imports</span>: [<span class=\"hljs-title class_ ngde\">CommonModule</span>, <span class=\"hljs-title class_ ngde\"><a href=\"/api/m-ui/classes/MDisturbanceDisplay\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MDisturbanceDisplay</a></span>, <span class=\"hljs-title class_ ngde\">Preview</span>],\n  <span class=\"hljs-attr ngde\">templateUrl</span>: <span class=\"hljs-string ngde\">\"./m-disturbance-display-demo.component.html\"</span>,\n  <span class=\"hljs-attr ngde\">styleUrls</span>: [<span class=\"hljs-string ngde\">\"./m-disturbance-display-demo.component.scss\"</span>],\n})\n<span class=\"hljs-keyword ngde\">export</span> <span class=\"hljs-keyword ngde\">class</span> <span class=\"hljs-title class_ ngde\">MDisturbanceDisplayDemo</span> {\n  <span class=\"hljs-keyword ngde\">public</span> disturbance = {\n    <span class=\"hljs-attr ngde\">activateHostbinding</span>: <span class=\"hljs-literal ngde\">true</span>,\n    <span class=\"hljs-attr ngde\">calculatedClasses</span>: <span class=\"hljs-string ngde\">\"\"</span>,\n    <span class=\"hljs-attr ngde\">disturbanceOptions</span>: { <span class=\"hljs-attr ngde\">lineOrNsv</span>: <span class=\"hljs-number ngde\">1</span> },\n    <span class=\"hljs-attr ngde\">hasDisturbance</span>: <span class=\"hljs-literal ngde\">false</span>,\n    <span class=\"hljs-attr ngde\">hostClass</span>: <span class=\"hljs-string ngde\">\"\"</span>,\n    <span class=\"hljs-attr ngde\">nsv</span>: <span class=\"hljs-number ngde\">1</span>,\n    <span class=\"hljs-attr ngde\">outer</span>: <span class=\"hljs-literal ngde\">true</span>,\n  };\n\n  <span class=\"hljs-keyword ngde\">public</span> <span class=\"hljs-attr ngde\">disturbanceCollection</span>: <span class=\"hljs-built_in ngde\">any</span> = {\n    <span class=\"hljs-attr ngde\">disturbances</span>: [\n      {\n        <span class=\"hljs-attr ngde\">type</span>: <span class=\"hljs-string ngde\">\"restriction_ltc\"</span>,\n        <span class=\"hljs-attr ngde\">code</span>: <span class=\"hljs-string ngde\">\"SEM_1396148590774867315\"</span>,\n        <span class=\"hljs-attr ngde\">dateDebut</span>: <span class=\"hljs-string ngde\">\"08/01/2025 09:00\"</span>,\n        <span class=\"hljs-attr ngde\">dateFin</span>: <span class=\"hljs-string ngde\">\"08/01/2025 16:30\"</span>,\n        <span class=\"hljs-attr ngde\">heureDebut</span>: <span class=\"hljs-string ngde\">\"00:00:00\"</span>,\n        <span class=\"hljs-attr ngde\">heureFin</span>: <span class=\"hljs-string ngde\">\"00:00:00\"</span>,\n        <span class=\"hljs-attr ngde\">latitude</span>: -<span class=\"hljs-number ngde\">1</span>,\n        <span class=\"hljs-attr ngde\">longitude</span>: -<span class=\"hljs-number ngde\">1</span>,\n        <span class=\"hljs-attr ngde\">weekEnd</span>: <span class=\"hljs-string ngde\">\"2\"</span>,\n        <span class=\"hljs-attr ngde\">texte</span>:\n          <span class=\"hljs-string ngde\">\"A : travaux|jusqu'au 08/01/2025 16:30|La ligne A ne circule pas entre les stations Polesud - Alpexpo et Malherbe en raison de travaux.\\nDes bus relais effectuent la liaison entre ces stations. \\n \\nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \\nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.\"</span>,\n        <span class=\"hljs-attr ngde\">titre</span>: <span class=\"hljs-string ngde\">\"travaux\"</span>,\n        <span class=\"hljs-attr ngde\">description</span>:\n          <span class=\"hljs-string ngde\">\"jusqu'au 08/01/2025 16:30\\r\\nLa ligne A ne circule pas entre les stations Polesud - Alpexpo et Malherbe en raison de travaux.\\nDes bus relais effectuent la liaison entre ces stations. \\n \\nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \\nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.\"</span>,\n        <span class=\"hljs-attr ngde\">plan</span>: <span class=\"hljs-string ngde\">\"https://www.tag.fr/include/downloadInfoCompPerturbation.php?f=Info coupure A _Malherbe Pole Sud.pdf\"</span>,\n        <span class=\"hljs-attr ngde\">visibleTC</span>: <span class=\"hljs-literal ngde\">true</span>,\n        <span class=\"hljs-attr ngde\">visibleVoiture</span>: <span class=\"hljs-literal ngde\">false</span>,\n        <span class=\"hljs-attr ngde\">visibleVelo</span>: <span class=\"hljs-literal ngde\">false</span>,\n        <span class=\"hljs-attr ngde\">visibleVenteTitres</span>: <span class=\"hljs-literal ngde\">false</span>,\n        <span class=\"hljs-attr ngde\">visibleBandeauSite</span>: <span class=\"hljs-literal ngde\">false</span>,\n        <span class=\"hljs-attr ngde\">visibleBandeauAppli</span>: <span class=\"hljs-literal ngde\">false</span>,\n        <span class=\"hljs-attr ngde\">listeLigne</span>: <span class=\"hljs-string ngde\">\"SEM_A\"</span>,\n        <span class=\"hljs-attr ngde\">listeLigneArret</span>: <span class=\"hljs-string ngde\">\"SEM_A\"</span>,\n        <span class=\"hljs-attr ngde\">nsv_id</span>: <span class=\"hljs-number ngde\">2</span>,\n        <span class=\"hljs-attr ngde\">computed</span>: {\n          <span class=\"hljs-attr ngde\">title</span>: <span class=\"hljs-string ngde\">\"travaux\"</span>,\n          <span class=\"hljs-attr ngde\">content</span>:\n            <span class=\"hljs-string ngde\">\"jusqu'au 08/01/2025 16:30&#x3C;br>La ligne A ne circule pas entre les stations Polesud - Alpexpo et Malherbe en raison de travaux.\\nDes bus relais effectuent la liaison entre ces stations. \\n \\nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \\nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.\"</span>,\n          <span class=\"hljs-attr ngde\">line</span>: {\n            <span class=\"hljs-attr ngde\">id</span>: <span class=\"hljs-string ngde\">\"SEM:A\"</span>,\n            <span class=\"hljs-attr ngde\">gtfsId</span>: <span class=\"hljs-string ngde\">\"SEM:A\"</span>,\n            <span class=\"hljs-attr ngde\">shortName</span>: <span class=\"hljs-string ngde\">\"A\"</span>,\n            <span class=\"hljs-attr ngde\">longName</span>: <span class=\"hljs-string ngde\">\"FONTAINE La Poya / PONT DE CLAIX L'Etoile\"</span>,\n            <span class=\"hljs-attr ngde\">color</span>: <span class=\"hljs-string ngde\">\"3376B8\"</span>,\n            <span class=\"hljs-attr ngde\">textColor</span>: <span class=\"hljs-string ngde\">\"FFFFFF\"</span>,\n            <span class=\"hljs-attr ngde\">mode</span>: <span class=\"hljs-string ngde\">\"TRAM\"</span>,\n            <span class=\"hljs-attr ngde\">type</span>: <span class=\"hljs-string ngde\">\"TRAM\"</span>,\n            <span class=\"hljs-attr ngde\">pdf</span>: <span class=\"hljs-string ngde\">\"https://data-pp.mobilites-m.fr/api/planligne/pdf?route=SEM:A\"</span>,\n          },\n        },\n      },\n      {\n        <span class=\"hljs-attr ngde\">type</span>: <span class=\"hljs-string ngde\">\"restriction_ltc\"</span>,\n        <span class=\"hljs-attr ngde\">code</span>: <span class=\"hljs-string ngde\">\"SEM_5828443584937855085\"</span>,\n        <span class=\"hljs-attr ngde\">dateDebut</span>: <span class=\"hljs-string ngde\">\"09/01/2025 22:00\"</span>,\n        <span class=\"hljs-attr ngde\">dateFin</span>: <span class=\"hljs-string ngde\">\"10/01/2025 03:00\"</span>,\n        <span class=\"hljs-attr ngde\">heureDebut</span>: <span class=\"hljs-string ngde\">\"22:00\"</span>,\n        <span class=\"hljs-attr ngde\">heureFin</span>: <span class=\"hljs-string ngde\">\"02:00\"</span>,\n        <span class=\"hljs-attr ngde\">latitude</span>: -<span class=\"hljs-number ngde\">1</span>,\n        <span class=\"hljs-attr ngde\">longitude</span>: -<span class=\"hljs-number ngde\">1</span>,\n        <span class=\"hljs-attr ngde\">weekEnd</span>: <span class=\"hljs-string ngde\">\"0\"</span>,\n        <span class=\"hljs-attr ngde\">texte</span>:\n          <span class=\"hljs-string ngde\">\"A : travaux de nuit Grand'Place|à partir du 09/01/2025 22:00|jusqu'au 10/01/2025 03:00|La ligne A ne circule pas entre les stations L'Etoile et Chavant en raison de travaux de nuit secteur Grand'Place.\\nDes bus relais effectuent la liaison entre ces stations.\\n \\nÀ noter que la station Echirolles Gare n'est pas desservie par les bus relais.\\n \\nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \\nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.\"</span>,\n        <span class=\"hljs-attr ngde\">titre</span>: <span class=\"hljs-string ngde\">\"travaux de nuit Grand'Place\"</span>,\n        <span class=\"hljs-attr ngde\">description</span>:\n          <span class=\"hljs-string ngde\">\"à partir du 09/01/2025 22:00|jusqu'au 10/01/2025 03:00\\r\\nLa ligne A ne circule pas entre les stations L'Etoile et Chavant en raison de travaux de nuit secteur Grand'Place.\\nDes bus relais effectuent la liaison entre ces stations.\\n \\nÀ noter que la station Echirolles Gare n'est pas desservie par les bus relais.\\n \\nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \\nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.\"</span>,\n        <span class=\"hljs-attr ngde\">plan</span>: <span class=\"hljs-string ngde\">\"https://www.tag.fr/include/downloadInfoCompPerturbation.php?f=Info Coupure A LEtoile-Chavant.pdf\"</span>,\n        <span class=\"hljs-attr ngde\">visibleTC</span>: <span class=\"hljs-literal ngde\">true</span>,\n        <span class=\"hljs-attr ngde\">visibleVoiture</span>: <span class=\"hljs-literal ngde\">false</span>,\n        <span class=\"hljs-attr ngde\">visibleVelo</span>: <span class=\"hljs-literal ngde\">false</span>,\n        <span class=\"hljs-attr ngde\">visibleVenteTitres</span>: <span class=\"hljs-literal ngde\">false</span>,\n        <span class=\"hljs-attr ngde\">visibleBandeauSite</span>: <span class=\"hljs-literal ngde\">false</span>,\n        <span class=\"hljs-attr ngde\">visibleBandeauAppli</span>: <span class=\"hljs-literal ngde\">false</span>,\n        <span class=\"hljs-attr ngde\">listeLigne</span>: <span class=\"hljs-string ngde\">\"SEM_A\"</span>,\n        <span class=\"hljs-attr ngde\">listeLigneArret</span>: <span class=\"hljs-string ngde\">\"SEM_A\"</span>,\n        <span class=\"hljs-attr ngde\">listeArret</span>: <span class=\"hljs-string ngde\">\"SEM:2207,SEM:2208\"</span>,\n        <span class=\"hljs-attr ngde\">nsv_id</span>: <span class=\"hljs-number ngde\">2</span>,\n        <span class=\"hljs-attr ngde\">computed</span>: {\n          <span class=\"hljs-attr ngde\">title</span>: <span class=\"hljs-string ngde\">\"travaux de nuit Grand'Place\"</span>,\n          <span class=\"hljs-attr ngde\">content</span>:\n            <span class=\"hljs-string ngde\">\"à partir du 09/01/2025 22:00&#x3C;br>jusqu'au 10/01/2025 03:00&#x3C;br>La ligne A ne circule pas entre les stations L'Etoile et Chavant en raison de travaux de nuit secteur Grand'Place.\\nDes bus relais effectuent la liaison entre ces stations.\\n \\nÀ noter que la station Echirolles Gare n'est pas desservie par les bus relais.\\n \\nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \\nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.\"</span>,\n          <span class=\"hljs-attr ngde\">line</span>: {\n            <span class=\"hljs-attr ngde\">id</span>: <span class=\"hljs-string ngde\">\"SEM:A\"</span>,\n            <span class=\"hljs-attr ngde\">gtfsId</span>: <span class=\"hljs-string ngde\">\"SEM:A\"</span>,\n            <span class=\"hljs-attr ngde\">shortName</span>: <span class=\"hljs-string ngde\">\"A\"</span>,\n            <span class=\"hljs-attr ngde\">longName</span>: <span class=\"hljs-string ngde\">\"FONTAINE La Poya / PONT DE CLAIX L'Etoile\"</span>,\n            <span class=\"hljs-attr ngde\">color</span>: <span class=\"hljs-string ngde\">\"3376B8\"</span>,\n            <span class=\"hljs-attr ngde\">textColor</span>: <span class=\"hljs-string ngde\">\"FFFFFF\"</span>,\n            <span class=\"hljs-attr ngde\">mode</span>: <span class=\"hljs-string ngde\">\"TRAM\"</span>,\n            <span class=\"hljs-attr ngde\">type</span>: <span class=\"hljs-string ngde\">\"TRAM\"</span>,\n            <span class=\"hljs-attr ngde\">pdf</span>: <span class=\"hljs-string ngde\">\"https://data-pp.mobilites-m.fr/api/planligne/pdf?route=SEM:A\"</span>,\n          },\n        },\n      },\n    ],\n    <span class=\"hljs-attr ngde\">dis</span>: {\n      <span class=\"hljs-attr ngde\">hasDisturbance</span>: <span class=\"hljs-literal ngde\">true</span>,\n      <span class=\"hljs-attr ngde\">nsv</span>: <span class=\"hljs-number ngde\">2</span>,\n      <span class=\"hljs-attr ngde\">calculatedClasses</span>: <span class=\"hljs-string ngde\">\"has-disturbance\"</span>,\n    },\n  };\n\n  <span class=\"hljs-title function_ ngde\">updateDisturbance</span>(<span class=\"hljs-attr ngde\">nsv</span>: <span class=\"hljs-built_in ngde\">number</span>): <span class=\"hljs-built_in ngde\">any</span> {\n    <span class=\"hljs-keyword ngde\">const</span> disturbance = <span class=\"hljs-title class_ ngde\">JSON</span>.<span class=\"hljs-title function_ ngde\">parse</span>(<span class=\"hljs-title class_ ngde\">JSON</span>.<span class=\"hljs-title function_ ngde\">stringify</span>(<span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">disturbance</span>));\n    disturbance.<span class=\"hljs-property ngde\">nsv</span> = nsv;\n    disturbance.<span class=\"hljs-property ngde\">disturbanceOptions</span>.<span class=\"hljs-property ngde\">lineOrNsv</span> = nsv;\n    <span class=\"hljs-keyword ngde\">return</span> disturbance;\n  }\n}\n</code></pre>");

/***/ }),

/***/ 6886:
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-ui/m-disturbance-display/index.html ***!
  \***********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<h1 id=\"m-disturbance-display\" class=\"ngde\">M-disturbance-display<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/m-ui/m-disturbance-display#m-disturbance-display\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1><h2 id=\"api\" class=\"ngde\">API<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/m-ui/m-disturbance-display#api\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><p class=\"ngde\">Shows how to use the <code class=\"ngde ng-doc-code-with-link\" class=\"ngde\"><a href=\"/api/m-ui/classes/MDisturbanceDisplay\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MDisturbanceDisplay</a></code> component.</p><h2 id=\"default-table\" class=\"ngde\">Default table<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/m-ui/m-disturbance-display#default-table\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><ng-doc-demo componentname=\"MDisturbanceDisplayDemo\" indexable=\"false\" class=\"ngde\"><div id=\"options\" class=\"ngde\">{}</div></ng-doc-demo>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_guides_m-ui_m-disturbance-display_module_ts.js.map