"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_api_api_m-ui_interfaces_disturbanceInfo_module_ts"],{

/***/ 78429:
/*!**************************************************************************!*\
  !*** ./.ng-doc/m-docs/api/api/m-ui/interfaces/disturbanceInfo/module.ts ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_api_api_m_ui_interfaces_disturbanceInfo_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/api/api/m-ui/interfaces/disturbanceInfo/index.html */ 37111);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);







class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = undefined;
    this.pageType = 'api';
    this.pageContent = _raw_loader_ng_doc_m_docs_api_api_m_ui_interfaces_disturbanceInfo_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
    this.demo = undefined;
    this.demoAssets = undefined;
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'disturbanceInfo'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
})();

/***/ }),

/***/ 37111:
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/api/api/m-ui/interfaces/disturbanceInfo/index.html ***!
  \*****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<header class=\"ngde\"><div class=\"ng-doc-page-tags ngde\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"ng-doc-scope\">m-ui</span> <span class=\"ng-doc-inline-delimiter ngde\" indexable=\"false\">/</span> <span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Interface\">Interface</span></div><h1 id=\"disturbanceinfo\" class=\"ngde\">disturbanceInfo<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/interfaces/disturbanceInfo#disturbanceinfo\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1></header><section class=\"ngde\"><p class=\"ngde\">disturbanceInfo</p><p class=\"ngde\">Represents the properties of a disturbance, including the disturbance level and whether it is outer.</p></section><section class=\"ngde\"><h2 id=\"properties\" class=\"ngde\">Properties<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/interfaces/disturbanceInfo#properties\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ng-doc-properties-table ngde\"><thead class=\"ngde\"><tr indexable=\"false\" class=\"ngde\"><th class=\"ng-doc-properties-table-name ngde\">Name</th><th class=\"ng-doc-properties-table-type ngde\">Type</th><th class=\"ng-doc-properties-table-description ngde\">Description</th></tr></thead><tbody class=\"ngde\"><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">hasDisturbance<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">boolean</code></td><td class=\"ngde\"><p class=\"ngde\">If <code class=\"ngde\">true</code>, the line has a disturbance</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">nsv<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">number</code></td><td class=\"ngde\"><p class=\"ngde\">The disturbance level.</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">outer<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">boolean</code></td><td class=\"ngde\"><p class=\"ngde\">If <code class=\"ngde\">true</code>, the disturbance is outer.</p></td></tr></tbody></table></div></section>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_api_api_m-ui_interfaces_disturbanceInfo_module_ts.js.map