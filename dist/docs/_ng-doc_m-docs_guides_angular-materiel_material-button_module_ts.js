"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_guides_angular-materiel_material-button_module_ts"],{

/***/ 10859:
/*!************************************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/angular-materiel/material-button/component-assets.ts ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   "demoAssets": () => (/* binding */ demoAssets)
/* harmony export */ });
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_button_assets_ButtonBasicDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-button/assets/ButtonBasicDemo/TypeScript/Asset0.html */ 65952);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_button_assets_ButtonBasicDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-button/assets/ButtonBasicDemo/HTML/Asset1.html */ 35811);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_button_assets_ButtonBasicDemo_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-button/assets/ButtonBasicDemo/SCSS/Asset2.html */ 67065);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_button_assets_ButtonVarietiesDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-button/assets/ButtonVarietiesDemo/TypeScript/Asset0.html */ 2318);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_button_assets_ButtonVarietiesDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-button/assets/ButtonVarietiesDemo/HTML/Asset1.html */ 99865);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_button_assets_ButtonVarietiesDemo_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-button/assets/ButtonVarietiesDemo/SCSS/Asset2.html */ 80429);






const demoAssets = {
  ButtonBasicDemo: [{
    title: 'TypeScript',
    codeType: 'TypeScript',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_button_assets_ButtonBasicDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__["default"]
  }, {
    title: 'HTML',
    codeType: 'HTML',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_button_assets_ButtonBasicDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__["default"]
  }, {
    title: 'SCSS',
    codeType: 'SCSS',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_button_assets_ButtonBasicDemo_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_2__["default"]
  }],
  ButtonVarietiesDemo: [{
    title: 'TypeScript',
    codeType: 'TypeScript',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_button_assets_ButtonVarietiesDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_3__["default"]
  }, {
    title: 'HTML',
    codeType: 'HTML',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_button_assets_ButtonVarietiesDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_4__["default"]
  }, {
    title: 'SCSS',
    codeType: 'SCSS',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_button_assets_ButtonVarietiesDemo_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_5__["default"]
  }]
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (demoAssets);

/***/ }),

/***/ 6403:
/*!**************************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/angular-materiel/material-button/module.ts ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_button_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-button/index.html */ 85645);
/* harmony import */ var _playgrounds__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./playgrounds */ 81922);
/* harmony import */ var src_app_docs_angular_material_material_button_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/docs/angular-material/material-button/ng-doc.dependencies */ 75437);
/* harmony import */ var _ng_doc_m_docs_guides_angular_materiel_material_button_component_assets__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! .ng-doc/m-docs/guides/angular-materiel/material-button/component-assets */ 10859);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_docs_angular_material_material_button_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/docs/angular-material/material-button/ng-doc.module */ 87188);




// noinspection ES6UnusedImports


// noinspection ES6UnusedImports





class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = '';
    this.pageType = 'guide';
    this.pageContent = _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_button_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
    this.dependencies = src_app_docs_angular_material_material_button_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"];
    this.demoAssets = _ng_doc_m_docs_guides_angular_materiel_material_button_component_assets__WEBPACK_IMPORTED_MODULE_3__["default"];
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, src_app_docs_angular_material_material_button_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"].module, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'Button'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, src_app_docs_angular_material_material_button_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__.MaterialButtonPageModule, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
})();

/***/ }),

/***/ 81922:
/*!*******************************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/angular-materiel/material-button/playgrounds.ts ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PlaygroundsModule": () => (/* binding */ PlaygroundsModule)
/* harmony export */ });
/* harmony import */ var src_app_docs_angular_material_material_button_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/docs/angular-material/material-button/ng-doc.dependencies */ 75437);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_docs_angular_material_material_button_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/docs/angular-material/material-button/ng-doc.module */ 87188);
// noinspection ES6UnusedImports




class PlaygroundsModule {
  static #_ = this.ɵfac = function PlaygroundsModule_Factory(t) {
    return new (t || PlaygroundsModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: PlaygroundsModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, src_app_docs_angular_material_material_button_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__["default"].module]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](PlaygroundsModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, src_app_docs_angular_material_material_button_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__.MaterialButtonPageModule]
  });
})();

/***/ }),

/***/ 84978:
/*!********************************************************************************************************!*\
  !*** ./src/app/docs/angular-material/material-button/button-basic-demo/button-basic-demo.component.ts ***!
  \********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ButtonBasicDemo": () => (/* binding */ ButtonBasicDemo)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/button */ 84522);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/divider */ 71528);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/icon */ 57822);
/* harmony import */ var src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/features/preview/preview.component */ 55355);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);








class ButtonBasicDemo {
  static #_ = this.ɵfac = function ButtonBasicDemo_Factory(t) {
    return new (t || ButtonBasicDemo)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
    type: ButtonBasicDemo,
    selectors: [["app-button-basic-demo"]],
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵStandaloneFeature"]],
    decls: 112,
    vars: 1,
    consts: [["title", "Basic"], [1, "example-button-row"], ["mat-button", ""], ["mat-button", "", "color", "primary"], ["mat-button", "", "color", "accent"], ["mat-button", "", "color", "warn"], ["mat-button", "", "disabled", ""], ["mat-button", "", "href", "https://www.google.com/", "target", "_blank"], ["title", "Raised"], ["mat-raised-button", ""], ["mat-raised-button", "", "color", "primary"], ["mat-raised-button", "", "color", "accent"], ["mat-raised-button", "", "color", "warn"], ["mat-raised-button", "", "disabled", ""], ["mat-raised-button", "", "href", "https://www.google.com/", "target", "_blank"], ["title", "Stroked"], ["mat-stroked-button", ""], ["mat-stroked-button", "", "color", "primary"], ["mat-stroked-button", "", "color", "accent"], ["mat-stroked-button", "", "color", "warn"], ["mat-stroked-button", "", "disabled", ""], ["mat-stroked-button", "", "href", "https://www.google.com/", "target", "_blank"], ["title", "Flat"], ["mat-flat-button", ""], ["mat-flat-button", "", "color", "primary"], ["mat-flat-button", "", "color", "accent"], ["mat-flat-button", "", "color", "warn"], ["mat-flat-button", "", "disabled", ""], ["mat-flat-button", "", "href", "https://www.google.com/", "target", "_blank"], ["title", "Icon"], [1, "example-flex-container"], ["mat-icon-button", "", "aria-label", "Example icon button with a vertical three dot icon"], ["mat-icon-button", "", "color", "primary", "aria-label", "Example icon button with a home icon"], ["mat-icon-button", "", "color", "accent", "aria-label", "Example icon button with a menu icon"], ["mat-icon-button", "", "color", "warn", "aria-label", "Example icon button with a heart icon"], ["mat-icon-button", "", "disabled", "", "aria-label", "Example icon button with a open in new tab icon"], ["title", "FAB"], [1, "example-button-container"], ["mat-fab", "", "color", "primary", "aria-label", "Example icon button with a delete icon"], ["mat-fab", "", "color", "accent", "aria-label", "Example icon button with a bookmark icon"], ["mat-fab", "", "color", "warn", "aria-label", "Example icon button with a home icon"], ["mat-fab", "", "disabled", "", "aria-label", "Example icon button with a heart icon"], ["title", "Mini FAB", 3, "nextElement"], ["mat-mini-fab", "", "color", "primary", "aria-label", "Example icon button with a menu icon"], ["mat-mini-fab", "", "color", "accent", "aria-label", "Example icon button with a plus one icon"], ["mat-mini-fab", "", "color", "warn", "aria-label", "Example icon button with a filter list icon"], ["mat-mini-fab", "", "disabled", "", "aria-label", "Example icon button with a home icon"]],
    template: function ButtonBasicDemo_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "app-preview", 0)(1, "div", 1)(2, "button", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Basic");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, "Primary");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "button", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7, "Accent");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "button", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, "Warn");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "button", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "Disabled");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13, "Link");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "app-preview", 8)(15, "div", 1)(16, "button", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17, "Basic");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](19, "Primary");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "button", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](21, "Accent");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](23, "Warn");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](25, "Disabled");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "a", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](27, "Link");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "app-preview", 15)(29, "div", 1)(30, "button", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](31, "Basic");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](32, "button", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](33, "Primary");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](34, "button", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](35, "Accent");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](36, "button", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](37, "Warn");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](38, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](39, "Disabled");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](40, "a", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](41, "Link");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](42, "app-preview", 22)(43, "div", 1)(44, "button", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](45, "Basic");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](46, "button", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](47, "Primary");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](48, "button", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](49, "Accent");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](50, "button", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](51, "Warn");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](52, "button", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](53, "Disabled");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](54, "a", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](55, "Link");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](56, "app-preview", 29)(57, "div", 1)(58, "div", 30)(59, "button", 31)(60, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](61, "more_vert");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](62, "button", 32)(63, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](64, "home");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](65, "button", 33)(66, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](67, "menu");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](68, "button", 34)(69, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](70, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](71, "button", 35)(72, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](73, "open_in_new");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](74, "app-preview", 36)(75, "div", 1)(76, "div", 30)(77, "div", 37)(78, "button", 38)(79, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](80, "delete");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](81, "div", 37)(82, "button", 39)(83, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](84, "bookmark");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](85, "div", 37)(86, "button", 40)(87, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](88, "home");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](89, "div", 37)(90, "button", 41)(91, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](92, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](93, "app-preview", 42)(94, "div", 1)(95, "div", 30)(96, "div", 37)(97, "button", 43)(98, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](99, "menu");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](100, "div", 37)(101, "button", 44)(102, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](103, "plus_one");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](104, "div", 37)(105, "button", 45)(106, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](107, "filter_list");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](108, "div", 37)(109, "button", 46)(110, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](111, "home");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()()()();
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](93);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("nextElement", false);
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, _angular_material_button__WEBPACK_IMPORTED_MODULE_3__.MatButtonModule, _angular_material_button__WEBPACK_IMPORTED_MODULE_3__.MatAnchor, _angular_material_button__WEBPACK_IMPORTED_MODULE_3__.MatButton, _angular_material_button__WEBPACK_IMPORTED_MODULE_3__.MatIconButton, _angular_material_button__WEBPACK_IMPORTED_MODULE_3__.MatMiniFabButton, _angular_material_button__WEBPACK_IMPORTED_MODULE_3__.MatFabButton, _angular_material_divider__WEBPACK_IMPORTED_MODULE_4__.MatDividerModule, _angular_material_icon__WEBPACK_IMPORTED_MODULE_5__.MatIconModule, _angular_material_icon__WEBPACK_IMPORTED_MODULE_5__.MatIcon, src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__.Preview],
    styles: ["section[_ngcontent-%COMP%] {\n  display: table;\n}\n\n.example-button-row[_ngcontent-%COMP%] {\n  display: table-cell;\n  max-width: 600px;\n}\n\n.example-button-row[_ngcontent-%COMP%]   .mat-mdc-button-base[_ngcontent-%COMP%] {\n  margin: 8px 8px 8px 0;\n}\n\n.example-flex-container[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: space-between;\n  flex-wrap: wrap;\n}\n\n.example-button-container[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  width: 120px;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvZG9jcy9hbmd1bGFyLW1hdGVyaWFsL21hdGVyaWFsLWJ1dHRvbi9idXR0b24tYmFzaWMtZGVtby9idXR0b24tYmFzaWMtZGVtby5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGNBQUE7QUFDRDs7QUFFQTtFQUNDLG1CQUFBO0VBQ0EsZ0JBQUE7QUFDRDs7QUFFQTtFQUNDLHFCQUFBO0FBQ0Q7O0FBRUE7RUFDQyxhQUFBO0VBQ0EsOEJBQUE7RUFDQSxlQUFBO0FBQ0Q7O0FBRUE7RUFDQyxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0FBQ0QiLCJzb3VyY2VzQ29udGVudCI6WyJzZWN0aW9uIHtcclxuXHRkaXNwbGF5OiB0YWJsZTtcclxufVxyXG5cclxuLmV4YW1wbGUtYnV0dG9uLXJvdyB7XHJcblx0ZGlzcGxheTogdGFibGUtY2VsbDtcclxuXHRtYXgtd2lkdGg6IDYwMHB4O1xyXG59XHJcblxyXG4uZXhhbXBsZS1idXR0b24tcm93IC5tYXQtbWRjLWJ1dHRvbi1iYXNlIHtcclxuXHRtYXJnaW46IDhweCA4cHggOHB4IDA7XHJcbn1cclxuXHJcbi5leGFtcGxlLWZsZXgtY29udGFpbmVyIHtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuXHRmbGV4LXdyYXA6IHdyYXA7XHJcbn1cclxuXHJcbi5leGFtcGxlLWJ1dHRvbi1jb250YWluZXIge1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcblx0d2lkdGg6IDEyMHB4O1xyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0= */"]
  });
}

/***/ }),

/***/ 10451:
/*!****************************************************************************************************************!*\
  !*** ./src/app/docs/angular-material/material-button/button-varieties-demo/button-varieties-demo.component.ts ***!
  \****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ButtonVarietiesDemo": () => (/* binding */ ButtonVarietiesDemo)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/button */ 84522);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/icon */ 57822);
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/tooltip */ 6896);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/features/preview/preview.component */ 55355);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);










class ButtonVarietiesDemo {
  static #_ = this.ɵfac = function ButtonVarietiesDemo_Factory(t) {
    return new (t || ButtonVarietiesDemo)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
    type: ButtonVarietiesDemo,
    selectors: [["app-button-varieties-demo"]],
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵStandaloneFeature"]],
    decls: 157,
    vars: 1,
    consts: [["title", "Basic Buttons"], [1, "example-button-row"], ["mat-button", ""], ["mat-button", "", "color", "primary"], ["mat-button", "", "color", "accent"], ["mat-button", "", "color", "warn"], ["mat-button", "", "disabled", ""], ["mat-button", "", "routerLink", "."], ["title", "Raised Button"], ["mat-raised-button", ""], ["mat-raised-button", "", "color", "primary"], ["mat-raised-button", "", "color", "accent"], ["mat-raised-button", "", "color", "warn"], ["mat-raised-button", "", "disabled", ""], ["mat-raised-button", "", "routerLink", "."], ["title", "Stroked Buttons"], ["mat-stroked-button", ""], ["mat-stroked-button", "", "color", "primary"], ["mat-stroked-button", "", "color", "accent"], ["mat-stroked-button", "", "color", "warn"], ["mat-stroked-button", "", "disabled", ""], ["mat-stroked-button", "", "routerLink", "."], ["title", "Flat Buttons"], ["mat-flat-button", ""], ["mat-flat-button", "", "color", "primary"], ["mat-flat-button", "", "color", "accent"], ["mat-flat-button", "", "color", "warn"], ["mat-flat-button", "", "disabled", ""], ["mat-flat-button", "", "routerLink", "."], ["title", "Icon Buttons"], ["mat-icon-button", "", "matTooltip", "Basic", "aria-label", "Example icon-button with a heart icon"], ["mat-icon-button", "", "matTooltip", "Primary", "color", "primary", "aria-label", "Example icon-button with a heart icon"], ["mat-icon-button", "", "matTooltip", "Accent", "color", "accent", "aria-label", "Example icon-button with a heart icon"], ["mat-icon-button", "", "matTooltip", "Warn", "color", "warn", "aria-label", "Example icon-button with a heart icon"], ["matTooltip", "Disabled", 1, "example-disabled"], ["mat-icon-button", "", "disabled", "", "aria-label", "Example icon-button with a heart icon"], ["mat-icon-button", "", "matTooltip", "Link", "routerLink", ".", "aria-label", "Example icon-button with a heart icon"], ["title", "Fab Buttons"], ["mat-fab", "", "matTooltip", "Primary", "color", "primary", "aria-label", "Example fab with a heart icon"], ["mat-fab", "", "matTooltip", "Accent", "color", "accent", "aria-label", "Example fab with a heart icon"], ["mat-fab", "", "matTooltip", "Warn", "color", "warn", "aria-label", "Example fab with a heart icon"], ["mat-fab", "", "disabled", "", "aria-label", "Example fab with a heart icon"], ["mat-fab", "", "matTooltip", "Link", "routerLink", ".", "aria-label", "Example fab with a heart icon"], ["title", "Mini Fab Buttons"], ["mat-mini-fab", "", "matTooltip", "Primary", "color", "primary", "aria-label", "Example mini fab with a heart icon"], ["mat-mini-fab", "", "matTooltip", "Accent", "color", "accent", "aria-label", "Example mini fab with a heart icon"], ["mat-mini-fab", "", "matTooltip", "Warn", "color", "warn", "aria-label", "Example mini fab with a heart icon"], ["mat-mini-fab", "", "disabled", "", "aria-label", "Example mini fab with a heart icon"], ["mat-mini-fab", "", "matTooltip", "Link", "routerLink", ".", "aria-label", "Example mini fab with a heart icon"], ["title", "Extended Fab"], ["mat-fab", "", "extended", "", "color", "primary"], ["mat-fab", "", "extended", "", "color", "accent"], ["mat-fab", "", "extended", "", "color", "warn"], ["mat-fab", "", "extended", "", "disabled", ""], ["mat-fab", "", "extended", "", "routerLink", "."], ["title", "Reverse Icon: [iconPositionEnd]", 3, "nextElement"], ["iconPositionEnd", ""]],
    template: function ButtonVarietiesDemo_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "app-preview", 0)(1, "div", 1)(2, "button", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Basic");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, "Primary");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "button", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](7, "Accent");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](8, "button", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, "Warn");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "button", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "Disabled");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "a", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13, "Link");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "app-preview", 8)(15, "div", 1)(16, "button", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](17, "Basic");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "button", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](19, "Primary");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "button", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](21, "Accent");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "button", 12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](23, "Warn");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "button", 13);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](25, "Disabled");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](26, "a", 14);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](27, "Link");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "app-preview", 15)(29, "div", 1)(30, "button", 16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](31, "Basic");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](32, "button", 17);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](33, "Primary");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](34, "button", 18);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](35, "Accent");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](36, "button", 19);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](37, "Warn");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](38, "button", 20);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](39, "Disabled");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](40, "a", 21);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](41, "Link");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](42, "app-preview", 22)(43, "div", 1)(44, "button", 23);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](45, "Basic");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](46, "button", 24);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](47, "Primary");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](48, "button", 25);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](49, "Accent");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](50, "button", 26);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](51, "Warn");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](52, "button", 27);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](53, "Disabled");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](54, "a", 28);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](55, "Link");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](56, "app-preview", 29)(57, "div", 1)(58, "button", 30)(59, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](60, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](61, "button", 31)(62, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](63, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](64, "button", 32)(65, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](66, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](67, "button", 33)(68, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](69, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](70, "div", 34)(71, "button", 35)(72, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](73, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](74, "a", 36)(75, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](76, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](77, "app-preview", 37)(78, "div", 1)(79, "button", 38)(80, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](81, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](82, "button", 39)(83, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](84, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](85, "button", 40)(86, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](87, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](88, "div", 34)(89, "button", 41)(90, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](91, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](92, "a", 42)(93, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](94, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](95, "app-preview", 43)(96, "div", 1)(97, "button", 44)(98, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](99, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](100, "button", 45)(101, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](102, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](103, "button", 46)(104, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](105, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](106, "div", 34)(107, "button", 47)(108, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](109, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](110, "a", 48)(111, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](112, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](113, "app-preview", 49)(114, "div", 1)(115, "button", 50)(116, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](117, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](118, " Primary ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](119, "button", 51)(120, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](121, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](122, " Accent ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](123, "button", 52)(124, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](125, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](126, " Warn ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](127, "button", 53)(128, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](129, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](130, " Disabled ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](131, "a", 54)(132, "mat-icon");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](133, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](134, " Link ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](135, "app-preview", 55)(136, "div", 1)(137, "button", 50)(138, "mat-icon", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](139, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](140, " Primary ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](141, "button", 51)(142, "mat-icon", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](143, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](144, " Accent ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](145, "button", 52)(146, "mat-icon", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](147, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](148, " Warn ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](149, "button", 53)(150, "mat-icon", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](151, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](152, " Disabled ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](153, "a", 54)(154, "mat-icon", 56);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](155, "favorite");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](156, " Link ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](135);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("nextElement", false);
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__.Preview, _angular_material_button__WEBPACK_IMPORTED_MODULE_3__.MatButtonModule, _angular_material_button__WEBPACK_IMPORTED_MODULE_3__.MatAnchor, _angular_material_button__WEBPACK_IMPORTED_MODULE_3__.MatButton, _angular_material_button__WEBPACK_IMPORTED_MODULE_3__.MatIconAnchor, _angular_material_button__WEBPACK_IMPORTED_MODULE_3__.MatIconButton, _angular_material_button__WEBPACK_IMPORTED_MODULE_3__.MatMiniFabAnchor, _angular_material_button__WEBPACK_IMPORTED_MODULE_3__.MatMiniFabButton, _angular_material_button__WEBPACK_IMPORTED_MODULE_3__.MatFabAnchor, _angular_material_button__WEBPACK_IMPORTED_MODULE_3__.MatFabButton, _angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterLink, _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_5__.MatTooltipModule, _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_5__.MatTooltip, _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__.MatIconModule, _angular_material_icon__WEBPACK_IMPORTED_MODULE_6__.MatIcon],
    styles: [".example-button-row[_ngcontent-%COMP%]   button[_ngcontent-%COMP%], .example-button-row[_ngcontent-%COMP%]   a[_ngcontent-%COMP%] {\n  margin-right: 8px;\n}\n\n.example-disabled[_ngcontent-%COMP%] {\n  display: inline-block;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvZG9jcy9hbmd1bGFyLW1hdGVyaWFsL21hdGVyaWFsLWJ1dHRvbi9idXR0b24tdmFyaWV0aWVzLWRlbW8vYnV0dG9uLXZhcmlldGllcy1kZW1vLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztFQUVDLGlCQUFBO0FBQ0Q7O0FBRUE7RUFDQyxxQkFBQTtBQUNEIiwic291cmNlc0NvbnRlbnQiOlsiLmV4YW1wbGUtYnV0dG9uLXJvdyBidXR0b24sXHJcbi5leGFtcGxlLWJ1dHRvbi1yb3cgYSB7XHJcblx0bWFyZ2luLXJpZ2h0OiA4cHg7XHJcbn1cclxuXHJcbi5leGFtcGxlLWRpc2FibGVkIHtcclxuXHRkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIifQ== */"]
  });
}

/***/ }),

/***/ 75437:
/*!******************************************************************************!*\
  !*** ./src/app/docs/angular-material/material-button/ng-doc.dependencies.ts ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _material_docs_material_button_button_basic_demo_button_basic_demo_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @material-docs/material-button/button-basic-demo/button-basic-demo.component */ 84978);
/* harmony import */ var _material_docs_material_button_button_varieties_demo_button_varieties_demo_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-docs/material-button/button-varieties-demo/button-varieties-demo.component */ 10451);
/* harmony import */ var src_app_docs_angular_material_material_button_ng_doc_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/docs/angular-material/material-button/ng-doc.module */ 87188);



const MaterialButtonPageDependencies = {
  module: src_app_docs_angular_material_material_button_ng_doc_module__WEBPACK_IMPORTED_MODULE_2__.MaterialButtonPageModule,
  // Add your demos that you are going to use in the page here
  demo: {
    ButtonBasicDemo: _material_docs_material_button_button_basic_demo_button_basic_demo_component__WEBPACK_IMPORTED_MODULE_0__.ButtonBasicDemo,
    ButtonVarietiesDemo: _material_docs_material_button_button_varieties_demo_button_varieties_demo_component__WEBPACK_IMPORTED_MODULE_1__.ButtonVarietiesDemo
  }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MaterialButtonPageDependencies);

/***/ }),

/***/ 87188:
/*!************************************************************************!*\
  !*** ./src/app/docs/angular-material/material-button/ng-doc.module.ts ***!
  \************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MaterialButtonPageModule": () => (/* binding */ MaterialButtonPageModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);


class MaterialButtonPageModule {
  static #_ = this.ɵfac = function MaterialButtonPageModule_Factory(t) {
    return new (t || MaterialButtonPageModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
    type: MaterialButtonPageModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](MaterialButtonPageModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
})();

/***/ }),

/***/ 55355:
/*!*******************************************************!*\
  !*** ./src/app/features/preview/preview.component.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Preview": () => (/* binding */ Preview)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/list */ 6517);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/divider */ 71528);




function Preview_span_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r0.title);
  }
}
function Preview_mat_divider_4_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "mat-divider");
  }
}
const _c0 = ["*"];
class Preview {
  constructor() {
    this.nextElement = true;
    this.fullWidth = false;
    this.oneLine = false;
  }
  static #_ = this.ɵfac = function Preview_Factory(t) {
    return new (t || Preview)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
    type: Preview,
    selectors: [["app-preview"]],
    inputs: {
      title: "title",
      nextElement: "nextElement",
      fullWidth: "fullWidth",
      oneLine: "oneLine"
    },
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵStandaloneFeature"]],
    ngContentSelectors: _c0,
    decls: 5,
    vars: 6,
    consts: [[4, "ngIf"], [1, "demo-preview-content"]],
    template: function Preview_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojectionDef"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, Preview_span_1_Template, 2, 1, "span", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojection"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, Preview_mat_divider_4_Template, 1, 0, "mat-divider", 0);
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("oneLine", ctx.oneLine);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.title);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("fullWidth", ctx.fullWidth);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.nextElement);
      }
    },
    dependencies: [_angular_material_list__WEBPACK_IMPORTED_MODULE_1__.MatListModule, _angular_material_divider__WEBPACK_IMPORTED_MODULE_2__.MatDivider, _angular_common__WEBPACK_IMPORTED_MODULE_3__.NgIf],
    styles: ["section[_ngcontent-%COMP%] {\n  padding: 16px;\n}\nsection.oneLine[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n}\nsection[_ngcontent-%COMP%]   div.demo-preview-content[_ngcontent-%COMP%] {\n  display: flex;\n  flex-wrap: wrap;\n  gap: 8px;\n  padding: 8px;\n}\nsection[_ngcontent-%COMP%]   div.demo-preview-content.fullWidth[_ngcontent-%COMP%] {\n  display: block;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvZmVhdHVyZXMvcHJldmlldy9wcmV2aWV3LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsYUFBQTtBQUNEO0FBQ0M7RUFDQyxhQUFBO0VBQ0EsbUJBQUE7QUFDRjtBQUVDO0VBQ0MsYUFBQTtFQUNBLGVBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtBQUFGO0FBRUU7RUFDQyxjQUFBO0FBQUgiLCJzb3VyY2VzQ29udGVudCI6WyJzZWN0aW9uIHtcclxuXHRwYWRkaW5nOiAxNnB4O1xyXG5cclxuXHQmLm9uZUxpbmV7XHJcblx0XHRkaXNwbGF5OiBmbGV4O1xyXG5cdFx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHR9XHJcblxyXG5cdGRpdi5kZW1vLXByZXZpZXctY29udGVudHtcclxuXHRcdGRpc3BsYXk6IGZsZXg7XHJcblx0XHRmbGV4LXdyYXA6IHdyYXA7XHJcblx0XHRnYXA6IDhweDtcclxuXHRcdHBhZGRpbmc6IDhweDtcclxuXHJcblx0XHQmLmZ1bGxXaWR0aHtcclxuXHRcdFx0ZGlzcGxheTogYmxvY2s7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0= */"]
  });
}

/***/ }),

/***/ 35811:
/*!**********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-button/assets/ButtonBasicDemo/HTML/Asset1.html ***!
  \**********************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"html\" class=\"hljs language-html ngde\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Basic\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-row\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-button</span>></span>Basic<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"primary\"</span>></span>Primary<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"accent\"</span>></span>Accent<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"warn\"</span>></span>Warn<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-button</span> <span class=\"hljs-attr ngde\">disabled</span>></span>Disabled<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">a</span> <span class=\"hljs-attr ngde\">mat-button</span> <span class=\"hljs-attr ngde\">href</span>=<span class=\"hljs-string ngde\">\"https://www.google.com/\"</span> <span class=\"hljs-attr ngde\">target</span>=<span class=\"hljs-string ngde\">\"_blank\"</span>></span>Link<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">a</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Raised\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-row\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-raised-button</span>></span>Basic<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-raised-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"primary\"</span>></span>Primary<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-raised-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"accent\"</span>></span>Accent<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-raised-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"warn\"</span>></span>Warn<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-raised-button</span> <span class=\"hljs-attr ngde\">disabled</span>></span>Disabled<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">a</span> <span class=\"hljs-attr ngde\">mat-raised-button</span> <span class=\"hljs-attr ngde\">href</span>=<span class=\"hljs-string ngde\">\"https://www.google.com/\"</span> <span class=\"hljs-attr ngde\">target</span>=<span class=\"hljs-string ngde\">\"_blank\"</span>></span>Link<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">a</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Stroked\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-row\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-stroked-button</span>></span>Basic<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-stroked-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"primary\"</span>></span>Primary<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-stroked-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"accent\"</span>></span>Accent<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-stroked-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"warn\"</span>></span>Warn<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-stroked-button</span> <span class=\"hljs-attr ngde\">disabled</span>></span>Disabled<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">a</span> <span class=\"hljs-attr ngde\">mat-stroked-button</span> <span class=\"hljs-attr ngde\">href</span>=<span class=\"hljs-string ngde\">\"https://www.google.com/\"</span> <span class=\"hljs-attr ngde\">target</span>=<span class=\"hljs-string ngde\">\"_blank\"</span>\n      ></span>Link&#x3C;/a\n    >\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Flat\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-row\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-flat-button</span>></span>Basic<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-flat-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"primary\"</span>></span>Primary<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-flat-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"accent\"</span>></span>Accent<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-flat-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"warn\"</span>></span>Warn<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-flat-button</span> <span class=\"hljs-attr ngde\">disabled</span>></span>Disabled<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">a</span> <span class=\"hljs-attr ngde\">mat-flat-button</span> <span class=\"hljs-attr ngde\">href</span>=<span class=\"hljs-string ngde\">\"https://www.google.com/\"</span> <span class=\"hljs-attr ngde\">target</span>=<span class=\"hljs-string ngde\">\"_blank\"</span>></span>Link<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">a</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Icon\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-row\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-flex-container\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n        <span class=\"hljs-attr ngde\">mat-icon-button</span>\n        <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon button with a vertical three dot icon\"</span>\n      ></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>more_vert<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n        <span class=\"hljs-attr ngde\">mat-icon-button</span>\n        <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"primary\"</span>\n        <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon button with a home icon\"</span>\n      ></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>home<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n        <span class=\"hljs-attr ngde\">mat-icon-button</span>\n        <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"accent\"</span>\n        <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon button with a menu icon\"</span>\n      ></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>menu<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n        <span class=\"hljs-attr ngde\">mat-icon-button</span>\n        <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"warn\"</span>\n        <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon button with a heart icon\"</span>\n      ></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n        <span class=\"hljs-attr ngde\">mat-icon-button</span>\n        <span class=\"hljs-attr ngde\">disabled</span>\n        <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon button with a open in new tab icon\"</span>\n      ></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>open_in_new<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"FAB\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-row\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-flex-container\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-container\"</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n          <span class=\"hljs-attr ngde\">mat-fab</span>\n          <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"primary\"</span>\n          <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon button with a delete icon\"</span>\n        ></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>delete<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-container\"</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n          <span class=\"hljs-attr ngde\">mat-fab</span>\n          <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"accent\"</span>\n          <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon button with a bookmark icon\"</span>\n        ></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>bookmark<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-container\"</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n          <span class=\"hljs-attr ngde\">mat-fab</span>\n          <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"warn\"</span>\n          <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon button with a home icon\"</span>\n        ></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>home<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-container\"</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n          <span class=\"hljs-attr ngde\">mat-fab</span>\n          <span class=\"hljs-attr ngde\">disabled</span>\n          <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon button with a heart icon\"</span>\n        ></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Mini FAB\"</span> [<span class=\"hljs-attr ngde\">nextElement</span>]=<span class=\"hljs-string ngde\">\"false\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-row\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-flex-container\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-container\"</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n          <span class=\"hljs-attr ngde\">mat-mini-fab</span>\n          <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"primary\"</span>\n          <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon button with a menu icon\"</span>\n        ></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>menu<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-container\"</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n          <span class=\"hljs-attr ngde\">mat-mini-fab</span>\n          <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"accent\"</span>\n          <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon button with a plus one icon\"</span>\n        ></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>plus_one<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-container\"</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n          <span class=\"hljs-attr ngde\">mat-mini-fab</span>\n          <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"warn\"</span>\n          <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon button with a filter list icon\"</span>\n        ></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>filter_list<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-container\"</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n          <span class=\"hljs-attr ngde\">mat-mini-fab</span>\n          <span class=\"hljs-attr ngde\">disabled</span>\n          <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon button with a home icon\"</span>\n        ></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>home<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n</code></pre>");

/***/ }),

/***/ 67065:
/*!**********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-button/assets/ButtonBasicDemo/SCSS/Asset2.html ***!
  \**********************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"scss\" class=\"hljs language-scss ngde\"><span class=\"hljs-selector-tag ngde\">section</span> {\n  <span class=\"hljs-attribute ngde\">display</span>: table;\n}\n\n<span class=\"hljs-selector-class ngde\">.example-button-row</span> {\n  <span class=\"hljs-attribute ngde\">display</span>: table-cell;\n  <span class=\"hljs-attribute ngde\">max-width</span>: <span class=\"hljs-number ngde\">600px</span>;\n}\n\n<span class=\"hljs-selector-class ngde\">.example-button-row</span> <span class=\"hljs-selector-class ngde\">.mat-mdc-button-base</span> {\n  <span class=\"hljs-attribute ngde\">margin</span>: <span class=\"hljs-number ngde\">8px</span> <span class=\"hljs-number ngde\">8px</span> <span class=\"hljs-number ngde\">8px</span> <span class=\"hljs-number ngde\">0</span>;\n}\n\n<span class=\"hljs-selector-class ngde\">.example-flex-container</span> {\n  <span class=\"hljs-attribute ngde\">display</span>: flex;\n  <span class=\"hljs-attribute ngde\">justify-content</span>: space-between;\n  <span class=\"hljs-attribute ngde\">flex-wrap</span>: wrap;\n}\n\n<span class=\"hljs-selector-class ngde\">.example-button-container</span> {\n  <span class=\"hljs-attribute ngde\">display</span>: flex;\n  <span class=\"hljs-attribute ngde\">justify-content</span>: center;\n  <span class=\"hljs-attribute ngde\">width</span>: <span class=\"hljs-number ngde\">120px</span>;\n}\n</code></pre>");

/***/ }),

/***/ 65952:
/*!****************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-button/assets/ButtonBasicDemo/TypeScript/Asset0.html ***!
  \****************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Component</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/core\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">CommonModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/common\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatButtonModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/button\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatDividerModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/divider\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatIconModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/icon\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Preview</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"src/app/features/preview/preview.component\"</span>;\n\n<span class=\"hljs-meta ngde\">@Component</span>({\n  <span class=\"hljs-attr ngde\">selector</span>: <span class=\"hljs-string ngde\">\"app-button-basic-demo\"</span>,\n  <span class=\"hljs-attr ngde\">standalone</span>: <span class=\"hljs-literal ngde\">true</span>,\n  <span class=\"hljs-attr ngde\">imports</span>: [\n    <span class=\"hljs-title class_ ngde\">CommonModule</span>,\n    <span class=\"hljs-title class_ ngde\">MatButtonModule</span>,\n    <span class=\"hljs-title class_ ngde\">MatDividerModule</span>,\n    <span class=\"hljs-title class_ ngde\">MatIconModule</span>,\n    <span class=\"hljs-title class_ ngde\">Preview</span>,\n  ],\n  <span class=\"hljs-attr ngde\">templateUrl</span>: <span class=\"hljs-string ngde\">\"./button-basic-demo.component.html\"</span>,\n  <span class=\"hljs-attr ngde\">styleUrls</span>: [<span class=\"hljs-string ngde\">\"./button-basic-demo.component.scss\"</span>],\n})\n<span class=\"hljs-keyword ngde\">export</span> <span class=\"hljs-keyword ngde\">class</span> <span class=\"hljs-title class_ ngde\">ButtonBasicDemo</span> {}\n</code></pre>");

/***/ }),

/***/ 99865:
/*!**************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-button/assets/ButtonVarietiesDemo/HTML/Asset1.html ***!
  \**************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"html\" class=\"hljs language-html ngde\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Basic Buttons\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-row\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-button</span>></span>Basic<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"primary\"</span>></span>Primary<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"accent\"</span>></span>Accent<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"warn\"</span>></span>Warn<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-button</span> <span class=\"hljs-attr ngde\">disabled</span>></span>Disabled<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">a</span> <span class=\"hljs-attr ngde\">mat-button</span> <span class=\"hljs-attr ngde\">routerLink</span>=<span class=\"hljs-string ngde\">\".\"</span>></span>Link<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">a</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Raised Button\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-row\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-raised-button</span>></span>Basic<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-raised-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"primary\"</span>></span>Primary<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-raised-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"accent\"</span>></span>Accent<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-raised-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"warn\"</span>></span>Warn<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-raised-button</span> <span class=\"hljs-attr ngde\">disabled</span>></span>Disabled<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">a</span> <span class=\"hljs-attr ngde\">mat-raised-button</span> <span class=\"hljs-attr ngde\">routerLink</span>=<span class=\"hljs-string ngde\">\".\"</span>></span>Link<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">a</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Stroked Buttons\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-row\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-stroked-button</span>></span>Basic<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-stroked-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"primary\"</span>></span>Primary<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-stroked-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"accent\"</span>></span>Accent<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-stroked-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"warn\"</span>></span>Warn<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-stroked-button</span> <span class=\"hljs-attr ngde\">disabled</span>></span>Disabled<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">a</span> <span class=\"hljs-attr ngde\">mat-stroked-button</span> <span class=\"hljs-attr ngde\">routerLink</span>=<span class=\"hljs-string ngde\">\".\"</span>></span>Link<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">a</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Flat Buttons\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-row\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-flat-button</span>></span>Basic<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-flat-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"primary\"</span>></span>Primary<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-flat-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"accent\"</span>></span>Accent<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-flat-button</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"warn\"</span>></span>Warn<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-flat-button</span> <span class=\"hljs-attr ngde\">disabled</span>></span>Disabled<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">a</span> <span class=\"hljs-attr ngde\">mat-flat-button</span> <span class=\"hljs-attr ngde\">routerLink</span>=<span class=\"hljs-string ngde\">\".\"</span>></span>Link<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">a</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Icon Buttons\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-row\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n      <span class=\"hljs-attr ngde\">mat-icon-button</span>\n      <span class=\"hljs-attr ngde\">matTooltip</span>=<span class=\"hljs-string ngde\">\"Basic\"</span>\n      <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon-button with a heart icon\"</span>\n    ></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n      <span class=\"hljs-attr ngde\">mat-icon-button</span>\n      <span class=\"hljs-attr ngde\">matTooltip</span>=<span class=\"hljs-string ngde\">\"Primary\"</span>\n      <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"primary\"</span>\n      <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon-button with a heart icon\"</span>\n    ></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n      <span class=\"hljs-attr ngde\">mat-icon-button</span>\n      <span class=\"hljs-attr ngde\">matTooltip</span>=<span class=\"hljs-string ngde\">\"Accent\"</span>\n      <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"accent\"</span>\n      <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon-button with a heart icon\"</span>\n    ></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n      <span class=\"hljs-attr ngde\">mat-icon-button</span>\n      <span class=\"hljs-attr ngde\">matTooltip</span>=<span class=\"hljs-string ngde\">\"Warn\"</span>\n      <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"warn\"</span>\n      <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon-button with a heart icon\"</span>\n    ></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">matTooltip</span>=<span class=\"hljs-string ngde\">\"Disabled\"</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-disabled\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n        <span class=\"hljs-attr ngde\">mat-icon-button</span>\n        <span class=\"hljs-attr ngde\">disabled</span>\n        <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon-button with a heart icon\"</span>\n      ></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">a</span>\n      <span class=\"hljs-attr ngde\">mat-icon-button</span>\n      <span class=\"hljs-attr ngde\">matTooltip</span>=<span class=\"hljs-string ngde\">\"Link\"</span>\n      <span class=\"hljs-attr ngde\">routerLink</span>=<span class=\"hljs-string ngde\">\".\"</span>\n      <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example icon-button with a heart icon\"</span>\n    ></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">a</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Fab Buttons\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-row\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n      <span class=\"hljs-attr ngde\">mat-fab</span>\n      <span class=\"hljs-attr ngde\">matTooltip</span>=<span class=\"hljs-string ngde\">\"Primary\"</span>\n      <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"primary\"</span>\n      <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example fab with a heart icon\"</span>\n    ></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n      <span class=\"hljs-attr ngde\">mat-fab</span>\n      <span class=\"hljs-attr ngde\">matTooltip</span>=<span class=\"hljs-string ngde\">\"Accent\"</span>\n      <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"accent\"</span>\n      <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example fab with a heart icon\"</span>\n    ></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n      <span class=\"hljs-attr ngde\">mat-fab</span>\n      <span class=\"hljs-attr ngde\">matTooltip</span>=<span class=\"hljs-string ngde\">\"Warn\"</span>\n      <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"warn\"</span>\n      <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example fab with a heart icon\"</span>\n    ></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">matTooltip</span>=<span class=\"hljs-string ngde\">\"Disabled\"</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-disabled\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-fab</span> <span class=\"hljs-attr ngde\">disabled</span> <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example fab with a heart icon\"</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">a</span>\n      <span class=\"hljs-attr ngde\">mat-fab</span>\n      <span class=\"hljs-attr ngde\">matTooltip</span>=<span class=\"hljs-string ngde\">\"Link\"</span>\n      <span class=\"hljs-attr ngde\">routerLink</span>=<span class=\"hljs-string ngde\">\".\"</span>\n      <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example fab with a heart icon\"</span>\n    ></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">a</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Mini Fab Buttons\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-row\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n      <span class=\"hljs-attr ngde\">mat-mini-fab</span>\n      <span class=\"hljs-attr ngde\">matTooltip</span>=<span class=\"hljs-string ngde\">\"Primary\"</span>\n      <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"primary\"</span>\n      <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example mini fab with a heart icon\"</span>\n    ></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n      <span class=\"hljs-attr ngde\">mat-mini-fab</span>\n      <span class=\"hljs-attr ngde\">matTooltip</span>=<span class=\"hljs-string ngde\">\"Accent\"</span>\n      <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"accent\"</span>\n      <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example mini fab with a heart icon\"</span>\n    ></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n      <span class=\"hljs-attr ngde\">mat-mini-fab</span>\n      <span class=\"hljs-attr ngde\">matTooltip</span>=<span class=\"hljs-string ngde\">\"Warn\"</span>\n      <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"warn\"</span>\n      <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example mini fab with a heart icon\"</span>\n    ></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">matTooltip</span>=<span class=\"hljs-string ngde\">\"Disabled\"</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-disabled\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span>\n        <span class=\"hljs-attr ngde\">mat-mini-fab</span>\n        <span class=\"hljs-attr ngde\">disabled</span>\n        <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example mini fab with a heart icon\"</span>\n      ></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">a</span>\n      <span class=\"hljs-attr ngde\">mat-mini-fab</span>\n      <span class=\"hljs-attr ngde\">matTooltip</span>=<span class=\"hljs-string ngde\">\"Link\"</span>\n      <span class=\"hljs-attr ngde\">routerLink</span>=<span class=\"hljs-string ngde\">\".\"</span>\n      <span class=\"hljs-attr ngde\">aria-label</span>=<span class=\"hljs-string ngde\">\"Example mini fab with a heart icon\"</span>\n    ></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">a</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Extended Fab\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-row\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-fab</span> <span class=\"hljs-attr ngde\">extended</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"primary\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      Primary\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-fab</span> <span class=\"hljs-attr ngde\">extended</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"accent\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      Accent\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-fab</span> <span class=\"hljs-attr ngde\">extended</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"warn\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      Warn\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-fab</span> <span class=\"hljs-attr ngde\">extended</span> <span class=\"hljs-attr ngde\">disabled</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      Disabled\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">a</span> <span class=\"hljs-attr ngde\">mat-fab</span> <span class=\"hljs-attr ngde\">extended</span> <span class=\"hljs-attr ngde\">routerLink</span>=<span class=\"hljs-string ngde\">\".\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      Link\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">a</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Reverse Icon: [iconPositionEnd]\"</span> [<span class=\"hljs-attr ngde\">nextElement</span>]=<span class=\"hljs-string ngde\">\"false\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-button-row\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-fab</span> <span class=\"hljs-attr ngde\">extended</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"primary\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span> <span class=\"hljs-attr ngde\">iconPositionEnd</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      Primary\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-fab</span> <span class=\"hljs-attr ngde\">extended</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"accent\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span> <span class=\"hljs-attr ngde\">iconPositionEnd</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      Accent\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-fab</span> <span class=\"hljs-attr ngde\">extended</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"warn\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span> <span class=\"hljs-attr ngde\">iconPositionEnd</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      Warn\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">button</span> <span class=\"hljs-attr ngde\">mat-fab</span> <span class=\"hljs-attr ngde\">extended</span> <span class=\"hljs-attr ngde\">disabled</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span> <span class=\"hljs-attr ngde\">iconPositionEnd</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      Disabled\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">button</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">a</span> <span class=\"hljs-attr ngde\">mat-fab</span> <span class=\"hljs-attr ngde\">extended</span> <span class=\"hljs-attr ngde\">routerLink</span>=<span class=\"hljs-string ngde\">\".\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-icon</span> <span class=\"hljs-attr ngde\">iconPositionEnd</span>></span>favorite<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-icon</span>></span>\n      Link\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">a</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n</code></pre>");

/***/ }),

/***/ 80429:
/*!**************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-button/assets/ButtonVarietiesDemo/SCSS/Asset2.html ***!
  \**************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"scss\" class=\"hljs language-scss ngde\"><span class=\"hljs-selector-class ngde\">.example-button-row</span> <span class=\"hljs-selector-tag ngde\">button</span>,\n<span class=\"hljs-selector-class ngde\">.example-button-row</span> <span class=\"hljs-selector-tag ngde\">a</span> {\n  <span class=\"hljs-attribute ngde\">margin-right</span>: <span class=\"hljs-number ngde\">8px</span>;\n}\n\n<span class=\"hljs-selector-class ngde\">.example-disabled</span> {\n  <span class=\"hljs-attribute ngde\">display</span>: inline-block;\n}\n</code></pre>");

/***/ }),

/***/ 2318:
/*!********************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-button/assets/ButtonVarietiesDemo/TypeScript/Asset0.html ***!
  \********************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">CommonModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/common\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Component</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/core\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatButtonModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/button\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatIconModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/icon\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatTooltipModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/tooltip\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">RouterLink</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/router\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Preview</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"src/app/features/preview/preview.component\"</span>;\n\n<span class=\"hljs-meta ngde\">@Component</span>({\n  <span class=\"hljs-attr ngde\">selector</span>: <span class=\"hljs-string ngde\">\"app-button-varieties-demo\"</span>,\n  <span class=\"hljs-attr ngde\">standalone</span>: <span class=\"hljs-literal ngde\">true</span>,\n  <span class=\"hljs-attr ngde\">imports</span>: [\n    <span class=\"hljs-title class_ ngde\">CommonModule</span>,\n    <span class=\"hljs-title class_ ngde\">Preview</span>,\n    <span class=\"hljs-title class_ ngde\">MatButtonModule</span>,\n    <span class=\"hljs-title class_ ngde\">RouterLink</span>,\n    <span class=\"hljs-title class_ ngde\">MatTooltipModule</span>,\n    <span class=\"hljs-title class_ ngde\">MatIconModule</span>,\n  ],\n  <span class=\"hljs-attr ngde\">templateUrl</span>: <span class=\"hljs-string ngde\">\"./button-varieties-demo.component.html\"</span>,\n  <span class=\"hljs-attr ngde\">styleUrls</span>: [<span class=\"hljs-string ngde\">\"./button-varieties-demo.component.scss\"</span>],\n})\n<span class=\"hljs-keyword ngde\">export</span> <span class=\"hljs-keyword ngde\">class</span> <span class=\"hljs-title class_ ngde\">ButtonVarietiesDemo</span> {}\n</code></pre>");

/***/ }),

/***/ 85645:
/*!*****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-button/index.html ***!
  \*****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<h1 id=\"button\" class=\"ngde\">Button<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/angular-materiel/material-button#button\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1><h2 id=\"basic-button\" class=\"ngde\">Basic Button<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/angular-materiel/material-button#basic-button\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><ng-doc-demo componentname=\"ButtonBasicDemo\" indexable=\"false\" class=\"ngde\"><div id=\"options\" class=\"ngde\">{}</div></ng-doc-demo><h2 id=\"varieties-buttons\" class=\"ngde\">Varieties Buttons<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/angular-materiel/material-button#varieties-buttons\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><ng-doc-demo componentname=\"ButtonVarietiesDemo\" indexable=\"false\" class=\"ngde\"><div id=\"options\" class=\"ngde\">{}</div></ng-doc-demo>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_guides_angular-materiel_material-button_module_ts.js.map