"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_guides_getting-started_usage_module_ts"],{

/***/ 35546:
/*!***************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/getting-started/usage/module.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_getting_started_usage_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/getting-started/usage/index.html */ 25556);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);







class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = '';
    this.pageType = 'guide';
    this.pageContent = _raw_loader_ng_doc_m_docs_guides_getting_started_usage_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'Usage'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
})();

/***/ }),

/***/ 25556:
/*!******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/getting-started/usage/index.html ***!
  \******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<h1 id=\"usage\" class=\"ngde\">Usage<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/usage#usage\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1><h2 id=\"m-theme\" class=\"ngde\">M-Theme<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/usage#m-theme\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><p class=\"ngde\">To enable light or dark themes, add the <code class=\"ngde\">dark-theme</code> or <code class=\"ngde\">light-theme</code> class to the <code class=\"ngde\">&#x3C;body></code> tag in your <code class=\"ngde\">index.html</code>:</p><pre class=\"ngde hljs\"><code class=\"hljs language-html ngde\" lang=\"html\" filename=\"\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">body</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"dark-theme\"</span>></span>\n    <span class=\"hljs-comment ngde\">&#x3C;!-- Your app content --></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">body</span>></span></code></pre><p class=\"ngde\">This theme provide custom palettes and provides the following variables:</p><ul class=\"ngde\"><li class=\"ngde\"><code class=\"ngde\">$light-theme-background</code></li><li class=\"ngde\"><code class=\"ngde\">$light-theme-foreground</code></li><li class=\"ngde\"><code class=\"ngde\">$dark-theme-background</code></li><li class=\"ngde\"><code class=\"ngde\">$dark-theme-foreground</code></li></ul><p class=\"ngde\">Example:</p><pre class=\"ngde hljs\"><code class=\"hljs language-scss ngde\" lang=\"scss\" filename=\"\"><span class=\"hljs-selector-class ngde\">.my-container</span> {\n  <span class=\"hljs-attribute ngde\">color</span>: <span class=\"hljs-built_in ngde\">map-get</span>(<span class=\"hljs-variable ngde\">$dark-theme-foreground</span>, text);\n}</code></pre><p class=\"ngde\">Some helper functions are also provided:</p><ul class=\"ngde\"><li class=\"ngde\"><code class=\"ngde\">dark-color-overlay($elevation)</code></li><li class=\"ngde\"><code class=\"ngde\">light-color-overlay($elevation)</code></li></ul><p class=\"ngde\"><code class=\"ngde\">$elevation</code> must be a value in <code class=\"ngde\">[0, 1, 2, 3, 4, 6, 8, 12, 16, 24]</code></p><p class=\"ngde\">Example:</p><pre class=\"ngde hljs\"><code class=\"hljs language-scss ngde\" lang=\"scss\" filename=\"\"><span class=\"hljs-selector-class ngde\">.my-container</span> {\n    <span class=\"hljs-attribute ngde\">background</span>: <span class=\"hljs-built_in ngde\">dark-color-overlay</span>(<span class=\"hljs-number ngde\">16</span>);\n}</code></pre><hr class=\"ngde\"><h4 id=\"helper-functions-and-variables\" class=\"ngde\">Helper Functions and Variables<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/usage#helper-functions-and-variables\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h4><h5 class=\"ngde\">Variables</h5><p class=\"ngde\">M-Theme provides pre-defined variables for customizing the look and feel of your app:</p><ul class=\"ngde\"><li class=\"ngde\"><code class=\"ngde\">$light-theme-background</code></li><li class=\"ngde\"><code class=\"ngde\">$light-theme-foreground</code></li><li class=\"ngde\"><code class=\"ngde\">$dark-theme-background</code></li><li class=\"ngde\"><code class=\"ngde\">$dark-theme-foreground</code></li></ul><p class=\"ngde\">Example:</p><pre class=\"ngde hljs\"><code class=\"hljs language-scss ngde\" lang=\"scss\" filename=\"\"><span class=\"hljs-selector-class ngde\">.my-container</span> {\n  <span class=\"hljs-attribute ngde\">color</span>: <span class=\"hljs-built_in ngde\">map-get</span>(<span class=\"hljs-variable ngde\">$dark-theme-foreground</span>, text);\n}</code></pre><hr class=\"ngde\"><h5 class=\"ngde\">Helper Functions</h5><p class=\"ngde\">Use helper functions for overlays and dynamic styles:</p><ul class=\"ngde\"><li class=\"ngde\"><code class=\"ngde\">dark-color-overlay($elevation)</code></li><li class=\"ngde\"><code class=\"ngde\">light-color-overlay($elevation)</code></li></ul><ng-doc-blockquote type=\"note\" class=\"ngde\"><p class=\"ngde\">$elevation must be one of [0, 1, 2, 3, 4, 6, 8, 12, 16, 24].</p></ng-doc-blockquote><p class=\"ngde\">Example:</p><pre class=\"ngde hljs\"><code class=\"hljs language-scss ngde\" lang=\"scss\" filename=\"\"><span class=\"hljs-selector-class ngde\">.my-container</span> {\n  <span class=\"hljs-attribute ngde\">background</span>: <span class=\"hljs-built_in ngde\">dark-color-overlay</span>(<span class=\"hljs-number ngde\">16</span>);\n}</code></pre><h4 id=\"testing-the-installation\" class=\"ngde\">Testing the Installation<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/usage#testing-the-installation\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h4><ol class=\"ngde\"><li class=\"ngde\">Ensure all configurations in angular.json are correctly applied.</li><li class=\"ngde\">Use the imported SCSS variables and functions in your components to verify that they work as expected.</li><li class=\"ngde\">If icons or assets aren't loading, double-check the assets array in angular.json.</li></ol><hr class=\"ngde\"><h2 id=\"m-ui\" class=\"ngde\">M-ui<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/usage#m-ui\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><p class=\"ngde\">All library components are prefixed with <code class=\"ngde\">m-</code> and are designed as standalone components.</p><pre class=\"ngde hljs\"><code class=\"hljs language-html ngde\" lang=\"html\" filename=\"\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">m-icons</span> <span class=\"hljs-attr ngde\">type</span>=<span class=\"hljs-string ngde\">\"accident\"</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"m-icon\"</span>></span><span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">m-icons</span>></span></code></pre><h3 id=\"default-import\" class=\"ngde\">Default import<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/usage#default-import\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h3><p class=\"ngde\">We can import the entire library in our Angular module.</p><pre class=\"ngde hljs\"><code class=\"hljs language-typescript ngde\" lang=\"typescript\" filename=\"\"><span class=\"hljs-keyword ngde\">import</span> {<span class=\"hljs-title class_ ngde\"><a href=\"/api/m-ui/classes/MIcons\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MIcons</a></span>} <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">'@metromobilite/m-ui'</span>;\n\n<span class=\"hljs-meta ngde\">@NgModule</span>({\n  <span class=\"hljs-attr ngde\">declarations</span>: [<span class=\"hljs-title class_ ngde\">AppComponent</span>], \n  <span class=\"hljs-attr ngde\">imports</span>: [<span class=\"hljs-title class_ ngde\"><a href=\"/api/m-ui/classes/MIcons\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MIcons</a></span>], \n  <span class=\"hljs-attr ngde\">bootstrap</span>: [<span class=\"hljs-title class_ ngde\">AppComponent</span>]\n})\n<span class=\"hljs-keyword ngde\">export</span> <span class=\"hljs-keyword ngde\">class</span> <span class=\"hljs-title class_ ngde\">AppModule</span> {}</code></pre><h3 id=\"import-unitary-components\" class=\"ngde\">Import unitary components<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/usage#import-unitary-components\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h3><p class=\"ngde\">We can also import the components individually.</p><pre class=\"ngde hljs\"><code class=\"hljs language-typescript ngde\" lang=\"typescript\" filename=\"\"><span class=\"hljs-keyword ngde\">import</span> {<span class=\"hljs-title class_ ngde\"><a href=\"/api/m-ui/classes/MIcons\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MIcons</a></span>} <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">'@metromobilite/m-ui/m-icons'</span>;</code></pre><ng-doc-blockquote type=\"note\" class=\"ngde\"><p class=\"ngde\">Add this in your tsconfig.json</p><pre class=\"ngde hljs\"><code class=\"hljs language-json ngde\" lang=\"json\" filename=\"\"><span class=\"hljs-attr ngde\">\"compilerOptions\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-punctuation ngde\">{</span>\n  <span class=\"hljs-attr ngde\">\"paths\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-punctuation ngde\">{</span>\n    <span class=\"hljs-attr ngde\">\"@metromobilite/m-ui/*\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-punctuation ngde\">[</span>\n      <span class=\"hljs-string ngde\">\"node_modules/@metromobilite/m-ui\"</span><span class=\"hljs-punctuation ngde\">,</span>\n      <span class=\"hljs-string ngde\">\"node_modules/@metromobilite/m-ui/lib/*\"</span>\n    <span class=\"hljs-punctuation ngde\">]</span>\n  <span class=\"hljs-punctuation ngde\">}</span> \n<span class=\"hljs-punctuation ngde\">}</span></code></pre></ng-doc-blockquote>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_guides_getting-started_usage_module_ts.js.map