"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_guides_getting-started_install_module_ts"],{

/***/ 44540:
/*!*****************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/getting-started/install/module.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_getting_started_install_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/getting-started/install/index.html */ 11414);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);







class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = '';
    this.pageType = 'guide';
    this.pageContent = _raw_loader_ng_doc_m_docs_guides_getting_started_install_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'Install'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
})();

/***/ }),

/***/ 11414:
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/getting-started/install/index.html ***!
  \********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<h1 id=\"install\" class=\"ngde\">Install<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/install#install\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1><h2 id=\"installing-the-library\" class=\"ngde\">Installing the Library<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/install#installing-the-library\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><ol class=\"ngde\"><li class=\"ngde\">Install via npm</li></ol><p class=\"ngde\">Run the following command to install the latest version of the library:</p><pre class=\"ngde hljs\"><code class=\"hljs language-bash ngde\" lang=\"bash\" filename=\"\">npm i @metromobilite/m-ui@latest</code></pre><h2 id=\"configuration\" class=\"ngde\">Configuration<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/install#configuration\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><h3 id=\"m-theme-setup\" class=\"ngde\">M-Theme Setup<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/install#m-theme-setup\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h3><h4 id=\"step-1-add-stylepreprocessoroptions-in-angularjson\" class=\"ngde\">Step 1: Add stylePreprocessorOptions in angular.json<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/install#step-1-add-stylepreprocessoroptions-in-angularjson\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h4><ng-doc-blockquote type=\"note\" class=\"ngde\"><p class=\"ngde\">The <code class=\"ngde\">stylePreprocessorOptions</code> in <code class=\"ngde\">angular.json</code> allows you to define custom paths for Sass to search for files during <code class=\"ngde\">@use</code> or <code class=\"ngde\">@import</code>.</p><p class=\"ngde\">Adding 'includePaths': ['node_modules'] lets Sass resolve imports from node_modules directly, enabling simplified imports for libraries.</p></ng-doc-blockquote><pre class=\"ngde hljs\"><code class=\"hljs language-json ngde\" lang=\"json\" filename=\"\"><span class=\"hljs-punctuation ngde\">{</span>\n    <span class=\"hljs-attr ngde\">\"projects\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-punctuation ngde\">{</span>\n        <span class=\"hljs-attr ngde\">\"project-name\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-punctuation ngde\">{</span>\n            <span class=\"hljs-attr ngde\">\"architect\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-punctuation ngde\">{</span>\n                <span class=\"hljs-attr ngde\">\"build\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-punctuation ngde\">{</span>\n                    <span class=\"hljs-attr ngde\">\"options\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-punctuation ngde\">{</span>\n                        <span class=\"hljs-attr ngde\">\"stylePreprocessorOptions\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-punctuation ngde\">{</span>\n                            <span class=\"hljs-attr ngde\">\"includePaths\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-punctuation ngde\">[</span><span class=\"hljs-string ngde\">\"node_modules\"</span><span class=\"hljs-punctuation ngde\">]</span>\n                        <span class=\"hljs-punctuation ngde\">}</span>\n                    <span class=\"hljs-punctuation ngde\">}</span>\n                <span class=\"hljs-punctuation ngde\">}</span>\n            <span class=\"hljs-punctuation ngde\">}</span>\n        <span class=\"hljs-punctuation ngde\">}</span>\n    <span class=\"hljs-punctuation ngde\">}</span>\t\n<span class=\"hljs-punctuation ngde\">}</span></code></pre><hr class=\"ngde\"><h4 id=\"step-2-add-assets-in-angularjson\" class=\"ngde\">Step 2: Add Assets in angular.json<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/install#step-2-add-assets-in-angularjson\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h4><p class=\"ngde\">To ensure that theme-related assets are properly loaded, include the following in the assets array of <code class=\"ngde\">angular.json</code>:</p><pre class=\"ngde hljs\"><code class=\"hljs language-json ngde\" lang=\"json\" filename=\"\"><span class=\"hljs-punctuation ngde\">{</span>\n    <span class=\"hljs-attr ngde\">\"assets\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-punctuation ngde\">[</span>\n        <span class=\"hljs-punctuation ngde\">{</span>\n            <span class=\"hljs-attr ngde\">\"glob\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-string ngde\">\"**/*\"</span><span class=\"hljs-punctuation ngde\">,</span>\n            <span class=\"hljs-attr ngde\">\"input\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-string ngde\">\"./node_modules/@metromobilite/m-ui/theme/assets/\"</span><span class=\"hljs-punctuation ngde\">,</span>\n            <span class=\"hljs-attr ngde\">\"output\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-string ngde\">\"./assets/\"</span>\n        <span class=\"hljs-punctuation ngde\">}</span>\n    <span class=\"hljs-punctuation ngde\">]</span>\t\n<span class=\"hljs-punctuation ngde\">}</span></code></pre><hr class=\"ngde\"><h4 id=\"step-3-import-styles-in-stylesscss\" class=\"ngde\">Step 3: Import Styles in styles.scss<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/install#step-3-import-styles-in-stylesscss\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h4><p class=\"ngde\">In your main SCSS file (e.g., src/styles.scss), import the M-Theme styles. You can choose to import all styles or only specific ones.</p><h5 class=\"ngde\">Option 1: Import All Styles</h5><pre class=\"ngde hljs\"><code class=\"hljs language-scss ngde\" lang=\"scss\" filename=\"\"><span class=\"hljs-variable ngde\">$assets-path</span>: <span class=\"hljs-string ngde\">'/assets'</span>; <span class=\"hljs-comment ngde\">// Define the assets path</span>\n<span class=\"hljs-keyword ngde\">@use</span> <span class=\"hljs-string ngde\">\"@metromobilite/m-ui/theme/style\"</span>;</code></pre><h5 class=\"ngde\">Option 2: Import Specific Files</h5><p class=\"ngde\">If you need more control over the imported styles, you can import them one by one:</p><pre class=\"ngde hljs\"><code class=\"hljs language-scss ngde\" lang=\"scss\" filename=\"\"><span class=\"hljs-keyword ngde\">@import</span> <span class=\"hljs-string ngde\">'reset'</span>;\n<span class=\"hljs-keyword ngde\">@import</span> <span class=\"hljs-string ngde\">'variables'</span>;\n<span class=\"hljs-keyword ngde\">@import</span> <span class=\"hljs-string ngde\">'font'</span>;\n<span class=\"hljs-keyword ngde\">@import</span> <span class=\"hljs-string ngde\">'@metromobilite/m-ui/theme/theme'</span>;\n<span class=\"hljs-keyword ngde\">@import</span> <span class=\"hljs-string ngde\">'classes'</span>;\n<span class=\"hljs-keyword ngde\">@import</span> <span class=\"hljs-string ngde\">'components/index'</span>;</code></pre><hr class=\"ngde\"><h3 id=\"m-ui-setup\" class=\"ngde\">M-UI Setup<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/install#m-ui-setup\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h3><h4 id=\"add-icon-assets-in-angularjson\" class=\"ngde\">Add Icon Assets in <code class=\"ngde\">angular.json</code><a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/install#add-icon-assets-in-angularjson\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h4><p class=\"ngde\">To use the icon library provided by M-UI, include the following configuration in <code class=\"ngde\">angular.json</code> under the assets array:</p><pre class=\"ngde hljs\"><code class=\"hljs language-json ngde\" lang=\"json\" filename=\"\"><span class=\"hljs-punctuation ngde\">{</span>\n    <span class=\"hljs-attr ngde\">\"assets\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-punctuation ngde\">[</span>\n        <span class=\"hljs-punctuation ngde\">{</span>\n            <span class=\"hljs-attr ngde\">\"glob\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-string ngde\">\"**/*\"</span><span class=\"hljs-punctuation ngde\">,</span>\n            <span class=\"hljs-attr ngde\">\"input\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-string ngde\">\"node_modules/@metromobilite/m-ui/src/assets\"</span><span class=\"hljs-punctuation ngde\">,</span>\n            <span class=\"hljs-attr ngde\">\"output\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-string ngde\">\"/assets/\"</span>\n        <span class=\"hljs-punctuation ngde\">}</span>\n    <span class=\"hljs-punctuation ngde\">]</span>\t\n<span class=\"hljs-punctuation ngde\">}</span></code></pre>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_guides_getting-started_install_module_ts.js.map