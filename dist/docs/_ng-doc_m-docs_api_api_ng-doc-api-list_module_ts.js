"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_api_api_ng-doc-api-list_module_ts"],{

/***/ 4330:
/*!**********************************************************!*\
  !*** ./.ng-doc/m-docs/api/api/ng-doc-api-list.module.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-doc/app */ 64314);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-doc/app */ 69357);
/* harmony import */ var _ng_doc_api_list_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ng-doc.api-list.json */ 15523);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);







class DynamicComponent {
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_2__.NG_DOC_API_LIST_TOKEN,
      useValue: _ng_doc_api_list_json__WEBPACK_IMPORTED_MODULE_0__
    }])],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](0, "ng-doc-api-list");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocApiListComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocApiListModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent
    }, {
      path: 'm-ui',
      loadChildren: () => __webpack_require__.e(/*! import() */ "_ng-doc_m-docs_api_api_m-ui_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/module */ 58509)).then(m => m.DynamicModule)
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocApiListModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
})();

/***/ }),

/***/ 69357:
/*!******************************************************************************!*\
  !*** ./node_modules/@ng-doc/app/fesm2020/ng-doc-app-components-api-list.mjs ***!
  \******************************************************************************/
/***/ ((__unused_webpack___webpack_module__, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NgDocApiListComponent": () => (/* binding */ NgDocApiListComponent),
/* harmony export */   "NgDocApiListModule": () => (/* binding */ NgDocApiListModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! tslib */ 42321);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ng_doc_app_tokens__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-doc/app/tokens */ 64314);
/* harmony import */ var _ng_doc_core_helpers_as_array__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-doc/core/helpers/as-array */ 39335);
/* harmony import */ var _ng_doc_ui_kit_decorators__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @ng-doc/ui-kit/decorators */ 44406);
/* harmony import */ var _ngneat_until_destroy__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ngneat/until-destroy */ 82777);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 71989);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ 44874);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ 50635);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ng-doc/ui-kit */ 81986);
/* harmony import */ var _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ng-doc/ui-kit */ 1124);
/* harmony import */ var _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ng-doc/ui-kit */ 13024);
/* harmony import */ var _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ng-doc/ui-kit */ 72092);
/* harmony import */ var _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ng-doc/ui-kit */ 74797);
/* harmony import */ var _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ng-doc/ui-kit */ 51271);
/* harmony import */ var _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @ng-doc/ui-kit */ 49565);
/* harmony import */ var _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ng-doc/ui-kit */ 78337);
/* harmony import */ var _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @ng-doc/ui-kit */ 63302);
/* harmony import */ var _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @ng-doc/ui-kit */ 28717);
/* harmony import */ var _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @ng-doc/ui-kit */ 42150);
/* harmony import */ var _ng_doc_app_components_kind_icon__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ng-doc/app/components/kind-icon */ 56247);


















function NgDocApiListComponent_div_2_ng_template_4_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "ng-doc-icon", 15);
  }
}
function NgDocApiListComponent_div_2_label_7_ng_doc_list_2_ng_doc_option_1_ng_container_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](0);
  }
}
const _c0 = function (a0) {
  return {
    $implicit: a0
  };
};
function NgDocApiListComponent_div_2_label_7_ng_doc_list_2_ng_doc_option_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ng-doc-option", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, NgDocApiListComponent_div_2_label_7_ng_doc_list_2_ng_doc_option_1_ng_container_1_Template, 1, 0, "ng-container", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const scope_r13 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    const _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", scope_r13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngTemplateOutlet", _r10)("ngTemplateOutletContext", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](3, _c0, scope_r13));
  }
}
function NgDocApiListComponent_div_2_label_7_ng_doc_list_2_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ng-doc-list");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, NgDocApiListComponent_div_2_label_7_ng_doc_list_2_ng_doc_option_1_Template, 2, 5, "ng-doc-option", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r9.scopes);
  }
}
function NgDocApiListComponent_div_2_label_7_ng_template_3_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](0);
  }
  if (rf & 2) {
    const value_r15 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", value_r15, " ");
  }
}
function NgDocApiListComponent_div_2_label_7_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "label", 16)(1, "ng-doc-combobox", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, NgDocApiListComponent_div_2_label_7_ng_doc_list_2_Template, 2, 1, "ng-doc-list", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, NgDocApiListComponent_div_2_label_7_ng_template_3_Template, 1, 1, "ng-template", null, 18, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
  }
  if (rf & 2) {
    const _r10 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](4);
    const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formControl", ctx_r4.formGroup.controls.scope)("valueContent", _r10)("readonly", true);
  }
}
function NgDocApiListComponent_div_2_ng_doc_list_10_ng_doc_option_1_ng_container_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainer"](0);
  }
}
function NgDocApiListComponent_div_2_ng_doc_list_10_ng_doc_option_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ng-doc-option", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, NgDocApiListComponent_div_2_ng_doc_list_10_ng_doc_option_1_ng_container_1_Template, 1, 0, "ng-container", 21);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const type_r17 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("value", type_r17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngTemplateOutlet", _r6)("ngTemplateOutletContext", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](3, _c0, type_r17));
  }
}
function NgDocApiListComponent_div_2_ng_doc_list_10_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ng-doc-list");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, NgDocApiListComponent_div_2_ng_doc_list_10_ng_doc_option_1_Template, 2, 5, "ng-doc-option", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r5.types);
  }
}
const _c1 = function () {
  return ["left-center", "right-center"];
};
function NgDocApiListComponent_div_2_ng_template_11_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "ng-doc-kind-icon", 22);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const value_r19 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("kind", value_r19)("ngDocTooltip", value_r19)("positions", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](4, _c1));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", value_r19, " ");
  }
}
const _c2 = function (a0) {
  return [a0];
};
function NgDocApiListComponent_div_2_div_14_ul_3_li_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li", 28)(1, "a", 29);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](2, "ng-doc-kind-icon", 30);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
  }
  if (rf & 2) {
    const apiReference_r23 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("routerLink", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction1"](4, _c2, apiReference_r23.route));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("kind", apiReference_r23.type)("ngDocTooltip", apiReference_r23.type);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", apiReference_r23.name, " ");
  }
}
function NgDocApiListComponent_div_2_div_14_ul_3_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ul", 26);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, NgDocApiListComponent_div_2_div_14_ul_3_li_1_Template, 4, 6, "li", 27);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const scope_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", scope_r20.items);
  }
}
function NgDocApiListComponent_div_2_div_14_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 23)(1, "h3", 24);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, NgDocApiListComponent_div_2_div_14_ul_3_Template, 2, 1, "ul", 25);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const scope_r20 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](scope_r20.title);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", scope_r20.items.length);
  }
}
function NgDocApiListComponent_div_2_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 2)(1, "form", 3)(2, "label", 4)(3, "ng-doc-input-wrapper", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, NgDocApiListComponent_div_2_ng_template_4_Template, 1, 0, "ng-template", null, 6, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](6, "input", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, NgDocApiListComponent_div_2_label_7_Template, 5, 3, "label", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "label", 9)(9, "ng-doc-combobox", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, NgDocApiListComponent_div_2_ng_doc_list_10_Template, 2, 1, "ng-doc-list", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, NgDocApiListComponent_div_2_ng_template_11_Template, 3, 5, "ng-template", null, 12, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()()();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](14, NgDocApiListComponent_div_2_div_14_Template, 4, 2, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
  }
  if (rf & 2) {
    const api_r1 = ctx.ngIf;
    const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](5);
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](12);
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formGroup", ctx_r0.formGroup);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("leftContent", _r2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formControl", ctx_r0.formGroup.controls.filter);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx_r0.scopes.length);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("formControl", ctx_r0.formGroup.controls.type)("valueContent", _r6)("readonly", true);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", api_r1);
  }
}
let NgDocApiListComponent = class NgDocApiListComponent {
  constructor(apiList, formBuilder, route, router) {
    this.apiList = apiList;
    this.formBuilder = formBuilder;
    this.route = route;
    this.router = router;
    this.formGroup = this.formBuilder.group({
      filter: [''],
      scope: [''],
      type: ['']
    });
    this.route.queryParamMap.pipe((0,_ngneat_until_destroy__WEBPACK_IMPORTED_MODULE_1__.untilDestroyed)(this)).subscribe(paramMap => this.formGroup.setValue({
      filter: paramMap.get('filter') || null,
      scope: paramMap.get('scope') || null,
      type: paramMap.get('type') || null
    }));
    this.formGroup.valueChanges.pipe((0,_ngneat_until_destroy__WEBPACK_IMPORTED_MODULE_1__.untilDestroyed)(this)).subscribe(formValue => this.router.navigate([], {
      relativeTo: this.route,
      queryParams: formValue,
      queryParamsHandling: 'merge'
    }));
    this.api$ = this.formGroup.valueChanges.pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.debounceTime)(100), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_3__.startWith)(null), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.map)(() => this.formGroup.value), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_4__.map)(form => this.apiList.filter(api => !form?.scope || api.title === form?.scope).map(api => ({
      ...api,
      items: api.items.filter(item => item.name.toLowerCase().includes(form?.filter?.toLowerCase() ?? '') && (!form?.type || item.type === form?.type)).sort((a, b) => a.type.localeCompare(b.type) || a.name.localeCompare(b.name))
    })).filter(api => api.items.length)), (0,_ngneat_until_destroy__WEBPACK_IMPORTED_MODULE_1__.untilDestroyed)(this));
  }
  get scopes() {
    return (0,_ng_doc_core_helpers_as_array__WEBPACK_IMPORTED_MODULE_5__.asArray)(new Set(this.apiList.flatMap(api => api.title))).sort();
  }
  get types() {
    return (0,_ng_doc_core_helpers_as_array__WEBPACK_IMPORTED_MODULE_5__.asArray)(new Set(this.apiList.flatMap(api => api.items).flatMap(item => item.type))).sort();
  }
};
NgDocApiListComponent.ɵfac = function NgDocApiListComponent_Factory(t) {
  return new (t || NgDocApiListComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_ng_doc_app_tokens__WEBPACK_IMPORTED_MODULE_6__.NG_DOC_API_LIST_TOKEN), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormBuilder), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_8__.ActivatedRoute), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_8__.Router));
};
NgDocApiListComponent.ɵcmp = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
  type: NgDocApiListComponent,
  selectors: [["ng-doc-api-list"]],
  decls: 4,
  vars: 3,
  consts: [["ng-doc-text", ""], ["class", "ng-doc-api-list-wrapper", 4, "ngIf"], [1, "ng-doc-api-list-wrapper"], [1, "ng-doc-api-filter", 3, "formGroup"], ["ng-doc-label", "Filter", 1, "ng-doc-api-filter-item"], [3, "leftContent"], ["leftContent", ""], ["ngDocInputString", "", "placeholder", "Type the name", "ngDocAutofocus", "", 3, "formControl"], ["class", "ng-doc-api-filter-item", "ng-doc-label", "Scope", 4, "ngIf"], ["ng-doc-label", "Type", 1, "ng-doc-api-filter-item"], ["placeholder", "Choose the entity type", 3, "formControl", "valueContent", "readonly"], [4, "ngDocData"], ["comboboxTypeItem", ""], [1, "ng-doc-api-list"], ["class", "ng-doc-api-scope", 4, "ngFor", "ngForOf"], ["icon", "search"], ["ng-doc-label", "Scope", 1, "ng-doc-api-filter-item"], ["placeholder", "Choose the scope", 3, "formControl", "valueContent", "readonly"], ["comboboxScopeItem", ""], [3, "value", 4, "ngFor", "ngForOf"], [3, "value"], [4, "ngTemplateOutlet", "ngTemplateOutletContext"], ["ngDocTextLeft", "", 3, "kind", "ngDocTooltip", "positions"], [1, "ng-doc-api-scope"], ["ng-doc-text", "", 1, "ng-doc-scope-title"], ["class", "ng-doc-scope-items", 4, "ngIf"], [1, "ng-doc-scope-items"], ["class", "ng-doc-scope-item", 4, "ngFor", "ngForOf"], [1, "ng-doc-scope-item"], [1, "ng-doc-scope-item-link", 3, "routerLink"], [3, "kind", "ngDocTooltip"]],
  template: function NgDocApiListComponent_Template(rf, ctx) {
    if (rf & 1) {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "h1", 0);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "API List");
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, NgDocApiListComponent_div_2_Template, 15, 8, "div", 1);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipe"](3, "async");
    }
    if (rf & 2) {
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
      _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpipeBind1"](3, 1, ctx.api$));
    }
  },
  dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_9__.NgForOf, _angular_common__WEBPACK_IMPORTED_MODULE_9__.NgIf, _angular_common__WEBPACK_IMPORTED_MODULE_9__.NgTemplateOutlet, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_10__.NgDocTextComponent, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_10__.NgDocTextLeftDirective, _ng_doc_app_components_kind_icon__WEBPACK_IMPORTED_MODULE_11__.NgDocKindIconComponent, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_12__.NgDocTooltipDirective, _angular_router__WEBPACK_IMPORTED_MODULE_8__.RouterLink, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_13__.NgDocInputWrapperComponent, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_14__.NgDocInputStringDirective, _angular_forms__WEBPACK_IMPORTED_MODULE_7__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_7__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.NgControlStatusGroup, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_15__.NgDocComboboxComponent, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_16__.NgDocOptionComponent, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_17__.NgDocDataDirective, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_18__.NgDocListComponent, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_19__.NgDocLabelComponent, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormControlDirective, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormGroupDirective, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_20__.NgDocIconComponent, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_21__.NgDocAutofocusDirective, _angular_common__WEBPACK_IMPORTED_MODULE_9__.AsyncPipe],
  styles: ["[_nghost-%COMP%]   h1[_ngcontent-%COMP%]{margin-top:0}[_nghost-%COMP%]   .ng-doc-api-list-wrapper[_ngcontent-%COMP%]   .ng-doc-api-filter[_ngcontent-%COMP%]{display:flex;margin-top:calc(var(--ng-doc-base-gutter) * 3)}[_nghost-%COMP%]   .ng-doc-api-list-wrapper[_ngcontent-%COMP%]   .ng-doc-api-filter[_ngcontent-%COMP%]   .ng-doc-api-filter-item[_ngcontent-%COMP%]{width:200px}[_nghost-%COMP%]   .ng-doc-api-list-wrapper[_ngcontent-%COMP%]   .ng-doc-api-filter[_ngcontent-%COMP%]   .ng-doc-api-filter-item[_ngcontent-%COMP%]:not(:last-child){margin-right:calc(var(--ng-doc-base-gutter) * 2)}[_nghost-%COMP%]   .ng-doc-api-list-wrapper[_ngcontent-%COMP%]   .ng-doc-api-list[_ngcontent-%COMP%]{margin-top:calc(var(--ng-doc-base-gutter) * 2)}[_nghost-%COMP%]   .ng-doc-api-list-wrapper[_ngcontent-%COMP%]   .ng-doc-api-list[_ngcontent-%COMP%]   .ng-doc-api-scope[_ngcontent-%COMP%]{display:flex;flex-direction:column}[_nghost-%COMP%]   .ng-doc-api-list-wrapper[_ngcontent-%COMP%]   .ng-doc-api-list[_ngcontent-%COMP%]   .ng-doc-api-scope[_ngcontent-%COMP%]   .ng-doc-scope-items[_ngcontent-%COMP%]{margin-top:calc(var(--ng-doc-base-gutter) * 3);list-style:none;padding:0}[_nghost-%COMP%]   .ng-doc-api-list-wrapper[_ngcontent-%COMP%]   .ng-doc-api-list[_ngcontent-%COMP%]   .ng-doc-api-scope[_ngcontent-%COMP%]   .ng-doc-scope-items[_ngcontent-%COMP%]   .ng-doc-scope-item[_ngcontent-%COMP%]{margin:var(--ng-doc-base-gutter) 0;float:left;width:33%;overflow:hidden;min-width:330px;text-overflow:ellipsis;white-space:nowrap}[_nghost-%COMP%]   .ng-doc-api-list-wrapper[_ngcontent-%COMP%]   .ng-doc-api-list[_ngcontent-%COMP%]   .ng-doc-api-scope[_ngcontent-%COMP%]   .ng-doc-scope-items[_ngcontent-%COMP%]   .ng-doc-scope-item[_ngcontent-%COMP%]   .ng-doc-scope-item-link[_ngcontent-%COMP%]{display:flex;align-items:center;color:var(--ng-doc-text);text-decoration:none}[_nghost-%COMP%]   .ng-doc-api-list-wrapper[_ngcontent-%COMP%]   .ng-doc-api-list[_ngcontent-%COMP%]   .ng-doc-api-scope[_ngcontent-%COMP%]   .ng-doc-scope-items[_ngcontent-%COMP%]   .ng-doc-scope-item[_ngcontent-%COMP%]   .ng-doc-scope-item-link[_ngcontent-%COMP%]:hover{text-decoration:underline}[_nghost-%COMP%]   .ng-doc-api-list-wrapper[_ngcontent-%COMP%]   .ng-doc-api-list[_ngcontent-%COMP%]   .ng-doc-api-scope[_ngcontent-%COMP%]   .ng-doc-scope-items[_ngcontent-%COMP%]   .ng-doc-scope-item[_ngcontent-%COMP%]   .ng-doc-scope-item-link[_ngcontent-%COMP%]   ng-doc-kind-icon[_ngcontent-%COMP%]{margin-right:var(--ng-doc-base-gutter);text-decoration:none!important}"],
  changeDetection: 0
});
(0,tslib__WEBPACK_IMPORTED_MODULE_22__.__decorate)([_ng_doc_ui_kit_decorators__WEBPACK_IMPORTED_MODULE_23__.ngDocMakePure, (0,tslib__WEBPACK_IMPORTED_MODULE_22__.__metadata)("design:type", Array), (0,tslib__WEBPACK_IMPORTED_MODULE_22__.__metadata)("design:paramtypes", [])], NgDocApiListComponent.prototype, "scopes", null);
(0,tslib__WEBPACK_IMPORTED_MODULE_22__.__decorate)([_ng_doc_ui_kit_decorators__WEBPACK_IMPORTED_MODULE_23__.ngDocMakePure, (0,tslib__WEBPACK_IMPORTED_MODULE_22__.__metadata)("design:type", Array), (0,tslib__WEBPACK_IMPORTED_MODULE_22__.__metadata)("design:paramtypes", [])], NgDocApiListComponent.prototype, "types", null);
NgDocApiListComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_22__.__decorate)([(0,_ngneat_until_destroy__WEBPACK_IMPORTED_MODULE_1__.UntilDestroy)(), (0,tslib__WEBPACK_IMPORTED_MODULE_22__.__metadata)("design:paramtypes", [Array, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormBuilder, _angular_router__WEBPACK_IMPORTED_MODULE_8__.ActivatedRoute, _angular_router__WEBPACK_IMPORTED_MODULE_8__.Router])], NgDocApiListComponent);
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgDocApiListComponent, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Component,
    args: [{
      selector: 'ng-doc-api-list',
      changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ChangeDetectionStrategy.OnPush,
      template: "<h1 ng-doc-text>API List</h1>\n<div class=\"ng-doc-api-list-wrapper\" *ngIf=\"api$ | async as api\">\n\t<form class=\"ng-doc-api-filter\" [formGroup]=\"formGroup\">\n\t\t<label class=\"ng-doc-api-filter-item\" ng-doc-label=\"Filter\">\n\t\t\t<ng-doc-input-wrapper [leftContent]=\"leftContent\">\n\t\t\t\t<ng-template #leftContent>\n\t\t\t\t\t<ng-doc-icon icon=\"search\"></ng-doc-icon>\n\t\t\t\t</ng-template>\n\t\t\t\t<input\n\t\t\t\t\tngDocInputString\n\t\t\t\t\t[formControl]=\"formGroup.controls.filter\"\n\t\t\t\t\tplaceholder=\"Type the name\"\n\t\t\t\t\tngDocAutofocus\n\t\t\t\t/>\n\t\t\t</ng-doc-input-wrapper>\n\t\t</label>\n\n\t\t<label class=\"ng-doc-api-filter-item\" ng-doc-label=\"Scope\" *ngIf=\"scopes.length\">\n\t\t\t<ng-doc-combobox\n\t\t\t\t[formControl]=\"formGroup.controls.scope\"\n\t\t\t\t[valueContent]=\"comboboxScopeItem\"\n\t\t\t\t[readonly]=\"true\"\n\t\t\t\tplaceholder=\"Choose the scope\"\n\t\t\t>\n\t\t\t\t<ng-doc-list *ngDocData>\n\t\t\t\t\t<ng-doc-option *ngFor=\"let scope of scopes\" [value]=\"scope\">\n\t\t\t\t\t\t<ng-container *ngTemplateOutlet=\"comboboxScopeItem; context: {$implicit: scope}\"></ng-container>\n\t\t\t\t\t</ng-doc-option>\n\t\t\t\t</ng-doc-list>\n\t\t\t\t<ng-template let-value #comboboxScopeItem>\n\t\t\t\t\t{{ value }}\n\t\t\t\t</ng-template>\n\t\t\t</ng-doc-combobox>\n\t\t</label>\n\n\t\t<label class=\"ng-doc-api-filter-item\" ng-doc-label=\"Type\">\n\t\t\t<ng-doc-combobox\n\t\t\t\t[formControl]=\"formGroup.controls.type\"\n\t\t\t\t[valueContent]=\"comboboxTypeItem\"\n\t\t\t\t[readonly]=\"true\"\n\t\t\t\tplaceholder=\"Choose the entity type\"\n\t\t\t>\n\t\t\t\t<ng-doc-list *ngDocData>\n\t\t\t\t\t<ng-doc-option *ngFor=\"let type of types\" [value]=\"type\">\n\t\t\t\t\t\t<ng-container *ngTemplateOutlet=\"comboboxTypeItem; context: {$implicit: type}\"></ng-container>\n\t\t\t\t\t</ng-doc-option>\n\t\t\t\t</ng-doc-list>\n\t\t\t\t<ng-template let-value #comboboxTypeItem>\n\t\t\t\t\t<div ng-doc-text>\n\t\t\t\t\t\t<ng-doc-kind-icon\n\t\t\t\t\t\t\t[kind]=\"value\"\n\t\t\t\t\t\t\t[ngDocTooltip]=\"value\"\n\t\t\t\t\t\t\t[positions]=\"['left-center', 'right-center']\"\n\t\t\t\t\t\t\tngDocTextLeft\n\t\t\t\t\t\t>\n\t\t\t\t\t\t</ng-doc-kind-icon>\n\t\t\t\t\t\t{{ value }}\n\t\t\t\t\t</div>\n\t\t\t\t</ng-template>\n\t\t\t</ng-doc-combobox>\n\t\t</label>\n\t</form>\n\t<div class=\"ng-doc-api-list\">\n\t\t<div class=\"ng-doc-api-scope\" *ngFor=\"let scope of api\">\n\t\t\t<h3 class=\"ng-doc-scope-title\" ng-doc-text>{{ scope.title }}</h3>\n\t\t\t<ul class=\"ng-doc-scope-items\" *ngIf=\"scope.items.length\">\n\t\t\t\t<li class=\"ng-doc-scope-item\" *ngFor=\"let apiReference of scope.items\">\n\t\t\t\t\t<a class=\"ng-doc-scope-item-link\" [routerLink]=\"[apiReference.route]\">\n\t\t\t\t\t\t<ng-doc-kind-icon [kind]=\"apiReference.type\" [ngDocTooltip]=\"apiReference.type\">\n\t\t\t\t\t\t</ng-doc-kind-icon>\n\t\t\t\t\t\t{{ apiReference.name }}\n\t\t\t\t\t</a>\n\t\t\t\t</li>\n\t\t\t</ul>\n\t\t</div>\n\t</div>\n</div>\n",
      styles: [":host h1{margin-top:0}:host .ng-doc-api-list-wrapper .ng-doc-api-filter{display:flex;margin-top:calc(var(--ng-doc-base-gutter) * 3)}:host .ng-doc-api-list-wrapper .ng-doc-api-filter .ng-doc-api-filter-item{width:200px}:host .ng-doc-api-list-wrapper .ng-doc-api-filter .ng-doc-api-filter-item:not(:last-child){margin-right:calc(var(--ng-doc-base-gutter) * 2)}:host .ng-doc-api-list-wrapper .ng-doc-api-list{margin-top:calc(var(--ng-doc-base-gutter) * 2)}:host .ng-doc-api-list-wrapper .ng-doc-api-list .ng-doc-api-scope{display:flex;flex-direction:column}:host .ng-doc-api-list-wrapper .ng-doc-api-list .ng-doc-api-scope .ng-doc-scope-items{margin-top:calc(var(--ng-doc-base-gutter) * 3);list-style:none;padding:0}:host .ng-doc-api-list-wrapper .ng-doc-api-list .ng-doc-api-scope .ng-doc-scope-items .ng-doc-scope-item{margin:var(--ng-doc-base-gutter) 0;float:left;width:33%;overflow:hidden;min-width:330px;text-overflow:ellipsis;white-space:nowrap}:host .ng-doc-api-list-wrapper .ng-doc-api-list .ng-doc-api-scope .ng-doc-scope-items .ng-doc-scope-item .ng-doc-scope-item-link{display:flex;align-items:center;color:var(--ng-doc-text);text-decoration:none}:host .ng-doc-api-list-wrapper .ng-doc-api-list .ng-doc-api-scope .ng-doc-scope-items .ng-doc-scope-item .ng-doc-scope-item-link:hover{text-decoration:underline}:host .ng-doc-api-list-wrapper .ng-doc-api-list .ng-doc-api-scope .ng-doc-scope-items .ng-doc-scope-item .ng-doc-scope-item-link ng-doc-kind-icon{margin-right:var(--ng-doc-base-gutter);text-decoration:none!important}\n"]
    }]
  }], function () {
    return [{
      type: undefined,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Inject,
        args: [_ng_doc_app_tokens__WEBPACK_IMPORTED_MODULE_6__.NG_DOC_API_LIST_TOKEN]
      }]
    }, {
      type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormBuilder
    }, {
      type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.ActivatedRoute
    }, {
      type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.Router
    }];
  }, {
    scopes: [],
    types: []
  });
})();
class NgDocApiListModule {}
NgDocApiListModule.ɵfac = function NgDocApiListModule_Factory(t) {
  return new (t || NgDocApiListModule)();
};
NgDocApiListModule.ɵmod = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
  type: NgDocApiListModule
});
NgDocApiListModule.ɵinj = /* @__PURE__ */_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
  imports: [_angular_common__WEBPACK_IMPORTED_MODULE_9__.CommonModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_10__.NgDocTextModule, _ng_doc_app_components_kind_icon__WEBPACK_IMPORTED_MODULE_11__.NgDocKindIconModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_12__.NgDocTooltipModule, _angular_router__WEBPACK_IMPORTED_MODULE_8__.RouterModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_13__.NgDocInputWrapperModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_14__.NgDocInputStringModule, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormsModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_15__.NgDocComboboxModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_16__.NgDocOptionModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_17__.NgDocDataModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_18__.NgDocListModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_19__.NgDocLabelModule, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.ReactiveFormsModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_20__.NgDocIconModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_21__.NgDocAutofocusModule]
});
(function () {
  (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](NgDocApiListModule, [{
    type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.NgModule,
    args: [{
      declarations: [NgDocApiListComponent],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_9__.CommonModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_10__.NgDocTextModule, _ng_doc_app_components_kind_icon__WEBPACK_IMPORTED_MODULE_11__.NgDocKindIconModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_12__.NgDocTooltipModule, _angular_router__WEBPACK_IMPORTED_MODULE_8__.RouterModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_13__.NgDocInputWrapperModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_14__.NgDocInputStringModule, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormsModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_15__.NgDocComboboxModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_16__.NgDocOptionModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_17__.NgDocDataModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_18__.NgDocListModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_19__.NgDocLabelModule, _angular_forms__WEBPACK_IMPORTED_MODULE_7__.ReactiveFormsModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_20__.NgDocIconModule, _ng_doc_ui_kit__WEBPACK_IMPORTED_MODULE_21__.NgDocAutofocusModule],
      exports: [NgDocApiListComponent]
    }]
  }], null, null);
})();

/**
 * Generated bundle index. Do not edit.
 */



/***/ }),

/***/ 15523:
/*!*****************************************************!*\
  !*** ./.ng-doc/m-docs/api/api/ng-doc.api-list.json ***!
  \*****************************************************/
/***/ ((module) => {

module.exports = JSON.parse('[{"title":"m-ui","items":[{"route":"m-ui/interfaces/IconMapping","type":"Interface","name":"IconMapping"},{"route":"m-ui/variables/ICONS_MAPPING","type":"Variable","name":"ICONS_MAPPING"},{"route":"m-ui/classes/MIcons","type":"Component","name":"MIcons"},{"route":"m-ui/classes/MIconsService","type":"Injectable","name":"MIconsService"},{"route":"m-ui/interfaces/Affluence","type":"Interface","name":"Affluence"},{"route":"m-ui/classes/MAffluence","type":"Component","name":"MAffluence"},{"route":"m-ui/interfaces/Line","type":"Interface","name":"Line"},{"route":"m-ui/interfaces/disturbanceInfo","type":"Interface","name":"disturbanceInfo"},{"route":"m-ui/classes/MLogoLines","type":"Component","name":"MLogoLines"},{"route":"m-ui/classes/MTable","type":"Component","name":"MTable"},{"route":"m-ui/enums/HeaderTypes","type":"Enum","name":"HeaderTypes"},{"route":"m-ui/interfaces/HeaderTable","type":"Interface","name":"HeaderTable"},{"route":"m-ui/interfaces/HeaderTableSelectOptions","type":"Interface","name":"HeaderTableSelectOptions"},{"route":"m-ui/interfaces/HeaderTableOptions","type":"Interface","name":"HeaderTableOptions"},{"route":"m-ui/interfaces/ConfigTableCustom","type":"Interface","name":"ConfigTableCustom"},{"route":"m-ui/interfaces/ConfigActionTable","type":"Interface","name":"ConfigActionTable"},{"route":"m-ui/interfaces/ActionTable","type":"Interface","name":"ActionTable"},{"route":"m-ui/interfaces/ConfigTable","type":"Interface","name":"ConfigTable"},{"route":"m-ui/interfaces/ConfigButtonTable","type":"Interface","name":"ConfigButtonTable"},{"route":"m-ui/interfaces/FilterText","type":"Interface","name":"FilterText"},{"route":"m-ui/classes/RelativeDatePipe","type":"Pipe","name":"RelativeDatePipe"},{"route":"m-ui/interfaces/AppDownload","type":"Interface","name":"AppDownload"},{"route":"m-ui/classes/MAppDownload","type":"Component","name":"MAppDownload"},{"route":"m-ui/classes/MListWrapper","type":"Component","name":"MListWrapper"},{"route":"m-ui/classes/MDisturbanceDisplay","type":"Component","name":"MDisturbanceDisplay"}]}]');

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_api_api_ng-doc-api-list_module_ts.js.map