"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_api_api_m-ui_classes_MAppDownload_module_ts"],{

/***/ 35003:
/*!********************************************************************!*\
  !*** ./.ng-doc/m-docs/api/api/m-ui/classes/MAppDownload/module.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_api_api_m_ui_classes_MAppDownload_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/api/api/m-ui/classes/MAppDownload/index.html */ 63965);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);







class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = undefined;
    this.pageType = 'api';
    this.pageContent = _raw_loader_ng_doc_m_docs_api_api_m_ui_classes_MAppDownload_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
    this.demo = undefined;
    this.demoAssets = undefined;
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'MAppDownload'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
})();

/***/ }),

/***/ 63965:
/*!***********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/api/api/m-ui/classes/MAppDownload/index.html ***!
  \***********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<header class=\"ngde\"><div class=\"ng-doc-page-tags ngde\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"ng-doc-scope\">m-ui</span> <span class=\"ng-doc-inline-delimiter ngde\" indexable=\"false\">/</span> <span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Class\">Class</span> <span class=\"ng-doc-inline-delimiter ngde\" indexable=\"false\">/</span><div class=\"ng-doc-decorators-group ngde\" indexable=\"false\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Component\">@Component</span></div><span class=\"ng-doc-inline-delimiter ngde\" indexable=\"false\">/</span> <span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"ng-doc-tag-selector\">m-app-download</span></div><h1 id=\"mappdownload\" class=\"ngde\">MAppDownload<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MAppDownload#mappdownload\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1><div class=\"ng-doc-header-details ngde\" indexable=\"false\"><span class=\"ng-doc-header-details-label ngde\">Implements</span><code indexable=\"false\" class=\"ngde\">AfterViewInit</code></div></header><section class=\"ngde\"><p class=\"ngde\">MAppDownload</p><p class=\"ngde\">Composant graphique pour afficher les liens de téléchargement d'une Appli Exemple : liens pour télécharger l'Appli M sur Apple Store</p><h3 id=\"usage-example\" class=\"ngde\">Usage example<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MAppDownload#usage-example\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h3><pre class=\"ngde hljs\"><code class=\"hljs language-html ngde\" lang=\"html\" filename=\"\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">m-app-download</span> [<span class=\"hljs-attr ngde\">data</span>]=<span class=\"hljs-string ngde\">\"App\"</span>></span><span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">m-app-download</span>></span></code></pre><p class=\"ngde\">m-app-download true CommonModule, MatIconModule, MatButtonModule</p></section><section class=\"ngde\"><h2 id=\"constructor\" class=\"ngde\">Constructor<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MAppDownload#constructor\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ng-doc-method-table ngde\"><tbody class=\"ngde\"><tr class=\"ngde\"><td class=\"ngde\"><span class=\"ng-doc-no-content ngde\" indexable=\"false\">No documentation has been provided.</span></td></tr><tr class=\"ngde\"><td class=\"ngde\"><h5 class=\"no-anchor ngde\" indexable=\"false\">Presentation</h5><pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-title function_ ngde\">constructor</span>(<span class=\"hljs-params ngde\">\n\ticonRegistry: MatIconRegistry, \n\tsanitizer: DomSanitizer, \n\t<span class=\"hljs-keyword ngde\">private</span> renderer: Renderer2\n</span>): <span class=\"hljs-title class_ ngde\"><a href=\"/api/m-ui/classes/MAppDownload\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MAppDownload</a></span>;</code></pre></td></tr><tr class=\"ngde\"><td class=\"ngde\"><h5 class=\"no-anchor ngde\" indexable=\"false\">Parameters</h5><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ng-doc-parameters-table ngde\"><thead class=\"ngde\"><tr indexable=\"false\" class=\"ngde\"><th class=\"ng-doc-parameters-table-name ngde\">Name</th><th class=\"ng-doc-parameters-table-type ngde\">Type</th><th class=\"ng-doc-parameters-table-description ngde\">Description</th></tr></thead><tbody class=\"ngde\"><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">iconRegistry<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">MatIconRegistry</code></td><td class=\"ngde\"></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">sanitizer<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">DomSanitizer</code></td><td class=\"ngde\"></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">renderer<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">Renderer2</code></td><td class=\"ngde\"></td></tr></tbody></table></div></td></tr></tbody></table></div></section><section class=\"ngde\"><h2 id=\"properties\" class=\"ngde\">Properties<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MAppDownload#properties\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ng-doc-properties-table ngde\"><thead class=\"ngde\"><tr indexable=\"false\" class=\"ngde\"><th class=\"ng-doc-properties-table-name ngde\">Name</th><th class=\"ng-doc-properties-table-type ngde\">Type</th><th class=\"ng-doc-properties-table-description ngde\">Description</th></tr></thead><tbody class=\"ngde\"><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\"><div class=\"ng-doc-decorators-group column ngde\" indexable=\"false\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Input\">@Input</span></div>data<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde ng-doc-code-with-link\" class=\"ngde\"><a href=\"/api/m-ui/interfaces/AppDownload\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Interface\" class=\"ngde\">AppDownload</a></code></td><td class=\"ngde\"><p class=\"ngde\">Logo + lien téléchargement + texte</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\"><div class=\"ng-doc-decorators-group column ngde\" indexable=\"false\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"ViewChild\">@ViewChild</span></div>iconPrincipal<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">ElementRef&#x3C;any></code></td><td class=\"ngde\"><p class=\"ngde\">Element HTML pour l'icon représentant la plateforme de téléchargement</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\"><div class=\"ng-doc-decorators-group column ngde\" indexable=\"false\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"ViewChild\">@ViewChild</span></div>text<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">ElementRef&#x3C;any></code></td><td class=\"ngde\"></td></tr></tbody></table></div></section><section class=\"ngde\"><h2 id=\"methods\" class=\"ngde\">Methods<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MAppDownload#methods\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ng-doc-method-table ngde\"><thead class=\"ngde\"><tr class=\"ngde\"><th indexable=\"false\" class=\"ngde\"><h3 id=\"ngafterviewinit\" class=\"ngde\">ngAfterViewInit()<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MAppDownload#ngafterviewinit\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h3><div class=\"ng-doc-node-details ngde\">implements <code class=\"ngde\">AfterViewInit</code></div></th></tr></thead><tbody class=\"ngde\"><tr class=\"ngde\"><td class=\"ngde\"><span class=\"ng-doc-no-content ngde\" indexable=\"false\">No documentation has been provided.</span></td></tr><tr class=\"ngde\"><td class=\"ngde\"><h5 class=\"no-anchor ngde\" indexable=\"false\">Presentation</h5><pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-title function_ ngde\">ngAfterViewInit</span>(): <span class=\"hljs-built_in ngde\">void</span>;</code></pre></td></tr><tr class=\"ngde\"><td class=\"ngde\"><h5 class=\"no-anchor ngde\" indexable=\"false\">Returns</h5><p class=\"ngde\"><code indexable=\"false\" class=\"ngde\">void</code></p></td></tr></tbody></table></div></section>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_api_api_m-ui_classes_MAppDownload_module_ts.js.map