"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_guides_angular-materiel_material-input_module_ts"],{

/***/ 23771:
/*!***********************************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/angular-materiel/material-input/component-assets.ts ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   "demoAssets": () => (/* binding */ demoAssets)
/* harmony export */ });
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_assets_InputDemoBasic_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-input/assets/InputDemoBasic/TypeScript/Asset0.html */ 42139);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_assets_InputDemoBasic_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-input/assets/InputDemoBasic/HTML/Asset1.html */ 68098);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_assets_InputDemoBasic_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-input/assets/InputDemoBasic/SCSS/Asset2.html */ 78580);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_assets_InputDemoError_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-input/assets/InputDemoError/TypeScript/Asset0.html */ 8344);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_assets_InputDemoError_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-input/assets/InputDemoError/HTML/Asset1.html */ 96758);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_assets_InputDemoHint_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-input/assets/InputDemoHint/TypeScript/Asset0.html */ 1080);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_assets_InputDemoHint_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-input/assets/InputDemoHint/HTML/Asset1.html */ 16329);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_assets_InputDemoHint_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-input/assets/InputDemoHint/SCSS/Asset2.html */ 80194);








const demoAssets = {
  InputDemoBasic: [{
    title: 'TypeScript',
    codeType: 'TypeScript',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_assets_InputDemoBasic_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__["default"]
  }, {
    title: 'HTML',
    codeType: 'HTML',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_assets_InputDemoBasic_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__["default"]
  }, {
    title: 'SCSS',
    codeType: 'SCSS',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_assets_InputDemoBasic_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_2__["default"]
  }],
  InputDemoError: [{
    title: 'TypeScript',
    codeType: 'TypeScript',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_assets_InputDemoError_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_3__["default"]
  }, {
    title: 'HTML',
    codeType: 'HTML',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_assets_InputDemoError_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_4__["default"]
  }],
  InputDemoHint: [{
    title: 'TypeScript',
    codeType: 'TypeScript',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_assets_InputDemoHint_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_5__["default"]
  }, {
    title: 'HTML',
    codeType: 'HTML',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_assets_InputDemoHint_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_6__["default"]
  }, {
    title: 'SCSS',
    codeType: 'SCSS',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_assets_InputDemoHint_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_7__["default"]
  }]
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (demoAssets);

/***/ }),

/***/ 72057:
/*!*************************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/angular-materiel/material-input/module.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-input/index.html */ 57709);
/* harmony import */ var _playgrounds__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./playgrounds */ 84751);
/* harmony import */ var src_app_docs_angular_material_material_input_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/docs/angular-material/material-input/ng-doc.dependencies */ 95250);
/* harmony import */ var _ng_doc_m_docs_guides_angular_materiel_material_input_component_assets__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! .ng-doc/m-docs/guides/angular-materiel/material-input/component-assets */ 23771);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_docs_angular_material_material_input_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/docs/angular-material/material-input/ng-doc.module */ 37537);




// noinspection ES6UnusedImports


// noinspection ES6UnusedImports





class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = '';
    this.pageType = 'guide';
    this.pageContent = _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_input_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
    this.dependencies = src_app_docs_angular_material_material_input_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"];
    this.demoAssets = _ng_doc_m_docs_guides_angular_materiel_material_input_component_assets__WEBPACK_IMPORTED_MODULE_3__["default"];
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, src_app_docs_angular_material_material_input_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"].module, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'Input'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, src_app_docs_angular_material_material_input_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__.MaterialInputPageModule, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
})();

/***/ }),

/***/ 84751:
/*!******************************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/angular-materiel/material-input/playgrounds.ts ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PlaygroundsModule": () => (/* binding */ PlaygroundsModule)
/* harmony export */ });
/* harmony import */ var src_app_docs_angular_material_material_input_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/docs/angular-material/material-input/ng-doc.dependencies */ 95250);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_docs_angular_material_material_input_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/docs/angular-material/material-input/ng-doc.module */ 37537);
// noinspection ES6UnusedImports




class PlaygroundsModule {
  static #_ = this.ɵfac = function PlaygroundsModule_Factory(t) {
    return new (t || PlaygroundsModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: PlaygroundsModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, src_app_docs_angular_material_material_input_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__["default"].module]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](PlaygroundsModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, src_app_docs_angular_material_material_input_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__.MaterialInputPageModule]
  });
})();

/***/ }),

/***/ 5789:
/*!*****************************************************************************************************!*\
  !*** ./src/app/docs/angular-material/material-input/input-demo-basic/input-demo-basic.component.ts ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "InputDemoBasic": () => (/* binding */ InputDemoBasic)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/input */ 68562);
/* harmony import */ var src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/features/preview/preview.component */ 55355);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/form-field */ 75074);






class InputDemoBasic {
  static #_ = this.ɵfac = function InputDemoBasic_Factory(t) {
    return new (t || InputDemoBasic)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
    type: InputDemoBasic,
    selectors: [["app-input-demo-basic"]],
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵStandaloneFeature"]],
    decls: 48,
    vars: 3,
    consts: [[3, "nextElement", "fullWidth"], [1, "example-form"], [1, "example-full-width"], ["matInput", "", "disabled", "", "value", "Google"], ["cellspacing", "0", 1, "example-full-width"], ["matInput", ""], ["matInput", "", "placeholder", "Ex. 100 Main St"], ["matInput", "", "placeholder", "Ex. San Francisco"], ["matInput", "", "placeholder", "Ex. California"], ["matInput", "", "maxlength", "5", "placeholder", "Ex. 94105", "value", "94043"], ["postalCode", ""], ["align", "end"]],
    template: function InputDemoBasic_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "app-preview", 0)(1, "form", 1)(2, "mat-form-field", 2)(3, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Company (disabled)");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](5, "input", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "table", 4)(7, "tr")(8, "td")(9, "mat-form-field", 2)(10, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "First name");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](12, "input", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "td")(14, "mat-form-field", 2)(15, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16, "Long Last Name That Will Be Truncated");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](17, "input", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](18, "p")(19, "mat-form-field", 2)(20, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](21, "Address");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](22, "textarea", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](23, "1600 Amphitheatre Pkwy");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](24, "mat-form-field", 2)(25, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](26, "Address 2");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](27, "textarea", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](28, "table", 4)(29, "tr")(30, "td")(31, "mat-form-field", 2)(32, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](33, "City");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](34, "input", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](35, "td")(36, "mat-form-field", 2)(37, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](38, "State");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](39, "input", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](40, "td")(41, "mat-form-field", 2)(42, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](43, "Postal Code");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](44, "input", 9, 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](46, "mat-hint", 11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](47);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()()()()();
      }
      if (rf & 2) {
        const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](45);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("nextElement", false)("fullWidth", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](47);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", _r0.value.length, " / 5");
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__.Preview, _angular_material_input__WEBPACK_IMPORTED_MODULE_3__.MatInputModule, _angular_material_input__WEBPACK_IMPORTED_MODULE_3__.MatInput, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__.MatFormField, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__.MatLabel, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__.MatHint],
    styles: [".example-form[_ngcontent-%COMP%] {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%;\n}\n\n.example-full-width[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\ntd[_ngcontent-%COMP%] {\n  padding-right: 8px;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvZG9jcy9hbmd1bGFyLW1hdGVyaWFsL21hdGVyaWFsLWlucHV0L2lucHV0LWRlbW8tYmFzaWMvaW5wdXQtZGVtby1iYXNpYy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0FBQ0Q7O0FBRUE7RUFDQyxXQUFBO0FBQ0Q7O0FBRUE7RUFDQyxrQkFBQTtBQUNEIiwic291cmNlc0NvbnRlbnQiOlsiLmV4YW1wbGUtZm9ybSB7XHJcblx0bWluLXdpZHRoOiAxNTBweDtcclxuXHRtYXgtd2lkdGg6IDUwMHB4O1xyXG5cdHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uZXhhbXBsZS1mdWxsLXdpZHRoIHtcclxuXHR3aWR0aDogMTAwJTtcclxufVxyXG5cclxudGQge1xyXG5cdHBhZGRpbmctcmlnaHQ6IDhweDtcclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9 */"]
  });
}

/***/ }),

/***/ 46110:
/*!*****************************************************************************************************!*\
  !*** ./src/app/docs/angular-material/material-input/input-demo-error/input-demo-error.component.ts ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "InputDemoError": () => (/* binding */ InputDemoError)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/input */ 68562);
/* harmony import */ var src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/features/preview/preview.component */ 55355);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/form-field */ 75074);









function InputDemoError_mat_error_6_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "mat-error");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, " Please enter a valid email address ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
  }
}
function InputDemoError_mat_error_7_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "mat-error");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, " Email is ");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "strong");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "required");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
  }
}
class InputDemoError {
  constructor() {
    this.emailFormControl = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__.FormControl('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.email]);
  }
  static #_ = this.ɵfac = function InputDemoError_Factory(t) {
    return new (t || InputDemoError)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
    type: InputDemoError,
    selectors: [["app-input-demo-error"]],
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵStandaloneFeature"]],
    decls: 8,
    vars: 5,
    consts: [[3, "nextElement", "fullWidth"], [1, "example-form"], [1, "example-full-width"], ["type", "email", "matInput", "", "placeholder", "Ex. pat@example.com", 3, "formControl"], [4, "ngIf"]],
    template: function InputDemoError_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "app-preview", 0)(1, "form", 1)(2, "mat-form-field", 2)(3, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Email");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](5, "input", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](6, InputDemoError_mat_error_6_Template, 2, 0, "mat-error", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](7, InputDemoError_mat_error_7_Template, 4, 0, "mat-error", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("nextElement", false)("fullWidth", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("formControl", ctx.emailFormControl);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.emailFormControl.hasError("email") && !ctx.emailFormControl.hasError("required"));
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngIf", ctx.emailFormControl.hasError("required"));
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, _angular_common__WEBPACK_IMPORTED_MODULE_3__.NgIf, src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__.Preview, _angular_material_input__WEBPACK_IMPORTED_MODULE_4__.MatInputModule, _angular_material_input__WEBPACK_IMPORTED_MODULE_4__.MatInput, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__.MatFormField, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__.MatLabel, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_5__.MatError, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.ReactiveFormsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ɵNgNoValidate"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__.DefaultValueAccessor, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.FormControlDirective],
    styles: ["\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZVJvb3QiOiIifQ== */"]
  });
}

/***/ }),

/***/ 55879:
/*!***************************************************************************************************!*\
  !*** ./src/app/docs/angular-material/material-input/input-demo-hint/input-demo-hint.component.ts ***!
  \***************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "InputDemoHint": () => (/* binding */ InputDemoHint)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/input */ 68562);
/* harmony import */ var src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/features/preview/preview.component */ 55355);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/form-field */ 75074);






class InputDemoHint {
  static #_ = this.ɵfac = function InputDemoHint_Factory(t) {
    return new (t || InputDemoHint)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
    type: InputDemoHint,
    selectors: [["app-input-demo-hint"]],
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵStandaloneFeature"]],
    decls: 12,
    vars: 3,
    consts: [[3, "nextElement", "fullWidth"], [1, "example-form"], [1, "example-full-width"], ["matInput", "", "maxlength", "256", "placeholder", "Ex. I need help with..."], ["message", ""], ["align", "start"], ["align", "end"]],
    template: function InputDemoHint_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "app-preview", 0)(1, "form", 1)(2, "mat-form-field", 2)(3, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "Message");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](5, "input", 3, 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "mat-hint", 5)(8, "strong");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, "Don't disclose personal info");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "mat-hint", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
      }
      if (rf & 2) {
        const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("nextElement", false)("fullWidth", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("", _r0.value.length, " / 256");
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__.Preview, _angular_material_input__WEBPACK_IMPORTED_MODULE_3__.MatInputModule, _angular_material_input__WEBPACK_IMPORTED_MODULE_3__.MatInput, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__.MatFormField, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__.MatLabel, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__.MatHint],
    styles: [".example-form[_ngcontent-%COMP%] {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%;\n}\n\n.example-full-width[_ngcontent-%COMP%] {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvZG9jcy9hbmd1bGFyLW1hdGVyaWFsL21hdGVyaWFsLWlucHV0L2lucHV0LWRlbW8taGludC9pbnB1dC1kZW1vLWhpbnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDQyxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQUNEOztBQUVBO0VBQ0MsV0FBQTtBQUNEIiwic291cmNlc0NvbnRlbnQiOlsiLmV4YW1wbGUtZm9ybSB7XHJcblx0bWluLXdpZHRoOiAxNTBweDtcclxuXHRtYXgtd2lkdGg6IDUwMHB4O1xyXG5cdHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uZXhhbXBsZS1mdWxsLXdpZHRoIHtcclxuXHR3aWR0aDogMTAwJTtcclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9 */"]
  });
}

/***/ }),

/***/ 95250:
/*!*****************************************************************************!*\
  !*** ./src/app/docs/angular-material/material-input/ng-doc.dependencies.ts ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _material_docs_material_input_input_demo_basic_input_demo_basic_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @material-docs/material-input/input-demo-basic/input-demo-basic.component */ 5789);
/* harmony import */ var _material_docs_material_input_input_demo_error_input_demo_error_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-docs/material-input/input-demo-error/input-demo-error.component */ 46110);
/* harmony import */ var _material_docs_material_input_input_demo_hint_input_demo_hint_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @material-docs/material-input/input-demo-hint/input-demo-hint.component */ 55879);
/* harmony import */ var src_app_docs_angular_material_material_input_ng_doc_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/docs/angular-material/material-input/ng-doc.module */ 37537);




const MaterialInputPageDependencies = {
  module: src_app_docs_angular_material_material_input_ng_doc_module__WEBPACK_IMPORTED_MODULE_3__.MaterialInputPageModule,
  // Add your demos that you are going to use in the page here
  demo: {
    InputDemoBasic: _material_docs_material_input_input_demo_basic_input_demo_basic_component__WEBPACK_IMPORTED_MODULE_0__.InputDemoBasic,
    InputDemoError: _material_docs_material_input_input_demo_error_input_demo_error_component__WEBPACK_IMPORTED_MODULE_1__.InputDemoError,
    InputDemoHint: _material_docs_material_input_input_demo_hint_input_demo_hint_component__WEBPACK_IMPORTED_MODULE_2__.InputDemoHint
  }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MaterialInputPageDependencies);

/***/ }),

/***/ 37537:
/*!***********************************************************************!*\
  !*** ./src/app/docs/angular-material/material-input/ng-doc.module.ts ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MaterialInputPageModule": () => (/* binding */ MaterialInputPageModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);


class MaterialInputPageModule {
  static #_ = this.ɵfac = function MaterialInputPageModule_Factory(t) {
    return new (t || MaterialInputPageModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
    type: MaterialInputPageModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](MaterialInputPageModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
})();

/***/ }),

/***/ 55355:
/*!*******************************************************!*\
  !*** ./src/app/features/preview/preview.component.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Preview": () => (/* binding */ Preview)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/list */ 6517);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/divider */ 71528);




function Preview_span_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r0.title);
  }
}
function Preview_mat_divider_4_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "mat-divider");
  }
}
const _c0 = ["*"];
class Preview {
  constructor() {
    this.nextElement = true;
    this.fullWidth = false;
    this.oneLine = false;
  }
  static #_ = this.ɵfac = function Preview_Factory(t) {
    return new (t || Preview)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
    type: Preview,
    selectors: [["app-preview"]],
    inputs: {
      title: "title",
      nextElement: "nextElement",
      fullWidth: "fullWidth",
      oneLine: "oneLine"
    },
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵStandaloneFeature"]],
    ngContentSelectors: _c0,
    decls: 5,
    vars: 6,
    consts: [[4, "ngIf"], [1, "demo-preview-content"]],
    template: function Preview_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojectionDef"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, Preview_span_1_Template, 2, 1, "span", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojection"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, Preview_mat_divider_4_Template, 1, 0, "mat-divider", 0);
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("oneLine", ctx.oneLine);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.title);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("fullWidth", ctx.fullWidth);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.nextElement);
      }
    },
    dependencies: [_angular_material_list__WEBPACK_IMPORTED_MODULE_1__.MatListModule, _angular_material_divider__WEBPACK_IMPORTED_MODULE_2__.MatDivider, _angular_common__WEBPACK_IMPORTED_MODULE_3__.NgIf],
    styles: ["section[_ngcontent-%COMP%] {\n  padding: 16px;\n}\nsection.oneLine[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n}\nsection[_ngcontent-%COMP%]   div.demo-preview-content[_ngcontent-%COMP%] {\n  display: flex;\n  flex-wrap: wrap;\n  gap: 8px;\n  padding: 8px;\n}\nsection[_ngcontent-%COMP%]   div.demo-preview-content.fullWidth[_ngcontent-%COMP%] {\n  display: block;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvZmVhdHVyZXMvcHJldmlldy9wcmV2aWV3LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsYUFBQTtBQUNEO0FBQ0M7RUFDQyxhQUFBO0VBQ0EsbUJBQUE7QUFDRjtBQUVDO0VBQ0MsYUFBQTtFQUNBLGVBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtBQUFGO0FBRUU7RUFDQyxjQUFBO0FBQUgiLCJzb3VyY2VzQ29udGVudCI6WyJzZWN0aW9uIHtcclxuXHRwYWRkaW5nOiAxNnB4O1xyXG5cclxuXHQmLm9uZUxpbmV7XHJcblx0XHRkaXNwbGF5OiBmbGV4O1xyXG5cdFx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHR9XHJcblxyXG5cdGRpdi5kZW1vLXByZXZpZXctY29udGVudHtcclxuXHRcdGRpc3BsYXk6IGZsZXg7XHJcblx0XHRmbGV4LXdyYXA6IHdyYXA7XHJcblx0XHRnYXA6IDhweDtcclxuXHRcdHBhZGRpbmc6IDhweDtcclxuXHJcblx0XHQmLmZ1bGxXaWR0aHtcclxuXHRcdFx0ZGlzcGxheTogYmxvY2s7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0= */"]
  });
}

/***/ }),

/***/ 68098:
/*!********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-input/assets/InputDemoBasic/HTML/Asset1.html ***!
  \********************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"html\" class=\"hljs language-html ngde\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> [<span class=\"hljs-attr ngde\">nextElement</span>]=<span class=\"hljs-string ngde\">\"false\"</span> [<span class=\"hljs-attr ngde\">fullWidth</span>]=<span class=\"hljs-string ngde\">\"true\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">form</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-form\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-form-field</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-full-width\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-label</span>></span>Company (disabled)<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-label</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">input</span> <span class=\"hljs-attr ngde\">matInput</span> <span class=\"hljs-attr ngde\">disabled</span> <span class=\"hljs-attr ngde\">value</span>=<span class=\"hljs-string ngde\">\"Google\"</span> /></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-form-field</span>></span>\n\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">table</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-full-width\"</span> <span class=\"hljs-attr ngde\">cellspacing</span>=<span class=\"hljs-string ngde\">\"0\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">tr</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">td</span>></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-form-field</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-full-width\"</span>></span>\n            <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-label</span>></span>First name<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-label</span>></span>\n            <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">input</span> <span class=\"hljs-attr ngde\">matInput</span> /></span>\n          <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-form-field</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">td</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">td</span>></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-form-field</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-full-width\"</span>></span>\n            <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-label</span>></span>Long Last Name That Will Be Truncated<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-label</span>></span>\n            <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">input</span> <span class=\"hljs-attr ngde\">matInput</span> /></span>\n          <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-form-field</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">td</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">tr</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">table</span>></span>\n\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">p</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-form-field</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-full-width\"</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-label</span>></span>Address<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-label</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">textarea</span> <span class=\"hljs-attr ngde\">matInput</span> <span class=\"hljs-attr ngde\">placeholder</span>=<span class=\"hljs-string ngde\">\"Ex. 100 Main St\"</span>></span>\n1600 Amphitheatre Pkwy&#x3C;/textarea\n        >\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-form-field</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-form-field</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-full-width\"</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-label</span>></span>Address 2<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-label</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">textarea</span> <span class=\"hljs-attr ngde\">matInput</span>></span><span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">textarea</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-form-field</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">p</span>></span>\n\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">table</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-full-width\"</span> <span class=\"hljs-attr ngde\">cellspacing</span>=<span class=\"hljs-string ngde\">\"0\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">tr</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">td</span>></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-form-field</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-full-width\"</span>></span>\n            <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-label</span>></span>City<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-label</span>></span>\n            <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">input</span> <span class=\"hljs-attr ngde\">matInput</span> <span class=\"hljs-attr ngde\">placeholder</span>=<span class=\"hljs-string ngde\">\"Ex. San Francisco\"</span> /></span>\n          <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-form-field</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">td</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">td</span>></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-form-field</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-full-width\"</span>></span>\n            <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-label</span>></span>State<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-label</span>></span>\n            <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">input</span> <span class=\"hljs-attr ngde\">matInput</span> <span class=\"hljs-attr ngde\">placeholder</span>=<span class=\"hljs-string ngde\">\"Ex. California\"</span> /></span>\n          <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-form-field</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">td</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">td</span>></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-form-field</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-full-width\"</span>></span>\n            <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-label</span>></span>Postal Code<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-label</span>></span>\n            <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">input</span>\n              <span class=\"hljs-attr ngde\">matInput</span>\n              #<span class=\"hljs-attr ngde\">postalCode</span>\n              <span class=\"hljs-attr ngde\">maxlength</span>=<span class=\"hljs-string ngde\">\"5\"</span>\n              <span class=\"hljs-attr ngde\">placeholder</span>=<span class=\"hljs-string ngde\">\"Ex. 94105\"</span>\n              <span class=\"hljs-attr ngde\">value</span>=<span class=\"hljs-string ngde\">\"94043\"</span>\n            /></span>\n            <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-hint</span> <span class=\"hljs-attr ngde\">align</span>=<span class=\"hljs-string ngde\">\"end\"</span>></span>{{postalCode.value.length}} / 5<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-hint</span>></span>\n          <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-form-field</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">td</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">tr</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">table</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">form</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n</code></pre>");

/***/ }),

/***/ 78580:
/*!********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-input/assets/InputDemoBasic/SCSS/Asset2.html ***!
  \********************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"scss\" class=\"hljs language-scss ngde\"><span class=\"hljs-selector-class ngde\">.example-form</span> {\n  <span class=\"hljs-attribute ngde\">min-width</span>: <span class=\"hljs-number ngde\">150px</span>;\n  <span class=\"hljs-attribute ngde\">max-width</span>: <span class=\"hljs-number ngde\">500px</span>;\n  <span class=\"hljs-attribute ngde\">width</span>: <span class=\"hljs-number ngde\">100%</span>;\n}\n\n<span class=\"hljs-selector-class ngde\">.example-full-width</span> {\n  <span class=\"hljs-attribute ngde\">width</span>: <span class=\"hljs-number ngde\">100%</span>;\n}\n\n<span class=\"hljs-selector-tag ngde\">td</span> {\n  <span class=\"hljs-attribute ngde\">padding-right</span>: <span class=\"hljs-number ngde\">8px</span>;\n}\n</code></pre>");

/***/ }),

/***/ 42139:
/*!**************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-input/assets/InputDemoBasic/TypeScript/Asset0.html ***!
  \**************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Component</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/core\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">CommonModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/common\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatInputModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/input\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Preview</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"src/app/features/preview/preview.component\"</span>;\n\n<span class=\"hljs-meta ngde\">@Component</span>({\n  <span class=\"hljs-attr ngde\">selector</span>: <span class=\"hljs-string ngde\">\"app-input-demo-basic\"</span>,\n  <span class=\"hljs-attr ngde\">standalone</span>: <span class=\"hljs-literal ngde\">true</span>,\n  <span class=\"hljs-attr ngde\">imports</span>: [<span class=\"hljs-title class_ ngde\">CommonModule</span>, <span class=\"hljs-title class_ ngde\">Preview</span>, <span class=\"hljs-title class_ ngde\">MatInputModule</span>],\n  <span class=\"hljs-attr ngde\">templateUrl</span>: <span class=\"hljs-string ngde\">\"./input-demo-basic.component.html\"</span>,\n  <span class=\"hljs-attr ngde\">styleUrls</span>: [<span class=\"hljs-string ngde\">\"./input-demo-basic.component.scss\"</span>],\n})\n<span class=\"hljs-keyword ngde\">export</span> <span class=\"hljs-keyword ngde\">class</span> <span class=\"hljs-title class_ ngde\">InputDemoBasic</span> {}\n</code></pre>");

/***/ }),

/***/ 96758:
/*!********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-input/assets/InputDemoError/HTML/Asset1.html ***!
  \********************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"html\" class=\"hljs language-html ngde\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> [<span class=\"hljs-attr ngde\">nextElement</span>]=<span class=\"hljs-string ngde\">\"false\"</span> [<span class=\"hljs-attr ngde\">fullWidth</span>]=<span class=\"hljs-string ngde\">\"true\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">form</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-form\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-form-field</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-full-width\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-label</span>></span>Email<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-label</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">input</span>\n        <span class=\"hljs-attr ngde\">type</span>=<span class=\"hljs-string ngde\">\"email\"</span>\n        <span class=\"hljs-attr ngde\">matInput</span>\n        [<span class=\"hljs-attr ngde\">formControl</span>]=<span class=\"hljs-string ngde\">\"emailFormControl\"</span>\n        <span class=\"hljs-attr ngde\">placeholder</span>=<span class=\"hljs-string ngde\">\"Ex. pat@example.com\"</span>\n      /></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-error</span>\n        *<span class=\"hljs-attr ngde\">ngIf</span>=<span class=\"hljs-string ngde\">\"emailFormControl.hasError('email') &#x26;&#x26; !emailFormControl.hasError('required')\"</span>\n      ></span>\n        Please enter a valid email address\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-error</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-error</span> *<span class=\"hljs-attr ngde\">ngIf</span>=<span class=\"hljs-string ngde\">\"emailFormControl.hasError('required')\"</span>></span>\n        Email is <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">strong</span>></span>required<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">strong</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-error</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-form-field</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">form</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n</code></pre>");

/***/ }),

/***/ 8344:
/*!**************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-input/assets/InputDemoError/TypeScript/Asset0.html ***!
  \**************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">CommonModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/common\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Component</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/core\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">FormControl</span>, <span class=\"hljs-title class_ ngde\">ReactiveFormsModule</span>, <span class=\"hljs-title class_ ngde\">Validators</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/forms\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatInputModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/input\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Preview</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"src/app/features/preview/preview.component\"</span>;\n\n<span class=\"hljs-meta ngde\">@Component</span>({\n  <span class=\"hljs-attr ngde\">selector</span>: <span class=\"hljs-string ngde\">\"app-input-demo-error\"</span>,\n  <span class=\"hljs-attr ngde\">standalone</span>: <span class=\"hljs-literal ngde\">true</span>,\n  <span class=\"hljs-attr ngde\">imports</span>: [<span class=\"hljs-title class_ ngde\">CommonModule</span>, <span class=\"hljs-title class_ ngde\">Preview</span>, <span class=\"hljs-title class_ ngde\">MatInputModule</span>, <span class=\"hljs-title class_ ngde\">ReactiveFormsModule</span>],\n  <span class=\"hljs-attr ngde\">templateUrl</span>: <span class=\"hljs-string ngde\">\"./input-demo-error.component.html\"</span>,\n  <span class=\"hljs-attr ngde\">styleUrls</span>: [<span class=\"hljs-string ngde\">\"./input-demo-error.component.scss\"</span>],\n})\n<span class=\"hljs-keyword ngde\">export</span> <span class=\"hljs-keyword ngde\">class</span> <span class=\"hljs-title class_ ngde\">InputDemoError</span> {\n  emailFormControl = <span class=\"hljs-keyword ngde\">new</span> <span class=\"hljs-title class_ ngde\">FormControl</span>(<span class=\"hljs-string ngde\">\"\"</span>, [\n    <span class=\"hljs-title class_ ngde\">Validators</span>.<span class=\"hljs-property ngde\">required</span>,\n    <span class=\"hljs-title class_ ngde\">Validators</span>.<span class=\"hljs-property ngde\">email</span>,\n  ]);\n}\n</code></pre>");

/***/ }),

/***/ 16329:
/*!*******************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-input/assets/InputDemoHint/HTML/Asset1.html ***!
  \*******************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"html\" class=\"hljs language-html ngde\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> [<span class=\"hljs-attr ngde\">nextElement</span>]=<span class=\"hljs-string ngde\">\"false\"</span> [<span class=\"hljs-attr ngde\">fullWidth</span>]=<span class=\"hljs-string ngde\">\"true\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">form</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-form\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-form-field</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-full-width\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-label</span>></span>Message<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-label</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">input</span>\n        <span class=\"hljs-attr ngde\">matInput</span>\n        #<span class=\"hljs-attr ngde\">message</span>\n        <span class=\"hljs-attr ngde\">maxlength</span>=<span class=\"hljs-string ngde\">\"256\"</span>\n        <span class=\"hljs-attr ngde\">placeholder</span>=<span class=\"hljs-string ngde\">\"Ex. I need help with...\"</span>\n      /></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-hint</span> <span class=\"hljs-attr ngde\">align</span>=<span class=\"hljs-string ngde\">\"start\"</span>\n        ></span><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">strong</span>></span>Don't disclose personal info<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">strong</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-hint</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-hint</span> <span class=\"hljs-attr ngde\">align</span>=<span class=\"hljs-string ngde\">\"end\"</span>></span>{{message.value.length}} / 256<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-hint</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-form-field</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">form</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n</code></pre>");

/***/ }),

/***/ 80194:
/*!*******************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-input/assets/InputDemoHint/SCSS/Asset2.html ***!
  \*******************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"scss\" class=\"hljs language-scss ngde\"><span class=\"hljs-selector-class ngde\">.example-form</span> {\n  <span class=\"hljs-attribute ngde\">min-width</span>: <span class=\"hljs-number ngde\">150px</span>;\n  <span class=\"hljs-attribute ngde\">max-width</span>: <span class=\"hljs-number ngde\">500px</span>;\n  <span class=\"hljs-attribute ngde\">width</span>: <span class=\"hljs-number ngde\">100%</span>;\n}\n\n<span class=\"hljs-selector-class ngde\">.example-full-width</span> {\n  <span class=\"hljs-attribute ngde\">width</span>: <span class=\"hljs-number ngde\">100%</span>;\n}\n</code></pre>");

/***/ }),

/***/ 1080:
/*!*************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-input/assets/InputDemoHint/TypeScript/Asset0.html ***!
  \*************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Component</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/core\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">CommonModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/common\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatInputModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/input\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Preview</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"src/app/features/preview/preview.component\"</span>;\n\n<span class=\"hljs-meta ngde\">@Component</span>({\n  <span class=\"hljs-attr ngde\">selector</span>: <span class=\"hljs-string ngde\">\"app-input-demo-hint\"</span>,\n  <span class=\"hljs-attr ngde\">standalone</span>: <span class=\"hljs-literal ngde\">true</span>,\n  <span class=\"hljs-attr ngde\">imports</span>: [<span class=\"hljs-title class_ ngde\">CommonModule</span>, <span class=\"hljs-title class_ ngde\">Preview</span>, <span class=\"hljs-title class_ ngde\">MatInputModule</span>],\n  <span class=\"hljs-attr ngde\">templateUrl</span>: <span class=\"hljs-string ngde\">\"./input-demo-hint.component.html\"</span>,\n  <span class=\"hljs-attr ngde\">styleUrls</span>: [<span class=\"hljs-string ngde\">\"./input-demo-hint.component.scss\"</span>],\n})\n<span class=\"hljs-keyword ngde\">export</span> <span class=\"hljs-keyword ngde\">class</span> <span class=\"hljs-title class_ ngde\">InputDemoHint</span> {}\n</code></pre>");

/***/ }),

/***/ 57709:
/*!****************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-input/index.html ***!
  \****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<h1 id=\"input\" class=\"ngde\">Input<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/angular-materiel/material-input#input\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1><h2 id=\"basic-inputs\" class=\"ngde\">Basic Inputs<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/angular-materiel/material-input#basic-inputs\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><ng-doc-demo componentname=\"InputDemoBasic\" indexable=\"false\" class=\"ngde\"><div id=\"options\" class=\"ngde\">{}</div></ng-doc-demo><h2 id=\"inputs-with-a-hint\" class=\"ngde\">Inputs with a hint<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/angular-materiel/material-input#inputs-with-a-hint\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><ng-doc-demo componentname=\"InputDemoHint\" indexable=\"false\" class=\"ngde\"><div id=\"options\" class=\"ngde\">{}</div></ng-doc-demo><h2 id=\"input-with-error-messages\" class=\"ngde\">Input with error messages<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/angular-materiel/material-input#input-with-error-messages\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><ng-doc-demo componentname=\"InputDemoError\" indexable=\"false\" class=\"ngde\"><div id=\"options\" class=\"ngde\">{}</div></ng-doc-demo>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_guides_angular-materiel_material-input_module_ts.js.map