"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_api_api_m-ui_classes_MIconsService_module_ts"],{

/***/ 27244:
/*!*********************************************************************!*\
  !*** ./.ng-doc/m-docs/api/api/m-ui/classes/MIconsService/module.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_api_api_m_ui_classes_MIconsService_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/api/api/m-ui/classes/MIconsService/index.html */ 54163);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);







class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = undefined;
    this.pageType = 'api';
    this.pageContent = _raw_loader_ng_doc_m_docs_api_api_m_ui_classes_MIconsService_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
    this.demo = undefined;
    this.demoAssets = undefined;
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'MIconsService'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
})();

/***/ }),

/***/ 54163:
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/api/api/m-ui/classes/MIconsService/index.html ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<header class=\"ngde\"><div class=\"ng-doc-page-tags ngde\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"ng-doc-scope\">m-ui</span> <span class=\"ng-doc-inline-delimiter ngde\" indexable=\"false\">/</span> <span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Class\">Class</span> <span class=\"ng-doc-inline-delimiter ngde\" indexable=\"false\">/</span><div class=\"ng-doc-decorators-group ngde\" indexable=\"false\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Injectable\">@Injectable</span></div></div><h1 id=\"miconsservice\" class=\"ngde\">MIconsService<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MIconsService#miconsservice\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1></header><section class=\"ngde\"><p class=\"ngde\">Service to register custom icons for use throughout the application. This service leverages Angular Material's MatIconRegistry and DomSanitizer to securely register and manage SVG icons, providing a centralized approach to icon registration.</p></section><section class=\"ngde\"><h2 id=\"constructor\" class=\"ngde\">Constructor<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MIconsService#constructor\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ng-doc-method-table ngde\"><tbody class=\"ngde\"><tr class=\"ngde\"><td class=\"ngde\"><p class=\"ngde\">Initializes the MIconsService, injecting dependencies for managing SVG icons. The constructor receives the Angular Material MatIconRegistry and Angular’s DomSanitizer to securely handle SVG icon URLs.</p></td></tr><tr class=\"ngde\"><td class=\"ngde\"><h5 class=\"no-anchor ngde\" indexable=\"false\">Presentation</h5><pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-title function_ ngde\">constructor</span>(<span class=\"hljs-params ngde\">\n\t<span class=\"hljs-keyword ngde\">private</span> iconRegistry: MatIconRegistry, \n\t<span class=\"hljs-keyword ngde\">private</span> sanitizer: DomSanitizer\n</span>): <span class=\"hljs-title class_ ngde\"><a href=\"/api/m-ui/classes/MIconsService\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Injectable\" class=\"ngde\">MIconsService</a></span>;</code></pre></td></tr><tr class=\"ngde\"><td class=\"ngde\"><h5 class=\"no-anchor ngde\" indexable=\"false\">Parameters</h5><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ng-doc-parameters-table ngde\"><thead class=\"ngde\"><tr indexable=\"false\" class=\"ngde\"><th class=\"ng-doc-parameters-table-name ngde\">Name</th><th class=\"ng-doc-parameters-table-type ngde\">Type</th><th class=\"ng-doc-parameters-table-description ngde\">Description</th></tr></thead><tbody class=\"ngde\"><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">iconRegistry<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">MatIconRegistry</code></td><td class=\"ngde\"><p class=\"ngde\">Injected Angular Material's icon registry used to register and manage SVG icons.</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">sanitizer<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">DomSanitizer</code></td><td class=\"ngde\"><p class=\"ngde\">Injected Angular DOM sanitizer to bypass security restrictions on resource URLs for SVG icons.</p></td></tr></tbody></table></div></td></tr></tbody></table></div></section><section class=\"ngde\"><h2 id=\"methods\" class=\"ngde\">Methods<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MIconsService#methods\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ng-doc-method-table ngde\"><thead class=\"ngde\"><tr class=\"ngde\"><th indexable=\"false\" class=\"ngde\"><h3 id=\"registericons\" class=\"ngde\">registerIcons()<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MIconsService#registericons\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h3><div class=\"ng-doc-node-details ngde\"></div></th></tr></thead><tbody class=\"ngde\"><tr class=\"ngde\"><td class=\"ngde\"><p class=\"ngde\">registerIcons</p><p class=\"ngde\">Registers a list of icons with Angular Material's MatIconRegistry. This method iterates over the provided icon mappings and registers each icon with MatIconRegistry using DomSanitizer to securely handle icon paths.</p></td></tr><tr class=\"ngde\"><td class=\"ngde\"><h5 class=\"no-anchor ngde\" indexable=\"false\">Presentation</h5><pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-title function_ ngde\">registerIcons</span>(<span class=\"hljs-attr ngde\">icon</span>: <span class=\"hljs-title class_ ngde\"><a href=\"/api/m-ui/interfaces/IconMapping\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Interface\" class=\"ngde\">IconMapping</a></span>[]): <span class=\"hljs-built_in ngde\">void</span>;</code></pre></td></tr><tr class=\"ngde\"><td class=\"ngde\"><h5 class=\"no-anchor ngde\" indexable=\"false\">Parameters</h5><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ng-doc-parameters-table ngde\"><thead class=\"ngde\"><tr indexable=\"false\" class=\"ngde\"><th class=\"ng-doc-parameters-table-name ngde\">Name</th><th class=\"ng-doc-parameters-table-type ngde\">Type</th><th class=\"ng-doc-parameters-table-description ngde\">Description</th></tr></thead><tbody class=\"ngde\"><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">icon<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde ng-doc-code-with-link\" class=\"ngde\"><a href=\"/api/m-ui/interfaces/IconMapping\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Interface\" class=\"ngde\">IconMapping</a>[]</code></td><td class=\"ngde\"><p class=\"ngde\">An array of IconMapping objects, each representing an icon with a unique <code class=\"ngde\">type</code> and <code class=\"ngde\">name</code> that maps to an SVG file.</p></td></tr></tbody></table></div><h5 class=\"no-anchor ngde\" indexable=\"false\">Returns</h5><p class=\"ngde\"><code indexable=\"false\" class=\"ngde\">void</code></p></td></tr></tbody></table></div></section>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_api_api_m-ui_classes_MIconsService_module_ts.js.map