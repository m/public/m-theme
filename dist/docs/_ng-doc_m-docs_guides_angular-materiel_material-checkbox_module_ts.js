"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_guides_angular-materiel_material-checkbox_module_ts"],{

/***/ 32884:
/*!**************************************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/angular-materiel/material-checkbox/component-assets.ts ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   "demoAssets": () => (/* binding */ demoAssets)
/* harmony export */ });
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_assets_CheckboxDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-checkbox/assets/CheckboxDemo/TypeScript/Asset0.html */ 28791);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_assets_CheckboxDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-checkbox/assets/CheckboxDemo/HTML/Asset1.html */ 5979);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_assets_CheckboxDemo_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-checkbox/assets/CheckboxDemo/SCSS/Asset2.html */ 38275);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_assets_CheckboxDemoBasic_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-checkbox/assets/CheckboxDemoBasic/TypeScript/Asset0.html */ 19953);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_assets_CheckboxDemoBasic_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-checkbox/assets/CheckboxDemoBasic/HTML/Asset1.html */ 30873);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_assets_CheckboxDemoBasic_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-checkbox/assets/CheckboxDemoBasic/SCSS/Asset2.html */ 42334);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_assets_CheckboxDemoForm_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-checkbox/assets/CheckboxDemoForm/TypeScript/Asset0.html */ 61586);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_assets_CheckboxDemoForm_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-checkbox/assets/CheckboxDemoForm/HTML/Asset1.html */ 53944);








const demoAssets = {
  CheckboxDemo: [{
    title: 'TypeScript',
    codeType: 'TypeScript',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_assets_CheckboxDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__["default"]
  }, {
    title: 'HTML',
    codeType: 'HTML',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_assets_CheckboxDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__["default"]
  }, {
    title: 'SCSS',
    codeType: 'SCSS',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_assets_CheckboxDemo_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_2__["default"]
  }],
  CheckboxDemoBasic: [{
    title: 'TypeScript',
    codeType: 'TypeScript',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_assets_CheckboxDemoBasic_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_3__["default"]
  }, {
    title: 'HTML',
    codeType: 'HTML',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_assets_CheckboxDemoBasic_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_4__["default"]
  }, {
    title: 'SCSS',
    codeType: 'SCSS',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_assets_CheckboxDemoBasic_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_5__["default"]
  }],
  CheckboxDemoForm: [{
    title: 'TypeScript',
    codeType: 'TypeScript',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_assets_CheckboxDemoForm_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_6__["default"]
  }, {
    title: 'HTML',
    codeType: 'HTML',
    code: _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_assets_CheckboxDemoForm_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_7__["default"]
  }]
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (demoAssets);

/***/ }),

/***/ 67444:
/*!****************************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/angular-materiel/material-checkbox/module.ts ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/angular-materiel/material-checkbox/index.html */ 64183);
/* harmony import */ var _playgrounds__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./playgrounds */ 68427);
/* harmony import */ var src_app_docs_angular_material_material_checkbox_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/docs/angular-material/material-checkbox/ng-doc.dependencies */ 72026);
/* harmony import */ var _ng_doc_m_docs_guides_angular_materiel_material_checkbox_component_assets__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! .ng-doc/m-docs/guides/angular-materiel/material-checkbox/component-assets */ 32884);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_docs_angular_material_material_checkbox_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/docs/angular-material/material-checkbox/ng-doc.module */ 17453);




// noinspection ES6UnusedImports


// noinspection ES6UnusedImports





class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = '';
    this.pageType = 'guide';
    this.pageContent = _raw_loader_ng_doc_m_docs_guides_angular_materiel_material_checkbox_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
    this.dependencies = src_app_docs_angular_material_material_checkbox_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"];
    this.demoAssets = _ng_doc_m_docs_guides_angular_materiel_material_checkbox_component_assets__WEBPACK_IMPORTED_MODULE_3__["default"];
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, src_app_docs_angular_material_material_checkbox_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"].module, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'Checkbox'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, src_app_docs_angular_material_material_checkbox_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__.MaterialCheckboxPageModule, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
})();

/***/ }),

/***/ 68427:
/*!*********************************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/angular-materiel/material-checkbox/playgrounds.ts ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PlaygroundsModule": () => (/* binding */ PlaygroundsModule)
/* harmony export */ });
/* harmony import */ var src_app_docs_angular_material_material_checkbox_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/docs/angular-material/material-checkbox/ng-doc.dependencies */ 72026);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_docs_angular_material_material_checkbox_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/docs/angular-material/material-checkbox/ng-doc.module */ 17453);
// noinspection ES6UnusedImports




class PlaygroundsModule {
  static #_ = this.ɵfac = function PlaygroundsModule_Factory(t) {
    return new (t || PlaygroundsModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: PlaygroundsModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, src_app_docs_angular_material_material_checkbox_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__["default"].module]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](PlaygroundsModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, src_app_docs_angular_material_material_checkbox_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__.MaterialCheckboxPageModule]
  });
})();

/***/ }),

/***/ 97115:
/*!**************************************************************************************************************!*\
  !*** ./src/app/docs/angular-material/material-checkbox/checkbox-demo-basic/checkbox-demo-basic.component.ts ***!
  \**************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CheckboxDemoBasic": () => (/* binding */ CheckboxDemoBasic)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/checkbox */ 44792);
/* harmony import */ var src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/features/preview/preview.component */ 55355);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);








function CheckboxDemoBasic_li_12_Template(rf, ctx) {
  if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "li")(1, "mat-checkbox", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function CheckboxDemoBasic_li_12_Template_mat_checkbox_ngModelChange_1_listener($event) {
      const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r3);
      const subtask_r1 = restoredCtx.$implicit;
      return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵresetView"](subtask_r1.completed = $event);
    })("ngModelChange", function CheckboxDemoBasic_li_12_Template_mat_checkbox_ngModelChange_1_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r3);
      const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵresetView"](ctx_r4.updateAllComplete());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
  }
  if (rf & 2) {
    const subtask_r1 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", subtask_r1.completed)("color", subtask_r1.color);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", subtask_r1.name, " ");
  }
}
class CheckboxDemoBasic {
  constructor() {
    this.task = {
      name: 'Indeterminate',
      completed: false,
      color: 'primary',
      subtasks: [{
        name: 'Primary',
        completed: false,
        color: 'primary'
      }, {
        name: 'Accent',
        completed: false,
        color: 'accent'
      }, {
        name: 'Warn',
        completed: false,
        color: 'warn'
      }]
    };
    this.allComplete = false;
  }
  updateAllComplete() {
    this.allComplete = this.task.subtasks != null && this.task.subtasks.every(t => t.completed);
  }
  someComplete() {
    if (this.task.subtasks == null) {
      return false;
    }
    return this.task.subtasks.filter(t => t.completed).length > 0 && !this.allComplete;
  }
  setAll(completed) {
    this.allComplete = completed;
    if (this.task.subtasks == null) {
      return;
    }
    this.task.subtasks.forEach(t => t.completed = completed);
  }
  static #_ = this.ɵfac = function CheckboxDemoBasic_Factory(t) {
    return new (t || CheckboxDemoBasic)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
    type: CheckboxDemoBasic,
    selectors: [["app-checkbox-demo-basic"]],
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵStandaloneFeature"]],
    decls: 13,
    vars: 8,
    consts: [[3, "nextElement", "fullWidth"], [1, "example-section"], [1, "example-margin"], [1, "example-margin", 3, "disabled"], [1, "example-list-section"], [1, "example-margin", 3, "checked", "color", "indeterminate", "change"], [4, "ngFor", "ngForOf"], [3, "ngModel", "color", "ngModelChange"]],
    template: function CheckboxDemoBasic_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "app-preview", 0)(1, "section", 1)(2, "mat-checkbox", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Check me!");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "mat-checkbox", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, "Disabled");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "section", 1)(7, "span", 4)(8, "mat-checkbox", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function CheckboxDemoBasic_Template_mat_checkbox_change_8_listener($event) {
          return ctx.setAll($event.checked);
        });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "span", 4)(11, "ul");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](12, CheckboxDemoBasic_li_12_Template, 3, 3, "li", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("nextElement", false)("fullWidth", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("disabled", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("checked", ctx.allComplete)("color", ctx.task.color)("indeterminate", ctx.someComplete());
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", ctx.task.name, " ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.task.subtasks);
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, _angular_common__WEBPACK_IMPORTED_MODULE_2__.NgForOf, src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__.Preview, _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_3__.MatCheckboxModule, _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_3__.MatCheckbox, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.NgModel],
    styles: [".example-section[_ngcontent-%COMP%] {\n  margin: 12px 0;\n}\n\n.example-margin[_ngcontent-%COMP%] {\n  margin: 0 12px;\n}\n\nul[_ngcontent-%COMP%] {\n  list-style-type: none;\n  margin-top: 4px;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvZG9jcy9hbmd1bGFyLW1hdGVyaWFsL21hdGVyaWFsLWNoZWNrYm94L2NoZWNrYm94LWRlbW8tYmFzaWMvY2hlY2tib3gtZGVtby1iYXNpYy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGNBQUE7QUFDRDs7QUFFQTtFQUNDLGNBQUE7QUFDRDs7QUFFQTtFQUNDLHFCQUFBO0VBQ0EsZUFBQTtBQUNEIiwic291cmNlc0NvbnRlbnQiOlsiLmV4YW1wbGUtc2VjdGlvbiB7XHJcblx0bWFyZ2luOiAxMnB4IDA7XHJcbn1cclxuXHJcbi5leGFtcGxlLW1hcmdpbiB7XHJcblx0bWFyZ2luOiAwIDEycHg7XHJcbn1cclxuXHJcbnVsIHtcclxuXHRsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XHJcblx0bWFyZ2luLXRvcDogNHB4O1xyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0= */"]
  });
}

/***/ }),

/***/ 73037:
/*!************************************************************************************************************!*\
  !*** ./src/app/docs/angular-material/material-checkbox/checkbox-demo-form/checkbox-demo-form.component.ts ***!
  \************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CheckboxDemoForm": () => (/* binding */ CheckboxDemoForm)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/checkbox */ 44792);
/* harmony import */ var src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/features/preview/preview.component */ 55355);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);








class CheckboxDemoForm {
  constructor(_formBuilder) {
    this._formBuilder = _formBuilder;
    this.toppings = this._formBuilder.group({
      pepperoni: false,
      extracheese: false,
      mushroom: false
    });
  }
  static #_ = this.ɵfac = function CheckboxDemoForm_Factory(t) {
    return new (t || CheckboxDemoForm)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_forms__WEBPACK_IMPORTED_MODULE_2__.FormBuilder));
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
    type: CheckboxDemoForm,
    selectors: [["app-checkbox-demo-form"]],
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵStandaloneFeature"]],
    decls: 18,
    vars: 7,
    consts: [[3, "nextElement", "fullWidth"], [1, "example-section", 3, "formGroup"], ["formControlName", "pepperoni"], ["formControlName", "extracheese"], ["formControlName", "mushroom"]],
    template: function CheckboxDemoForm_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "app-preview", 0)(1, "section", 1)(2, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Select your toppings:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "p")(5, "mat-checkbox", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "Pepperoni");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "p")(8, "mat-checkbox", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](9, "Extra Cheese");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "p")(11, "mat-checkbox", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](12, "Mushroom");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](13, "section", 1)(14, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](15, "You chose:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipe"](17, "json");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("nextElement", false)("fullWidth", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("formGroup", ctx.toppings);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](12);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("formGroup", ctx.toppings);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"](" ", _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵpipeBind1"](17, 5, ctx.toppings.value), " ");
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, _angular_common__WEBPACK_IMPORTED_MODULE_3__.JsonPipe, src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__.Preview, _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_4__.MatCheckboxModule, _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_4__.MatCheckbox, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.ReactiveFormsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.NgControlStatusGroup, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.FormGroupDirective, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.FormControlName],
    styles: ["\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZVJvb3QiOiIifQ== */"]
  });
}

/***/ }),

/***/ 94173:
/*!**************************************************************************************************!*\
  !*** ./src/app/docs/angular-material/material-checkbox/checkbox-demo/checkbox-demo.component.ts ***!
  \**************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CheckboxDemo": () => (/* binding */ CheckboxDemo)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_material_card__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/card */ 82156);
/* harmony import */ var _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/checkbox */ 44792);
/* harmony import */ var _angular_material_radio__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/radio */ 52922);
/* harmony import */ var src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/features/preview/preview.component */ 55355);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);










class CheckboxDemo {
  constructor() {
    this.checked = false;
    this.indeterminate = false;
    this.labelPosition = 'after';
    this.disabled = false;
  }
  static #_ = this.ɵfac = function CheckboxDemo_Factory(t) {
    return new (t || CheckboxDemo)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
    type: CheckboxDemo,
    selectors: [["app-checkbox-demo"]],
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵStandaloneFeature"]],
    decls: 23,
    vars: 10,
    consts: [["title", "Checkbox configuration", 3, "nextElement", "fullWidth"], [1, "example-section"], [1, "example-margin", 3, "ngModel", "ngModelChange"], [1, "example-margin"], [3, "ngModel", "ngModelChange"], ["value", "after", 1, "example-margin"], ["value", "before", 1, "example-margin"], [1, "example-h2"], [1, "example-margin", 3, "ngModel", "indeterminate", "labelPosition", "disabled", "ngModelChange", "indeterminateChange"]],
    template: function CheckboxDemo_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "app-preview", 0)(1, "section", 1)(2, "mat-checkbox", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function CheckboxDemo_Template_mat_checkbox_ngModelChange_2_listener($event) {
          return ctx.checked = $event;
        });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Checked");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](4, "mat-checkbox", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function CheckboxDemo_Template_mat_checkbox_ngModelChange_4_listener($event) {
          return ctx.indeterminate = $event;
        });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](5, "Indeterminate");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](6, "section", 1)(7, "label", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8, "Align:");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "mat-radio-group", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function CheckboxDemo_Template_mat_radio_group_ngModelChange_9_listener($event) {
          return ctx.labelPosition = $event;
        });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](10, "mat-radio-button", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](11, "After");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](12, "mat-radio-button", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](13, "Before");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](14, "section", 1)(15, "mat-checkbox", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function CheckboxDemo_Template_mat_checkbox_ngModelChange_15_listener($event) {
          return ctx.disabled = $event;
        });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](16, "Disabled");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "div")(18, "h2", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](19, "Result");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](20, "section", 1)(21, "mat-checkbox", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("ngModelChange", function CheckboxDemo_Template_mat_checkbox_ngModelChange_21_listener($event) {
          return ctx.checked = $event;
        })("indeterminateChange", function CheckboxDemo_Template_mat_checkbox_indeterminateChange_21_listener($event) {
          return ctx.indeterminate = $event;
        });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](22, " I'm a checkbox ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()()();
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("nextElement", false)("fullWidth", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.checked);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.indeterminate);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.labelPosition);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.disabled);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngModel", ctx.checked)("indeterminate", ctx.indeterminate)("labelPosition", ctx.labelPosition)("disabled", ctx.disabled);
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, _angular_material_radio__WEBPACK_IMPORTED_MODULE_3__.MatRadioModule, _angular_material_radio__WEBPACK_IMPORTED_MODULE_3__.MatRadioGroup, _angular_material_radio__WEBPACK_IMPORTED_MODULE_3__.MatRadioButton, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.NgControlStatus, _angular_forms__WEBPACK_IMPORTED_MODULE_4__.NgModel, _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_5__.MatCheckboxModule, _angular_material_checkbox__WEBPACK_IMPORTED_MODULE_5__.MatCheckbox, _angular_material_card__WEBPACK_IMPORTED_MODULE_6__.MatCardModule, src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__.Preview],
    styles: [".example-h2[_ngcontent-%COMP%] {\n  margin: 10px;\n}\n\n.example-section[_ngcontent-%COMP%] {\n  display: flex;\n  align-content: center;\n  align-items: center;\n  height: 60px;\n}\n\n.example-margin[_ngcontent-%COMP%] {\n  margin: 0 10px;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvZG9jcy9hbmd1bGFyLW1hdGVyaWFsL21hdGVyaWFsLWNoZWNrYm94L2NoZWNrYm94LWRlbW8vY2hlY2tib3gtZGVtby5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLFlBQUE7QUFDRDs7QUFFQTtFQUNDLGFBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQUNEOztBQUVBO0VBQ0MsY0FBQTtBQUNEIiwic291cmNlc0NvbnRlbnQiOlsiLmV4YW1wbGUtaDIge1xyXG5cdG1hcmdpbjogMTBweDtcclxufVxyXG5cclxuLmV4YW1wbGUtc2VjdGlvbiB7XHJcblx0ZGlzcGxheTogZmxleDtcclxuXHRhbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcblx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHRoZWlnaHQ6IDYwcHg7XHJcbn1cclxuXHJcbi5leGFtcGxlLW1hcmdpbiB7XHJcblx0bWFyZ2luOiAwIDEwcHg7XHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIifQ== */"]
  });
}

/***/ }),

/***/ 72026:
/*!********************************************************************************!*\
  !*** ./src/app/docs/angular-material/material-checkbox/ng-doc.dependencies.ts ***!
  \********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _material_docs_material_checkbox_checkbox_demo_basic_checkbox_demo_basic_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @material-docs/material-checkbox/checkbox-demo-basic/checkbox-demo-basic.component */ 97115);
/* harmony import */ var _material_docs_material_checkbox_checkbox_demo_form_checkbox_demo_form_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @material-docs/material-checkbox/checkbox-demo-form/checkbox-demo-form.component */ 73037);
/* harmony import */ var src_app_docs_angular_material_material_checkbox_checkbox_demo_checkbox_demo_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/docs/angular-material/material-checkbox/checkbox-demo/checkbox-demo.component */ 94173);
/* harmony import */ var src_app_docs_angular_material_material_checkbox_ng_doc_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/docs/angular-material/material-checkbox/ng-doc.module */ 17453);




const MaterialCheckboxPageDependencies = {
  module: src_app_docs_angular_material_material_checkbox_ng_doc_module__WEBPACK_IMPORTED_MODULE_3__.MaterialCheckboxPageModule,
  // Add your demos that you are going to use in the page here
  demo: {
    CheckboxDemo: src_app_docs_angular_material_material_checkbox_checkbox_demo_checkbox_demo_component__WEBPACK_IMPORTED_MODULE_2__.CheckboxDemo,
    CheckboxDemoBasic: _material_docs_material_checkbox_checkbox_demo_basic_checkbox_demo_basic_component__WEBPACK_IMPORTED_MODULE_0__.CheckboxDemoBasic,
    CheckboxDemoForm: _material_docs_material_checkbox_checkbox_demo_form_checkbox_demo_form_component__WEBPACK_IMPORTED_MODULE_1__.CheckboxDemoForm
  }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MaterialCheckboxPageDependencies);

/***/ }),

/***/ 17453:
/*!**************************************************************************!*\
  !*** ./src/app/docs/angular-material/material-checkbox/ng-doc.module.ts ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MaterialCheckboxPageModule": () => (/* binding */ MaterialCheckboxPageModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);


class MaterialCheckboxPageModule {
  static #_ = this.ɵfac = function MaterialCheckboxPageModule_Factory(t) {
    return new (t || MaterialCheckboxPageModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
    type: MaterialCheckboxPageModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](MaterialCheckboxPageModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
})();

/***/ }),

/***/ 30873:
/*!**************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-checkbox/assets/CheckboxDemoBasic/HTML/Asset1.html ***!
  \**************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"html\" class=\"hljs language-html ngde\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> [<span class=\"hljs-attr ngde\">nextElement</span>]=<span class=\"hljs-string ngde\">\"false\"</span> [<span class=\"hljs-attr ngde\">fullWidth</span>]=<span class=\"hljs-string ngde\">\"true\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">section</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-section\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-checkbox</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-margin\"</span>></span>Check me!<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-checkbox</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-checkbox</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-margin\"</span> [<span class=\"hljs-attr ngde\">disabled</span>]=<span class=\"hljs-string ngde\">\"true\"</span>\n      ></span>Disabled&#x3C;/mat-checkbox\n    >\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">section</span>></span>\n\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">section</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-section\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">span</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-list-section\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-checkbox</span>\n        <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-margin\"</span>\n        [<span class=\"hljs-attr ngde\">checked</span>]=<span class=\"hljs-string ngde\">\"allComplete\"</span>\n        [<span class=\"hljs-attr ngde\">color</span>]=<span class=\"hljs-string ngde\">\"task.color\"</span>\n        [<span class=\"hljs-attr ngde\">indeterminate</span>]=<span class=\"hljs-string ngde\">\"someComplete()\"</span>\n        (<span class=\"hljs-attr ngde\">change</span>)=<span class=\"hljs-string ngde\">\"setAll($event.checked)\"</span>\n      ></span>\n        {{ task.name }}\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-checkbox</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">span</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">span</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-list-section\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">ul</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">li</span> *<span class=\"hljs-attr ngde\">ngFor</span>=<span class=\"hljs-string ngde\">\"let subtask of task.subtasks\"</span>></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-checkbox</span>\n            [(<span class=\"hljs-attr ngde\">ngModel</span>)]=<span class=\"hljs-string ngde\">\"subtask.completed\"</span>\n            [<span class=\"hljs-attr ngde\">color</span>]=<span class=\"hljs-string ngde\">\"subtask.color\"</span>\n            (<span class=\"hljs-attr ngde\">ngModelChange</span>)=<span class=\"hljs-string ngde\">\"updateAllComplete()\"</span>\n          ></span>\n            {{ subtask.name }}\n          <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-checkbox</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">li</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">ul</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">span</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">section</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n</code></pre>");

/***/ }),

/***/ 42334:
/*!**************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-checkbox/assets/CheckboxDemoBasic/SCSS/Asset2.html ***!
  \**************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"scss\" class=\"hljs language-scss ngde\"><span class=\"hljs-selector-class ngde\">.example-section</span> {\n  <span class=\"hljs-attribute ngde\">margin</span>: <span class=\"hljs-number ngde\">12px</span> <span class=\"hljs-number ngde\">0</span>;\n}\n\n<span class=\"hljs-selector-class ngde\">.example-margin</span> {\n  <span class=\"hljs-attribute ngde\">margin</span>: <span class=\"hljs-number ngde\">0</span> <span class=\"hljs-number ngde\">12px</span>;\n}\n\n<span class=\"hljs-selector-tag ngde\">ul</span> {\n  <span class=\"hljs-attribute ngde\">list-style-type</span>: none;\n  <span class=\"hljs-attribute ngde\">margin-top</span>: <span class=\"hljs-number ngde\">4px</span>;\n}\n</code></pre>");

/***/ }),

/***/ 19953:
/*!********************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-checkbox/assets/CheckboxDemoBasic/TypeScript/Asset0.html ***!
  \********************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">CommonModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/common\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Component</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/core\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">FormsModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/forms\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatCheckboxModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/checkbox\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">ThemePalette</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/core\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Preview</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"src/app/features/preview/preview.component\"</span>;\n\n<span class=\"hljs-keyword ngde\">export</span> <span class=\"hljs-keyword ngde\">interface</span> <span class=\"hljs-title class_ ngde\">Task</span> {\n  <span class=\"hljs-attr ngde\">name</span>: <span class=\"hljs-built_in ngde\">string</span>;\n  <span class=\"hljs-attr ngde\">completed</span>: <span class=\"hljs-built_in ngde\">boolean</span>;\n  <span class=\"hljs-attr ngde\">color</span>: <span class=\"hljs-title class_ ngde\">ThemePalette</span>;\n  subtasks?: <span class=\"hljs-title class_ ngde\">Task</span>[];\n}\n\n<span class=\"hljs-meta ngde\">@Component</span>({\n  <span class=\"hljs-attr ngde\">selector</span>: <span class=\"hljs-string ngde\">\"app-checkbox-demo-basic\"</span>,\n  <span class=\"hljs-attr ngde\">standalone</span>: <span class=\"hljs-literal ngde\">true</span>,\n  <span class=\"hljs-attr ngde\">imports</span>: [<span class=\"hljs-title class_ ngde\">CommonModule</span>, <span class=\"hljs-title class_ ngde\">Preview</span>, <span class=\"hljs-title class_ ngde\">MatCheckboxModule</span>, <span class=\"hljs-title class_ ngde\">FormsModule</span>],\n  <span class=\"hljs-attr ngde\">templateUrl</span>: <span class=\"hljs-string ngde\">\"./checkbox-demo-basic.component.html\"</span>,\n  <span class=\"hljs-attr ngde\">styleUrls</span>: [<span class=\"hljs-string ngde\">\"./checkbox-demo-basic.component.scss\"</span>],\n})\n<span class=\"hljs-keyword ngde\">export</span> <span class=\"hljs-keyword ngde\">class</span> <span class=\"hljs-title class_ ngde\">CheckboxDemoBasic</span> {\n  <span class=\"hljs-attr ngde\">task</span>: <span class=\"hljs-title class_ ngde\">Task</span> = {\n    <span class=\"hljs-attr ngde\">name</span>: <span class=\"hljs-string ngde\">\"Indeterminate\"</span>,\n    <span class=\"hljs-attr ngde\">completed</span>: <span class=\"hljs-literal ngde\">false</span>,\n    <span class=\"hljs-attr ngde\">color</span>: <span class=\"hljs-string ngde\">\"primary\"</span>,\n    <span class=\"hljs-attr ngde\">subtasks</span>: [\n      { <span class=\"hljs-attr ngde\">name</span>: <span class=\"hljs-string ngde\">\"Primary\"</span>, <span class=\"hljs-attr ngde\">completed</span>: <span class=\"hljs-literal ngde\">false</span>, <span class=\"hljs-attr ngde\">color</span>: <span class=\"hljs-string ngde\">\"primary\"</span> },\n      { <span class=\"hljs-attr ngde\">name</span>: <span class=\"hljs-string ngde\">\"Accent\"</span>, <span class=\"hljs-attr ngde\">completed</span>: <span class=\"hljs-literal ngde\">false</span>, <span class=\"hljs-attr ngde\">color</span>: <span class=\"hljs-string ngde\">\"accent\"</span> },\n      { <span class=\"hljs-attr ngde\">name</span>: <span class=\"hljs-string ngde\">\"Warn\"</span>, <span class=\"hljs-attr ngde\">completed</span>: <span class=\"hljs-literal ngde\">false</span>, <span class=\"hljs-attr ngde\">color</span>: <span class=\"hljs-string ngde\">\"warn\"</span> },\n    ],\n  };\n\n  <span class=\"hljs-attr ngde\">allComplete</span>: <span class=\"hljs-built_in ngde\">boolean</span> = <span class=\"hljs-literal ngde\">false</span>;\n\n  <span class=\"hljs-title function_ ngde\">updateAllComplete</span>(<span class=\"hljs-params ngde\"></span>) {\n    <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">allComplete</span> =\n      <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">task</span>.<span class=\"hljs-property ngde\">subtasks</span> != <span class=\"hljs-literal ngde\">null</span> &#x26;&#x26;\n      <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">task</span>.<span class=\"hljs-property ngde\">subtasks</span>.<span class=\"hljs-title function_ ngde\">every</span>(<span class=\"hljs-function ngde\">(<span class=\"hljs-params ngde\">t</span>) =></span> t.<span class=\"hljs-property ngde\">completed</span>);\n  }\n\n  <span class=\"hljs-title function_ ngde\">someComplete</span>(): <span class=\"hljs-built_in ngde\">boolean</span> {\n    <span class=\"hljs-keyword ngde\">if</span> (<span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">task</span>.<span class=\"hljs-property ngde\">subtasks</span> == <span class=\"hljs-literal ngde\">null</span>) {\n      <span class=\"hljs-keyword ngde\">return</span> <span class=\"hljs-literal ngde\">false</span>;\n    }\n    <span class=\"hljs-keyword ngde\">return</span> (\n      <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">task</span>.<span class=\"hljs-property ngde\">subtasks</span>.<span class=\"hljs-title function_ ngde\">filter</span>(<span class=\"hljs-function ngde\">(<span class=\"hljs-params ngde\">t</span>) =></span> t.<span class=\"hljs-property ngde\">completed</span>).<span class=\"hljs-property ngde\">length</span> > <span class=\"hljs-number ngde\">0</span> &#x26;&#x26;\n      !<span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">allComplete</span>\n    );\n  }\n\n  <span class=\"hljs-title function_ ngde\">setAll</span>(<span class=\"hljs-params ngde\">completed: <span class=\"hljs-built_in ngde\">boolean</span></span>) {\n    <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">allComplete</span> = completed;\n    <span class=\"hljs-keyword ngde\">if</span> (<span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">task</span>.<span class=\"hljs-property ngde\">subtasks</span> == <span class=\"hljs-literal ngde\">null</span>) {\n      <span class=\"hljs-keyword ngde\">return</span>;\n    }\n    <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">task</span>.<span class=\"hljs-property ngde\">subtasks</span>.<span class=\"hljs-title function_ ngde\">forEach</span>(<span class=\"hljs-function ngde\">(<span class=\"hljs-params ngde\">t</span>) =></span> (t.<span class=\"hljs-property ngde\">completed</span> = completed));\n  }\n}\n</code></pre>");

/***/ }),

/***/ 53944:
/*!*************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-checkbox/assets/CheckboxDemoForm/HTML/Asset1.html ***!
  \*************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"html\" class=\"hljs language-html ngde\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> [<span class=\"hljs-attr ngde\">nextElement</span>]=<span class=\"hljs-string ngde\">\"false\"</span> [<span class=\"hljs-attr ngde\">fullWidth</span>]=<span class=\"hljs-string ngde\">\"true\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">section</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-section\"</span> [<span class=\"hljs-attr ngde\">formGroup</span>]=<span class=\"hljs-string ngde\">\"toppings\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">h4</span>></span>Select your toppings:<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">h4</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">p</span>></span><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-checkbox</span> <span class=\"hljs-attr ngde\">formControlName</span>=<span class=\"hljs-string ngde\">\"pepperoni\"</span>></span>Pepperoni<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-checkbox</span>></span><span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">p</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">p</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-checkbox</span> <span class=\"hljs-attr ngde\">formControlName</span>=<span class=\"hljs-string ngde\">\"extracheese\"</span>></span>Extra Cheese<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-checkbox</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">p</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">p</span>></span><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-checkbox</span> <span class=\"hljs-attr ngde\">formControlName</span>=<span class=\"hljs-string ngde\">\"mushroom\"</span>></span>Mushroom<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-checkbox</span>></span><span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">p</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">section</span>></span>\n\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">section</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-section\"</span> [<span class=\"hljs-attr ngde\">formGroup</span>]=<span class=\"hljs-string ngde\">\"toppings\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">h4</span>></span>You chose:<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">h4</span>></span>\n    {{toppings.value | json}}\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">section</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n</code></pre>");

/***/ }),

/***/ 61586:
/*!*******************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-checkbox/assets/CheckboxDemoForm/TypeScript/Asset0.html ***!
  \*******************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">CommonModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/common\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Component</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/core\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">FormBuilder</span>, <span class=\"hljs-title class_ ngde\">ReactiveFormsModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/forms\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatCheckboxModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/checkbox\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Preview</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"src/app/features/preview/preview.component\"</span>;\n\n<span class=\"hljs-meta ngde\">@Component</span>({\n  <span class=\"hljs-attr ngde\">selector</span>: <span class=\"hljs-string ngde\">\"app-checkbox-demo-form\"</span>,\n  <span class=\"hljs-attr ngde\">standalone</span>: <span class=\"hljs-literal ngde\">true</span>,\n  <span class=\"hljs-attr ngde\">imports</span>: [<span class=\"hljs-title class_ ngde\">CommonModule</span>, <span class=\"hljs-title class_ ngde\">Preview</span>, <span class=\"hljs-title class_ ngde\">MatCheckboxModule</span>, <span class=\"hljs-title class_ ngde\">ReactiveFormsModule</span>],\n  <span class=\"hljs-attr ngde\">templateUrl</span>: <span class=\"hljs-string ngde\">\"./checkbox-demo-form.component.html\"</span>,\n  <span class=\"hljs-attr ngde\">styleUrls</span>: [<span class=\"hljs-string ngde\">\"./checkbox-demo-form.component.scss\"</span>],\n})\n<span class=\"hljs-keyword ngde\">export</span> <span class=\"hljs-keyword ngde\">class</span> <span class=\"hljs-title class_ ngde\">CheckboxDemoForm</span> {\n  toppings = <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">_formBuilder</span>.<span class=\"hljs-title function_ ngde\">group</span>({\n    <span class=\"hljs-attr ngde\">pepperoni</span>: <span class=\"hljs-literal ngde\">false</span>,\n    <span class=\"hljs-attr ngde\">extracheese</span>: <span class=\"hljs-literal ngde\">false</span>,\n    <span class=\"hljs-attr ngde\">mushroom</span>: <span class=\"hljs-literal ngde\">false</span>,\n  });\n\n  <span class=\"hljs-title function_ ngde\">constructor</span>(<span class=\"hljs-params ngde\"><span class=\"hljs-keyword ngde\">private</span> _formBuilder: FormBuilder</span>) {}\n}\n</code></pre>");

/***/ }),

/***/ 5979:
/*!*********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-checkbox/assets/CheckboxDemo/HTML/Asset1.html ***!
  \*********************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"html\" class=\"hljs language-html ngde\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span>\n  <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Checkbox configuration\"</span>\n  [<span class=\"hljs-attr ngde\">nextElement</span>]=<span class=\"hljs-string ngde\">\"false\"</span>\n  [<span class=\"hljs-attr ngde\">fullWidth</span>]=<span class=\"hljs-string ngde\">\"true\"</span>\n></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">section</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-section\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-checkbox</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-margin\"</span> [(<span class=\"hljs-attr ngde\">ngModel</span>)]=<span class=\"hljs-string ngde\">\"checked\"</span>\n      ></span>Checked&#x3C;/mat-checkbox\n    >\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-checkbox</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-margin\"</span> [(<span class=\"hljs-attr ngde\">ngModel</span>)]=<span class=\"hljs-string ngde\">\"indeterminate\"</span>\n      ></span>Indeterminate&#x3C;/mat-checkbox\n    >\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">section</span>></span>\n\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">section</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-section\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">label</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-margin\"</span>></span>Align:<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">label</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-radio-group</span> [(<span class=\"hljs-attr ngde\">ngModel</span>)]=<span class=\"hljs-string ngde\">\"labelPosition\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-radio-button</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-margin\"</span> <span class=\"hljs-attr ngde\">value</span>=<span class=\"hljs-string ngde\">\"after\"</span>\n        ></span>After&#x3C;/mat-radio-button\n      >\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-radio-button</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-margin\"</span> <span class=\"hljs-attr ngde\">value</span>=<span class=\"hljs-string ngde\">\"before\"</span>\n        ></span>Before&#x3C;/mat-radio-button\n      >\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-radio-group</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">section</span>></span>\n\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">section</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-section\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-checkbox</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-margin\"</span> [(<span class=\"hljs-attr ngde\">ngModel</span>)]=<span class=\"hljs-string ngde\">\"disabled\"</span>\n      ></span>Disabled&#x3C;/mat-checkbox\n    >\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">section</span>></span>\n\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">h2</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-h2\"</span>></span>Result<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">h2</span>></span>\n\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">section</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-section\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-checkbox</span>\n        <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"example-margin\"</span>\n        [(<span class=\"hljs-attr ngde\">ngModel</span>)]=<span class=\"hljs-string ngde\">\"checked\"</span>\n        [(<span class=\"hljs-attr ngde\">indeterminate</span>)]=<span class=\"hljs-string ngde\">\"indeterminate\"</span>\n        [<span class=\"hljs-attr ngde\">labelPosition</span>]=<span class=\"hljs-string ngde\">\"labelPosition\"</span>\n        [<span class=\"hljs-attr ngde\">disabled</span>]=<span class=\"hljs-string ngde\">\"disabled\"</span>\n      ></span>\n        I'm a checkbox\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-checkbox</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">section</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n</code></pre>");

/***/ }),

/***/ 38275:
/*!*********************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-checkbox/assets/CheckboxDemo/SCSS/Asset2.html ***!
  \*********************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"scss\" class=\"hljs language-scss ngde\"><span class=\"hljs-selector-class ngde\">.example-h2</span> {\n  <span class=\"hljs-attribute ngde\">margin</span>: <span class=\"hljs-number ngde\">10px</span>;\n}\n\n<span class=\"hljs-selector-class ngde\">.example-section</span> {\n  <span class=\"hljs-attribute ngde\">display</span>: flex;\n  <span class=\"hljs-attribute ngde\">align-content</span>: center;\n  <span class=\"hljs-attribute ngde\">align-items</span>: center;\n  <span class=\"hljs-attribute ngde\">height</span>: <span class=\"hljs-number ngde\">60px</span>;\n}\n\n<span class=\"hljs-selector-class ngde\">.example-margin</span> {\n  <span class=\"hljs-attribute ngde\">margin</span>: <span class=\"hljs-number ngde\">0</span> <span class=\"hljs-number ngde\">10px</span>;\n}\n</code></pre>");

/***/ }),

/***/ 28791:
/*!***************************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-checkbox/assets/CheckboxDemo/TypeScript/Asset0.html ***!
  \***************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Component</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/core\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">CommonModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/common\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">FormsModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/forms\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatCardModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/card\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatCheckboxModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/checkbox\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatRadioModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/radio\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Preview</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"src/app/features/preview/preview.component\"</span>;\n\n<span class=\"hljs-meta ngde\">@Component</span>({\n  <span class=\"hljs-attr ngde\">selector</span>: <span class=\"hljs-string ngde\">\"app-checkbox-demo\"</span>,\n  <span class=\"hljs-attr ngde\">standalone</span>: <span class=\"hljs-literal ngde\">true</span>,\n  <span class=\"hljs-attr ngde\">imports</span>: [\n    <span class=\"hljs-title class_ ngde\">CommonModule</span>,\n    <span class=\"hljs-title class_ ngde\">MatRadioModule</span>,\n    <span class=\"hljs-title class_ ngde\">FormsModule</span>,\n    <span class=\"hljs-title class_ ngde\">MatCheckboxModule</span>,\n    <span class=\"hljs-title class_ ngde\">MatCardModule</span>,\n    <span class=\"hljs-title class_ ngde\">Preview</span>,\n  ],\n  <span class=\"hljs-attr ngde\">templateUrl</span>: <span class=\"hljs-string ngde\">\"./checkbox-demo.component.html\"</span>,\n  <span class=\"hljs-attr ngde\">styleUrls</span>: [<span class=\"hljs-string ngde\">\"./checkbox-demo.component.scss\"</span>],\n})\n<span class=\"hljs-keyword ngde\">export</span> <span class=\"hljs-keyword ngde\">class</span> <span class=\"hljs-title class_ ngde\">CheckboxDemo</span> {\n  checked = <span class=\"hljs-literal ngde\">false</span>;\n  indeterminate = <span class=\"hljs-literal ngde\">false</span>;\n  <span class=\"hljs-attr ngde\">labelPosition</span>: <span class=\"hljs-string ngde\">\"before\"</span> | <span class=\"hljs-string ngde\">\"after\"</span> = <span class=\"hljs-string ngde\">\"after\"</span>;\n  disabled = <span class=\"hljs-literal ngde\">false</span>;\n}\n</code></pre>");

/***/ }),

/***/ 64183:
/*!*******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/angular-materiel/material-checkbox/index.html ***!
  \*******************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<h1 id=\"checkbox\" class=\"ngde\">Checkbox<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/angular-materiel/material-checkbox#checkbox\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1><h2 id=\"configurable-checkbox\" class=\"ngde\">Configurable checkbox<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/angular-materiel/material-checkbox#configurable-checkbox\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><ng-doc-demo componentname=\"CheckboxDemo\" indexable=\"false\" class=\"ngde\"><div id=\"options\" class=\"ngde\">{}</div></ng-doc-demo><h2 id=\"basic-checkboxes\" class=\"ngde\">Basic checkboxes<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/angular-materiel/material-checkbox#basic-checkboxes\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><ng-doc-demo componentname=\"CheckboxDemoBasic\" indexable=\"false\" class=\"ngde\"><div id=\"options\" class=\"ngde\">{}</div></ng-doc-demo><h2 id=\"checkboxes-with-reactive-forms\" class=\"ngde\">Checkboxes with reactive forms<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/angular-materiel/material-checkbox#checkboxes-with-reactive-forms\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><ng-doc-demo componentname=\"CheckboxDemoForm\" indexable=\"false\" class=\"ngde\"><div id=\"options\" class=\"ngde\">{}</div></ng-doc-demo>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_guides_angular-materiel_material-checkbox_module_ts.js.map