"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_guides_m-theme_theming_module_ts"],{

/***/ 33005:
/*!*******************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-theme/theming/component-assets.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   "demoAssets": () => (/* binding */ demoAssets)
/* harmony export */ });
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_theme_theming_assets_ThemingColorsDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-theme/theming/assets/ThemingColorsDemo/TypeScript/Asset0.html */ 53539);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_theme_theming_assets_ThemingColorsDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-theme/theming/assets/ThemingColorsDemo/HTML/Asset1.html */ 96816);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_theme_theming_assets_ThemingColorsDemo_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-theme/theming/assets/ThemingColorsDemo/SCSS/Asset2.html */ 49438);



const demoAssets = {
  ThemingColorsDemo: [{
    title: 'TypeScript',
    codeType: 'TypeScript',
    code: _raw_loader_ng_doc_m_docs_guides_m_theme_theming_assets_ThemingColorsDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__["default"]
  }, {
    title: 'HTML',
    codeType: 'HTML',
    code: _raw_loader_ng_doc_m_docs_guides_m_theme_theming_assets_ThemingColorsDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__["default"]
  }, {
    title: 'SCSS',
    codeType: 'SCSS',
    code: _raw_loader_ng_doc_m_docs_guides_m_theme_theming_assets_ThemingColorsDemo_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_2__["default"]
  }]
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (demoAssets);

/***/ }),

/***/ 64665:
/*!*********************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-theme/theming/module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_theme_theming_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-theme/theming/index.html */ 26856);
/* harmony import */ var _playgrounds__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./playgrounds */ 93106);
/* harmony import */ var src_app_docs_theming_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/docs/theming/ng-doc.dependencies */ 5776);
/* harmony import */ var _ng_doc_m_docs_guides_m_theme_theming_component_assets__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! .ng-doc/m-docs/guides/m-theme/theming/component-assets */ 33005);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_docs_theming_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/docs/theming/ng-doc.module */ 11405);




// noinspection ES6UnusedImports


// noinspection ES6UnusedImports





class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = '';
    this.pageType = 'guide';
    this.pageContent = _raw_loader_ng_doc_m_docs_guides_m_theme_theming_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
    this.dependencies = src_app_docs_theming_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"];
    this.demoAssets = _ng_doc_m_docs_guides_m_theme_theming_component_assets__WEBPACK_IMPORTED_MODULE_3__["default"];
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, src_app_docs_theming_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"].module, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'Colors'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, src_app_docs_theming_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__.ThemingPageModule, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
})();

/***/ }),

/***/ 93106:
/*!**************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-theme/theming/playgrounds.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PlaygroundsModule": () => (/* binding */ PlaygroundsModule)
/* harmony export */ });
/* harmony import */ var src_app_docs_theming_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/docs/theming/ng-doc.dependencies */ 5776);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_docs_theming_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/docs/theming/ng-doc.module */ 11405);
// noinspection ES6UnusedImports




class PlaygroundsModule {
  static #_ = this.ɵfac = function PlaygroundsModule_Factory(t) {
    return new (t || PlaygroundsModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: PlaygroundsModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, src_app_docs_theming_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__["default"].module]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](PlaygroundsModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, src_app_docs_theming_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__.ThemingPageModule]
  });
})();

/***/ }),

/***/ 5776:
/*!*****************************************************!*\
  !*** ./src/app/docs/theming/ng-doc.dependencies.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var src_app_docs_theming_ng_doc_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/docs/theming/ng-doc.module */ 11405);
/* harmony import */ var src_app_docs_theming_theming_colors_demo_theming_colors_demo_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/docs/theming/theming-colors-demo/theming-colors-demo.component */ 96680);


const ThemingPageDependencies = {
  module: src_app_docs_theming_ng_doc_module__WEBPACK_IMPORTED_MODULE_0__.ThemingPageModule,
  // Add your demos that you are going to use in the page here
  demo: {
    ThemingColorsDemo: src_app_docs_theming_theming_colors_demo_theming_colors_demo_component__WEBPACK_IMPORTED_MODULE_1__.ThemingColorsDemo
  }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ThemingPageDependencies);

/***/ }),

/***/ 11405:
/*!***********************************************!*\
  !*** ./src/app/docs/theming/ng-doc.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ThemingPageModule": () => (/* binding */ ThemingPageModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);


class ThemingPageModule {
  static #_ = this.ɵfac = function ThemingPageModule_Factory(t) {
    return new (t || ThemingPageModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
    type: ThemingPageModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](ThemingPageModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
})();

/***/ }),

/***/ 96680:
/*!***********************************************************************************!*\
  !*** ./src/app/docs/theming/theming-colors-demo/theming-colors-demo.component.ts ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ThemingColorsDemo": () => (/* binding */ ThemingColorsDemo)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_services_theming_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/services/theming.service */ 95661);



function ThemingColorsDemo_div_1_div_4_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "div", 8)(3, "span", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "div", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
  }
  if (rf & 2) {
    const color_r3 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵstyleProp"]("background-color", color_r3.color);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](color_r3.name);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](color_r3.color);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Contraste : ", color_r3.contrast, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate1"]("Usage : ", color_r3.usage, "");
  }
}
function ThemingColorsDemo_div_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 2)(1, "h2", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "div", 4);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, ThemingColorsDemo_div_1_div_4_Template, 11, 6, "div", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
  }
  if (rf & 2) {
    const paletteName_r1 = ctx.$implicit;
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](paletteName_r1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx_r0.colors[paletteName_r1]);
  }
}
class ThemingColorsDemo {
  constructor(themingService, cdRef) {
    this.themingService = themingService;
    this.cdRef = cdRef;
    this.colors = {};
    this.theme = this.themingService.theming;
    this.palettes = ['primary', 'accent', 'warn'];
    this.themingService.themingObservable().subscribe(theme => {
      if (theme !== this.theme) {
        this.theme = theme;
        this.loadColors(this.theme);
        this.cdRef.detectChanges();
      }
    });
  }
  ngOnInit() {
    // Appeler `getComputedStyle` après le chargement de la vue
    this.loadColors(this.theme);
  }
  loadColors(theme) {
    console.log(theme);
    this.colors = {};
    // Obtenir les variables CSS de la palette
    const styles = getComputedStyle(document.documentElement);
    // Nom des palettes et nuances
    const palettesVar = {
      primary: '$primary-palette',
      accent: '$accent-palette',
      warn: '$warn-palette'
    };
    this.palettes.forEach(palette => this.colors[palette] = []);
    const shades = ['50', '100', '200', '300', '400', '500', '600', '700', '800', '900', 'A100', 'A200', 'A400', 'A700'];
    const additionalKeys = ['default', 'lighter', 'darker', 'text']; // Pour les autres couleurs
    // Récupérer chaque couleur avec ses contrastes associés
    this.palettes.forEach(palette => {
      // Boucle pour chaque nuance standard
      shades.forEach(shade => {
        const colorVar = `--${theme}--${palette}-${shade}`;
        const contrastVar = `${colorVar}-contrast`;
        const colorValue = styles.getPropertyValue(colorVar).trim();
        const contrastValue = styles.getPropertyValue(contrastVar).trim();
        if (colorValue) {
          this.colors[palette].push({
            name: `${palette} ${shade}`,
            color: colorValue,
            contrast: contrastValue || 'N/A',
            usage: `map.get(${palettesVar[palette]}, ${JSON.stringify(shade)}))`
          });
        }
      });
      // Boucle pour les autres clés dans chaque palette (default, lighter, darker, etc.)
      additionalKeys.forEach(key => {
        const colorVar = `--${theme}--${palette}-${key}`;
        const contrastVar = `${colorVar}-contrast`;
        const colorValue = styles.getPropertyValue(colorVar).trim();
        const contrastValue = styles.getPropertyValue(contrastVar).trim();
        if (colorValue) {
          this.colors[palette].push({
            name: `${palette} ${key}`,
            color: colorValue,
            contrast: contrastValue || 'N/A',
            usage: `map.get(${palettesVar[palette]}, ${JSON.stringify(key)}))`
          });
        }
      });
    });
  }
  static #_ = this.ɵfac = function ThemingColorsDemo_Factory(t) {
    return new (t || ThemingColorsDemo)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_theming_service__WEBPACK_IMPORTED_MODULE_0__.ThemingService), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_1__.ChangeDetectorRef));
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
    type: ThemingColorsDemo,
    selectors: [["app-theming-colors-demo"]],
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵStandaloneFeature"]],
    decls: 2,
    vars: 1,
    consts: [[1, "palette-container"], ["class", "palette-group", 4, "ngFor", "ngForOf"], [1, "palette-group"], [1, "palette-title"], [1, "palette"], ["class", "color-card", 4, "ngFor", "ngForOf"], [1, "color-card"], [1, "color-sample"], [1, "color-info"], [1, "color-name"], [1, "color-code"], [1, "contrast-code"], [1, "usage"]],
    template: function ThemingColorsDemo_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](1, ThemingColorsDemo_div_1_Template, 5, 2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.palettes);
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.NgForOf],
    styles: [".palette-container[_ngcontent-%COMP%] {\n  padding: 1rem;\n}\n\n.palette-group[_ngcontent-%COMP%] {\n  margin-bottom: 2rem;\n}\n.palette-group[_ngcontent-%COMP%]   .palette-title[_ngcontent-%COMP%] {\n  font-size: 1.5em;\n  font-weight: bold;\n  margin-bottom: 16px;\n  text-transform: capitalize;\n}\n.palette-group[_ngcontent-%COMP%]   .palette[_ngcontent-%COMP%] {\n  display: flex;\n  flex-wrap: wrap;\n  gap: 1.5rem;\n  justify-content: center;\n}\n.palette-group[_ngcontent-%COMP%]   .color-card[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  text-align: center;\n  width: 100px;\n  flex: 1 1 120px;\n}\n.palette-group[_ngcontent-%COMP%]   .color-card[_ngcontent-%COMP%]   .color-sample[_ngcontent-%COMP%] {\n  height: 60px;\n  width: 60px;\n  border-radius: 50%;\n  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);\n  margin-bottom: 0.5rem;\n}\n.palette-group[_ngcontent-%COMP%]   .color-card[_ngcontent-%COMP%]   .color-info[_ngcontent-%COMP%] {\n  font-size: 0.85em;\n  line-height: 1.4;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n}\n.palette-group[_ngcontent-%COMP%]   .color-card[_ngcontent-%COMP%]   .color-info[_ngcontent-%COMP%]   .color-name[_ngcontent-%COMP%], .palette-group[_ngcontent-%COMP%]   .color-card[_ngcontent-%COMP%]   .color-info[_ngcontent-%COMP%]   .color-shade[_ngcontent-%COMP%], .palette-group[_ngcontent-%COMP%]   .color-card[_ngcontent-%COMP%]   .color-info[_ngcontent-%COMP%]   .color-code[_ngcontent-%COMP%], .palette-group[_ngcontent-%COMP%]   .color-card[_ngcontent-%COMP%]   .color-info[_ngcontent-%COMP%]   .contrast-code[_ngcontent-%COMP%], .palette-group[_ngcontent-%COMP%]   .color-card[_ngcontent-%COMP%]   .color-info[_ngcontent-%COMP%]   .usage[_ngcontent-%COMP%] {\n  font-size: 0.8em;\n  margin-top: 0.2rem;\n}\n.palette-group[_ngcontent-%COMP%]   .color-card[_ngcontent-%COMP%]   .color-info[_ngcontent-%COMP%]   .color-name[_ngcontent-%COMP%] {\n  font-size: 1em;\n  font-weight: bold;\n  margin-bottom: 0.2rem;\n}\n.palette-group[_ngcontent-%COMP%]   .color-card[_ngcontent-%COMP%]   .color-info[_ngcontent-%COMP%]   .color-shade[_ngcontent-%COMP%] {\n  font-size: 0.9em;\n}\n.palette-group[_ngcontent-%COMP%]   .color-card[_ngcontent-%COMP%]   .color-info[_ngcontent-%COMP%]   .usage[_ngcontent-%COMP%] {\n  font-weight: normal;\n  margin-top: 0.4rem;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvZG9jcy90aGVtaW5nL3RoZW1pbmctY29sb3JzLWRlbW8vdGhlbWluZy1jb2xvcnMtZGVtby5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGFBQUE7QUFDRDs7QUFFQTtFQUNDLG1CQUFBO0FBQ0Q7QUFDQztFQUNDLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLDBCQUFBO0FBQ0Y7QUFFQztFQUNDLGFBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLHVCQUFBO0FBQUY7QUFHQztFQUNDLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQURGO0FBR0U7RUFDQyxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esd0NBQUE7RUFDQSxxQkFBQTtBQURIO0FBSUU7RUFDQyxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7QUFGSDtBQUlHOzs7OztFQUtDLGdCQUFBO0VBQ0Esa0JBQUE7QUFGSjtBQUtHO0VBQ0MsY0FBQTtFQUNBLGlCQUFBO0VBQ0EscUJBQUE7QUFISjtBQU1HO0VBQ0MsZ0JBQUE7QUFKSjtBQU9HO0VBQ0MsbUJBQUE7RUFDQSxrQkFBQTtBQUxKIiwic291cmNlc0NvbnRlbnQiOlsiLnBhbGV0dGUtY29udGFpbmVyIHtcclxuXHRwYWRkaW5nOiAxcmVtO1xyXG59XHJcblxyXG4ucGFsZXR0ZS1ncm91cCB7XHJcblx0bWFyZ2luLWJvdHRvbTogMnJlbTtcclxuXHJcblx0LnBhbGV0dGUtdGl0bGUge1xyXG5cdFx0Zm9udC1zaXplOiAxLjVlbTtcclxuXHRcdGZvbnQtd2VpZ2h0OiBib2xkO1xyXG5cdFx0bWFyZ2luLWJvdHRvbTogMTZweDtcclxuXHRcdHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xyXG5cdH1cclxuXHJcblx0LnBhbGV0dGUge1xyXG5cdFx0ZGlzcGxheTogZmxleDtcclxuXHRcdGZsZXgtd3JhcDogd3JhcDtcclxuXHRcdGdhcDogMS41cmVtO1xyXG5cdFx0anVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcblx0fVxyXG5cclxuXHQuY29sb3ItY2FyZCB7XHJcblx0XHRkaXNwbGF5OiBmbGV4O1xyXG5cdFx0ZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuXHRcdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblx0XHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblx0XHR3aWR0aDogMTAwcHg7XHJcblx0XHRmbGV4OiAxIDEgMTIwcHg7IC8vIFJlc3BvbnNpdmUgYXZlYyB1bmUgdGFpbGxlIG1pbmltYWxlIGRlIDEyMHB4XHJcblxyXG5cdFx0LmNvbG9yLXNhbXBsZSB7XHJcblx0XHRcdGhlaWdodDogNjBweDtcclxuXHRcdFx0d2lkdGg6IDYwcHg7XHJcblx0XHRcdGJvcmRlci1yYWRpdXM6IDUwJTtcclxuXHRcdFx0Ym94LXNoYWRvdzogMCAycHggNHB4IHJnYmEoMCwgMCwgMCwgMC4xKTtcclxuXHRcdFx0bWFyZ2luLWJvdHRvbTogMC41cmVtO1xyXG5cdFx0fVxyXG5cclxuXHRcdC5jb2xvci1pbmZvIHtcclxuXHRcdFx0Zm9udC1zaXplOiAwLjg1ZW07XHJcblx0XHRcdGxpbmUtaGVpZ2h0OiAxLjQ7XHJcblx0XHRcdGRpc3BsYXk6IGZsZXg7XHJcblx0XHRcdGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblx0XHRcdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblxyXG5cdFx0XHQuY29sb3ItbmFtZSxcclxuXHRcdFx0LmNvbG9yLXNoYWRlLFxyXG5cdFx0XHQuY29sb3ItY29kZSxcclxuXHRcdFx0LmNvbnRyYXN0LWNvZGUsXHJcblx0XHRcdC51c2FnZSB7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAwLjhlbTtcclxuXHRcdFx0XHRtYXJnaW4tdG9wOiAwLjJyZW07XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdC5jb2xvci1uYW1lIHtcclxuXHRcdFx0XHRmb250LXNpemU6IDFlbTtcclxuXHRcdFx0XHRmb250LXdlaWdodDogYm9sZDtcclxuXHRcdFx0XHRtYXJnaW4tYm90dG9tOiAwLjJyZW07XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdC5jb2xvci1zaGFkZSB7XHJcblx0XHRcdFx0Zm9udC1zaXplOiAwLjllbTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0LnVzYWdlIHtcclxuXHRcdFx0XHRmb250LXdlaWdodDogbm9ybWFsO1xyXG5cdFx0XHRcdG1hcmdpbi10b3A6IDAuNHJlbTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9 */"]
  });
}

/***/ }),

/***/ 96816:
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-theme/theming/assets/ThemingColorsDemo/HTML/Asset1.html ***!
  \*******************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"html\" class=\"hljs language-html ngde\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"palette-container\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"palette-group\"</span> *<span class=\"hljs-attr ngde\">ngFor</span>=<span class=\"hljs-string ngde\">\"let paletteName of palettes\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">h2</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"palette-title\"</span>></span>{{ paletteName }}<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">h2</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"palette\"</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"color-card\"</span> *<span class=\"hljs-attr ngde\">ngFor</span>=<span class=\"hljs-string ngde\">\"let color of colors[paletteName]\"</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"color-sample\"</span> [<span class=\"hljs-attr ngde\">style.background-color</span>]=<span class=\"hljs-string ngde\">\"color.color\"</span>></span><span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"color-info\"</span>></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">span</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"color-name\"</span>></span>{{ color.name }}<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">span</span>></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"color-code\"</span>></span>{{ color.color }}<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"contrast-code\"</span>></span>Contraste : {{ color.contrast }}<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n          <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"usage\"</span>></span>Usage : {{ color.usage }}<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n        <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n</code></pre>");

/***/ }),

/***/ 49438:
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-theme/theming/assets/ThemingColorsDemo/SCSS/Asset2.html ***!
  \*******************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"scss\" class=\"hljs language-scss ngde\"><span class=\"hljs-selector-class ngde\">.palette-container</span> {\n  <span class=\"hljs-attribute ngde\">padding</span>: <span class=\"hljs-number ngde\">1rem</span>;\n}\n\n<span class=\"hljs-selector-class ngde\">.palette-group</span> {\n  <span class=\"hljs-attribute ngde\">margin-bottom</span>: <span class=\"hljs-number ngde\">2rem</span>;\n\n  <span class=\"hljs-selector-class ngde\">.palette-title</span> {\n    <span class=\"hljs-attribute ngde\">font-size</span>: <span class=\"hljs-number ngde\">1.5em</span>;\n    <span class=\"hljs-attribute ngde\">font-weight</span>: bold;\n    <span class=\"hljs-attribute ngde\">margin-bottom</span>: <span class=\"hljs-number ngde\">16px</span>;\n    <span class=\"hljs-attribute ngde\">text-transform</span>: capitalize;\n  }\n\n  <span class=\"hljs-selector-class ngde\">.palette</span> {\n    <span class=\"hljs-attribute ngde\">display</span>: flex;\n    <span class=\"hljs-attribute ngde\">flex-wrap</span>: wrap;\n    <span class=\"hljs-attribute ngde\">gap</span>: <span class=\"hljs-number ngde\">1.5rem</span>;\n    <span class=\"hljs-attribute ngde\">justify-content</span>: center;\n  }\n\n  <span class=\"hljs-selector-class ngde\">.color-card</span> {\n    <span class=\"hljs-attribute ngde\">display</span>: flex;\n    <span class=\"hljs-attribute ngde\">flex-direction</span>: column;\n    <span class=\"hljs-attribute ngde\">align-items</span>: center;\n    <span class=\"hljs-attribute ngde\">text-align</span>: center;\n    <span class=\"hljs-attribute ngde\">width</span>: <span class=\"hljs-number ngde\">100px</span>;\n    <span class=\"hljs-attribute ngde\">flex</span>: <span class=\"hljs-number ngde\">1</span> <span class=\"hljs-number ngde\">1</span> <span class=\"hljs-number ngde\">120px</span>; <span class=\"hljs-comment ngde\">// Responsive avec une taille minimale de 120px</span>\n\n    <span class=\"hljs-selector-class ngde\">.color-sample</span> {\n      <span class=\"hljs-attribute ngde\">height</span>: <span class=\"hljs-number ngde\">60px</span>;\n      <span class=\"hljs-attribute ngde\">width</span>: <span class=\"hljs-number ngde\">60px</span>;\n      <span class=\"hljs-attribute ngde\">border-radius</span>: <span class=\"hljs-number ngde\">50%</span>;\n      <span class=\"hljs-attribute ngde\">box-shadow</span>: <span class=\"hljs-number ngde\">0</span> <span class=\"hljs-number ngde\">2px</span> <span class=\"hljs-number ngde\">4px</span> <span class=\"hljs-built_in ngde\">rgba</span>(<span class=\"hljs-number ngde\">0</span>, <span class=\"hljs-number ngde\">0</span>, <span class=\"hljs-number ngde\">0</span>, <span class=\"hljs-number ngde\">0.1</span>);\n      <span class=\"hljs-attribute ngde\">margin-bottom</span>: <span class=\"hljs-number ngde\">0.5rem</span>;\n    }\n\n    <span class=\"hljs-selector-class ngde\">.color-info</span> {\n      <span class=\"hljs-attribute ngde\">font-size</span>: <span class=\"hljs-number ngde\">0.85em</span>;\n      <span class=\"hljs-attribute ngde\">line-height</span>: <span class=\"hljs-number ngde\">1.4</span>;\n      <span class=\"hljs-attribute ngde\">display</span>: flex;\n      <span class=\"hljs-attribute ngde\">flex-direction</span>: column;\n      <span class=\"hljs-attribute ngde\">align-items</span>: center;\n\n      <span class=\"hljs-selector-class ngde\">.color-name</span>,\n      <span class=\"hljs-selector-class ngde\">.color-shade</span>,\n      <span class=\"hljs-selector-class ngde\">.color-code</span>,\n      <span class=\"hljs-selector-class ngde\">.contrast-code</span>,\n      <span class=\"hljs-selector-class ngde\">.usage</span> {\n        <span class=\"hljs-attribute ngde\">font-size</span>: <span class=\"hljs-number ngde\">0.8em</span>;\n        <span class=\"hljs-attribute ngde\">margin-top</span>: <span class=\"hljs-number ngde\">0.2rem</span>;\n      }\n\n      <span class=\"hljs-selector-class ngde\">.color-name</span> {\n        <span class=\"hljs-attribute ngde\">font-size</span>: <span class=\"hljs-number ngde\">1em</span>;\n        <span class=\"hljs-attribute ngde\">font-weight</span>: bold;\n        <span class=\"hljs-attribute ngde\">margin-bottom</span>: <span class=\"hljs-number ngde\">0.2rem</span>;\n      }\n\n      <span class=\"hljs-selector-class ngde\">.color-shade</span> {\n        <span class=\"hljs-attribute ngde\">font-size</span>: <span class=\"hljs-number ngde\">0.9em</span>;\n      }\n\n      <span class=\"hljs-selector-class ngde\">.usage</span> {\n        <span class=\"hljs-attribute ngde\">font-weight</span>: normal;\n        <span class=\"hljs-attribute ngde\">margin-top</span>: <span class=\"hljs-number ngde\">0.4rem</span>;\n      }\n    }\n  }\n}\n</code></pre>");

/***/ }),

/***/ 53539:
/*!*************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-theme/theming/assets/ThemingColorsDemo/TypeScript/Asset0.html ***!
  \*************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">NgForOf</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/common\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">ChangeDetectorRef</span>, <span class=\"hljs-title class_ ngde\">Component</span>, <span class=\"hljs-title class_ ngde\">OnInit</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/core\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-variable constant_ ngde\">THEME</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"src/app/app.component\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">ThemingService</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"src/app/services/theming.service\"</span>;\n\n<span class=\"hljs-keyword ngde\">export</span> <span class=\"hljs-keyword ngde\">interface</span> <span class=\"hljs-title class_ ngde\">PaletteColor</span> {\n  [<span class=\"hljs-attr ngde\">palette</span>: <span class=\"hljs-built_in ngde\">string</span>]: {\n    <span class=\"hljs-attr ngde\">name</span>: <span class=\"hljs-built_in ngde\">string</span>;\n    <span class=\"hljs-attr ngde\">color</span>: <span class=\"hljs-built_in ngde\">string</span>;\n    <span class=\"hljs-attr ngde\">contrast</span>: <span class=\"hljs-built_in ngde\">string</span>;\n    <span class=\"hljs-attr ngde\">usage</span>: <span class=\"hljs-built_in ngde\">string</span>;\n  }[];\n}\n\n<span class=\"hljs-meta ngde\">@Component</span>({\n  <span class=\"hljs-attr ngde\">selector</span>: <span class=\"hljs-string ngde\">\"app-theming-colors-demo\"</span>,\n  <span class=\"hljs-attr ngde\">templateUrl</span>: <span class=\"hljs-string ngde\">\"./theming-colors-demo.component.html\"</span>,\n  <span class=\"hljs-attr ngde\">styleUrls</span>: [<span class=\"hljs-string ngde\">\"./theming-colors-demo.component.scss\"</span>],\n  <span class=\"hljs-attr ngde\">imports</span>: [<span class=\"hljs-title class_ ngde\">NgForOf</span>],\n  <span class=\"hljs-attr ngde\">standalone</span>: <span class=\"hljs-literal ngde\">true</span>,\n})\n<span class=\"hljs-keyword ngde\">export</span> <span class=\"hljs-keyword ngde\">class</span> <span class=\"hljs-title class_ ngde\">ThemingColorsDemo</span> <span class=\"hljs-keyword ngde\">implements</span> <span class=\"hljs-title class_ ngde\">OnInit</span> {\n  <span class=\"hljs-attr ngde\">colors</span>: <span class=\"hljs-title class_ ngde\">PaletteColor</span> = {};\n  <span class=\"hljs-attr ngde\">theme</span>: <span class=\"hljs-variable constant_ ngde\">THEME</span> = <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">themingService</span>.<span class=\"hljs-property ngde\">theming</span>;\n  palettes = [<span class=\"hljs-string ngde\">\"primary\"</span>, <span class=\"hljs-string ngde\">\"accent\"</span>, <span class=\"hljs-string ngde\">\"warn\"</span>];\n\n  <span class=\"hljs-title function_ ngde\">constructor</span>(<span class=\"hljs-params ngde\">\n    <span class=\"hljs-keyword ngde\">private</span> themingService: ThemingService,\n    <span class=\"hljs-keyword ngde\">private</span> cdRef: ChangeDetectorRef\n  </span>) {\n    <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">themingService</span>.<span class=\"hljs-title function_ ngde\">themingObservable</span>().<span class=\"hljs-title function_ ngde\">subscribe</span>(<span class=\"hljs-function ngde\">(<span class=\"hljs-params ngde\">theme</span>) =></span> {\n      <span class=\"hljs-keyword ngde\">if</span> (theme !== <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">theme</span>) {\n        <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">theme</span> = theme;\n        <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-title function_ ngde\">loadColors</span>(<span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">theme</span>);\n        <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">cdRef</span>.<span class=\"hljs-title function_ ngde\">detectChanges</span>();\n      }\n    });\n  }\n\n  <span class=\"hljs-title function_ ngde\">ngOnInit</span>(): <span class=\"hljs-built_in ngde\">void</span> {\n    <span class=\"hljs-comment ngde\">// Appeler `getComputedStyle` après le chargement de la vue</span>\n    <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-title function_ ngde\">loadColors</span>(<span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">theme</span>);\n  }\n\n  <span class=\"hljs-title function_ ngde\">loadColors</span>(<span class=\"hljs-params ngde\">theme: THEME</span>) {\n    <span class=\"hljs-variable language_ ngde\">console</span>.<span class=\"hljs-title function_ ngde\">log</span>(theme);\n    <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">colors</span> = {};\n\n    <span class=\"hljs-comment ngde\">// Obtenir les variables CSS de la palette</span>\n    <span class=\"hljs-keyword ngde\">const</span> styles = <span class=\"hljs-title function_ ngde\">getComputedStyle</span>(<span class=\"hljs-variable language_ ngde\">document</span>.<span class=\"hljs-property ngde\">documentElement</span>);\n\n    <span class=\"hljs-comment ngde\">// Nom des palettes et nuances</span>\n    <span class=\"hljs-keyword ngde\">const</span> palettesVar = {\n      <span class=\"hljs-attr ngde\">primary</span>: <span class=\"hljs-string ngde\">\"$primary-palette\"</span>,\n      <span class=\"hljs-attr ngde\">accent</span>: <span class=\"hljs-string ngde\">\"$accent-palette\"</span>,\n      <span class=\"hljs-attr ngde\">warn</span>: <span class=\"hljs-string ngde\">\"$warn-palette\"</span>,\n    };\n    <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">palettes</span>.<span class=\"hljs-title function_ ngde\">forEach</span>(<span class=\"hljs-function ngde\">(<span class=\"hljs-params ngde\">palette</span>) =></span> (<span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">colors</span>[palette] = []));\n\n    <span class=\"hljs-keyword ngde\">const</span> shades = [\n      <span class=\"hljs-string ngde\">\"50\"</span>,\n      <span class=\"hljs-string ngde\">\"100\"</span>,\n      <span class=\"hljs-string ngde\">\"200\"</span>,\n      <span class=\"hljs-string ngde\">\"300\"</span>,\n      <span class=\"hljs-string ngde\">\"400\"</span>,\n      <span class=\"hljs-string ngde\">\"500\"</span>,\n      <span class=\"hljs-string ngde\">\"600\"</span>,\n      <span class=\"hljs-string ngde\">\"700\"</span>,\n      <span class=\"hljs-string ngde\">\"800\"</span>,\n      <span class=\"hljs-string ngde\">\"900\"</span>,\n      <span class=\"hljs-string ngde\">\"A100\"</span>,\n      <span class=\"hljs-string ngde\">\"A200\"</span>,\n      <span class=\"hljs-string ngde\">\"A400\"</span>,\n      <span class=\"hljs-string ngde\">\"A700\"</span>,\n    ];\n    <span class=\"hljs-keyword ngde\">const</span> additionalKeys = [<span class=\"hljs-string ngde\">\"default\"</span>, <span class=\"hljs-string ngde\">\"lighter\"</span>, <span class=\"hljs-string ngde\">\"darker\"</span>, <span class=\"hljs-string ngde\">\"text\"</span>]; <span class=\"hljs-comment ngde\">// Pour les autres couleurs</span>\n\n    <span class=\"hljs-comment ngde\">// Récupérer chaque couleur avec ses contrastes associés</span>\n    <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">palettes</span>.<span class=\"hljs-title function_ ngde\">forEach</span>(<span class=\"hljs-function ngde\">(<span class=\"hljs-params ngde\">palette</span>) =></span> {\n      <span class=\"hljs-comment ngde\">// Boucle pour chaque nuance standard</span>\n      shades.<span class=\"hljs-title function_ ngde\">forEach</span>(<span class=\"hljs-function ngde\">(<span class=\"hljs-params ngde\">shade</span>) =></span> {\n        <span class=\"hljs-keyword ngde\">const</span> colorVar = <span class=\"hljs-string ngde\">`--<span class=\"hljs-subst ngde\">${theme}</span>--<span class=\"hljs-subst ngde\">${palette}</span>-<span class=\"hljs-subst ngde\">${shade}</span>`</span>;\n        <span class=\"hljs-keyword ngde\">const</span> contrastVar = <span class=\"hljs-string ngde\">`<span class=\"hljs-subst ngde\">${colorVar}</span>-contrast`</span>;\n\n        <span class=\"hljs-keyword ngde\">const</span> colorValue = styles.<span class=\"hljs-title function_ ngde\">getPropertyValue</span>(colorVar).<span class=\"hljs-title function_ ngde\">trim</span>();\n        <span class=\"hljs-keyword ngde\">const</span> contrastValue = styles.<span class=\"hljs-title function_ ngde\">getPropertyValue</span>(contrastVar).<span class=\"hljs-title function_ ngde\">trim</span>();\n\n        <span class=\"hljs-keyword ngde\">if</span> (colorValue) {\n          <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">colors</span>[palette].<span class=\"hljs-title function_ ngde\">push</span>({\n            <span class=\"hljs-attr ngde\">name</span>: <span class=\"hljs-string ngde\">`<span class=\"hljs-subst ngde\">${palette}</span> <span class=\"hljs-subst ngde\">${shade}</span>`</span>,\n            <span class=\"hljs-attr ngde\">color</span>: colorValue,\n            <span class=\"hljs-attr ngde\">contrast</span>: contrastValue || <span class=\"hljs-string ngde\">\"N/A\"</span>,\n            <span class=\"hljs-attr ngde\">usage</span>: <span class=\"hljs-string ngde\">`map.get(<span class=\"hljs-subst ngde\">${palettesVar[palette]}</span>, <span class=\"hljs-subst ngde\">${<span class=\"hljs-built_in ngde\">JSON</span>.stringify(\n              shade\n            )}</span>))`</span>,\n          });\n        }\n      });\n\n      <span class=\"hljs-comment ngde\">// Boucle pour les autres clés dans chaque palette (default, lighter, darker, etc.)</span>\n      additionalKeys.<span class=\"hljs-title function_ ngde\">forEach</span>(<span class=\"hljs-function ngde\">(<span class=\"hljs-params ngde\">key</span>) =></span> {\n        <span class=\"hljs-keyword ngde\">const</span> colorVar = <span class=\"hljs-string ngde\">`--<span class=\"hljs-subst ngde\">${theme}</span>--<span class=\"hljs-subst ngde\">${palette}</span>-<span class=\"hljs-subst ngde\">${key}</span>`</span>;\n        <span class=\"hljs-keyword ngde\">const</span> contrastVar = <span class=\"hljs-string ngde\">`<span class=\"hljs-subst ngde\">${colorVar}</span>-contrast`</span>;\n\n        <span class=\"hljs-keyword ngde\">const</span> colorValue = styles.<span class=\"hljs-title function_ ngde\">getPropertyValue</span>(colorVar).<span class=\"hljs-title function_ ngde\">trim</span>();\n        <span class=\"hljs-keyword ngde\">const</span> contrastValue = styles.<span class=\"hljs-title function_ ngde\">getPropertyValue</span>(contrastVar).<span class=\"hljs-title function_ ngde\">trim</span>();\n\n        <span class=\"hljs-keyword ngde\">if</span> (colorValue) {\n          <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">colors</span>[palette].<span class=\"hljs-title function_ ngde\">push</span>({\n            <span class=\"hljs-attr ngde\">name</span>: <span class=\"hljs-string ngde\">`<span class=\"hljs-subst ngde\">${palette}</span> <span class=\"hljs-subst ngde\">${key}</span>`</span>,\n            <span class=\"hljs-attr ngde\">color</span>: colorValue,\n            <span class=\"hljs-attr ngde\">contrast</span>: contrastValue || <span class=\"hljs-string ngde\">\"N/A\"</span>,\n            <span class=\"hljs-attr ngde\">usage</span>: <span class=\"hljs-string ngde\">`map.get(<span class=\"hljs-subst ngde\">${palettesVar[palette]}</span>, <span class=\"hljs-subst ngde\">${<span class=\"hljs-built_in ngde\">JSON</span>.stringify(key)}</span>))`</span>,\n          });\n        }\n      });\n    });\n  }\n}\n</code></pre>");

/***/ }),

/***/ 26856:
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-theme/theming/index.html ***!
  \************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<h1 id=\"colors\" class=\"ngde\">Colors<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/m-theme/theming#colors\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1><ng-doc-demo componentname=\"ThemingColorsDemo\" indexable=\"false\" class=\"ngde\"><div id=\"options\" class=\"ngde\">{\"container\":false}</div></ng-doc-demo>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_guides_m-theme_theming_module_ts.js.map