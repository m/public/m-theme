"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_guides_m-ui_m-scroll_module_ts"],{

/***/ 72288:
/*!*****************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-ui/m-scroll/component-assets.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   "demoAssets": () => (/* binding */ demoAssets)
/* harmony export */ });
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_ui_m_scroll_assets_MScrollDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-ui/m-scroll/assets/MScrollDemo/TypeScript/Asset0.html */ 63971);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_ui_m_scroll_assets_MScrollDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-ui/m-scroll/assets/MScrollDemo/HTML/Asset1.html */ 48096);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_ui_m_scroll_assets_MScrollDemo_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-ui/m-scroll/assets/MScrollDemo/SCSS/Asset2.html */ 88850);



const demoAssets = {
  MScrollDemo: [{
    title: 'TypeScript',
    codeType: 'TypeScript',
    code: _raw_loader_ng_doc_m_docs_guides_m_ui_m_scroll_assets_MScrollDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__["default"]
  }, {
    title: 'HTML',
    codeType: 'HTML',
    code: _raw_loader_ng_doc_m_docs_guides_m_ui_m_scroll_assets_MScrollDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__["default"]
  }, {
    title: 'SCSS',
    codeType: 'SCSS',
    code: _raw_loader_ng_doc_m_docs_guides_m_ui_m_scroll_assets_MScrollDemo_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_2__["default"]
  }]
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (demoAssets);

/***/ }),

/***/ 62552:
/*!*******************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-ui/m-scroll/module.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_ui_m_scroll_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-ui/m-scroll/index.html */ 47605);
/* harmony import */ var _playgrounds__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./playgrounds */ 11208);
/* harmony import */ var src_app_docs_m_ui_m_scroll_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/docs/m-ui/m-scroll/ng-doc.dependencies */ 14729);
/* harmony import */ var _ng_doc_m_docs_guides_m_ui_m_scroll_component_assets__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! .ng-doc/m-docs/guides/m-ui/m-scroll/component-assets */ 72288);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _src_app_docs_m_ui_m_scroll_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../src/app/docs/m-ui/m-scroll/ng-doc.module */ 40184);




// noinspection ES6UnusedImports


// noinspection ES6UnusedImports





class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = '';
    this.pageType = 'guide';
    this.pageContent = _raw_loader_ng_doc_m_docs_guides_m_ui_m_scroll_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
    this.dependencies = src_app_docs_m_ui_m_scroll_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"];
    this.demoAssets = _ng_doc_m_docs_guides_m_ui_m_scroll_component_assets__WEBPACK_IMPORTED_MODULE_3__["default"];
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, src_app_docs_m_ui_m_scroll_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"].module, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'M-scroll'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, _src_app_docs_m_ui_m_scroll_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__.MScrollPageModule, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
})();

/***/ }),

/***/ 11208:
/*!************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-ui/m-scroll/playgrounds.ts ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PlaygroundsModule": () => (/* binding */ PlaygroundsModule)
/* harmony export */ });
/* harmony import */ var src_app_docs_m_ui_m_scroll_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/docs/m-ui/m-scroll/ng-doc.dependencies */ 14729);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _src_app_docs_m_ui_m_scroll_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../src/app/docs/m-ui/m-scroll/ng-doc.module */ 40184);
// noinspection ES6UnusedImports




class PlaygroundsModule {
  static #_ = this.ɵfac = function PlaygroundsModule_Factory(t) {
    return new (t || PlaygroundsModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: PlaygroundsModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, src_app_docs_m_ui_m_scroll_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__["default"].module]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](PlaygroundsModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, _src_app_docs_m_ui_m_scroll_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__.MScrollPageModule]
  });
})();

/***/ }),

/***/ 305:
/*!*****************************************************************************!*\
  !*** ./src/app/docs/m-ui/m-scroll/m-scroll-demo/m-scroll-demo.component.ts ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MScrollDemo": () => (/* binding */ MScrollDemo)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/features/preview/preview.component */ 55355);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);



class MScrollDemo {
  static #_ = this.ɵfac = function MScrollDemo_Factory(t) {
    return new (t || MScrollDemo)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
    type: MScrollDemo,
    selectors: [["app-m-scroll-demo"]],
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵStandaloneFeature"]],
    decls: 4,
    vars: 1,
    consts: [["title", "Scroll", 3, "nextElement"], [1, "demo-content"]],
    template: function MScrollDemo_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "app-preview", 0)(1, "div", 1)(2, "p");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a dictum orci. Vivamus nec erat elementum nunc semper semper ac vel tortor. Aenean rutrum rutrum diam, facilisis consectetur nisi auctor at. Etiam dui felis, porta sit amet tincidunt nec, aliquet sed nunc. Morbi mattis elit a tempor pulvinar. Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque feugiat turpis at risus suscipit porttitor. Nullam sit amet efficitur nulla. Praesent lobortis est ac nulla consequat, posuere efficitur enim pellentesque. Cras vel ante eu orci ornare dictum eu eu odio. Fusce placerat id nibh sed maximus. Vivamus ac ante consequat massa euismod fringilla at eget mauris. Mauris posuere tortor eu urna iaculis viverra. Sed consequat tristique nibh, id molestie nibh scelerisque a. Vestibulum mollis, diam non congue cursus, mi risus tincidunt odio, quis congue lectus sem vestibulum orci. Phasellus lobortis, ante ut suscipit gravida, nisl risus imperdiet arcu, eget condimentum felis odio in sapien. Fusce lectus lectus, sollicitudin non efficitur nec, blandit quis neque. Duis eleifend sem quis laoreet dictum. Donec convallis a eros convallis bibendum. Sed erat libero, porttitor at dolor ac, porta elementum quam. Curabitur at nibh dictum, accumsan nulla at, consectetur dui. Aliquam a fringilla diam, id rhoncus est. Nullam ultricies imperdiet diam, et lobortis tortor commodo non. Donec at finibus ex, non sollicitudin metus. Nulla luctus, quam non sodales finibus, diam nulla lobortis dui, eget faucibus nunc augue a libero. Sed auctor, metus eget pretium interdum, nisl ex egestas enim, vitae maximus ante diam at ex. Curabitur eu ipsum et est facilisis sollicitudin. Pellentesque nec lacus ut libero malesuada aliquam. Nunc eleifend quam faucibus ligula tincidunt iaculis. Pellentesque vulputate, augue vitae dignissim malesuada, leo odio malesuada est, non tincidunt neque arcu sit amet nulla. Vivamus porta mollis sem, et congue neque facilisis nec. Cras dictum augue urna, at facilisis nibh sagittis at. Vestibulum tempus ipsum neque, vel gravida orci vulputate non. Vestibulum blandit suscipit egestas. Sed a nisi pretium, ullamcorper nibh non, tempus felis. Suspendisse potenti. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vivamus semper interdum molestie. Nunc quis lacinia turpis, tincidunt dapibus eros. In vel metus eget lorem blandit finibus et sit amet nisl. Curabitur sit amet dignissim nibh. Aenean fringilla gravida augue non sagittis. Suspendisse feugiat lobortis mauris vitae commodo. Quisque eleifend est sed massa imperdiet sagittis. Sed sed ornare libero. Morbi vitae ipsum nec mi pharetra interdum. Fusce eget erat ac dolor volutpat rutrum lacinia sed sem. In ultrices felis et lectus commodo suscipit. In mattis diam ex, eu consectetur felis condimentum a. Nunc quam nibh, pharetra non mauris a, pharetra congue ipsum. Nullam ultricies sem ut neque pellentesque, nec dignissim odio aliquet. Praesent vel dapibus orci, sit amet eleifend odio. Integer eget cursus diam, ac sollicitudin massa. Duis pulvinar sit amet odio a consectetur. Cras venenatis nulla at volutpat porttitor. Sed porta ante in nibh pharetra tempus. Curabitur sed dui vel risus ultricies mattis. Sed sapien sapien, feugiat vel libero quis, porta iaculis metus. Nunc aliquam pharetra quam, in vestibulum ante interdum et. Nunc porta, libero id pharetra pulvinar, ipsum sem imperdiet lectus, at posuere massa ligula ac nisl. ");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()()();
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("nextElement", false);
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__.Preview],
    styles: [".demo-content[_ngcontent-%COMP%] {\n  overflow: scroll;\n  height: 150px;\n}\n.demo-content[_ngcontent-%COMP%]   p[_ngcontent-%COMP%] {\n  width: 1500px;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvZG9jcy9tLXVpL20tc2Nyb2xsL20tc2Nyb2xsLWRlbW8vbS1zY3JvbGwtZGVtby5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLGdCQUFBO0VBQ0EsYUFBQTtBQUNEO0FBQ0M7RUFDQyxhQUFBO0FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyIuZGVtby1jb250ZW50IHtcclxuXHRvdmVyZmxvdzogc2Nyb2xsO1xyXG5cdGhlaWdodDogMTUwcHg7XHJcblxyXG5cdHAge1xyXG5cdFx0d2lkdGg6IDE1MDBweDtcclxuXHR9XHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIifQ== */"]
  });
}

/***/ }),

/***/ 14729:
/*!***********************************************************!*\
  !*** ./src/app/docs/m-ui/m-scroll/ng-doc.dependencies.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _m_ui_m_scroll_m_scroll_demo_m_scroll_demo_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @m-ui/m-scroll/m-scroll-demo/m-scroll-demo.component */ 305);
/* harmony import */ var _ng_doc_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ng-doc.module */ 40184);


const MScrollPageDependencies = {
  module: _ng_doc_module__WEBPACK_IMPORTED_MODULE_1__.MScrollPageModule,
  // Add your demos that you are going to use in the page here
  demo: {
    MScrollDemo: _m_ui_m_scroll_m_scroll_demo_m_scroll_demo_component__WEBPACK_IMPORTED_MODULE_0__.MScrollDemo
  }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MScrollPageDependencies);

/***/ }),

/***/ 40184:
/*!*****************************************************!*\
  !*** ./src/app/docs/m-ui/m-scroll/ng-doc.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MScrollPageModule": () => (/* binding */ MScrollPageModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);


class MScrollPageModule {
  static #_ = this.ɵfac = function MScrollPageModule_Factory(t) {
    return new (t || MScrollPageModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
    type: MScrollPageModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](MScrollPageModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
})();

/***/ }),

/***/ 55355:
/*!*******************************************************!*\
  !*** ./src/app/features/preview/preview.component.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Preview": () => (/* binding */ Preview)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/list */ 6517);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_divider__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/divider */ 71528);




function Preview_span_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "span");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx_r0.title);
  }
}
function Preview_mat_divider_4_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "mat-divider");
  }
}
const _c0 = ["*"];
class Preview {
  constructor() {
    this.nextElement = true;
    this.fullWidth = false;
    this.oneLine = false;
  }
  static #_ = this.ɵfac = function Preview_Factory(t) {
    return new (t || Preview)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
    type: Preview,
    selectors: [["app-preview"]],
    inputs: {
      title: "title",
      nextElement: "nextElement",
      fullWidth: "fullWidth",
      oneLine: "oneLine"
    },
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵStandaloneFeature"]],
    ngContentSelectors: _c0,
    decls: 5,
    vars: 6,
    consts: [[4, "ngIf"], [1, "demo-preview-content"]],
    template: function Preview_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojectionDef"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "section");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, Preview_span_1_Template, 2, 1, "span", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵprojection"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, Preview_mat_divider_4_Template, 1, 0, "mat-divider", 0);
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("oneLine", ctx.oneLine);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.title);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("fullWidth", ctx.fullWidth);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.nextElement);
      }
    },
    dependencies: [_angular_material_list__WEBPACK_IMPORTED_MODULE_1__.MatListModule, _angular_material_divider__WEBPACK_IMPORTED_MODULE_2__.MatDivider, _angular_common__WEBPACK_IMPORTED_MODULE_3__.NgIf],
    styles: ["section[_ngcontent-%COMP%] {\n  padding: 16px;\n}\nsection.oneLine[_ngcontent-%COMP%] {\n  display: flex;\n  align-items: center;\n}\nsection[_ngcontent-%COMP%]   div.demo-preview-content[_ngcontent-%COMP%] {\n  display: flex;\n  flex-wrap: wrap;\n  gap: 8px;\n  padding: 8px;\n}\nsection[_ngcontent-%COMP%]   div.demo-preview-content.fullWidth[_ngcontent-%COMP%] {\n  display: block;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvZmVhdHVyZXMvcHJldmlldy9wcmV2aWV3LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsYUFBQTtBQUNEO0FBQ0M7RUFDQyxhQUFBO0VBQ0EsbUJBQUE7QUFDRjtBQUVDO0VBQ0MsYUFBQTtFQUNBLGVBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtBQUFGO0FBRUU7RUFDQyxjQUFBO0FBQUgiLCJzb3VyY2VzQ29udGVudCI6WyJzZWN0aW9uIHtcclxuXHRwYWRkaW5nOiAxNnB4O1xyXG5cclxuXHQmLm9uZUxpbmV7XHJcblx0XHRkaXNwbGF5OiBmbGV4O1xyXG5cdFx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHR9XHJcblxyXG5cdGRpdi5kZW1vLXByZXZpZXctY29udGVudHtcclxuXHRcdGRpc3BsYXk6IGZsZXg7XHJcblx0XHRmbGV4LXdyYXA6IHdyYXA7XHJcblx0XHRnYXA6IDhweDtcclxuXHRcdHBhZGRpbmc6IDhweDtcclxuXHJcblx0XHQmLmZ1bGxXaWR0aHtcclxuXHRcdFx0ZGlzcGxheTogYmxvY2s7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0= */"]
  });
}

/***/ }),

/***/ 48096:
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-ui/m-scroll/assets/MScrollDemo/HTML/Asset1.html ***!
  \***********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"html\" class=\"hljs language-html ngde\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Scroll\"</span> [<span class=\"hljs-attr ngde\">nextElement</span>]=<span class=\"hljs-string ngde\">\"false\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">div</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"demo-content\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">p</span>></span>\n      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce a dictum\n      orci. Vivamus nec erat elementum nunc semper semper ac vel tortor. Aenean\n      rutrum rutrum diam, facilisis consectetur nisi auctor at. Etiam dui felis,\n      porta sit amet tincidunt nec, aliquet sed nunc. Morbi mattis elit a tempor\n      pulvinar. Interdum et malesuada fames ac ante ipsum primis in faucibus.\n      Pellentesque feugiat turpis at risus suscipit porttitor. Nullam sit amet\n      efficitur nulla. Praesent lobortis est ac nulla consequat, posuere\n      efficitur enim pellentesque. Cras vel ante eu orci ornare dictum eu eu\n      odio. Fusce placerat id nibh sed maximus. Vivamus ac ante consequat massa\n      euismod fringilla at eget mauris. Mauris posuere tortor eu urna iaculis\n      viverra. Sed consequat tristique nibh, id molestie nibh scelerisque a.\n      Vestibulum mollis, diam non congue cursus, mi risus tincidunt odio, quis\n      congue lectus sem vestibulum orci. Phasellus lobortis, ante ut suscipit\n      gravida, nisl risus imperdiet arcu, eget condimentum felis odio in sapien.\n      Fusce lectus lectus, sollicitudin non efficitur nec, blandit quis neque.\n      Duis eleifend sem quis laoreet dictum. Donec convallis a eros convallis\n      bibendum. Sed erat libero, porttitor at dolor ac, porta elementum quam.\n      Curabitur at nibh dictum, accumsan nulla at, consectetur dui. Aliquam a\n      fringilla diam, id rhoncus est. Nullam ultricies imperdiet diam, et\n      lobortis tortor commodo non. Donec at finibus ex, non sollicitudin metus.\n      Nulla luctus, quam non sodales finibus, diam nulla lobortis dui, eget\n      faucibus nunc augue a libero. Sed auctor, metus eget pretium interdum,\n      nisl ex egestas enim, vitae maximus ante diam at ex. Curabitur eu ipsum et\n      est facilisis sollicitudin. Pellentesque nec lacus ut libero malesuada\n      aliquam. Nunc eleifend quam faucibus ligula tincidunt iaculis.\n      Pellentesque vulputate, augue vitae dignissim malesuada, leo odio\n      malesuada est, non tincidunt neque arcu sit amet nulla. Vivamus porta\n      mollis sem, et congue neque facilisis nec. Cras dictum augue urna, at\n      facilisis nibh sagittis at. Vestibulum tempus ipsum neque, vel gravida\n      orci vulputate non. Vestibulum blandit suscipit egestas. Sed a nisi\n      pretium, ullamcorper nibh non, tempus felis. Suspendisse potenti.\n      Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere\n      cubilia curae; Vivamus semper interdum molestie. Nunc quis lacinia turpis,\n      tincidunt dapibus eros. In vel metus eget lorem blandit finibus et sit\n      amet nisl. Curabitur sit amet dignissim nibh. Aenean fringilla gravida\n      augue non sagittis. Suspendisse feugiat lobortis mauris vitae commodo.\n      Quisque eleifend est sed massa imperdiet sagittis. Sed sed ornare libero.\n      Morbi vitae ipsum nec mi pharetra interdum. Fusce eget erat ac dolor\n      volutpat rutrum lacinia sed sem. In ultrices felis et lectus commodo\n      suscipit. In mattis diam ex, eu consectetur felis condimentum a. Nunc quam\n      nibh, pharetra non mauris a, pharetra congue ipsum. Nullam ultricies sem\n      ut neque pellentesque, nec dignissim odio aliquet. Praesent vel dapibus\n      orci, sit amet eleifend odio. Integer eget cursus diam, ac sollicitudin\n      massa. Duis pulvinar sit amet odio a consectetur. Cras venenatis nulla at\n      volutpat porttitor. Sed porta ante in nibh pharetra tempus. Curabitur sed\n      dui vel risus ultricies mattis. Sed sapien sapien, feugiat vel libero\n      quis, porta iaculis metus. Nunc aliquam pharetra quam, in vestibulum ante\n      interdum et. Nunc porta, libero id pharetra pulvinar, ipsum sem imperdiet\n      lectus, at posuere massa ligula ac nisl.\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">p</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">div</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n</code></pre>");

/***/ }),

/***/ 88850:
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-ui/m-scroll/assets/MScrollDemo/SCSS/Asset2.html ***!
  \***********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"scss\" class=\"hljs language-scss ngde\"><span class=\"hljs-selector-class ngde\">.demo-content</span> {\n  <span class=\"hljs-attribute ngde\">overflow</span>: scroll;\n  <span class=\"hljs-attribute ngde\">height</span>: <span class=\"hljs-number ngde\">150px</span>;\n\n  <span class=\"hljs-selector-tag ngde\">p</span> {\n    <span class=\"hljs-attribute ngde\">width</span>: <span class=\"hljs-number ngde\">1500px</span>;\n  }\n}\n</code></pre>");

/***/ }),

/***/ 63971:
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-ui/m-scroll/assets/MScrollDemo/TypeScript/Asset0.html ***!
  \*****************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">CommonModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/common\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Component</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/core\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Preview</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"src/app/features/preview/preview.component\"</span>;\n\n<span class=\"hljs-meta ngde\">@Component</span>({\n  <span class=\"hljs-attr ngde\">selector</span>: <span class=\"hljs-string ngde\">\"app-m-scroll-demo\"</span>,\n  <span class=\"hljs-attr ngde\">standalone</span>: <span class=\"hljs-literal ngde\">true</span>,\n  <span class=\"hljs-attr ngde\">imports</span>: [<span class=\"hljs-title class_ ngde\">CommonModule</span>, <span class=\"hljs-title class_ ngde\">Preview</span>],\n  <span class=\"hljs-attr ngde\">templateUrl</span>: <span class=\"hljs-string ngde\">\"./m-scroll-demo.component.html\"</span>,\n  <span class=\"hljs-attr ngde\">styleUrls</span>: [<span class=\"hljs-string ngde\">\"./m-scroll-demo.component.scss\"</span>],\n})\n<span class=\"hljs-keyword ngde\">export</span> <span class=\"hljs-keyword ngde\">class</span> <span class=\"hljs-title class_ ngde\">MScrollDemo</span> {}\n</code></pre>");

/***/ }),

/***/ 47605:
/*!**********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-ui/m-scroll/index.html ***!
  \**********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<h1 id=\"m-scroll\" class=\"ngde\">M-scroll<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/m-ui/m-scroll#m-scroll\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1><p class=\"ngde\">The scrollbar is customized natively (no additional classes are needed). However, Firefox does not allow for as much precise customization of the scrollbar as other browsers do. <a href=\"https://developer.mozilla.org/fr/docs/Web/CSS/::-webkit-scrollbar\" class=\"ngde\">More infomation</a></p><ng-doc-demo componentname=\"MScrollDemo\" indexable=\"false\" class=\"ngde\"><div id=\"options\" class=\"ngde\">{}</div></ng-doc-demo>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_guides_m-ui_m-scroll_module_ts.js.map