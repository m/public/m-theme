"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_guides_getting-started_what-is_module_ts"],{

/***/ 69029:
/*!*****************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/getting-started/what-is/module.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_getting_started_what_is_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/getting-started/what-is/index.html */ 24565);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);







class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = '';
    this.pageType = 'guide';
    this.pageContent = _raw_loader_ng_doc_m_docs_guides_getting_started_what_is_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'What is'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
})();

/***/ }),

/***/ 24565:
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/getting-started/what-is/index.html ***!
  \********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<h1 id=\"what-is-m-ui--m-theme\" class=\"ngde\">What is M-UI | M-THEME<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/what-is#what-is-m-ui--m-theme\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1><p class=\"ngde\">M-UI is a component library built on top of <a href=\"https://v15.material.angular.io/\" class=\"ngde\">Angular Material 15</a>, designed to extend its capabilities while leveraging M-Theme for styling consistency. It provides:</p><ul class=\"ngde\"><li class=\"ngde\">Ready-to-use Angular components tailored for specific use cases.</li><li class=\"ngde\">An intuitive API for developers, enabling seamless integration.</li><li class=\"ngde\">Theming support out-of-the-box, powered by M-Theme.</li></ul><hr class=\"ngde\"><h2 id=\"motivation\" class=\"ngde\">Motivation<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/what-is#motivation\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><p class=\"ngde\">While working on various projects, especially when building Angular applications, I noticed a common need for:</p><ol class=\"ngde\"><li class=\"ngde\">A unified theming system that simplifies the customization of Angular Material components.</li><li class=\"ngde\">A component library that goes beyond the basic Angular Material design system to meet specific business needs.</li></ol><p class=\"ngde\">Other solutions often fell short:</p><ul class=\"ngde\"><li class=\"ngde\">Writing custom themes manually was repetitive and prone to errors.</li><li class=\"ngde\">Extending Angular Material components often required complex configurations.</li><li class=\"ngde\">Integrating theming and custom components into a single cohesive package was challenging.</li></ul><p class=\"ngde\">M-Theme and M-UI address these gaps:</p><ul class=\"ngde\"><li class=\"ngde\">M-Theme simplifies theming by centralizing styles, reducing repetitive code, and offering pre-built palettes and functions.</li><li class=\"ngde\">M-UI extends Angular Material with additional components, allowing developers to create rich, feature-complete applications faster.</li></ul><h2 id=\"how-does-it-work\" class=\"ngde\">How does it work?<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/what-is#how-does-it-work\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><h3 id=\"m-theme\" class=\"ngde\">M-Theme<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/what-is#m-theme\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h3><ul class=\"ngde\"><li class=\"ngde\">Provides SCSS files for defining light and dark themes.</li><li class=\"ngde\">Offers pre-configured variables and helper functions to make styling Angular Material components easier.</li><li class=\"ngde\">Works seamlessly with Angular's build process, requiring only the addition of the necessary imports in your main SCSS file.</li></ul><h3 id=\"m-ui\" class=\"ngde\">M-ui<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/what-is#m-ui\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h3><ul class=\"ngde\"><li class=\"ngde\">Built as a library of Angular components that extend Angular Material's functionality.</li><li class=\"ngde\">Components are designed to be modular:<ul class=\"ngde\"><li class=\"ngde\">Import only the components you need or the entire library.</li><li class=\"ngde\">All components are styled consistently using M-Theme.</li></ul></li><li class=\"ngde\">Supports standalone usage and integration into existing Angular projects.</li></ul>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_guides_getting-started_what-is_module_ts.js.map