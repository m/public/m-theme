(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["main"],{

/***/ 86539:
/*!*********************************!*\
  !*** ./.ng-doc/m-docs/index.ts ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NG_DOC_ROUTING": () => (/* reexport safe */ _ng_doc_routing__WEBPACK_IMPORTED_MODULE_2__.NG_DOC_ROUTING),
/* harmony export */   "NgDocGeneratedModule": () => (/* reexport safe */ _ng_doc_generated_module__WEBPACK_IMPORTED_MODULE_1__.NgDocGeneratedModule),
/* harmony export */   "ngDocContextProvider": () => (/* reexport safe */ _ng_doc_context__WEBPACK_IMPORTED_MODULE_0__.ngDocContextProvider)
/* harmony export */ });
/* harmony import */ var _ng_doc_context__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ng-doc.context */ 53495);
/* harmony import */ var _ng_doc_generated_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ng-doc.generated.module */ 35399);
/* harmony import */ var _ng_doc_routing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ng-doc.routing */ 18516);




/***/ }),

/***/ 53495:
/*!******************************************!*\
  !*** ./.ng-doc/m-docs/ng-doc.context.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ngDocContextProvider": () => (/* binding */ ngDocContextProvider)
/* harmony export */ });
/* harmony import */ var _ng_doc_app_tokens__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ng-doc/app/tokens */ 64314);

const ngDocContextProvider = {
  provide: _ng_doc_app_tokens__WEBPACK_IMPORTED_MODULE_0__.NG_DOC_CONTEXT,
  useValue: {
    navigation: [{
      title: 'Getting Started',
      route: '/getting-started',
      expandable: true,
      expanded: true,
      order: 1,
      hidden: false,
      children: [{
        title: 'What is',
        route: '/getting-started/what-is',
        order: 1,
        hidden: false
      }, {
        title: 'Peer dependency',
        route: '/getting-started/peer-dependency',
        order: 2,
        hidden: false
      }, {
        title: 'Install',
        route: '/getting-started/install',
        order: 3,
        hidden: false
      }, {
        title: 'Usage',
        route: '/getting-started/usage',
        order: 4,
        hidden: false
      }, {
        title: 'Docs',
        route: '/getting-started/docs',
        order: 5,
        hidden: false
      }, {
        title: 'Migration',
        route: '/getting-started/migration',
        order: 6,
        hidden: false
      }]
    }, {
      title: 'M-theme',
      route: '/m-theme',
      expandable: true,
      expanded: false,
      order: 2,
      hidden: false,
      children: [{
        title: 'Colors',
        route: '/m-theme/theming'
      }]
    }, {
      title: 'M-ui',
      route: '/m-ui',
      expandable: true,
      expanded: false,
      order: 3,
      hidden: false,
      children: [{
        title: 'M-affluence',
        route: '/m-ui/m-affluence'
      }, {
        title: 'M-app-download',
        route: '/m-ui/m-app-download'
      }, {
        title: 'M-disturbance-display',
        route: '/m-ui/m-disturbance-display'
      }, {
        title: 'M-icons',
        route: '/m-ui/m-icons'
      }, {
        title: 'M-list-wrapper',
        route: '/m-ui/m-list-wrapper'
      }, {
        title: 'M-logo-lines',
        route: '/m-ui/m-logo-lines'
      }, {
        title: 'M-scroll',
        route: '/m-ui/m-scroll'
      }, {
        title: 'M-table',
        route: '/m-ui/m-table'
      }]
    }, {
      title: 'Angular-materiel',
      route: '/angular-materiel',
      expandable: true,
      expanded: false,
      order: 4,
      hidden: false,
      children: [{
        title: 'Autocomplete',
        route: '/angular-materiel/material-autocomplete'
      }, {
        title: 'Badge',
        route: '/angular-materiel/material-badge'
      }, {
        title: 'Bottom-sheet',
        route: '/angular-materiel/material-bottom-sheet'
      }, {
        title: 'Button',
        route: '/angular-materiel/material-button'
      }, {
        title: 'Button-toggle',
        route: '/angular-materiel/material-button-toggle'
      }, {
        title: 'Card',
        route: '/angular-materiel/material-card'
      }, {
        title: 'Checkbox',
        route: '/angular-materiel/material-checkbox'
      }, {
        title: 'Chips',
        route: '/angular-materiel/material-chips'
      }, {
        title: 'Datepicker',
        route: '/angular-materiel/material-datepicker'
      }, {
        title: 'Dialog',
        route: '/angular-materiel/material-dialog'
      }, {
        title: 'Divider',
        route: '/angular-materiel/material-divider'
      }, {
        title: 'Expansion Panel',
        route: '/angular-materiel/material-expansion-panel'
      }, {
        title: 'Form field',
        route: '/angular-materiel/material-form-field'
      }, {
        title: 'Input',
        route: '/angular-materiel/material-input'
      }, {
        title: 'List',
        route: '/angular-materiel/material-list'
      }, {
        title: 'Progress bar',
        route: '/angular-materiel/material-progress-bar'
      }, {
        title: 'Progress spinner',
        route: '/angular-materiel/material-progress-spinner'
      }, {
        title: 'Radio',
        route: '/angular-materiel/material-radio'
      }, {
        title: 'Ripples',
        route: '/angular-materiel/material-ripples'
      }, {
        title: 'Select',
        route: '/angular-materiel/material-select'
      }, {
        title: 'Sidenav',
        route: '/angular-materiel/material-sidenav'
      }, {
        title: 'Slide toggle',
        route: '/angular-materiel/material-slide-toggle'
      }, {
        title: 'Slider',
        route: '/angular-materiel/material-slider'
      }, {
        title: 'Snackbar',
        route: '/angular-materiel/material-snackbar'
      }, {
        title: 'Sort header',
        route: '/angular-materiel/material-sort-header'
      }, {
        title: 'Stepper',
        route: '/angular-materiel/material-stepper'
      }, {
        title: 'Table',
        route: '/angular-materiel/material-table'
      }, {
        title: 'Tabs',
        route: '/angular-materiel/material-tabs'
      }, {
        title: 'Toolbar',
        route: '/angular-materiel/material-toolbar'
      }, {
        title: 'Tooltip',
        route: '/angular-materiel/material-tooltip'
      }, {
        title: 'Tree',
        route: '/angular-materiel/material-tree'
      }]
    }, {
      title: 'API',
      route: '/api'
    }, {
      title: 'Others',
      route: '/others',
      expandable: true,
      expanded: false
    }]
  }
};

/***/ }),

/***/ 35399:
/*!***************************************************!*\
  !*** ./.ng-doc/m-docs/ng-doc.generated.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NgDocGeneratedModule": () => (/* binding */ NgDocGeneratedModule)
/* harmony export */ });
/* harmony import */ var _ng_doc_context__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ng-doc.context */ 53495);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);


class NgDocGeneratedModule {
  static forRoot() {
    return {
      ngModule: NgDocGeneratedModule,
      providers: [_ng_doc_context__WEBPACK_IMPORTED_MODULE_0__.ngDocContextProvider]
    };
  }
  static #_ = this.ɵfac = function NgDocGeneratedModule_Factory(t) {
    return new (t || NgDocGeneratedModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineNgModule"]({
    type: NgDocGeneratedModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjector"]({});
}

/***/ }),

/***/ 18516:
/*!******************************************!*\
  !*** ./.ng-doc/m-docs/ng-doc.routing.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NG_DOC_ROUTING": () => (/* binding */ NG_DOC_ROUTING)
/* harmony export */ });
const NG_DOC_ROUTING = [{
  path: 'api',
  loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-kind-icon_mjs-node_modules_ng--95dcfb"), __webpack_require__.e("_ng-doc_m-docs_api_api_ng-doc-api-list_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/ng-doc-api-list.module */ 4330)).then(m => m.DynamicModule)
}, {
  path: 'angular-materiel',
  loadChildren: () => __webpack_require__.e(/*! import() */ "_ng-doc_m-docs_guides_angular-materiel_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/angular-materiel/module */ 38551)).then(m => m.DynamicModule)
}, {
  path: 'others',
  loadChildren: () => __webpack_require__.e(/*! import() */ "_ng-doc_m-docs_guides_others_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/others/module */ 31622)).then(m => m.DynamicModule)
}, {
  path: 'm-theme',
  loadChildren: () => __webpack_require__.e(/*! import() */ "_ng-doc_m-docs_guides_m-theme_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/m-theme/module */ 93204)).then(m => m.DynamicModule)
}, {
  path: 'm-ui',
  loadChildren: () => __webpack_require__.e(/*! import() */ "_ng-doc_m-docs_guides_m-ui_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/m-ui/module */ 66856)).then(m => m.DynamicModule)
}, {
  path: 'getting-started',
  loadChildren: () => __webpack_require__.e(/*! import() */ "_ng-doc_m-docs_guides_getting-started_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/getting-started/module */ 81715)).then(m => m.DynamicModule)
}];

/***/ }),

/***/ 90158:
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppRoutingModule": () => (/* binding */ AppRoutingModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);



const routes = [{
  path: '',
  redirectTo: '/getting-started/what-is',
  pathMatch: 'full'
}];
class AppRoutingModule {
  static #_ = this.ɵfac = function AppRoutingModule_Factory(t) {
    return new (t || AppRoutingModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
    type: AppRoutingModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule.forRoot(routes, {}), _angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppRoutingModule, {
    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule]
  });
})();

/***/ }),

/***/ 55041:
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppComponent": () => (/* binding */ AppComponent),
/* harmony export */   "THEME": () => (/* binding */ THEME)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-doc/app */ 65162);
/* harmony import */ var src_app_services_theming_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/services/theming.service */ 95661);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ng_doc_app_components_navbar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ng-doc/app/components/navbar */ 68983);
/* harmony import */ var _ng_doc_app_components_sidebar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-doc/app/components/sidebar */ 3301);
/* harmony import */ var _ng_doc_app_components_root__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-doc/app/components/root */ 44690);







function AppComponent_ng_template_2_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "h3", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](1, "M-theme/M-ui");
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
  }
}
function AppComponent_ng_template_4_Template(rf, ctx) {
  if (rf & 1) {
    const _r5 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "label", 4)(1, "input", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("change", function AppComponent_ng_template_4_Template_input_change_1_listener() {
      _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r5);
      const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵresetView"](ctx_r4.switchTheme());
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "span", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
  }
  if (rf & 2) {
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("checked", ctx_r3.theming === ctx_r3.THEME.LIGHT);
  }
}
var THEME;
(function (THEME) {
  THEME["DARK"] = "dark-theme";
  THEME["LIGHT"] = "light-theme";
})(THEME || (THEME = {}));
class AppComponent {
  static #_ = this.THEME_KEY = 'm-ui:theme';
  constructor(rendererFactory, themeService, themingService) {
    this.rendererFactory = rendererFactory;
    this.themeService = themeService;
    this.themingService = themingService;
    this.THEME = THEME;
    this.theming = THEME.DARK;
    this.renderer = this.rendererFactory.createRenderer(null, null);
    this.initializeTheme();
  }
  /**
   * Toggles between DARK and LIGHT themes
   */
  switchTheme() {
    const newTheme = this.theming === THEME.DARK ? THEME.LIGHT : THEME.DARK;
    this.updateTheme(newTheme);
  }
  /**
   * Initializes the theme based on user preferences or localStorage
   */
  initializeTheme() {
    const savedTheme = localStorage.getItem(AppComponent.THEME_KEY);
    const systemPreference = this.getSystemTheme();
    const defaultTheme = savedTheme || systemPreference || THEME.DARK;
    this.updateTheme(defaultTheme);
    // Listen to system preference changes.
    this.listenToSystemThemeChanges();
  }
  /**
   * Updates the current theme
   * @param theme - The new theme to apply
   */
  updateTheme(theme) {
    // Update the body class
    this.renderer.removeClass(document.body, this.theming);
    this.renderer.addClass(document.body, theme);
    // Save to localStorage and synchronize services
    localStorage.setItem(AppComponent.THEME_KEY, theme);
    this.themingService.theming = theme;
    this.themeService.set(theme).then();
    this.theming = theme;
  }
  /**
   * Detects the system theme (light/dark) using prefers-color-scheme
   * @returns The detected system theme
   */
  getSystemTheme() {
    return window.matchMedia('(prefers-color-scheme: dark)').matches ? THEME.DARK : THEME.LIGHT;
  }
  /**
   * Listens for system theme changes and updates the theme if no preference is saved
   */
  listenToSystemThemeChanges() {
    window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', event => {
      const systemTheme = event.matches ? THEME.DARK : THEME.LIGHT;
      if (!localStorage.getItem(AppComponent.THEME_KEY)) this.updateTheme(systemTheme);
    });
  }
  static #_2 = this.ɵfac = function AppComponent_Factory(t) {
    return new (t || AppComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_1__.RendererFactory2), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_ng_doc_app__WEBPACK_IMPORTED_MODULE_2__.NgDocThemeService), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](src_app_services_theming_service__WEBPACK_IMPORTED_MODULE_0__.ThemingService));
  };
  static #_3 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
    type: AppComponent,
    selectors: [["app-root"]],
    decls: 8,
    vars: 2,
    consts: [[3, "leftContent", "rightContent"], ["leftContent", ""], ["rightContent", ""], ["routerLink", ""], ["id", "switch", 1, "switch"], ["type", "checkbox", "id", "slider", 3, "checked", "change"], [1, "slider", "round"]],
    template: function AppComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "ng-doc-root")(1, "ng-doc-navbar", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](2, AppComponent_ng_template_2_Template, 2, 0, "ng-template", null, 1, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](4, AppComponent_ng_template_4_Template, 3, 1, "ng-template", null, 2, _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplateRefExtractor"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](6, "ng-doc-sidebar")(7, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
      }
      if (rf & 2) {
        const _r0 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](3);
        const _r2 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵreference"](5);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("leftContent", _r0)("rightContent", _r2);
      }
    },
    dependencies: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterOutlet, _angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterLink, _ng_doc_app_components_navbar__WEBPACK_IMPORTED_MODULE_4__.NgDocNavbarComponent, _ng_doc_app_components_sidebar__WEBPACK_IMPORTED_MODULE_5__.NgDocSidebarComponent, _ng_doc_app_components_root__WEBPACK_IMPORTED_MODULE_6__.NgDocRootComponent],
    styles: ["h3[_ngcontent-%COMP%] {\n  cursor: pointer;\n  margin: 0;\n}\n\n.switch[_ngcontent-%COMP%] {\n  position: relative;\n  display: inline-block;\n  width: 40px;\n  height: 20px;\n}\n.switch[_ngcontent-%COMP%]   input[_ngcontent-%COMP%] {\n  opacity: 0;\n  width: 0;\n  height: 0;\n}\n.switch[_ngcontent-%COMP%]   .slider[_ngcontent-%COMP%] {\n  position: absolute;\n  cursor: pointer;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  transition: 0.4s;\n}\n.switch[_ngcontent-%COMP%]   .slider[_ngcontent-%COMP%]::before {\n  position: absolute;\n  content: \"\";\n  height: 30px;\n  width: 30px;\n  left: 0;\n  top: 0;\n  bottom: 0;\n  margin: auto;\n  transition: 0.4s;\n  box-shadow: 0 0 15px rgba(32, 32, 32, 0.2392156863);\n  background: white url('moon.svg') no-repeat center;\n}\n.switch[_ngcontent-%COMP%]   .slider.round[_ngcontent-%COMP%] {\n  border-radius: 34px;\n}\n.switch[_ngcontent-%COMP%]   .slider.round[_ngcontent-%COMP%]::before {\n  border-radius: 50%;\n}\n.switch[_ngcontent-%COMP%]   input[_ngcontent-%COMP%]:checked    + .slider[_ngcontent-%COMP%]::before {\n  transform: translateX(18px);\n  background: white url('sun.svg') no-repeat center;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0MsZUFBQTtFQUNBLFNBQUE7QUFDRDs7QUFFQTtFQUNDLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQUNEO0FBQ0M7RUFDQyxVQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7QUFDRjtBQUVDO0VBQ0Msa0JBQUE7RUFDQSxlQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLGdCQUFBO0FBQUY7QUFFRTtFQUNDLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsT0FBQTtFQUNBLE1BQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsbURBQUE7RUFDQSxrREFBQTtBQUFIO0FBR0U7RUFDQyxtQkFBQTtBQURIO0FBR0c7RUFDQyxrQkFBQTtBQURKO0FBT0U7RUFDQywyQkFBQTtFQUNBLGlEQUFBO0FBTEgiLCJzb3VyY2VzQ29udGVudCI6WyJoMyB7XHJcblx0Y3Vyc29yOiBwb2ludGVyO1xyXG5cdG1hcmdpbjogMDtcclxufVxyXG5cclxuLnN3aXRjaCB7XHJcblx0cG9zaXRpb246IHJlbGF0aXZlO1xyXG5cdGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuXHR3aWR0aDogNDBweDtcclxuXHRoZWlnaHQ6IDIwcHg7XHJcblxyXG5cdGlucHV0IHtcclxuXHRcdG9wYWNpdHk6IDA7XHJcblx0XHR3aWR0aDogMDtcclxuXHRcdGhlaWdodDogMDtcclxuXHR9XHJcblxyXG5cdC5zbGlkZXIge1xyXG5cdFx0cG9zaXRpb246IGFic29sdXRlO1xyXG5cdFx0Y3Vyc29yOiBwb2ludGVyO1xyXG5cdFx0dG9wOiAwO1xyXG5cdFx0bGVmdDogMDtcclxuXHRcdHJpZ2h0OiAwO1xyXG5cdFx0Ym90dG9tOiAwO1xyXG5cdFx0dHJhbnNpdGlvbjogMC40cztcclxuXHJcblx0XHQmOjpiZWZvcmUge1xyXG5cdFx0XHRwb3NpdGlvbjogYWJzb2x1dGU7XHJcblx0XHRcdGNvbnRlbnQ6IFwiXCI7XHJcblx0XHRcdGhlaWdodDogMzBweDtcclxuXHRcdFx0d2lkdGg6IDMwcHg7XHJcblx0XHRcdGxlZnQ6IDA7XHJcblx0XHRcdHRvcDogMDtcclxuXHRcdFx0Ym90dG9tOiAwO1xyXG5cdFx0XHRtYXJnaW46IGF1dG87XHJcblx0XHRcdHRyYW5zaXRpb246IDAuNHM7XHJcblx0XHRcdGJveC1zaGFkb3c6IDAgMCAxNXB4ICMyMDIwMjAzZDtcclxuXHRcdFx0YmFja2dyb3VuZDogd2hpdGUgdXJsKCcuLi9hc3NldHMvbW9vbi5zdmcnKSBuby1yZXBlYXQgY2VudGVyO1xyXG5cdFx0fVxyXG5cclxuXHRcdCYucm91bmQge1xyXG5cdFx0XHRib3JkZXItcmFkaXVzOiAzNHB4O1xyXG5cclxuXHRcdFx0Jjo6YmVmb3JlIHtcclxuXHRcdFx0XHRib3JkZXItcmFkaXVzOiA1MCU7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdGlucHV0OmNoZWNrZWQgKyAuc2xpZGVyIHtcclxuXHRcdCY6OmJlZm9yZSB7XHJcblx0XHRcdHRyYW5zZm9ybTogdHJhbnNsYXRlWCgxOHB4KTtcclxuXHRcdFx0YmFja2dyb3VuZDogd2hpdGUgdXJsKCcuLi9hc3NldHMvc3VuLnN2ZycpIG5vLXJlcGVhdCBjZW50ZXI7XHJcblx0XHR9XHJcblx0fVxyXG59XHJcbiJdLCJzb3VyY2VSb290IjoiIn0= */"]
  });
}

/***/ }),

/***/ 36747:
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppModule": () => (/* binding */ AppModule)
/* harmony export */ });
/* harmony import */ var _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/cdk/a11y */ 24218);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ 58987);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/forms */ 2508);
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/core */ 59121);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/platform-browser */ 34497);
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/platform-browser/animations */ 37146);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ng-doc/app */ 11382);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ng-doc/app */ 5216);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ng-doc/app */ 19166);
/* harmony import */ var _ng_doc_app_components_navbar__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ng-doc/app/components/navbar */ 68983);
/* harmony import */ var _ng_doc_app_components_sidebar__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @ng-doc/app/components/sidebar */ 3301);
/* harmony import */ var _ng_doc_generated__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ng-doc/generated */ 86539);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app-routing.module */ 90158);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ 55041);
/* harmony import */ var _angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/locales/fr */ 58384);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _ng_doc_m_docs_ng_doc_generated_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../.ng-doc/m-docs/ng-doc.generated.module */ 35399);














// Angular





(0,_angular_common__WEBPACK_IMPORTED_MODULE_4__.registerLocaleData)(_angular_common_locales_fr__WEBPACK_IMPORTED_MODULE_5__["default"], 'fr');
class AppModule {
  static #_ = this.ɵfac = function AppModule_Factory(t) {
    return new (t || AppModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineNgModule"]({
    type: AppModule,
    bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__.AppComponent]
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjector"]({
    providers: [(0,_angular_common_http__WEBPACK_IMPORTED_MODULE_7__.provideHttpClient)((0,_angular_common_http__WEBPACK_IMPORTED_MODULE_7__.withInterceptorsFromDi)()), (0,_ng_doc_app__WEBPACK_IMPORTED_MODULE_8__.provideSearchEngine)(_ng_doc_app__WEBPACK_IMPORTED_MODULE_9__.NgDocDefaultSearchEngine)],
    imports: [_angular_material_core__WEBPACK_IMPORTED_MODULE_10__.MatNativeDateModule, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_11__.BrowserModule, _app_routing_module__WEBPACK_IMPORTED_MODULE_1__.AppRoutingModule, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__.BrowserAnimationsModule, _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_13__.A11yModule, _angular_forms__WEBPACK_IMPORTED_MODULE_14__.ReactiveFormsModule, _angular_router__WEBPACK_IMPORTED_MODULE_15__.RouterModule.forRoot(_ng_doc_generated__WEBPACK_IMPORTED_MODULE_0__.NG_DOC_ROUTING, {
      scrollPositionRestoration: 'enabled',
      anchorScrolling: 'enabled',
      scrollOffset: [0, 70]
    }), _ng_doc_app_components_navbar__WEBPACK_IMPORTED_MODULE_16__.NgDocNavbarModule, _ng_doc_app_components_sidebar__WEBPACK_IMPORTED_MODULE_17__.NgDocSidebarModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_18__.NgDocModule.forRoot({
      defaultThemeId: _app_component__WEBPACK_IMPORTED_MODULE_2__.THEME.DARK,
      themes: [{
        id: _app_component__WEBPACK_IMPORTED_MODULE_2__.THEME.DARK,
        path: 'assets/themes/dark-theme.css'
      }, {
        id: _app_component__WEBPACK_IMPORTED_MODULE_2__.THEME.LIGHT,
        path: 'assets/themes/light-theme.css'
      }]
    }), _ng_doc_generated__WEBPACK_IMPORTED_MODULE_0__.NgDocGeneratedModule.forRoot()]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵsetNgModuleScope"](AppModule, {
    declarations: [_app_component__WEBPACK_IMPORTED_MODULE_2__.AppComponent],
    imports: [_angular_material_core__WEBPACK_IMPORTED_MODULE_10__.MatNativeDateModule, _angular_platform_browser__WEBPACK_IMPORTED_MODULE_11__.BrowserModule, _app_routing_module__WEBPACK_IMPORTED_MODULE_1__.AppRoutingModule, _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_12__.BrowserAnimationsModule, _angular_cdk_a11y__WEBPACK_IMPORTED_MODULE_13__.A11yModule, _angular_forms__WEBPACK_IMPORTED_MODULE_14__.ReactiveFormsModule, _angular_router__WEBPACK_IMPORTED_MODULE_15__.RouterModule, _ng_doc_app_components_navbar__WEBPACK_IMPORTED_MODULE_16__.NgDocNavbarModule, _ng_doc_app_components_sidebar__WEBPACK_IMPORTED_MODULE_17__.NgDocSidebarModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_18__.NgDocModule, _ng_doc_m_docs_ng_doc_generated_module__WEBPACK_IMPORTED_MODULE_3__.NgDocGeneratedModule]
  });
})();

/***/ }),

/***/ 95661:
/*!*********************************************!*\
  !*** ./src/app/services/theming.service.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ThemingService": () => (/* binding */ ThemingService)
/* harmony export */ });
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs */ 76317);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);


class ThemingService {
  constructor() {
    this._theming = new rxjs__WEBPACK_IMPORTED_MODULE_0__.BehaviorSubject(null);
  }
  get theming() {
    return this._theming.value;
  }
  set theming(value) {
    this._theming.next(value);
  }
  themingObservable() {
    return this._theming.asObservable();
  }
  static #_ = this.ɵfac = function ThemingService_Factory(t) {
    return new (t || ThemingService)();
  };
  static #_2 = this.ɵprov = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineInjectable"]({
    token: ThemingService,
    factory: ThemingService.ɵfac,
    providedIn: 'root'
  });
}

/***/ }),

/***/ 92340:
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "environment": () => (/* binding */ environment)
/* harmony export */ });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
  production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.

/***/ }),

/***/ 14431:
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ 34497);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app/app.module */ 36747);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ 92340);




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.production) {
  (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.enableProdMode)();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__.platformBrowser().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_0__.AppModule).catch(err => console.error(err));

/***/ }),

/***/ 17374:
/*!**************************************************************************************************!*\
  !*** ./node_modules/@orama/orama/dist/components/ lazy ^\.\/.*\/.*\.js$ strict namespace object ***!
  \**************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./tokenizer/diacritics.js": 57365,
	"./tokenizer/index.js": 74350,
	"./tokenizer/languages.js": 74627,
	"./tokenizer/stemmers.js": 93580,
	"./tokenizer/stop-words/de.js": 46222,
	"./tokenizer/stop-words/dk.js": 58017,
	"./tokenizer/stop-words/en.js": 76416,
	"./tokenizer/stop-words/es.js": 35621,
	"./tokenizer/stop-words/fi.js": 6717,
	"./tokenizer/stop-words/fr.js": 26675,
	"./tokenizer/stop-words/index.js": 56176,
	"./tokenizer/stop-words/it.js": 53098,
	"./tokenizer/stop-words/nl.js": 74376,
	"./tokenizer/stop-words/no.js": 18266,
	"./tokenizer/stop-words/pt.js": 41738,
	"./tokenizer/stop-words/ru.js": 56180,
	"./tokenizer/stop-words/se.js": 56105
};

function webpackAsyncContext(req) {
	return Promise.resolve().then(() => {
		if(!__webpack_require__.o(map, req)) {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		}

		var id = map[req];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = () => (Object.keys(map));
webpackAsyncContext.id = 17374;
module.exports = webpackAsyncContext;

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendor"], () => (__webpack_exec__(14431)));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=main.js.map