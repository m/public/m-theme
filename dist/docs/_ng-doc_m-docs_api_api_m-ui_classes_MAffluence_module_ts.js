"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_api_api_m-ui_classes_MAffluence_module_ts"],{

/***/ 51215:
/*!******************************************************************!*\
  !*** ./.ng-doc/m-docs/api/api/m-ui/classes/MAffluence/module.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_api_api_m_ui_classes_MAffluence_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/api/api/m-ui/classes/MAffluence/index.html */ 36132);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);







class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = undefined;
    this.pageType = 'api';
    this.pageContent = _raw_loader_ng_doc_m_docs_api_api_m_ui_classes_MAffluence_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
    this.demo = undefined;
    this.demoAssets = undefined;
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'MAffluence'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
})();

/***/ }),

/***/ 36132:
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/api/api/m-ui/classes/MAffluence/index.html ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<header class=\"ngde\"><div class=\"ng-doc-page-tags ngde\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"ng-doc-scope\">m-ui</span> <span class=\"ng-doc-inline-delimiter ngde\" indexable=\"false\">/</span> <span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Class\">Class</span> <span class=\"ng-doc-inline-delimiter ngde\" indexable=\"false\">/</span><div class=\"ng-doc-decorators-group ngde\" indexable=\"false\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Component\">@Component</span></div><span class=\"ng-doc-inline-delimiter ngde\" indexable=\"false\">/</span> <span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"ng-doc-tag-selector\">m-affluence</span></div><h1 id=\"maffluence\" class=\"ngde\">MAffluence<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MAffluence#maffluence\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1><div class=\"ng-doc-header-details ngde\" indexable=\"false\"><span class=\"ng-doc-header-details-label ngde\">Implements</span><code indexable=\"false\" class=\"ngde\">OnInit</code></div></header><section class=\"ngde\"><p class=\"ngde\">MAffluence</p><p class=\"ngde\">The <code class=\"ngde ng-doc-code-with-link\" class=\"ngde\"><a href=\"/api/m-ui/classes/MAffluence\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MAffluence</a></code> component displays the level of occupancy with a description. This component uses Angular Material List and Icon to present the occupancy level.</p><h3 id=\"usage-example\" class=\"ngde\">Usage example<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MAffluence#usage-example\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h3><pre class=\"ngde hljs\"><code class=\"hljs language-html ngde\" lang=\"html\" filename=\"\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">m-affluence</span> [<span class=\"hljs-attr ngde\">lvl</span>]=<span class=\"hljs-string ngde\">\"1\"</span>></span><span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">m-affluence</span>></span></code></pre><p class=\"ngde\">m-affluence true CommonModule, MatListModule, MatIconModule, MIcons</p></section><section class=\"ngde\"><h2 id=\"properties\" class=\"ngde\">Properties<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MAffluence#properties\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ng-doc-properties-table ngde\"><thead class=\"ngde\"><tr indexable=\"false\" class=\"ngde\"><th class=\"ng-doc-properties-table-name ngde\">Name</th><th class=\"ng-doc-properties-table-type ngde\">Type</th><th class=\"ng-doc-properties-table-description ngde\">Description</th></tr></thead><tbody class=\"ngde\"><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">affluence<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">any[]</code></td><td class=\"ngde\"><p class=\"ngde\">Array used to display occupancy information based on the selected level (<code class=\"ngde\">lvl</code>).</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\"><div class=\"ng-doc-decorators-group column ngde\" indexable=\"false\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Input\">@Input</span></div>lightMode<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">boolean</code></td><td class=\"ngde\"><p class=\"ngde\">Enable/disable light mode.</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\"><div class=\"ng-doc-decorators-group column ngde\" indexable=\"false\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Input\">@Input</span></div>lvl<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">number</code></td><td class=\"ngde\"><p class=\"ngde\">Occupancy level that determines the description displayed. Possible values:</p><ul class=\"ngde\"><li class=\"ngde\">1 : Low</li><li class=\"ngde\">2 : Moderate</li><li class=\"ngde\">3 : High</li></ul><p class=\"ngde\">undefined (displays all descriptions)</p></td></tr></tbody></table></div></section><section class=\"ngde\"><h2 id=\"methods\" class=\"ngde\">Methods<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MAffluence#methods\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ng-doc-method-table ngde\"><thead class=\"ngde\"><tr class=\"ngde\"><th indexable=\"false\" class=\"ngde\"><h3 id=\"ngoninit\" class=\"ngde\">ngOnInit()<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MAffluence#ngoninit\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h3><div class=\"ng-doc-node-details ngde\">implements <code class=\"ngde\">OnInit</code></div></th></tr></thead><tbody class=\"ngde\"><tr class=\"ngde\"><td class=\"ngde\"><p class=\"ngde\">Initializes the <code class=\"ngde\">affluence</code> array based on the provided occupancy level (<code class=\"ngde\">lvl</code>). If no level is defined, displays all descriptions.</p></td></tr><tr class=\"ngde\"><td class=\"ngde\"><h5 class=\"no-anchor ngde\" indexable=\"false\">Presentation</h5><pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-title function_ ngde\">ngOnInit</span>(): <span class=\"hljs-built_in ngde\">void</span>;</code></pre></td></tr><tr class=\"ngde\"><td class=\"ngde\"><h5 class=\"no-anchor ngde\" indexable=\"false\">Returns</h5><p class=\"ngde\"><code indexable=\"false\" class=\"ngde\">void</code></p></td></tr></tbody></table></div></section>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_api_api_m-ui_classes_MAffluence_module_ts.js.map