"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_guides_m-ui_m-app-download_module_ts"],{

/***/ 83427:
/*!***********************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-ui/m-app-download/component-assets.ts ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   "demoAssets": () => (/* binding */ demoAssets)
/* harmony export */ });
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_ui_m_app_download_assets_MAppDownloadDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-ui/m-app-download/assets/MAppDownloadDemo/TypeScript/Asset0.html */ 17680);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_ui_m_app_download_assets_MAppDownloadDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-ui/m-app-download/assets/MAppDownloadDemo/HTML/Asset1.html */ 4792);


const demoAssets = {
  MAppDownloadDemo: [{
    title: 'TypeScript',
    codeType: 'TypeScript',
    code: _raw_loader_ng_doc_m_docs_guides_m_ui_m_app_download_assets_MAppDownloadDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__["default"]
  }, {
    title: 'HTML',
    codeType: 'HTML',
    code: _raw_loader_ng_doc_m_docs_guides_m_ui_m_app_download_assets_MAppDownloadDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__["default"]
  }]
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (demoAssets);

/***/ }),

/***/ 78571:
/*!*************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-ui/m-app-download/module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_ui_m_app_download_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-ui/m-app-download/index.html */ 81575);
/* harmony import */ var _playgrounds__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./playgrounds */ 90466);
/* harmony import */ var src_app_docs_m_ui_m_app_download_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/docs/m-ui/m-app-download/ng-doc.dependencies */ 62068);
/* harmony import */ var _ng_doc_m_docs_guides_m_ui_m_app_download_component_assets__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! .ng-doc/m-docs/guides/m-ui/m-app-download/component-assets */ 83427);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _src_app_docs_m_ui_m_app_download_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../src/app/docs/m-ui/m-app-download/ng-doc.module */ 70330);




// noinspection ES6UnusedImports


// noinspection ES6UnusedImports





class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = '';
    this.pageType = 'guide';
    this.pageContent = _raw_loader_ng_doc_m_docs_guides_m_ui_m_app_download_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
    this.dependencies = src_app_docs_m_ui_m_app_download_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"];
    this.demoAssets = _ng_doc_m_docs_guides_m_ui_m_app_download_component_assets__WEBPACK_IMPORTED_MODULE_3__["default"];
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, src_app_docs_m_ui_m_app_download_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"].module, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'M-app-download'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, _src_app_docs_m_ui_m_app_download_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__.MAppDownloadPageModule, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
})();

/***/ }),

/***/ 90466:
/*!******************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-ui/m-app-download/playgrounds.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PlaygroundsModule": () => (/* binding */ PlaygroundsModule)
/* harmony export */ });
/* harmony import */ var src_app_docs_m_ui_m_app_download_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/docs/m-ui/m-app-download/ng-doc.dependencies */ 62068);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _src_app_docs_m_ui_m_app_download_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../src/app/docs/m-ui/m-app-download/ng-doc.module */ 70330);
// noinspection ES6UnusedImports




class PlaygroundsModule {
  static #_ = this.ɵfac = function PlaygroundsModule_Factory(t) {
    return new (t || PlaygroundsModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: PlaygroundsModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, src_app_docs_m_ui_m_app_download_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__["default"].module]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](PlaygroundsModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, _src_app_docs_m_ui_m_app_download_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__.MAppDownloadPageModule]
  });
})();

/***/ }),

/***/ 92853:
/*!**************************************************************************!*\
  !*** ./projects/m-ui/src/lib/m-app-download/m-app-download.component.ts ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MAppDownload": () => (/* binding */ MAppDownload)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material/icon */ 57822);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/button */ 84522);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ 34497);








const _c0 = ["iconPrincipal"];
const _c1 = ["text"];
/**
 * @component
 * @name MAppDownload
 * @description
 * Composant graphique pour afficher les liens de téléchargement d'une Appli
 * Exemple : liens pour télécharger l'Appli M sur Apple Store
 *
 * ### Usage example
 * ```html
 * <m-app-download [data]="App"></m-app-download>
 * ```
 *
 * @selector m-app-download
 * @standalone true
 * @module CommonModule, MatIconModule, MatButtonModule
 */
class MAppDownload {
  constructor(iconRegistry, sanitizer, renderer) {
    this.iconRegistry = iconRegistry;
    this.sanitizer = sanitizer;
    this.renderer = renderer;
  }
  ngAfterViewInit() {
    this.iconRegistry.getSvgIconFromUrl(this.sanitizer.bypassSecurityTrustResourceUrl(this.data.Logo.url)).subscribe(icon => {
      icon.setAttribute('fill', '');
      //   let colortext = document.defaultView?.getComputedStyle(document.getElementById('textBase')!)['color']; 
      let colortext = this.text.nativeElement.style.color;
      let childrens = Array.from(icon.children);
      childrens.forEach(element => {
        this.renderer.setAttribute(element, 'fill', colortext);
        Array.from(element.children).forEach(element2 => {
          this.renderer.setAttribute(element2, 'fill', `${colortext} !important`);
        });
      });
      this.renderer.appendChild(this.iconPrincipal.nativeElement, icon);
    });
  }
  static #_ = this.ɵfac = function MAppDownload_Factory(t) {
    return new (t || MAppDownload)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_material_icon__WEBPACK_IMPORTED_MODULE_1__.MatIconRegistry), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__.DomSanitizer), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.Renderer2));
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
    type: MAppDownload,
    selectors: [["m-app-download"]],
    viewQuery: function MAppDownload_Query(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 5, _angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c1, 5, _angular_core__WEBPACK_IMPORTED_MODULE_0__.ElementRef);
      }
      if (rf & 2) {
        let _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.iconPrincipal = _t.first);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.text = _t.first);
      }
    },
    inputs: {
      data: "data"
    },
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵStandaloneFeature"]],
    decls: 15,
    vars: 5,
    consts: [["id", "button", "mat-stroked-button", "", "color", "primary", "target", "_blank", 1, "buttonDownload", 3, "href"], ["id", "icon2"], ["iconPositionStart", "", "id", "icon", 1, "icon", "iconPrincipal"], ["iconPrincipal", ""], [1, "text"], ["aria-hidden", "true", "id", "textBase", 1, "text-transparent"], ["text", ""], [1, "textInApp"], ["aria-hidden", "true", 1, "textGras"], ["aria-hidden", "true", "iconPositionEnd", "", 1, "external", "arrow-outward"], [1, "cdk-visually-hidden"]],
    template: function MAppDownload_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "a", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "div", 1)(2, "mat-icon", 2, 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "span", 4)(5, "span", 5, 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "span", 7)(9, "span", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "mat-icon", 9);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "arrow_outward");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()()();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "span", 10);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()();
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("href", ctx.data.Lien, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.data.Texte);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.data.TexteGras);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate2"]("", ctx.data.Texte, " ", ctx.data.TexteGras, "");
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, _angular_material_icon__WEBPACK_IMPORTED_MODULE_1__.MatIconModule, _angular_material_icon__WEBPACK_IMPORTED_MODULE_1__.MatIcon, _angular_material_button__WEBPACK_IMPORTED_MODULE_4__.MatButtonModule, _angular_material_button__WEBPACK_IMPORTED_MODULE_4__.MatAnchor],
    styles: [".buttonDownload[_ngcontent-%COMP%] {\n  width: 160px;\n  display: flex;\n  justify-content: flex-start;\n  padding: 8px 12px;\n  height: auto !important;\n  border-radius: 8px !important;\n  border-style: solid;\n  border-width: 2px;\n  text-transform: none !important;\n  color: inherit !important;\n  border-color: inherit !important;\n  background-color: inherit;\n}\n.buttonDownload[_ngcontent-%COMP%]:hover   .textGras[_ngcontent-%COMP%] {\n  text-decoration: underline;\n}\n.buttonDownload[_ngcontent-%COMP%]   .iconPrincipal[_ngcontent-%COMP%] {\n  width: 32px;\n  height: 32px;\n  font-size: 32px;\n}\n.buttonDownload[_ngcontent-%COMP%]   .arrow-outward[_ngcontent-%COMP%] {\n  font-size: 16px;\n  width: 16px;\n  height: 16px;\n}\n.buttonDownload[_ngcontent-%COMP%]   .text-transparent[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: flex-start;\n  font-size: 11px;\n  font-weight: 300;\n  line-height: 16px;\n  letter-spacing: 0.5px;\n}\n.buttonDownload[_ngcontent-%COMP%]   .textInApp[_ngcontent-%COMP%] {\n  display: flex;\n  gap: 8px;\n  align-items: center;\n  font-size: 11px;\n  font-weight: 500;\n  line-height: 16px;\n  letter-spacing: 0.5px;\n}\n.buttonDownload[_ngcontent-%COMP%]   .external[_ngcontent-%COMP%] {\n  font-size: 1.2em;\n  display: flex;\n  align-items: center;\n}\n.buttonDownload[_ngcontent-%COMP%]   .cdk-visually-hidden[_ngcontent-%COMP%] {\n  border: 0;\n  clip: rect(0 0 0 0);\n  height: 1px;\n  margin: -1px;\n  overflow: hidden;\n  padding: 0;\n  position: absolute;\n  width: 1px;\n  white-space: nowrap;\n  outline: 0;\n  -webkit-appearance: none;\n  -moz-appearance: none;\n  left: 0;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3Byb2plY3RzL20tdWkvc3JjL3RoZW1lL2dsb2JhbC9fdmFyaWFibGVzLnNjc3MiLCJ3ZWJwYWNrOi8vLi9wcm9qZWN0cy9tLXVpL3NyYy90aGVtZS9nbG9iYWwvX2hlbHBlcnMuc2NzcyIsIndlYnBhY2s6Ly8uL3Byb2plY3RzL20tdWkvc3JjL2xpYi9tLWFwcC1kb3dubG9hZC9tLWFwcC1kb3dubG9hZC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFnQkEsa0JBQUE7QUFJQSxlQUFBO0FBR0Esb0JBQUE7QUFPQSxnQkFBQTtBQUdBLG9CQUFBO0FBR0EsaUJBQUE7QUFJQSxnQkFBQTtBQUdBLG9CQUFBO0FDcENBOzs7Ozs7R0FBQTtBQVdBOzs7Ozs7R0FBQTtBQTZCQTs7RUFBQTtBQVlBOztFQUFBO0FBc0NBOztHQUFBO0FDNUZBO0VBQ0MsWUFBQTtFQUNBLGFBQUE7RUFDQSwyQkFBQTtFQUNBLGlCQUFBO0VBQ0EsdUJBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSwrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZ0NBQUE7RUFDQSx5QkFBQTtBQTJCRDtBQXhCRTtFQUNDLDBCQUFBO0FBMEJIO0FBdEJDO0VBQ0MsV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FBd0JGO0FBdEJDO0VBQ0MsZUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBd0JGO0FBckJDO0VBQ0MsYUFBQTtFQUNBLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxxQkFBQTtBQXVCRjtBQXBCQztFQUNDLGFBQUE7RUFDQSxRQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLHFCQUFBO0FBc0JGO0FBbkJDO0VBQ0MsZ0JBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7QUFxQkY7QUFsQkM7RUFDQyxTQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtFQUNBLHdCQUFBO0VBQ0EscUJBQUE7RUFDQSxPQUFBO0FBb0JGIiwic291cmNlc0NvbnRlbnQiOlsiJGFzc2V0cy1wYXRoOiBcIl4uL2Fzc2V0c1wiICFkZWZhdWx0O1xyXG4kc3BhY2luZzogOHB4ICFkZWZhdWx0O1xyXG5cclxuJGVsZXZhdGlvbi1jb2xvcnM6IChcclxuICAwOiAwJSxcclxuICAxOiA1JSxcclxuICAyOiA3JSxcclxuICAzOiA4JSxcclxuICA0OiA5JSxcclxuICA2OiAxMSUsXHJcbiAgODogMTIlLFxyXG4gIDEyOiAxNCUsXHJcbiAgMTY6IDE1JSxcclxuICAyNDogMTYlLFxyXG4pICFkZWZhdWx0O1xyXG5cclxuLyoqKiogdG9vbGJhciAqKioqL1xyXG4kdG9vbGJhci1oZWlnaHQ6IDU2cHggIWRlZmF1bHQ7XHJcbiR0b29sYmFyLW1haW4tbmF2LWhlaWdodDogJHRvb2xiYXItaGVpZ2h0ICsgJHNwYWNpbmcgIWRlZmF1bHQ7XHJcblxyXG4vKioqKiBmb250ICoqKiovXHJcbiRmb250LWZhbWlseTogUm9ib3RvLCBcIkhlbHZldGljYSBOZXVlXCIsIHNhbnMtc2VyaWY7XHJcblxyXG4vKioqKiBmb250IHNpemUgKioqKi9cclxuJGRlZmF1bHQtZno6IDEuNHJlbSAhZGVmYXVsdDtcclxuJGRlZmF1bHQtZnotc21hbGw6IDEuMnJlbSAhZGVmYXVsdDtcclxuJGRlZmF1bHQtZnotYmlnOiAxLjhyZW0gIWRlZmF1bHQ7XHJcbiRoZWFkZXItZno6ICRkZWZhdWx0LWZ6ICsgMC4ycmVtICFkZWZhdWx0O1xyXG4kaGVhZGVyLWZ6LWJpZzogJGhlYWRlci1meiArIDAuNHJlbSAhZGVmYXVsdDtcclxuXHJcbi8qKioqIEljb25zICoqKiovXHJcbiRpY29uLWxhcmdlOiA0MHB4ICFkZWZhdWx0O1xyXG5cclxuLyoqKiogQ29udGFpbmVyICoqKiovXHJcbiRlbmQtYmctc2l6ZTogMTQzcHggKyAkc3BhY2luZyAqIDIgIWRlZmF1bHQ7XHJcblxyXG4vKioqKiBzaGFkb3cgKioqKi9cclxuJGxpZ2h0LWJveC1zaGFkb3c6IDAgNHB4IDhweCByZ2JhKGJsYWNrLCAwLjUpICFkZWZhdWx0O1xyXG4kZGFyay1ib3gtc2hhZG93OiAwIDRweCA4cHggcmdiYShibGFjaywgMC43KSAhZGVmYXVsdDtcclxuXHJcbi8qKioqIFNoYXBlICoqKiovXHJcbiRzaGFwZS1yYWRpdXM6IDhweCAhZGVmYXVsdDtcclxuXHJcbi8qKiogQnJlYWtwb2ludCAqKioqL1xyXG4kYnJlYWtwb2ludDogMTA3OXB4O1xyXG4iLCJAdXNlIFwic2FzczptYXBcIjtcclxuQHVzZSBcInNhc3M6bWV0YVwiO1xyXG5AdXNlIFwic2FzczpsaXN0XCI7XHJcbkB1c2UgXCJzYXNzOm1hdGhcIjtcclxuQHVzZSBcInNhc3M6c2VsZWN0b3JcIjtcclxuQHVzZSBcInZhcmlhYmxlc1wiIGFzIHZhcjtcclxuXHJcbi8qKlxyXG4gKiBTbGlnaHRseSBsaWdodGVuIGEgY29sb3JcclxuICogQGFjY2VzcyBwdWJsaWNcclxuICogQHBhcmFtIHtDb2xvcn0gJGNvbG9yIC0gY29sb3IgdG8gdGludFxyXG4gKiBAcGFyYW0ge051bWJlcn0gJHBlcmNlbnRhZ2UgLSBwZXJjZW50YWdlIG9mIGAkY29sb3JgIGluIHJldHVybmVkIGNvbG9yXHJcbiAqIEByZXR1cm4ge0NvbG9yfVxyXG4gKiovXHJcbkBmdW5jdGlvbiB0aW50KCRjb2xvciwgJHBlcmNlbnRhZ2UpIHtcclxuXHRAcmV0dXJuIG1peCh3aGl0ZSwgJGNvbG9yLCAkcGVyY2VudGFnZSk7XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBTbGlnaHRseSBkYXJrZW4gYSBjb2xvclxyXG4gKiBAYWNjZXNzIHB1YmxpY1xyXG4gKiBAcGFyYW0ge0NvbG9yfSAkY29sb3IgLSBjb2xvciB0byBzaGFkZVxyXG4gKiBAcGFyYW0ge051bWJlcn0gJHBlcmNlbnRhZ2UgLSBwZXJjZW50YWdlIG9mIGAkY29sb3JgIGluIHJldHVybmVkIGNvbG9yXHJcbiAqIEByZXR1cm4ge0NvbG9yfVxyXG4gKiovXHJcbkBmdW5jdGlvbiBzaGFkZSgkY29sb3IsICRwZXJjZW50YWdlKSB7XHJcblx0QHJldHVybiBtaXgoYmxhY2ssICRjb2xvciwgJHBlcmNlbnRhZ2UpO1xyXG59XHJcblxyXG5AbWl4aW4gZnVsbC1oZWlnaHQoKSB7XHJcblx0aGVpZ2h0OiBjYWxjKDEwMHZoIC0gI3t2YXIuJHRvb2xiYXItbWFpbi1uYXYtaGVpZ2h0fSk7XHJcbn1cclxuXHJcbkBtaXhpbiBpY29uLW1hcmdpbigkZGlyZWN0aW9uKSB7XHJcblx0bWFyZ2luLSN7JGRpcmVjdGlvbn06IHZhci4kc3BhY2luZztcclxuXHJcblx0Ji1sYXJnZSB7XHJcblx0XHRtYXJnaW4tI3skZGlyZWN0aW9ufTogdmFyLiRzcGFjaW5nICogMjtcclxuXHR9XHJcblx0Ji12ZXJ5LWxhcmdlIHtcclxuXHRcdG1hcmdpbi0jeyRkaXJlY3Rpb259OiB2YXIuJHNwYWNpbmcgKiA0O1xyXG5cdH1cclxuXHQmLXNtYWxsIHtcclxuXHRcdG1hcmdpbi0jeyRkaXJlY3Rpb259OiBtYXRoLmRpdih2YXIuJHNwYWNpbmcsIDIpO1xyXG5cdH1cclxufVxyXG5cclxuLyoqXHJcbiAqIE11c3QgYmUgdXNlZCBvbmx5IGluc2lkZSBhIG92ZXJyaWRlLVggbWl4aW4uXHJcbiAqL1xyXG5AbWl4aW4gZHJvcC1zaGFkb3coKSB7XHJcblx0QGF0LXJvb3QgI3tzZWxlY3Rvci5yZXBsYWNlKCYsIFwiYm9keVwiLCBcImJvZHkuZGFyay10aGVtZVwiKX0ge1xyXG5cdFx0Ym94LXNoYWRvdzogdmFyLiRkYXJrLWJveC1zaGFkb3c7XHJcblx0fVxyXG5cdEBhdC1yb290ICN7c2VsZWN0b3IucmVwbGFjZSgmLCBcImJvZHlcIiwgXCJib2R5LmxpZ2h0LXRoZW1lXCIpfSB7XHJcblx0XHRib3gtc2hhZG93OiB2YXIuJGxpZ2h0LWJveC1zaGFkb3c7XHJcblx0fVxyXG59XHJcblxyXG4vKipcclxuICogTXVzdCBiZSB1c2VkIG9ubHkgaW5zaWRlIGEgb3ZlcnJpZGUtWCBtaXhpbi5cclxuICovXHJcbkBtaXhpbiBib3R0b20tcGljdHVyZSgpIHtcclxuXHQmOjphZnRlciB7XHJcblx0XHRjb250ZW50OiBcIlwiO1xyXG5cdFx0ZGlzcGxheTogYmxvY2s7XHJcblx0XHRoZWlnaHQ6IHZhci4kZW5kLWJnLXNpemU7XHJcblx0XHRiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xyXG5cdFx0YmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuXHRcdEBhdC1yb290ICN7c2VsZWN0b3IucmVwbGFjZSgmLCBcImJvZHlcIiwgXCJib2R5LmRhcmstdGhlbWVcIil9IHtcclxuXHRcdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKHZhci4kYXNzZXRzLXBhdGggKyBcIi9pbWFnZXMvYm90dG9tX3BpY3R1cmVfbWV0cm9tb2JfZGFyay9ib3R0b21fcGljdHVyZV9tZXRyb21vYl9kYXJrLnBuZ1wiKTtcclxuXHJcblx0XHRcdEBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1pbi1yZXNvbHV0aW9uOiAyMDBkcGkpLCBvbmx5IHNjcmVlbiBhbmQgKG1pbi13aWR0aDogMTAyNHB4KSB7XHJcblx0XHRcdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKHZhci4kYXNzZXRzLXBhdGggKyBcIi9pbWFnZXMvYm90dG9tX3BpY3R1cmVfbWV0cm9tb2JfZGFyay9ib3R0b21fcGljdHVyZV9tZXRyb21vYl9kYXJrQDJ4LnBuZ1wiKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0QGF0LXJvb3QgI3tzZWxlY3Rvci5yZXBsYWNlKCYsIFwiYm9keVwiLCBcImJvZHkubGlnaHQtdGhlbWVcIil9IHtcclxuXHRcdFx0YmFja2dyb3VuZC1pbWFnZTogdXJsKHZhci4kYXNzZXRzLXBhdGggKyBcIi9pbWFnZXMvYm90dG9tX3BpY3R1cmVfbWV0cm9tb2JfbGlnaHQvYm90dG9tX3BpY3R1cmVfbWV0cm9tb2JfbGlnaHQucG5nXCIpO1xyXG5cclxuXHRcdFx0QG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXJlc29sdXRpb246IDIwMGRwaSksIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMDI0cHgpIHtcclxuXHRcdFx0XHRiYWNrZ3JvdW5kLWltYWdlOiB1cmwodmFyLiRhc3NldHMtcGF0aCArIFwiL2ltYWdlcy9ib3R0b21fcGljdHVyZV9tZXRyb21vYl9saWdodC9ib3R0b21fcGljdHVyZV9tZXRyb21vYl9saWdodEAyeC5wbmdcIik7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcbn1cclxuXHJcbkBmdW5jdGlvbiBzd2l0Y2goJHRlc3QsICRhcmdzLi4uKSB7XHJcblx0JGNhc2VzOiBtZXRhLmtleXdvcmRzKCRhcmdzKTtcclxuXHRAaWYgKG1hcC5oYXMta2V5KCRjYXNlcywgJHRlc3QpKSB7XHJcblx0XHRAcmV0dXJuIG1hcC5nZXQoJGNhc2VzLCAkdGVzdCk7XHJcblx0fSBAZWxzZSBpZiBtYXAuaGFzLWtleSgkY2FzZXMsIFwiZGVmYXVsdFwiKSB7XHJcblx0XHRAcmV0dXJuIG1hcC5nZXQoJGNhc2VzLCBcImRlZmF1bHRcIik7XHJcblx0fSBAZWxzZSB7XHJcblx0XHRAcmV0dXJuIG51bGw7XHJcblx0fVxyXG59XHJcblxyXG4vKipcclxuICogU2FzcyBkZWJ1ZyBmdW5jdGlvbiBmcm9tIGh0dHBzOi8vY29kZXBlbi5pby9LaXR0eUdpcmF1ZGVsL3Blbi91bnlCSFxyXG4gKiovXHJcbkBmdW5jdGlvbiBkZWJ1Zy1tYXAoJGxpc3QsICRwcmU6IHRydWUsICRsZXZlbDogMSkge1xyXG5cdCR0YWI6IFwiICAgIFwiO1xyXG5cdCRpbmRlbnQ6IFwiXCI7XHJcblx0JGJyZWFrOiBpZigkcHJlLCBcIlxcQSBcIiwgXCJcIik7XHJcblxyXG5cdEBpZiBsZW5ndGgoJGxpc3QpID09IDAge1xyXG5cdFx0QHJldHVybiBcIiggKVwiO1xyXG5cdH1cclxuXHJcblx0QGlmIGxlbmd0aCgkbGlzdCkgPT0gMSB7XHJcblx0XHRAcmV0dXJuIGlmKCRwcmUsIFwiKFwiICsgdHlwZS1vZigkbGlzdCkgKyBcIikgXCIsIFwiXCIpICsgJGxpc3Q7XHJcblx0fVxyXG5cclxuXHRAZm9yICRpIGZyb20gMSB0byAkbGV2ZWwge1xyXG5cdFx0JGluZGVudDogJGluZGVudCArICR0YWI7XHJcblx0fVxyXG5cclxuXHQkcmVzdWx0OiBcIltcIiArICRicmVhaztcclxuXHJcblx0QGZvciAkaSBmcm9tIDEgdGhyb3VnaCBsZW5ndGgoJGxpc3QpIHtcclxuXHRcdCRpdGVtOiBudGgoJGxpc3QsICRpKTtcclxuXHRcdCRyZXN1bHQ6ICRyZXN1bHQgKyBpZigkcHJlLCAkaW5kZW50ICsgJHRhYiwgXCIgXCIpO1xyXG5cclxuXHRcdEBpZiBsZW5ndGgoJGl0ZW0pID4gMSB7XHJcblx0XHRcdCRyZXN1bHQ6ICRyZXN1bHQgKyBpZigkcHJlLCBcIihsaXN0OiBcIiArIGxlbmd0aCgkaXRlbSkgKyBcIikgXCIsIFwiXCIpICsgZGVidWctbWFwKCRpdGVtLCAkcHJlLCAkbGV2ZWwgKyAxKTtcclxuXHRcdH0gQGVsc2Uge1xyXG5cdFx0XHRAaWYgJHByZSB7XHJcblx0XHRcdFx0JHJlc3VsdDogJHJlc3VsdCArIFwiKFwiICsgdHlwZS1vZigkaXRlbSkgKyBcIikgXCI7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdEBpZiBsZW5ndGgoJGl0ZW0pID09IDAge1xyXG5cdFx0XHRcdCRyZXN1bHQ6ICRyZXN1bHQgKyBcIiggKVwiO1xyXG5cdFx0XHR9IEBlbHNlIGlmIHR5cGUtb2YoJGl0ZW0pID09IHN0cmluZyB7XHJcblx0XHRcdFx0JHJlc3VsdDogJHJlc3VsdCArIHF1b3RlKCRpdGVtKTtcclxuXHRcdFx0fSBAZWxzZSBpZiAkaXRlbSA9PSBudWxsIHtcclxuXHRcdFx0XHQkcmVzdWx0OiAkcmVzdWx0ICsgXCJudWxsXCI7XHJcblx0XHRcdH0gQGVsc2Uge1xyXG5cdFx0XHRcdCRyZXN1bHQ6ICRyZXN1bHQgKyAkaXRlbTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHRcdEBpZiAkaSAhPSBsZW5ndGgoJGxpc3QpIHtcclxuXHRcdFx0JHJlc3VsdDogJHJlc3VsdCArIFwiLFwiICsgJGJyZWFrO1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcblx0JHJlc3VsdDogJHJlc3VsdCArICRicmVhayArIGlmKCRwcmUsIGlmKCRsZXZlbCA+IDEsICRpbmRlbnQsIFwiXCIpLCBcIiBcIikgKyBcIl1cIjtcclxuXHJcblx0QHJldHVybiBxdW90ZSgkcmVzdWx0KTtcclxufVxyXG4iLCJAdXNlIFwic2FzczptYXBcIjtcclxuQHVzZSBcInNhc3M6bWF0aFwiO1xyXG5AdXNlIFwiQGFuZ3VsYXIvbWF0ZXJpYWxcIiBhcyBtYXQ7XHJcbkB1c2UgXCIuLi8uLi90aGVtZS9nbG9iYWxcIiBhcyBnbG9iYWw7XHJcblxyXG4uYnV0dG9uRG93bmxvYWQge1xyXG5cdHdpZHRoOiBnbG9iYWwuJHNwYWNpbmcgKiAyMDtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuXHRwYWRkaW5nOiBnbG9iYWwuJHNwYWNpbmcgY2FsYyhnbG9iYWwuJHNwYWNpbmcgKiAxLjUpO1xyXG5cdGhlaWdodDogYXV0byAhaW1wb3J0YW50O1xyXG5cdGJvcmRlci1yYWRpdXM6IGdsb2JhbC4kc3BhY2luZyAhaW1wb3J0YW50O1xyXG5cdGJvcmRlci1zdHlsZTogc29saWQ7XHJcblx0Ym9yZGVyLXdpZHRoOiAycHg7XHJcblx0dGV4dC10cmFuc2Zvcm06IG5vbmUgIWltcG9ydGFudDtcclxuXHRjb2xvcjogaW5oZXJpdCAhaW1wb3J0YW50O1xyXG5cdGJvcmRlci1jb2xvcjogaW5oZXJpdCAhaW1wb3J0YW50O1xyXG5cdGJhY2tncm91bmQtY29sb3I6IGluaGVyaXQ7XHJcblxyXG5cdCY6aG92ZXIge1xyXG5cdFx0LnRleHRHcmFzIHtcclxuXHRcdFx0dGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQuaWNvblByaW5jaXBhbCB7XHJcblx0XHR3aWR0aDogZ2xvYmFsLiRzcGFjaW5nICogNDtcclxuXHRcdGhlaWdodDogZ2xvYmFsLiRzcGFjaW5nICogNDtcclxuXHRcdGZvbnQtc2l6ZTogZ2xvYmFsLiRzcGFjaW5nICogNDtcclxuXHR9XHJcblx0LmFycm93LW91dHdhcmQge1xyXG5cdFx0Zm9udC1zaXplOiBnbG9iYWwuJHNwYWNpbmcgKiAyO1xyXG5cdFx0d2lkdGg6IGdsb2JhbC4kc3BhY2luZyAqIDI7XHJcblx0XHRoZWlnaHQ6IGdsb2JhbC4kc3BhY2luZyAqIDI7XHJcblx0fVxyXG5cclxuXHQudGV4dC10cmFuc3BhcmVudCB7XHJcblx0XHRkaXNwbGF5OiBmbGV4O1xyXG5cdFx0anVzdGlmeS1jb250ZW50OiBmbGV4LXN0YXJ0O1xyXG5cdFx0Zm9udC1zaXplOiAxMXB4O1xyXG5cdFx0Zm9udC13ZWlnaHQ6IDMwMDtcclxuXHRcdGxpbmUtaGVpZ2h0OiAxNnB4O1xyXG5cdFx0bGV0dGVyLXNwYWNpbmc6IC41cHg7XHJcblx0fVxyXG5cclxuXHQudGV4dEluQXBwIHtcclxuXHRcdGRpc3BsYXk6IGZsZXg7XHJcblx0XHRnYXA6IDhweDtcclxuXHRcdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblx0XHRmb250LXNpemU6IDExcHg7XHJcblx0XHRmb250LXdlaWdodDogNTAwO1xyXG5cdFx0bGluZS1oZWlnaHQ6IDE2cHg7XHJcblx0XHRsZXR0ZXItc3BhY2luZzogLjVweDtcclxuXHR9XHJcblxyXG5cdC5leHRlcm5hbCB7XHJcblx0XHRmb250LXNpemU6IDEuMmVtO1xyXG5cdFx0ZGlzcGxheTogZmxleDtcclxuXHRcdGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblx0fVxyXG5cclxuXHQuY2RrLXZpc3VhbGx5LWhpZGRlbiB7XHJcblx0XHRib3JkZXI6IDA7XHJcblx0XHRjbGlwOiByZWN0KDAgMCAwIDApO1xyXG5cdFx0aGVpZ2h0OiAxcHg7XHJcblx0XHRtYXJnaW46IC0xcHg7XHJcblx0XHRvdmVyZmxvdzogaGlkZGVuO1xyXG5cdFx0cGFkZGluZzogMDtcclxuXHRcdHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuXHRcdHdpZHRoOiAxcHg7XHJcblx0XHR3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG5cdFx0b3V0bGluZTogMDtcclxuXHRcdC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcclxuXHRcdC1tb3otYXBwZWFyYW5jZTogbm9uZTtcclxuXHRcdGxlZnQ6IDA7XHJcblx0fVxyXG59XHJcblxyXG5cclxuQG1peGluIG92ZXJyaWRlLWNvbG9yKCR0aGVtZSkge1xyXG5cdCQtdGhlbWU6IG1hcC5nZXQoJHRoZW1lLCB0aGVtZSk7XHJcblx0JGNvbG9yLWNvbmZpZzogbWF0LmdldC1jb2xvci1jb25maWcoJHRoZW1lKTtcclxuXHQkcHJpbWFyeS1wYWxldHRlOiBtYXAuZ2V0KCRjb2xvci1jb25maWcsIFwicHJpbWFyeVwiKTtcclxuXHQkZm9yZWdyb3VuZDogbWFwLmdldCgkdGhlbWUsIGZvcmVncm91bmQpO1xyXG5cclxuXHQuYnV0dG9uRG93bmxvYWQge1xyXG5cdFx0Jjpob3ZlciB7XHJcblx0XHRcdC5hcnJvdy1vdXR3YXJkIHtcclxuXHRcdFx0XHRjb2xvcjogbWFwLmdldCgkcHJpbWFyeS1wYWxldHRlLCBkZWZhdWx0KTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9 */"]
  });
}

/***/ }),

/***/ 99591:
/*!***********************************************************************************************!*\
  !*** ./src/app/docs/m-ui/m-app-download/m-app-download-demo/m-app-download-demo.component.ts ***!
  \***********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MAppDownloadDemo": () => (/* binding */ MAppDownloadDemo)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _metromobilite_m_ui_lib_m_app_download__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @metromobilite/m-ui/lib/m-app-download */ 92853);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);



class MAppDownloadDemo {
  constructor() {
    this.data = {
      'Lien': 'destination',
      'Logo': {
        'url': 'https://data-pp.mobilites-m.fr/uploads/computer_line_546bba1226.svg'
      },
      'TexteGras': 'App Store',
      'Texte': 'Télécharger en'
    };
  }
  static #_ = this.ɵfac = function MAppDownloadDemo_Factory(t) {
    return new (t || MAppDownloadDemo)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
    type: MAppDownloadDemo,
    selectors: [["appm-app-download-demo-demo"]],
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵStandaloneFeature"]],
    decls: 1,
    vars: 1,
    consts: [[3, "data"]],
    template: function MAppDownloadDemo_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "m-app-download", 0);
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("data", ctx.data);
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule, _metromobilite_m_ui_lib_m_app_download__WEBPACK_IMPORTED_MODULE_2__.MAppDownload],
    styles: ["\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZVJvb3QiOiIifQ== */"]
  });
}

/***/ }),

/***/ 62068:
/*!*****************************************************************!*\
  !*** ./src/app/docs/m-ui/m-app-download/ng-doc.dependencies.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _ng_doc_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ng-doc.module */ 70330);
/* harmony import */ var _m_app_download_demo_m_app_download_demo_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./m-app-download-demo/m-app-download-demo.component */ 99591);


const MAppDownloadPageDependencies = {
  module: _ng_doc_module__WEBPACK_IMPORTED_MODULE_0__.MAppDownloadPageModule,
  demo: {
    MAppDownloadDemo: _m_app_download_demo_m_app_download_demo_component__WEBPACK_IMPORTED_MODULE_1__.MAppDownloadDemo
  }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MAppDownloadPageDependencies);

/***/ }),

/***/ 70330:
/*!***********************************************************!*\
  !*** ./src/app/docs/m-ui/m-app-download/ng-doc.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MAppDownloadPageModule": () => (/* binding */ MAppDownloadPageModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);


class MAppDownloadPageModule {
  static #_ = this.ɵfac = function MAppDownloadPageModule_Factory(t) {
    return new (t || MAppDownloadPageModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
    type: MAppDownloadPageModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](MAppDownloadPageModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
})();

/***/ }),

/***/ 4792:
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-ui/m-app-download/assets/MAppDownloadDemo/HTML/Asset1.html ***!
  \**********************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"html\" class=\"hljs language-html ngde\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">m-app-download</span> [<span class=\"hljs-attr ngde\">data</span>]=<span class=\"hljs-string ngde\">\"data\"</span>></span><span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">m-app-download</span>></span>\n</code></pre>");

/***/ }),

/***/ 17680:
/*!****************************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-ui/m-app-download/assets/MAppDownloadDemo/TypeScript/Asset0.html ***!
  \****************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">CommonModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/common\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Component</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/core\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\"><a href=\"/api/m-ui/classes/MAppDownload\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MAppDownload</a></span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@metromobilite/m-ui/lib/m-app-download\"</span>;\n\n<span class=\"hljs-meta ngde\">@Component</span>({\n  <span class=\"hljs-attr ngde\">selector</span>: <span class=\"hljs-string ngde\">\"appm-app-download-demo-demo\"</span>,\n  <span class=\"hljs-attr ngde\">standalone</span>: <span class=\"hljs-literal ngde\">true</span>,\n  <span class=\"hljs-attr ngde\">imports</span>: [<span class=\"hljs-title class_ ngde\">CommonModule</span>, <span class=\"hljs-title class_ ngde\"><a href=\"/api/m-ui/classes/MAppDownload\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MAppDownload</a></span>],\n  <span class=\"hljs-attr ngde\">templateUrl</span>: <span class=\"hljs-string ngde\">\"./m-app-download-demo.component.html\"</span>,\n  <span class=\"hljs-attr ngde\">styleUrls</span>: [<span class=\"hljs-string ngde\">\"./m-app-download-demo.component.scss\"</span>],\n})\n<span class=\"hljs-keyword ngde\">export</span> <span class=\"hljs-keyword ngde\">class</span> <span class=\"hljs-title class_ ngde\">MAppDownloadDemo</span> {\n  <span class=\"hljs-keyword ngde\">public</span> data = {\n    <span class=\"hljs-title class_ ngde\">Lien</span>: <span class=\"hljs-string ngde\">\"destination\"</span>,\n    <span class=\"hljs-title class_ ngde\">Logo</span>: {\n      <span class=\"hljs-attr ngde\">url</span>: <span class=\"hljs-string ngde\">\"https://data-pp.mobilites-m.fr/uploads/computer_line_546bba1226.svg\"</span>,\n    },\n    <span class=\"hljs-title class_ ngde\">TexteGras</span>: <span class=\"hljs-string ngde\">\"App Store\"</span>,\n    <span class=\"hljs-title class_ ngde\">Texte</span>: <span class=\"hljs-string ngde\">\"Télécharger en\"</span>,\n  };\n}\n</code></pre>");

/***/ }),

/***/ 81575:
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-ui/m-app-download/index.html ***!
  \****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<h1 id=\"m-app-download\" class=\"ngde\">M-app-download<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/m-ui/m-app-download#m-app-download\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1><h2 id=\"api\" class=\"ngde\">API<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/m-ui/m-app-download#api\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><p class=\"ngde\">Shows how to use the <code class=\"ngde ng-doc-code-with-link\" class=\"ngde\"><a href=\"/api/m-ui/classes/MAppDownload\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MAppDownload</a></code> component.</p><h2 id=\"demo\" class=\"ngde\">Demo<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/m-ui/m-app-download#demo\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><ng-doc-demo componentname=\"MAppDownloadDemo\" indexable=\"false\" class=\"ngde\"><div id=\"options\" class=\"ngde\">{}</div></ng-doc-demo>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_guides_m-ui_m-app-download_module_ts.js.map