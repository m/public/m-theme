"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_guides_m-ui_module_ts"],{

/***/ 66856:
/*!**********************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-ui/module.ts ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);



class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule.forChild([{
      path: '',
      redirectTo: 'm-app-download',
      pathMatch: 'full'
    }, {
      path: '',
      title: 'M-ui',
      children: [{
        path: 'm-app-download',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-kind-icon_mjs-node_modules_ng--95dcfb"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_button_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_icon_mjs"), __webpack_require__.e("_ng-doc_m-docs_guides_m-ui_m-app-download_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/m-ui/m-app-download/module */ 78571)).then(m => m.DynamicModule)
      }, {
        path: 'm-affluence',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-kind-icon_mjs-node_modules_ng--95dcfb"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_list_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_icon_mjs"), __webpack_require__.e("default-projects_m-ui_src_lib_m-icons_m-icons_component_ts-src_app_features_preview_preview_c-6a8ada"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_expansion_mjs"), __webpack_require__.e("_ng-doc_m-docs_guides_m-ui_m-affluence_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/m-ui/m-affluence/module */ 7371)).then(m => m.DynamicModule)
      }, {
        path: 'm-disturbance-display',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-kind-icon_mjs-node_modules_ng--95dcfb"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_list_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_button_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_icon_mjs"), __webpack_require__.e("default-projects_m-ui_src_lib_m-icons_m-icons_component_ts-src_app_features_preview_preview_c-6a8ada"), __webpack_require__.e("_ng-doc_m-docs_guides_m-ui_m-disturbance-display_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/m-ui/m-disturbance-display/module */ 91564)).then(m => m.DynamicModule)
      }, {
        path: 'm-list-wrapper',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-kind-icon_mjs-node_modules_ng--95dcfb"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_list_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_button_mjs"), __webpack_require__.e("_ng-doc_m-docs_guides_m-ui_m-list-wrapper_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/m-ui/m-list-wrapper/module */ 27696)).then(m => m.DynamicModule)
      }, {
        path: 'm-icons',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-kind-icon_mjs-node_modules_ng--95dcfb"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_list_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_button_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_icon_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_form-field_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_input_mjs"), __webpack_require__.e("default-projects_m-ui_src_lib_m-icons_m-icons_component_ts-src_app_features_preview_preview_c-6a8ada"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_tooltip_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_snack-bar_mjs"), __webpack_require__.e("_ng-doc_m-docs_guides_m-ui_m-icons_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/m-ui/m-icons/module */ 70938)).then(m => m.DynamicModule)
      }, {
        path: 'm-scroll',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-kind-icon_mjs-node_modules_ng--95dcfb"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_list_mjs"), __webpack_require__.e("_ng-doc_m-docs_guides_m-ui_m-scroll_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/m-ui/m-scroll/module */ 62552)).then(m => m.DynamicModule)
      }, {
        path: 'm-logo-lines',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-kind-icon_mjs-node_modules_ng--95dcfb"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_list_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_checkbox_mjs"), __webpack_require__.e("default-projects_m-ui_src_lib_m-logo-lines_m-logo-lines_component_ts"), __webpack_require__.e("_ng-doc_m-docs_guides_m-ui_m-logo-lines_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/m-ui/m-logo-lines/module */ 85404)).then(m => m.DynamicModule)
      }, {
        path: 'm-table',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-kind-icon_mjs-node_modules_ng--95dcfb"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_list_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_button_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_icon_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_form-field_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_input_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_checkbox_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_select_mjs"), __webpack_require__.e("default-projects_m-ui_src_lib_m-icons_m-icons_component_ts-src_app_features_preview_preview_c-6a8ada"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_tooltip_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_table_mjs"), __webpack_require__.e("default-node_modules_angular_material_fesm2020_sort_mjs"), __webpack_require__.e("default-projects_m-ui_src_lib_m-logo-lines_m-logo-lines_component_ts"), __webpack_require__.e("_ng-doc_m-docs_guides_m-ui_m-table_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/m-ui/m-table/module */ 54672)).then(m => m.DynamicModule)
      }]
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](DynamicModule, {
    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule]
  });
})();

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_guides_m-ui_module_ts.js.map