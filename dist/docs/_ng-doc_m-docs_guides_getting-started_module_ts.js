"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_guides_getting-started_module_ts"],{

/***/ 81715:
/*!*********************************************************!*\
  !*** ./.ng-doc/m-docs/guides/getting-started/module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);



class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule.forChild([{
      path: '',
      redirectTo: 'install',
      pathMatch: 'full'
    }, {
      path: '',
      title: 'Getting Started',
      children: [{
        path: 'install',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-kind-icon_mjs-node_modules_ng--95dcfb"), __webpack_require__.e("_ng-doc_m-docs_guides_getting-started_install_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/getting-started/install/module */ 44540)).then(m => m.DynamicModule)
      }, {
        path: 'docs',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-kind-icon_mjs-node_modules_ng--95dcfb"), __webpack_require__.e("_ng-doc_m-docs_guides_getting-started_docs_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/getting-started/docs/module */ 22076)).then(m => m.DynamicModule)
      }, {
        path: 'what-is',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-kind-icon_mjs-node_modules_ng--95dcfb"), __webpack_require__.e("_ng-doc_m-docs_guides_getting-started_what-is_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/getting-started/what-is/module */ 69029)).then(m => m.DynamicModule)
      }, {
        path: 'peer-dependency',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-kind-icon_mjs-node_modules_ng--95dcfb"), __webpack_require__.e("_ng-doc_m-docs_guides_getting-started_peer-dependency_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/getting-started/peer-dependency/module */ 93742)).then(m => m.DynamicModule)
      }, {
        path: 'usage',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-kind-icon_mjs-node_modules_ng--95dcfb"), __webpack_require__.e("_ng-doc_m-docs_guides_getting-started_usage_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/getting-started/usage/module */ 35546)).then(m => m.DynamicModule)
      }, {
        path: 'migration',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-kind-icon_mjs-node_modules_ng--95dcfb"), __webpack_require__.e("_ng-doc_m-docs_guides_getting-started_migration_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/guides/getting-started/migration/module */ 89172)).then(m => m.DynamicModule)
      }]
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](DynamicModule, {
    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule]
  });
})();

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_guides_getting-started_module_ts.js.map