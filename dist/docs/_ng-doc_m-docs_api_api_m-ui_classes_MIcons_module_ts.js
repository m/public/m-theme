"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_api_api_m-ui_classes_MIcons_module_ts"],{

/***/ 4322:
/*!**************************************************************!*\
  !*** ./.ng-doc/m-docs/api/api/m-ui/classes/MIcons/module.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_api_api_m_ui_classes_MIcons_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/api/api/m-ui/classes/MIcons/index.html */ 80122);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);







class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = undefined;
    this.pageType = 'api';
    this.pageContent = _raw_loader_ng_doc_m_docs_api_api_m_ui_classes_MIcons_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
    this.demo = undefined;
    this.demoAssets = undefined;
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'MIcons'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
})();

/***/ }),

/***/ 80122:
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/api/api/m-ui/classes/MIcons/index.html ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<header class=\"ngde\"><div class=\"ng-doc-page-tags ngde\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"ng-doc-scope\">m-ui</span> <span class=\"ng-doc-inline-delimiter ngde\" indexable=\"false\">/</span> <span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Class\">Class</span> <span class=\"ng-doc-inline-delimiter ngde\" indexable=\"false\">/</span><div class=\"ng-doc-decorators-group ngde\" indexable=\"false\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Component\">@Component</span></div><span class=\"ng-doc-inline-delimiter ngde\" indexable=\"false\">/</span> <span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"ng-doc-tag-selector\">m-icons</span></div><h1 id=\"micons\" class=\"ngde\">MIcons<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MIcons#micons\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1></header><section class=\"ngde\"><p class=\"ngde\">MIcons</p><p class=\"ngde\">Icon component that wraps Angular Material's icon component to provide a customized icon display. The component utilizes the <code class=\"ngde ng-doc-code-with-link\" class=\"ngde\"><a href=\"/api/m-ui/variables/ICONS_MAPPING\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Variable\" class=\"ngde\">ICONS_MAPPING</a></code> constant for registering a set of icons and relies on <code class=\"ngde ng-doc-code-with-link\" class=\"ngde\"><a href=\"/api/m-ui/classes/MIconsService\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Injectable\" class=\"ngde\">MIconsService</a></code> to manage icon registration.</p><h3 id=\"usage-example\" class=\"ngde\">Usage example<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MIcons#usage-example\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h3><pre class=\"ngde hljs\"><code class=\"hljs language-html ngde\" lang=\"html\" filename=\"\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">m-icons</span> <span class=\"hljs-attr ngde\">type</span>=<span class=\"hljs-string ngde\">\"home\"</span>></span><span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">m-icons</span>></span></code></pre></section><section class=\"ngde\"><h2 id=\"constructor\" class=\"ngde\">Constructor<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MIcons#constructor\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ng-doc-method-table ngde\"><tbody class=\"ngde\"><tr class=\"ngde\"><td class=\"ngde\"><p class=\"ngde\">Initializes the component and registers a set of icons defined in the <code class=\"ngde ng-doc-code-with-link\" class=\"ngde\"><a href=\"/api/m-ui/variables/ICONS_MAPPING\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Variable\" class=\"ngde\">ICONS_MAPPING</a></code> constant. Calls <code class=\"ngde\">registerIcons</code> method of <code class=\"ngde ng-doc-code-with-link\" class=\"ngde\"><a href=\"/api/m-ui/classes/MIconsService\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Injectable\" class=\"ngde\">MIconsService</a></code> to ensure that all necessary icons are available for use within the application.</p></td></tr><tr class=\"ngde\"><td class=\"ngde\"><h5 class=\"no-anchor ngde\" indexable=\"false\">Presentation</h5><pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-title function_ ngde\">constructor</span>(<span class=\"hljs-params ngde\">\n\t<span class=\"hljs-keyword ngde\">private</span> mIconService: <a href=\"/api/m-ui/classes/MIconsService\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Injectable\" class=\"ngde\">MIconsService</a>\n</span>): <span class=\"hljs-title class_ ngde\"><a href=\"/api/m-ui/classes/MIcons\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MIcons</a></span>;</code></pre></td></tr><tr class=\"ngde\"><td class=\"ngde\"><h5 class=\"no-anchor ngde\" indexable=\"false\">Parameters</h5><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ng-doc-parameters-table ngde\"><thead class=\"ngde\"><tr indexable=\"false\" class=\"ngde\"><th class=\"ng-doc-parameters-table-name ngde\">Name</th><th class=\"ng-doc-parameters-table-type ngde\">Type</th><th class=\"ng-doc-parameters-table-description ngde\">Description</th></tr></thead><tbody class=\"ngde\"><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">mIconService<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde ng-doc-code-with-link\" class=\"ngde\"><a href=\"/api/m-ui/classes/MIconsService\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Injectable\" class=\"ngde\">MIconsService</a></code></td><td class=\"ngde\"><p class=\"ngde\">The MIconsService instance to use for registering icons</p></td></tr></tbody></table></div></td></tr></tbody></table></div></section><section class=\"ngde\"><h2 id=\"properties\" class=\"ngde\">Properties<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MIcons#properties\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ng-doc-properties-table ngde\"><thead class=\"ngde\"><tr indexable=\"false\" class=\"ngde\"><th class=\"ng-doc-properties-table-name ngde\">Name</th><th class=\"ng-doc-properties-table-type ngde\">Type</th><th class=\"ng-doc-properties-table-description ngde\">Description</th></tr></thead><tbody class=\"ngde\"><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\"><div class=\"ng-doc-decorators-group column ngde\" indexable=\"false\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Input\">@Input</span></div>color<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">ThemePalette</code></td><td class=\"ngde\"><p class=\"ngde\">Input property that defines the color palette for the icon. This should match one of the <code class=\"ngde\">colors</code> values in <code class=\"ngde\">ThemePalette</code> to ensure the correct color is applied to the icon.</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\"><div class=\"ng-doc-decorators-group column ngde\" indexable=\"false\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Input\">@Input</span></div>type<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">string</code></td><td class=\"ngde\"><p class=\"ngde\">Input property that defines the type of icon to display. This should match one of the <code class=\"ngde\">type</code> values in <code class=\"ngde ng-doc-code-with-link\" class=\"ngde\"><a href=\"/api/m-ui/variables/ICONS_MAPPING\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Variable\" class=\"ngde\">ICONS_MAPPING</a></code> to ensure the correct icon is displayed.</p></td></tr></tbody></table></div></section>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_api_api_m-ui_classes_MIcons_module_ts.js.map