"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_guides_getting-started_docs_module_ts"],{

/***/ 22076:
/*!**************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/getting-started/docs/module.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_getting_started_docs_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/getting-started/docs/index.html */ 87228);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);







class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = '';
    this.pageType = 'guide';
    this.pageContent = _raw_loader_ng_doc_m_docs_guides_getting_started_docs_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'Docs'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
})();

/***/ }),

/***/ 87228:
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/getting-started/docs/index.html ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<h1 id=\"creating-a-new-component\" class=\"ngde\">Creating a New Component<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/docs#creating-a-new-component\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1><h2 id=\"generate-the-component\" class=\"ngde\">Generate the Component<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/docs#generate-the-component\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><ul class=\"ngde\"><li class=\"ngde\">Use the following command to create a new component in the projects\\m-ui\\src\\lib folder:</li></ul><pre class=\"ngde hljs\"><code class=\"hljs language-bash ngde\" lang=\"bash\" filename=\"\">ng g c m-{component-name}</code></pre><ul class=\"ngde\"><li class=\"ngde\">Add an Index File In the projects\\m-ui\\src\\lib folder, create an index.ts file and export the new component:</li></ul><pre class=\"ngde hljs\"><code class=\"hljs language-typescript ngde\" lang=\"typescript\" filename=\"\"><span class=\"hljs-keyword ngde\">export</span> * <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">'./m-{component-name}.component'</span>;</code></pre><p class=\"ngde\"><strong class=\"ngde\">note</strong>: Remember to document all component parts to ensure they appear in the auto-generated documentation.</p><h2 id=\"create-a-documentation-page\" class=\"ngde\">Create a Documentation Page<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/docs#create-a-documentation-page\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><ul class=\"ngde\"><li class=\"ngde\">Use the following command to generate a new documentation page (with the --demo flag to create a demo page) in the src\\app\\docs folder:</li></ul><pre class=\"ngde hljs\"><code class=\"hljs language-bash ngde\" lang=\"bash\" filename=\"\">ng g @ng-doc/builder:page m-{component-name} --demo</code></pre><ul class=\"ngde\"><li class=\"ngde\">Update index.ts for Documentation Display In the documentation index, include relevant sections to correctly display documentation and the menu. Add the following structure:</li></ul><pre class=\"ngde hljs\"><code class=\"hljs language-markdown ngde\" lang=\"markdown\" filename=\"\"><span class=\"hljs-section ngde\">## API</span>\nShows how to use the <span class=\"hljs-code ngde\">`M{Component}`</span> component.\n\n<span class=\"hljs-section ngde\">## Demo</span>\n<span class=\"xml ngde\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">ng-doc-demo</span> <span class=\"hljs-attr ngde\">componentName</span>=<span class=\"hljs-string ngde\">\"DemoComponent\"</span> <span class=\"hljs-attr ngde\">indexable</span>=<span class=\"hljs-string ngde\">\"false\"</span>></span></span>\n<span class=\"hljs-code ngde\">                        &#x3C;div id=\"options\">{}&#x3C;/div>\n                    &#x3C;/ng-doc-demo>\n</span></code></pre><ul class=\"ngde\"><li class=\"ngde\">Add Demo Component to <code class=\"ngde\">ng-doc.dependencies.ts</code> Register the demo component:</li></ul><pre class=\"ngde hljs\"><code class=\"hljs language-typescript ngde\" lang=\"typescript\" filename=\"\"><span class=\"hljs-attr ngde\">demo</span>: {<span class=\"hljs-title class_ ngde\">DemoComponent</span>}</code></pre><ul class=\"ngde\"><li class=\"ngde\">Add Component Category in <code class=\"ngde\">ng-doc.config.ts</code> Assign a category to the documentation:</li></ul><pre class=\"ngde hljs\"><code class=\"hljs language-typescript ngde\" lang=\"typescript\" filename=\"\"><span class=\"hljs-attr ngde\">category</span>: <span class=\"hljs-title class_ ngde\">MUiCategory</span></code></pre><ul class=\"ngde\"><li class=\"ngde\">Create a Demo Component To demonstrate the component’s usage, create a demo component in the <code class=\"ngde\">src\\app\\docs\\m-{component-name}</code> folder:</li></ul><pre class=\"ngde hljs\"><code class=\"hljs language-bash ngde\" lang=\"bash\" filename=\"\">ng g c {component-name}-demo</code></pre><h2 id=\"api-docs\" class=\"ngde\">API DOCS<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/getting-started/docs#api-docs\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><p class=\"ngde\">see more at <a href=\"/api\" class=\"ngde\">API DOCS</a> or <a href=\"https://github.com/ng-doc/ng-doc/tree/v15.13.0\" class=\"ngde\">ngDoc</a></p>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_guides_getting-started_docs_module_ts.js.map