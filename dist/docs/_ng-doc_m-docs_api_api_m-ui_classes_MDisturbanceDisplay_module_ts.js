"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_api_api_m-ui_classes_MDisturbanceDisplay_module_ts"],{

/***/ 65660:
/*!***************************************************************************!*\
  !*** ./.ng-doc/m-docs/api/api/m-ui/classes/MDisturbanceDisplay/module.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_api_api_m_ui_classes_MDisturbanceDisplay_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/api/api/m-ui/classes/MDisturbanceDisplay/index.html */ 81106);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);







class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = undefined;
    this.pageType = 'api';
    this.pageContent = _raw_loader_ng_doc_m_docs_api_api_m_ui_classes_MDisturbanceDisplay_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
    this.demo = undefined;
    this.demoAssets = undefined;
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'MDisturbanceDisplay'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
})();

/***/ }),

/***/ 81106:
/*!******************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/api/api/m-ui/classes/MDisturbanceDisplay/index.html ***!
  \******************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<header class=\"ngde\"><div class=\"ng-doc-page-tags ngde\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"ng-doc-scope\">m-ui</span> <span class=\"ng-doc-inline-delimiter ngde\" indexable=\"false\">/</span> <span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Class\">Class</span> <span class=\"ng-doc-inline-delimiter ngde\" indexable=\"false\">/</span><div class=\"ng-doc-decorators-group ngde\" indexable=\"false\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Component\">@Component</span></div><span class=\"ng-doc-inline-delimiter ngde\" indexable=\"false\">/</span> <span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"ng-doc-tag-selector\">m-disturbance-display</span></div><h1 id=\"mdisturbancedisplay\" class=\"ngde\">MDisturbanceDisplay<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MDisturbanceDisplay#mdisturbancedisplay\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1><!-- This is a hack to make the declaration name available to the search index. --><div style=\"display: none\" class=\"ngde\">%%API_NAME_ANCHOR%%</div></header><section class=\"ngde\"><span class=\"ng-doc-no-content ngde\" indexable=\"false\">No documentation has been provided.</span></section><section class=\"ngde\"><h2 id=\"properties\" class=\"ngde\">Properties<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MDisturbanceDisplay#properties\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ng-doc-properties-table ngde\"><thead class=\"ngde\"><tr indexable=\"false\" class=\"ngde\"><th class=\"ng-doc-properties-table-name ngde\">Name</th><th class=\"ng-doc-properties-table-type ngde\">Type</th><th class=\"ng-doc-properties-table-description ngde\">Description</th></tr></thead><tbody class=\"ngde\"><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\"><div class=\"ng-doc-decorators-group column ngde\" indexable=\"false\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Input\">@Input</span></div>disturbance<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">any</code></td><td class=\"ngde\"><p class=\"ngde\">The disturbance to display: TYPE show -> [M-features]</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\"><div class=\"ng-doc-decorators-group column ngde\" indexable=\"false\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Input\">@Input</span></div>disturbanceCollection<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">any[]</code></td><td class=\"ngde\"><p class=\"ngde\">The disturbance collection: TYPE show -> [M-features]</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\"><div class=\"ng-doc-decorators-group column ngde\" indexable=\"false\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Input\">@Input</span></div>link<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">string</code></td><td class=\"ngde\"><p class=\"ngde\">The link for redirection</p></td></tr></tbody></table></div></section><section class=\"ngde\"><h2 id=\"methods\" class=\"ngde\">Methods<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MDisturbanceDisplay#methods\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ng-doc-method-table ngde\"><thead class=\"ngde\"><tr class=\"ngde\"><th indexable=\"false\" class=\"ngde\"><h3 id=\"getdynamicclasses\" class=\"ngde\">getDynamicClasses()<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/classes/MDisturbanceDisplay#getdynamicclasses\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h3><div class=\"ng-doc-node-details ngde\"></div></th></tr></thead><tbody class=\"ngde\"><tr class=\"ngde\"><td class=\"ngde\"><p class=\"ngde\">Get the dynamic classes for the disturbance</p></td></tr><tr class=\"ngde\"><td class=\"ngde\"><h5 class=\"no-anchor ngde\" indexable=\"false\">Presentation</h5><pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-title function_ ngde\">getDynamicClasses</span>(<span class=\"hljs-attr ngde\">disturbance</span>: <span class=\"hljs-built_in ngde\">any</span>): <span class=\"hljs-built_in ngde\">string</span>[];</code></pre></td></tr><tr class=\"ngde\"><td class=\"ngde\"><h5 class=\"no-anchor ngde\" indexable=\"false\">Parameters</h5><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ng-doc-parameters-table ngde\"><thead class=\"ngde\"><tr indexable=\"false\" class=\"ngde\"><th class=\"ng-doc-parameters-table-name ngde\">Name</th><th class=\"ng-doc-parameters-table-type ngde\">Type</th><th class=\"ng-doc-parameters-table-description ngde\">Description</th></tr></thead><tbody class=\"ngde\"><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">disturbance<div class=\"ng-doc-node-details ngde\"></div></td><td class=\"ngde\"><code indexable=\"false\" class=\"ngde\">any</code></td><td class=\"ngde\"><p class=\"ngde\">any string[]</p></td></tr></tbody></table></div><h5 class=\"no-anchor ngde\" indexable=\"false\">Returns</h5><p class=\"ngde\"><code indexable=\"false\" class=\"ngde\">string[]</code></p></td></tr></tbody></table></div></section>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_api_api_m-ui_classes_MDisturbanceDisplay_module_ts.js.map