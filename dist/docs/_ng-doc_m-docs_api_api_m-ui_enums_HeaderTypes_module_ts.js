"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_api_api_m-ui_enums_HeaderTypes_module_ts"],{

/***/ 64653:
/*!*****************************************************************!*\
  !*** ./.ng-doc/m-docs/api/api/m-ui/enums/HeaderTypes/module.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_api_api_m_ui_enums_HeaderTypes_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/api/api/m-ui/enums/HeaderTypes/index.html */ 70389);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);







class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = undefined;
    this.pageType = 'api';
    this.pageContent = _raw_loader_ng_doc_m_docs_api_api_m_ui_enums_HeaderTypes_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
    this.demo = undefined;
    this.demoAssets = undefined;
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_1__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'HeaderTypes'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_3__.NgDocPageModule, _angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
  });
})();

/***/ }),

/***/ 70389:
/*!********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/api/api/m-ui/enums/HeaderTypes/index.html ***!
  \********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<header class=\"ngde\"><div class=\"ng-doc-page-tags ngde\"><span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"ng-doc-scope\">m-ui</span> <span class=\"ng-doc-inline-delimiter ngde\" indexable=\"false\">/</span> <span class=\"ng-doc-tag ngde\" indexable=\"false\" data-content=\"Enum\">Enum</span></div><h1 id=\"headertypes\" class=\"ngde\">HeaderTypes<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/enums/HeaderTypes#headertypes\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1></header><section class=\"ngde\"><p class=\"ngde\">Enumeration of the different types of headers supported in the table.</p></section><section class=\"ngde\"><h2 id=\"members\" class=\"ngde\">Members<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/api/m-ui/enums/HeaderTypes#members\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><div class=\"ng-doc-table-wrapper ngde\"><table class=\"ngde\"><thead class=\"ngde\"><tr indexable=\"false\" class=\"ngde\"><th class=\"ngde\">Name</th><th class=\"ngde\">Value</th><th class=\"ngde\">Description</th></tr></thead><tbody class=\"ngde\"><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">TEXT</td><td indexable=\"false\" class=\"ngde\"><code class=\"ngde\">\"text\"</code></td><td class=\"ngde\"><p class=\"ngde\">Text-based column.</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">RELATIVE_DATE</td><td indexable=\"false\" class=\"ngde\"><code class=\"ngde\">\"relative_date\"</code></td><td class=\"ngde\"><p class=\"ngde\">Relative Date-based</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">DATE</td><td indexable=\"false\" class=\"ngde\"><code class=\"ngde\">\"date\"</code></td><td class=\"ngde\"><p class=\"ngde\">Date-based column.</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">NUMBER</td><td indexable=\"false\" class=\"ngde\"><code class=\"ngde\">\"number\"</code></td><td class=\"ngde\"><p class=\"ngde\">Numeric column.</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">BOOLEAN</td><td indexable=\"false\" class=\"ngde\"><code class=\"ngde\">\"boolean\"</code></td><td class=\"ngde\"><p class=\"ngde\">Boolean column.</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">ACTIONS</td><td indexable=\"false\" class=\"ngde\"><code class=\"ngde\">\"actions\"</code></td><td class=\"ngde\"><p class=\"ngde\">Column for action buttons.</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">SELECT</td><td indexable=\"false\" class=\"ngde\"><code class=\"ngde\">\"select\"</code></td><td class=\"ngde\"><p class=\"ngde\">Dropdown selection in a row.</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">SELECTION</td><td indexable=\"false\" class=\"ngde\"><code class=\"ngde\">\"selection\"</code></td><td class=\"ngde\"><p class=\"ngde\">Checkbox for selecting rows.</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">MATCHING</td><td indexable=\"false\" class=\"ngde\"><code class=\"ngde\">\"matching\"</code></td><td class=\"ngde\"><p class=\"ngde\">Column for matching criteria.</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">ARRAY</td><td indexable=\"false\" class=\"ngde\"><code class=\"ngde\">\"array\"</code></td><td class=\"ngde\"><p class=\"ngde\">Array data column.</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">LIST</td><td indexable=\"false\" class=\"ngde\"><code class=\"ngde\">\"list\"</code></td><td class=\"ngde\"><p class=\"ngde\">List data column.</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">LINK</td><td indexable=\"false\" class=\"ngde\"><code class=\"ngde\">\"link\"</code></td><td class=\"ngde\"><p class=\"ngde\">Column with anchor (<code class=\"ngde\">&#x3C;a></code>) links.</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">ICON</td><td indexable=\"false\" class=\"ngde\"><code class=\"ngde\">\"icon\"</code></td><td class=\"ngde\"><p class=\"ngde\">Icon column.</p></td></tr><tr class=\"ngde\"><td indexable=\"false\" class=\"ngde\">LINE</td><td indexable=\"false\" class=\"ngde\"><code class=\"ngde\">\"line\"</code></td><td class=\"ngde\"><p class=\"ngde\">Transport line column.</p></td></tr></tbody></table></div></section>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_api_api_m-ui_enums_HeaderTypes_module_ts.js.map