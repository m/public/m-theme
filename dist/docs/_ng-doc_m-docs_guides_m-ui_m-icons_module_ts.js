"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_guides_m-ui_m-icons_module_ts"],{

/***/ 29276:
/*!****************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-ui/m-icons/component-assets.ts ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   "demoAssets": () => (/* binding */ demoAssets)
/* harmony export */ });
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_ui_m_icons_assets_MIconsDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-ui/m-icons/assets/MIconsDemo/TypeScript/Asset0.html */ 74148);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_ui_m_icons_assets_MIconsDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-ui/m-icons/assets/MIconsDemo/HTML/Asset1.html */ 30225);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_ui_m_icons_assets_MIconsDemo_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-ui/m-icons/assets/MIconsDemo/SCSS/Asset2.html */ 32490);



const demoAssets = {
  MIconsDemo: [{
    title: 'TypeScript',
    codeType: 'TypeScript',
    code: _raw_loader_ng_doc_m_docs_guides_m_ui_m_icons_assets_MIconsDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__["default"]
  }, {
    title: 'HTML',
    codeType: 'HTML',
    code: _raw_loader_ng_doc_m_docs_guides_m_ui_m_icons_assets_MIconsDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__["default"]
  }, {
    title: 'SCSS',
    codeType: 'SCSS',
    code: _raw_loader_ng_doc_m_docs_guides_m_ui_m_icons_assets_MIconsDemo_SCSS_Asset2_html__WEBPACK_IMPORTED_MODULE_2__["default"]
  }]
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (demoAssets);

/***/ }),

/***/ 70938:
/*!******************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-ui/m-icons/module.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_ui_m_icons_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-ui/m-icons/index.html */ 82224);
/* harmony import */ var _playgrounds__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./playgrounds */ 10969);
/* harmony import */ var src_app_docs_m_ui_m_icons_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/docs/m-ui/m-icons/ng-doc.dependencies */ 50647);
/* harmony import */ var _ng_doc_m_docs_guides_m_ui_m_icons_component_assets__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! .ng-doc/m-docs/guides/m-ui/m-icons/component-assets */ 29276);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_docs_m_ui_m_icons_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/docs/m-ui/m-icons/ng-doc.module */ 78762);




// noinspection ES6UnusedImports


// noinspection ES6UnusedImports





class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = '';
    this.pageType = 'guide';
    this.pageContent = _raw_loader_ng_doc_m_docs_guides_m_ui_m_icons_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
    this.dependencies = src_app_docs_m_ui_m_icons_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"];
    this.demoAssets = _ng_doc_m_docs_guides_m_ui_m_icons_component_assets__WEBPACK_IMPORTED_MODULE_3__["default"];
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, src_app_docs_m_ui_m_icons_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"].module, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'M-icons'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, src_app_docs_m_ui_m_icons_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__.MIconsPageModule, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
})();

/***/ }),

/***/ 10969:
/*!***********************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-ui/m-icons/playgrounds.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PlaygroundsModule": () => (/* binding */ PlaygroundsModule)
/* harmony export */ });
/* harmony import */ var src_app_docs_m_ui_m_icons_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/docs/m-ui/m-icons/ng-doc.dependencies */ 50647);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var src_app_docs_m_ui_m_icons_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/docs/m-ui/m-icons/ng-doc.module */ 78762);
// noinspection ES6UnusedImports




class PlaygroundsModule {
  static #_ = this.ɵfac = function PlaygroundsModule_Factory(t) {
    return new (t || PlaygroundsModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: PlaygroundsModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, src_app_docs_m_ui_m_icons_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__["default"].module]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](PlaygroundsModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, src_app_docs_m_ui_m_icons_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__.MIconsPageModule]
  });
})();

/***/ }),

/***/ 8384:
/*!********************************************************************!*\
  !*** ./src/app/docs/m-ui/m-icons/icon-demo/icon-demo.component.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MIconsDemo": () => (/* binding */ MIconsDemo)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/input */ 68562);
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/snack-bar */ 10930);
/* harmony import */ var _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/tooltip */ 6896);
/* harmony import */ var _metromobilite_m_ui_m_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @metromobilite/m-ui/m-icons */ 73029);
/* harmony import */ var src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/features/preview/preview.component */ 55355);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/form-field */ 75074);












function MIconsDemo_li_18_Template(rf, ctx) {
  if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "li", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("click", function MIconsDemo_li_18_Template_li_click_0_listener() {
      const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵrestoreView"](_r4);
      const i_r2 = restoredCtx.$implicit;
      const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵnextContext"]();
      return _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵresetView"](ctx_r3.copyToClipboard(i_r2));
    });
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](1, "m-icons", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](2, "span", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
  }
  if (rf & 2) {
    const i_r2 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("matTooltip", i_r2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("type", i_r2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtextInterpolate"](i_r2);
  }
}
class MIconsDemo {
  constructor(snackBar) {
    this.snackBar = snackBar;
    this.iconList = _metromobilite_m_ui_m_icons__WEBPACK_IMPORTED_MODULE_2__.ICONS_MAPPING.map(icon => icon.type);
  }
  applyFilter(event) {
    const filterValue = event.target.value;
    this.iconList = _metromobilite_m_ui_m_icons__WEBPACK_IMPORTED_MODULE_2__.ICONS_MAPPING.map(icon => icon.type).filter(icon => icon.toLowerCase().includes(filterValue.toLowerCase()));
  }
  copyToClipboard(text) {
    navigator.clipboard.writeText(text).then(() => {
      this.snackBar.open(`Texte copié : ${text}`, 'Fermer', {
        duration: 2000
      });
    }).catch(err => {
      console.error('Erreur lors de la copie :', err);
    });
  }
  static #_ = this.ɵfac = function MIconsDemo_Factory(t) {
    return new (t || MIconsDemo)(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdirectiveInject"](_angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_3__.MatSnackBar));
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
    type: MIconsDemo,
    selectors: [["app-icon-demo"]],
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵProvidersFeature"]([_angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_3__.MatSnackBar]), _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵStandaloneFeature"]],
    decls: 19,
    vars: 4,
    consts: [["title", "Mat-icons Default", 3, "fullWidth"], ["color", "primary"], ["color", "accent"], ["color", "warn"], ["title", "Icons liste", 3, "fullWidth", "nextElement"], ["matInput", "", "placeholder", "Ex. Mia", 3, "keyup"], ["input", ""], [1, "icon-grid"], ["class", "icon-item", "matTooltipPosition", "above", 3, "matTooltip", "click", 4, "ngFor", "ngForOf"], ["matTooltipPosition", "above", 1, "icon-item", 3, "matTooltip", "click"], [1, "m-icon", 3, "type"], [1, "icon-label"]],
    template: function MIconsDemo_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](0, "app-preview", 0)(1, "m-icons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](2, "close");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](3, "m-icons");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](4, "home");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](5, "m-icons", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](6, "home");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](7, "m-icons", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](8, "home");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](9, "m-icons", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](10, "home");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](11, "app-preview", 4)(12, "mat-form-field")(13, "mat-label");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtext"](14, "Filter");
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](15, "input", 5, 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵlistener"]("keyup", function MIconsDemo_Template_input_keyup_15_listener($event) {
          return ctx.applyFilter($event);
        });
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](17, "ul", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](18, MIconsDemo_li_18_Template, 4, 3, "li", 8);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]()();
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("fullWidth", true);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](11);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("fullWidth", true)("nextElement", false);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.iconList);
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule, _angular_common__WEBPACK_IMPORTED_MODULE_4__.NgForOf, src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__.Preview, _metromobilite_m_ui_m_icons__WEBPACK_IMPORTED_MODULE_2__.MIcons, _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_5__.MatTooltipModule, _angular_material_tooltip__WEBPACK_IMPORTED_MODULE_5__.MatTooltip, _angular_material_input__WEBPACK_IMPORTED_MODULE_6__.MatInputModule, _angular_material_input__WEBPACK_IMPORTED_MODULE_6__.MatInput, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_7__.MatFormField, _angular_material_form_field__WEBPACK_IMPORTED_MODULE_7__.MatLabel],
    styles: ["mat-form-field[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n.icon-grid[_ngcontent-%COMP%] {\n  display: grid;\n  grid-template-columns: repeat(auto-fit, minmax(80px, 1fr));\n  gap: 8px;\n  padding: 8px;\n}\n.icon-grid[_ngcontent-%COMP%]   .icon-item[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n  border-radius: 8px;\n  padding: 8px;\n  transition: transform 0.2s ease, box-shadow 0.2s ease;\n  cursor: pointer;\n}\n.icon-grid[_ngcontent-%COMP%]   .icon-item[_ngcontent-%COMP%]:hover {\n  transform: translateY(-2px);\n}\n.icon-grid[_ngcontent-%COMP%]   .icon-item[_ngcontent-%COMP%]:hover   .m-icon[_ngcontent-%COMP%] {\n  transform: scale(1.6);\n}\n.icon-grid[_ngcontent-%COMP%]   .icon-item[_ngcontent-%COMP%]   .m-icon[_ngcontent-%COMP%] {\n  transition: transform 0.3s ease;\n}\n.icon-grid[_ngcontent-%COMP%]   .icon-item[_ngcontent-%COMP%]   .icon-label[_ngcontent-%COMP%] {\n  word-break: break-all;\n  margin-top: 4px;\n  text-align: center;\n  transition: opacity 0.2s ease;\n  font-size: 12px;\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3NyYy9hcHAvZG9jcy9tLXVpL20taWNvbnMvaWNvbi1kZW1vL2ljb24tZGVtby5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNDLFdBQUE7QUFDRDs7QUFFQTtFQUNDLGFBQUE7RUFDQSwwREFBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0FBQ0Q7QUFDQztFQUNDLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxxREFBQTtFQUNBLGVBQUE7QUFDRjtBQUNFO0VBQ0MsMkJBQUE7QUFDSDtBQUFHO0VBQ0MscUJBQUE7QUFFSjtBQUVFO0VBQ0MsK0JBQUE7QUFBSDtBQUdFO0VBQ0MscUJBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSw2QkFBQTtFQUNBLGVBQUE7QUFESCIsInNvdXJjZXNDb250ZW50IjpbIm1hdC1mb3JtLWZpZWxke1xyXG5cdHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uaWNvbi1ncmlkIHtcclxuXHRkaXNwbGF5OiBncmlkO1xyXG5cdGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KGF1dG8tZml0LCBtaW5tYXgoODBweCwgMWZyKSk7XHJcblx0Z2FwOiA4cHg7XHJcblx0cGFkZGluZzogOHB4O1xyXG5cclxuXHQuaWNvbi1pdGVtIHtcclxuXHRcdGRpc3BsYXk6IGZsZXg7XHJcblx0XHRmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG5cdFx0YWxpZ24taXRlbXM6IGNlbnRlcjtcclxuXHRcdGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG5cdFx0Ym9yZGVyLXJhZGl1czogOHB4O1xyXG5cdFx0cGFkZGluZzogOHB4O1xyXG5cdFx0dHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuMnMgZWFzZSwgYm94LXNoYWRvdyAwLjJzIGVhc2U7XHJcblx0XHRjdXJzb3I6IHBvaW50ZXI7XHJcblxyXG5cdFx0Jjpob3ZlciB7XHJcblx0XHRcdHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtMnB4KTtcclxuXHRcdFx0Lm0taWNvbiB7XHJcblx0XHRcdFx0dHJhbnNmb3JtOiBzY2FsZSgxLjYpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdFx0Lm0taWNvbiB7XHJcblx0XHRcdHRyYW5zaXRpb246IHRyYW5zZm9ybSAwLjNzIGVhc2U7XHJcblx0XHR9XHJcblxyXG5cdFx0Lmljb24tbGFiZWwge1xyXG5cdFx0XHR3b3JkLWJyZWFrOiBicmVhay1hbGw7XHJcblx0XHRcdG1hcmdpbi10b3A6IDRweDtcclxuXHRcdFx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdFx0XHR0cmFuc2l0aW9uOiBvcGFjaXR5IDAuMnMgZWFzZTtcclxuXHRcdFx0Zm9udC1zaXplOiAxMnB4O1xyXG5cdFx0fVxyXG5cdH1cclxufVxyXG4iXSwic291cmNlUm9vdCI6IiJ9 */"]
  });
}

/***/ }),

/***/ 50647:
/*!**********************************************************!*\
  !*** ./src/app/docs/m-ui/m-icons/ng-doc.dependencies.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var src_app_docs_m_ui_m_icons_icon_demo_icon_demo_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/docs/m-ui/m-icons/icon-demo/icon-demo.component */ 8384);
/* harmony import */ var src_app_docs_m_ui_m_icons_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/docs/m-ui/m-icons/ng-doc.module */ 78762);


const MIconsPageDependencies = {
  module: src_app_docs_m_ui_m_icons_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__.MIconsPageModule,
  // Add your demos that you are going to use in the page here
  demo: {
    MIconsDemo: src_app_docs_m_ui_m_icons_icon_demo_icon_demo_component__WEBPACK_IMPORTED_MODULE_0__.MIconsDemo
  }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MIconsPageDependencies);

/***/ }),

/***/ 78762:
/*!****************************************************!*\
  !*** ./src/app/docs/m-ui/m-icons/ng-doc.module.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MIconsPageModule": () => (/* binding */ MIconsPageModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);


class MIconsPageModule {
  static #_ = this.ɵfac = function MIconsPageModule_Factory(t) {
    return new (t || MIconsPageModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
    type: MIconsPageModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](MIconsPageModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
})();

/***/ }),

/***/ 30225:
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-ui/m-icons/assets/MIconsDemo/HTML/Asset1.html ***!
  \*********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"html\" class=\"hljs language-html ngde\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Mat-icons Default\"</span> [<span class=\"hljs-attr ngde\">fullWidth</span>]=<span class=\"hljs-string ngde\">\"true\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">m-icons</span>></span>close<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">m-icons</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">m-icons</span>></span>home<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">m-icons</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">m-icons</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"primary\"</span>></span>home<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">m-icons</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">m-icons</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"accent\"</span>></span>home<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">m-icons</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">m-icons</span> <span class=\"hljs-attr ngde\">color</span>=<span class=\"hljs-string ngde\">\"warn\"</span>></span>home<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">m-icons</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n\n<span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> <span class=\"hljs-attr ngde\">title</span>=<span class=\"hljs-string ngde\">\"Icons liste\"</span> [<span class=\"hljs-attr ngde\">fullWidth</span>]=<span class=\"hljs-string ngde\">\"true\"</span> [<span class=\"hljs-attr ngde\">nextElement</span>]=<span class=\"hljs-string ngde\">\"false\"</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-form-field</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">mat-label</span>></span>Filter<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-label</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">input</span>\n      <span class=\"hljs-attr ngde\">matInput</span>\n      (<span class=\"hljs-attr ngde\">keyup</span>)=<span class=\"hljs-string ngde\">\"applyFilter($event)\"</span>\n      <span class=\"hljs-attr ngde\">placeholder</span>=<span class=\"hljs-string ngde\">\"Ex. Mia\"</span>\n      #<span class=\"hljs-attr ngde\">input</span>\n    /></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">mat-form-field</span>></span>\n\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">ul</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"icon-grid\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">li</span>\n      *<span class=\"hljs-attr ngde\">ngFor</span>=<span class=\"hljs-string ngde\">\"let i of iconList\"</span>\n      <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"icon-item\"</span>\n      [<span class=\"hljs-attr ngde\">matTooltip</span>]=<span class=\"hljs-string ngde\">\"i\"</span>\n      <span class=\"hljs-attr ngde\">matTooltipPosition</span>=<span class=\"hljs-string ngde\">\"above\"</span>\n      (<span class=\"hljs-attr ngde\">click</span>)=<span class=\"hljs-string ngde\">\"copyToClipboard(i)\"</span>\n    ></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">m-icons</span> [<span class=\"hljs-attr ngde\">type</span>]=<span class=\"hljs-string ngde\">\"i\"</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"m-icon\"</span>></span><span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">m-icons</span>></span>\n      <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">span</span> <span class=\"hljs-attr ngde\">class</span>=<span class=\"hljs-string ngde\">\"icon-label\"</span>></span>{{i}}<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">span</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">li</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">ul</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n</code></pre>");

/***/ }),

/***/ 32490:
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-ui/m-icons/assets/MIconsDemo/SCSS/Asset2.html ***!
  \*********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"scss\" class=\"hljs language-scss ngde\">mat-<span class=\"hljs-selector-tag ngde\">form</span>-field {\n  <span class=\"hljs-attribute ngde\">width</span>: <span class=\"hljs-number ngde\">100%</span>;\n}\n\n<span class=\"hljs-selector-class ngde\">.icon-grid</span> {\n  <span class=\"hljs-attribute ngde\">display</span>: grid;\n  <span class=\"hljs-attribute ngde\">grid-template-columns</span>: <span class=\"hljs-built_in ngde\">repeat</span>(auto-fit, <span class=\"hljs-built_in ngde\">minmax</span>(<span class=\"hljs-number ngde\">80px</span>, <span class=\"hljs-number ngde\">1</span>fr));\n  <span class=\"hljs-attribute ngde\">gap</span>: <span class=\"hljs-number ngde\">8px</span>;\n  <span class=\"hljs-attribute ngde\">padding</span>: <span class=\"hljs-number ngde\">8px</span>;\n\n  <span class=\"hljs-selector-class ngde\">.icon-item</span> {\n    <span class=\"hljs-attribute ngde\">display</span>: flex;\n    <span class=\"hljs-attribute ngde\">flex-direction</span>: column;\n    <span class=\"hljs-attribute ngde\">align-items</span>: center;\n    <span class=\"hljs-attribute ngde\">justify-content</span>: center;\n    <span class=\"hljs-attribute ngde\">border-radius</span>: <span class=\"hljs-number ngde\">8px</span>;\n    <span class=\"hljs-attribute ngde\">padding</span>: <span class=\"hljs-number ngde\">8px</span>;\n    <span class=\"hljs-attribute ngde\">transition</span>: transform <span class=\"hljs-number ngde\">0.2s</span> ease, box-shadow <span class=\"hljs-number ngde\">0.2s</span> ease;\n    <span class=\"hljs-attribute ngde\">cursor</span>: pointer;\n\n    &#x26;<span class=\"hljs-selector-pseudo ngde\">:hover</span> {\n      <span class=\"hljs-attribute ngde\">transform</span>: <span class=\"hljs-built_in ngde\">translateY</span>(-<span class=\"hljs-number ngde\">2px</span>);\n      <span class=\"hljs-selector-class ngde\">.m-icon</span> {\n        <span class=\"hljs-attribute ngde\">transform</span>: <span class=\"hljs-built_in ngde\">scale</span>(<span class=\"hljs-number ngde\">1.6</span>);\n      }\n    }\n\n    <span class=\"hljs-selector-class ngde\">.m-icon</span> {\n      <span class=\"hljs-attribute ngde\">transition</span>: transform <span class=\"hljs-number ngde\">0.3s</span> ease;\n    }\n\n    <span class=\"hljs-selector-class ngde\">.icon-label</span> {\n      <span class=\"hljs-attribute ngde\">word-break</span>: break-all;\n      <span class=\"hljs-attribute ngde\">margin-top</span>: <span class=\"hljs-number ngde\">4px</span>;\n      <span class=\"hljs-attribute ngde\">text-align</span>: center;\n      <span class=\"hljs-attribute ngde\">transition</span>: opacity <span class=\"hljs-number ngde\">0.2s</span> ease;\n      <span class=\"hljs-attribute ngde\">font-size</span>: <span class=\"hljs-number ngde\">12px</span>;\n    }\n  }\n}\n</code></pre>");

/***/ }),

/***/ 74148:
/*!***************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-ui/m-icons/assets/MIconsDemo/TypeScript/Asset0.html ***!
  \***************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">CommonModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/common\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Component</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/core\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatInputModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/input\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatSnackBar</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/snack-bar\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatTooltipModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/tooltip\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> {\n  <span class=\"hljs-title class_ ngde\"><a href=\"/api/m-ui/interfaces/IconMapping\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Interface\" class=\"ngde\">IconMapping</a></span>,\n  <span class=\"hljs-variable constant_ ngde\"><a href=\"/api/m-ui/variables/ICONS_MAPPING\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Variable\" class=\"ngde\">ICONS_MAPPING</a></span>,\n  <span class=\"hljs-title class_ ngde\"><a href=\"/api/m-ui/classes/MIcons\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MIcons</a></span>,\n} <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@metromobilite/m-ui/m-icons\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Preview</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"src/app/features/preview/preview.component\"</span>;\n\n<span class=\"hljs-meta ngde\">@Component</span>({\n  <span class=\"hljs-attr ngde\">selector</span>: <span class=\"hljs-string ngde\">\"app-icon-demo\"</span>,\n  <span class=\"hljs-attr ngde\">standalone</span>: <span class=\"hljs-literal ngde\">true</span>,\n  <span class=\"hljs-attr ngde\">imports</span>: [<span class=\"hljs-title class_ ngde\">CommonModule</span>, <span class=\"hljs-title class_ ngde\">Preview</span>, <span class=\"hljs-title class_ ngde\"><a href=\"/api/m-ui/classes/MIcons\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MIcons</a></span>, <span class=\"hljs-title class_ ngde\">MatTooltipModule</span>, <span class=\"hljs-title class_ ngde\">MatInputModule</span>],\n  <span class=\"hljs-attr ngde\">templateUrl</span>: <span class=\"hljs-string ngde\">\"./icon-demo.component.html\"</span>,\n  <span class=\"hljs-attr ngde\">styleUrls</span>: [<span class=\"hljs-string ngde\">\"./icon-demo.component.scss\"</span>],\n  <span class=\"hljs-attr ngde\">providers</span>: [<span class=\"hljs-title class_ ngde\">MatSnackBar</span>],\n})\n<span class=\"hljs-keyword ngde\">export</span> <span class=\"hljs-keyword ngde\">class</span> <span class=\"hljs-title class_ ngde\">MIconsDemo</span> {\n  <span class=\"hljs-keyword ngde\">public</span> <span class=\"hljs-attr ngde\">iconList</span>: <span class=\"hljs-built_in ngde\">string</span>[] = <span class=\"hljs-variable constant_ ngde\"><a href=\"/api/m-ui/variables/ICONS_MAPPING\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Variable\" class=\"ngde\">ICONS_MAPPING</a></span>.<span class=\"hljs-title function_ ngde\">map</span>(<span class=\"hljs-function ngde\">(<span class=\"hljs-params ngde\">icon</span>) =></span> icon.<span class=\"hljs-property ngde\">type</span>);\n\n  <span class=\"hljs-title function_ ngde\">constructor</span>(<span class=\"hljs-params ngde\"><span class=\"hljs-keyword ngde\">private</span> snackBar: MatSnackBar</span>) {}\n\n  <span class=\"hljs-title function_ ngde\">applyFilter</span>(<span class=\"hljs-attr ngde\">event</span>: <span class=\"hljs-title class_ ngde\">Event</span>): <span class=\"hljs-built_in ngde\">void</span> {\n    <span class=\"hljs-keyword ngde\">const</span> filterValue = (event.<span class=\"hljs-property ngde\">target</span> <span class=\"hljs-keyword ngde\">as</span> <span class=\"hljs-title class_ ngde\">HTMLInputElement</span>).<span class=\"hljs-property ngde\">value</span>;\n    <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">iconList</span> = <span class=\"hljs-variable constant_ ngde\"><a href=\"/api/m-ui/variables/ICONS_MAPPING\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Variable\" class=\"ngde\">ICONS_MAPPING</a></span>.<span class=\"hljs-title function_ ngde\">map</span>(<span class=\"hljs-function ngde\">(<span class=\"hljs-params ngde\">icon: <a href=\"/api/m-ui/interfaces/IconMapping\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Interface\" class=\"ngde\">IconMapping</a></span>) =></span> icon.<span class=\"hljs-property ngde\">type</span>).<span class=\"hljs-title function_ ngde\">filter</span>(\n      <span class=\"hljs-function ngde\">(<span class=\"hljs-params ngde\">icon: <span class=\"hljs-built_in ngde\">string</span></span>) =></span> icon.<span class=\"hljs-title function_ ngde\">toLowerCase</span>().<span class=\"hljs-title function_ ngde\">includes</span>(filterValue.<span class=\"hljs-title function_ ngde\">toLowerCase</span>())\n    );\n  }\n\n  <span class=\"hljs-title function_ ngde\">copyToClipboard</span>(<span class=\"hljs-attr ngde\">text</span>: <span class=\"hljs-built_in ngde\">string</span>): <span class=\"hljs-built_in ngde\">void</span> {\n    navigator.<span class=\"hljs-property ngde\">clipboard</span>\n      .<span class=\"hljs-title function_ ngde\">writeText</span>(text)\n      .<span class=\"hljs-title function_ ngde\">then</span>(<span class=\"hljs-function ngde\">() =></span> {\n        <span class=\"hljs-variable language_ ngde\">this</span>.<span class=\"hljs-property ngde\">snackBar</span>.<span class=\"hljs-title function_ ngde\">open</span>(<span class=\"hljs-string ngde\">`Texte copié : <span class=\"hljs-subst ngde\">${text}</span>`</span>, <span class=\"hljs-string ngde\">\"Fermer\"</span>, {\n          <span class=\"hljs-attr ngde\">duration</span>: <span class=\"hljs-number ngde\">2000</span>,\n        });\n      })\n      .<span class=\"hljs-title function_ ngde\">catch</span>(<span class=\"hljs-function ngde\">(<span class=\"hljs-params ngde\">err</span>) =></span> {\n        <span class=\"hljs-variable language_ ngde\">console</span>.<span class=\"hljs-title function_ ngde\">error</span>(<span class=\"hljs-string ngde\">\"Erreur lors de la copie :\"</span>, err);\n      });\n  }\n}\n</code></pre>");

/***/ }),

/***/ 82224:
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-ui/m-icons/index.html ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<h1 id=\"m-icons\" class=\"ngde\">M-icons<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/m-ui/m-icons#m-icons\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1><h2 id=\"api\" class=\"ngde\">API<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/m-ui/m-icons#api\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><p class=\"ngde\">Shows how to use the <code class=\"ngde ng-doc-code-with-link\" class=\"ngde\"><a href=\"/api/m-ui/classes/MIcons\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MIcons</a></code> component.</p><h2 id=\"installation\" class=\"ngde\">Installation<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/m-ui/m-icons#installation\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><ul class=\"ngde\"><li class=\"ngde\">Import the <code class=\"ngde\">MIconsModule</code> and <code class=\"ngde\">HttpClientModule</code> in your application module.<ul class=\"ngde\"><li class=\"ngde\">MIconsModule use the icons in the application.</li><li class=\"ngde\">HttpClientModule is required to load the icons from the server.</li></ul></li><li class=\"ngde\">Add icons path in angular.json files.</li></ul><pre class=\"ngde hljs\"><code class=\"hljs language-json ngde\" lang=\"json\" filename=\"\">    <span class=\"hljs-punctuation ngde\">{</span>\n    <span class=\"hljs-attr ngde\">\"assets\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-punctuation ngde\">[</span>\n        <span class=\"hljs-punctuation ngde\">{</span>\n            <span class=\"hljs-attr ngde\">\"glob\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-string ngde\">\"**/*\"</span><span class=\"hljs-punctuation ngde\">,</span>\n            <span class=\"hljs-attr ngde\">\"input\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-string ngde\">\"node_modules/@metromobilite/m-ui/assets\"</span><span class=\"hljs-punctuation ngde\">,</span>\n            <span class=\"hljs-attr ngde\">\"output\"</span><span class=\"hljs-punctuation ngde\">:</span> <span class=\"hljs-string ngde\">\"/assets/\"</span>\n        <span class=\"hljs-punctuation ngde\">}</span>\n    <span class=\"hljs-punctuation ngde\">]</span>\n<span class=\"hljs-punctuation ngde\">}</span></code></pre><h2 id=\"add-new-icons\" class=\"ngde\">Add new icons<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/m-ui/m-icons#add-new-icons\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><ul class=\"ngde\"><li class=\"ngde\">Add the new icon in the <code class=\"ngde\">project/m-ui/src/assets/icons</code> folder.</li><li class=\"ngde\">Add the icon definition in component MIcons</li></ul><pre class=\"ngde hljs\"><code class=\"hljs language-typescript ngde\" lang=\"typescript\" filename=\"\">    <span class=\"hljs-keyword ngde\">const</span> <span class=\"hljs-variable constant_ ngde\"><a href=\"/api/m-ui/variables/ICONS_MAPPING\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Variable\" class=\"ngde\">ICONS_MAPPING</a></span> = [\n        {<span class=\"hljs-attr ngde\">type</span>: <span class=\"hljs-string ngde\">'newIcon'</span>, <span class=\"hljs-attr ngde\">name</span>: <span class=\"hljs-string ngde\">'newIcon.svg'</span>}\n    ];</code></pre><ul class=\"ngde\"><li class=\"ngde\">Add mat-icon in the html file</li></ul><pre class=\"ngde hljs\"><code class=\"hljs language-angular2html ngde\" lang=\"angular2html\" filename=\"\">    &#x3C;mat-icon *ngSwitchCase=\"'newIcon'\"  svgIcon=\"newIcon\">&#x3C;/mat-icon></code></pre><ul class=\"ngde\"><li class=\"ngde\">Rebuild the project for share the new icon.</li><li class=\"ngde\">Use the new icon in the application.</li></ul><pre class=\"ngde hljs\"><code class=\"hljs language-angular2html ngde\" lang=\"angular2html\" filename=\"\">    &#x3C;mat-icon svgIcon=\"newIcon\">&#x3C;/mat-icon></code></pre><h2 id=\"icons-list\" class=\"ngde\">Icons List<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/m-ui/m-icons#icons-list\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><ng-doc-demo componentname=\"MIconsDemo\" indexable=\"false\" class=\"ngde\"><div id=\"options\" class=\"ngde\">{}</div></ng-doc-demo>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_guides_m-ui_m-icons_module_ts.js.map