"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_api_api_m-ui_module_ts"],{

/***/ 58509:
/*!***********************************************!*\
  !*** ./.ng-doc/m-docs/api/api/m-ui/module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);



class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule.forChild([{
      path: '',
      redirectTo: 'interfaces/IconMapping',
      pathMatch: 'full'
    }, {
      path: '',
      title: 'm-ui',
      children: [{
        path: 'interfaces/IconMapping',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_interfaces_IconMapping_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/interfaces/IconMapping/module */ 96933)).then(m => m.DynamicModule)
      }, {
        path: 'variables/ICONS_MAPPING',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_variables_ICONS_MAPPING_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/variables/ICONS_MAPPING/module */ 3242)).then(m => m.DynamicModule)
      }, {
        path: 'classes/MIcons',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_classes_MIcons_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/classes/MIcons/module */ 4322)).then(m => m.DynamicModule)
      }, {
        path: 'classes/MIconsService',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_classes_MIconsService_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/classes/MIconsService/module */ 27244)).then(m => m.DynamicModule)
      }, {
        path: 'interfaces/Affluence',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_interfaces_Affluence_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/interfaces/Affluence/module */ 86753)).then(m => m.DynamicModule)
      }, {
        path: 'classes/MAffluence',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_classes_MAffluence_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/classes/MAffluence/module */ 51215)).then(m => m.DynamicModule)
      }, {
        path: 'interfaces/Line',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_interfaces_Line_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/interfaces/Line/module */ 80621)).then(m => m.DynamicModule)
      }, {
        path: 'interfaces/disturbanceInfo',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_interfaces_disturbanceInfo_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/interfaces/disturbanceInfo/module */ 78429)).then(m => m.DynamicModule)
      }, {
        path: 'classes/MLogoLines',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_classes_MLogoLines_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/classes/MLogoLines/module */ 96476)).then(m => m.DynamicModule)
      }, {
        path: 'classes/MTable',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_classes_MTable_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/classes/MTable/module */ 23012)).then(m => m.DynamicModule)
      }, {
        path: 'enums/HeaderTypes',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_enums_HeaderTypes_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/enums/HeaderTypes/module */ 64653)).then(m => m.DynamicModule)
      }, {
        path: 'interfaces/HeaderTable',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_interfaces_HeaderTable_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/interfaces/HeaderTable/module */ 68054)).then(m => m.DynamicModule)
      }, {
        path: 'interfaces/HeaderTableSelectOptions',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_interfaces_HeaderTableSelectOptions_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/interfaces/HeaderTableSelectOptions/module */ 84784)).then(m => m.DynamicModule)
      }, {
        path: 'interfaces/HeaderTableOptions',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_interfaces_HeaderTableOptions_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/interfaces/HeaderTableOptions/module */ 95321)).then(m => m.DynamicModule)
      }, {
        path: 'interfaces/ConfigTableCustom',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_interfaces_ConfigTableCustom_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/interfaces/ConfigTableCustom/module */ 31958)).then(m => m.DynamicModule)
      }, {
        path: 'interfaces/ConfigActionTable',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_interfaces_ConfigActionTable_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/interfaces/ConfigActionTable/module */ 79844)).then(m => m.DynamicModule)
      }, {
        path: 'interfaces/ActionTable',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_interfaces_ActionTable_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/interfaces/ActionTable/module */ 79041)).then(m => m.DynamicModule)
      }, {
        path: 'interfaces/ConfigTable',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_interfaces_ConfigTable_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/interfaces/ConfigTable/module */ 79808)).then(m => m.DynamicModule)
      }, {
        path: 'interfaces/ConfigButtonTable',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_interfaces_ConfigButtonTable_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/interfaces/ConfigButtonTable/module */ 19779)).then(m => m.DynamicModule)
      }, {
        path: 'interfaces/FilterText',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_interfaces_FilterText_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/interfaces/FilterText/module */ 41638)).then(m => m.DynamicModule)
      }, {
        path: 'classes/RelativeDatePipe',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_classes_RelativeDatePipe_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/classes/RelativeDatePipe/module */ 50416)).then(m => m.DynamicModule)
      }, {
        path: 'interfaces/AppDownload',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_interfaces_AppDownload_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/interfaces/AppDownload/module */ 38660)).then(m => m.DynamicModule)
      }, {
        path: 'classes/MAppDownload',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_classes_MAppDownload_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/classes/MAppDownload/module */ 35003)).then(m => m.DynamicModule)
      }, {
        path: 'classes/MListWrapper',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_classes_MListWrapper_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/classes/MListWrapper/module */ 11795)).then(m => m.DynamicModule)
      }, {
        path: 'classes/MDisturbanceDisplay',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-node_modules_ng-doc_app_fesm2020_ng-doc-app-components-page_mjs"), __webpack_require__.e("_ng-doc_m-docs_api_api_m-ui_classes_MDisturbanceDisplay_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! .ng-doc/m-docs/api/api/m-ui/classes/MDisturbanceDisplay/module */ 65660)).then(m => m.DynamicModule)
      }]
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](DynamicModule, {
    imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__.RouterModule]
  });
})();

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_api_api_m-ui_module_ts.js.map