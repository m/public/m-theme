"use strict";
(self["webpackChunkm_docs"] = self["webpackChunkm_docs"] || []).push([["_ng-doc_m-docs_guides_m-ui_m-affluence_module_ts"],{

/***/ 21303:
/*!********************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-ui/m-affluence/component-assets.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),
/* harmony export */   "demoAssets": () => (/* binding */ demoAssets)
/* harmony export */ });
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_ui_m_affluence_assets_MAffluenceDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-ui/m-affluence/assets/MAffluenceDemo/TypeScript/Asset0.html */ 54092);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_ui_m_affluence_assets_MAffluenceDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-ui/m-affluence/assets/MAffluenceDemo/HTML/Asset1.html */ 59990);


const demoAssets = {
  MAffluenceDemo: [{
    title: 'TypeScript',
    codeType: 'TypeScript',
    code: _raw_loader_ng_doc_m_docs_guides_m_ui_m_affluence_assets_MAffluenceDemo_TypeScript_Asset0_html__WEBPACK_IMPORTED_MODULE_0__["default"]
  }, {
    title: 'HTML',
    codeType: 'HTML',
    code: _raw_loader_ng_doc_m_docs_guides_m_ui_m_affluence_assets_MAffluenceDemo_HTML_Asset1_html__WEBPACK_IMPORTED_MODULE_1__["default"]
  }]
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (demoAssets);

/***/ }),

/***/ 7371:
/*!**********************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-ui/m-affluence/module.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DynamicComponent": () => (/* binding */ DynamicComponent),
/* harmony export */   "DynamicModule": () => (/* binding */ DynamicModule)
/* harmony export */ });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 60124);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-doc/app */ 64127);
/* harmony import */ var _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-doc/app */ 4031);
/* harmony import */ var _raw_loader_ng_doc_m_docs_guides_m_ui_m_affluence_index_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !!raw-loader!.ng-doc/m-docs/guides/m-ui/m-affluence/index.html */ 59510);
/* harmony import */ var _playgrounds__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./playgrounds */ 9910);
/* harmony import */ var src_app_docs_m_ui_m_affluence_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/docs/m-ui/m-affluence/ng-doc.dependencies */ 3474);
/* harmony import */ var _ng_doc_m_docs_guides_m_ui_m_affluence_component_assets__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! .ng-doc/m-docs/guides/m-ui/m-affluence/component-assets */ 21303);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _src_app_docs_m_ui_m_affluence_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../src/app/docs/m-ui/m-affluence/ng-doc.module */ 51365);




// noinspection ES6UnusedImports


// noinspection ES6UnusedImports





class DynamicComponent extends _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage {
  constructor() {
    super();
    this.routePrefix = '';
    this.pageType = 'guide';
    this.pageContent = _raw_loader_ng_doc_m_docs_guides_m_ui_m_affluence_index_html__WEBPACK_IMPORTED_MODULE_0__["default"];
    this.dependencies = src_app_docs_m_ui_m_affluence_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"];
    this.demoAssets = _ng_doc_m_docs_guides_m_ui_m_affluence_component_assets__WEBPACK_IMPORTED_MODULE_3__["default"];
  }
  static #_ = this.ɵfac = function DynamicComponent_Factory(t) {
    return new (t || DynamicComponent)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineComponent"]({
    type: DynamicComponent,
    selectors: [["ng-component"]],
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵProvidersFeature"]([{
      provide: _ng_doc_app__WEBPACK_IMPORTED_MODULE_5__.NgDocRootPage,
      useExisting: DynamicComponent
    }]), _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵInheritDefinitionFeature"]],
    decls: 1,
    vars: 0,
    template: function DynamicComponent_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵelement"](0, "ng-doc-page");
      }
    },
    dependencies: [_ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageComponent],
    encapsulation: 2,
    changeDetection: 0
  });
}
class DynamicModule {
  static #_ = this.ɵfac = function DynamicModule_Factory(t) {
    return new (t || DynamicModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineNgModule"]({
    type: DynamicModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, src_app_docs_m_ui_m_affluence_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_2__["default"].module, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule.forChild([{
      path: '',
      component: DynamicComponent,
      title: 'M-affluence'
    }]), _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_6__["ɵɵsetNgModuleScope"](DynamicModule, {
    declarations: [DynamicComponent],
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule, _ng_doc_app__WEBPACK_IMPORTED_MODULE_7__.NgDocPageModule, _src_app_docs_m_ui_m_affluence_ng_doc_module__WEBPACK_IMPORTED_MODULE_4__.MAffluencePageModule, _playgrounds__WEBPACK_IMPORTED_MODULE_1__.PlaygroundsModule, _angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule],
    exports: [_angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
  });
})();

/***/ }),

/***/ 9910:
/*!***************************************************************!*\
  !*** ./.ng-doc/m-docs/guides/m-ui/m-affluence/playgrounds.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PlaygroundsModule": () => (/* binding */ PlaygroundsModule)
/* harmony export */ });
/* harmony import */ var src_app_docs_m_ui_m_affluence_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/docs/m-ui/m-affluence/ng-doc.dependencies */ 3474);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 22560);
/* harmony import */ var _src_app_docs_m_ui_m_affluence_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../../src/app/docs/m-ui/m-affluence/ng-doc.module */ 51365);
// noinspection ES6UnusedImports




class PlaygroundsModule {
  static #_ = this.ɵfac = function PlaygroundsModule_Factory(t) {
    return new (t || PlaygroundsModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineNgModule"]({
    type: PlaygroundsModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, src_app_docs_m_ui_m_affluence_ng_doc_dependencies__WEBPACK_IMPORTED_MODULE_0__["default"].module]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_2__["ɵɵsetNgModuleScope"](PlaygroundsModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, _src_app_docs_m_ui_m_affluence_ng_doc_module__WEBPACK_IMPORTED_MODULE_1__.MAffluencePageModule]
  });
})();

/***/ }),

/***/ 79160:
/*!********************************************************************!*\
  !*** ./projects/m-ui/src/lib/m-affluence/m-affluence.component.ts ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MAffluence": () => (/* binding */ MAffluence)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material/icon */ 57822);
/* harmony import */ var _angular_material_list__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/list */ 6517);
/* harmony import */ var _m_icons__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../m-icons */ 73029);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);







function MAffluence_ng_container_0_ng_container_1_mat_list_1_m_icons_3_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "m-icons", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "group");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
}
function MAffluence_ng_container_0_ng_container_1_mat_list_1_m_icons_4_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "m-icons", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "group");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
}
function MAffluence_ng_container_0_ng_container_1_mat_list_1_m_icons_5_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "m-icons", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1, "group");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
  }
}
function MAffluence_ng_container_0_ng_container_1_mat_list_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "mat-list", 5)(1, "mat-list-item", 6)(2, "div", 7);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, MAffluence_ng_container_0_ng_container_1_mat_list_1_m_icons_3_Template, 2, 0, "m-icons", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, MAffluence_ng_container_0_ng_container_1_mat_list_1_m_icons_4_Template, 2, 0, "m-icons", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](5, MAffluence_ng_container_0_ng_container_1_mat_list_1_m_icons_5_Template, 2, 0, "m-icons", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 11)(7, "div", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 13);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]()()()();
  }
  if (rf & 2) {
    const aff_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngSwitch", aff_r2.lvl);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngSwitchCase", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngSwitchCase", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](aff_r2.name);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](aff_r2.description);
  }
}
function MAffluence_ng_container_0_ng_container_1_ng_template_2_ng_container_0_ng_container_2_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "m-icons", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "group");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    const aff_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](aff_r11.name);
  }
}
function MAffluence_ng_container_0_ng_container_1_ng_template_2_ng_container_0_ng_container_3_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "m-icons", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "group");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    const aff_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](aff_r11.name);
  }
}
function MAffluence_ng_container_0_ng_container_1_ng_template_2_ng_container_0_ng_container_4_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "m-icons", 16);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](2, "group");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "span", 20);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    const aff_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](aff_r11.name);
  }
}
function MAffluence_ng_container_0_ng_container_1_ng_template_2_ng_container_0_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "span", 17);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, MAffluence_ng_container_0_ng_container_1_ng_template_2_ng_container_0_ng_container_2_Template, 5, 1, "ng-container", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](3, MAffluence_ng_container_0_ng_container_1_ng_template_2_ng_container_0_ng_container_3_Template, 5, 1, "ng-container", 18);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, MAffluence_ng_container_0_ng_container_1_ng_template_2_ng_container_0_ng_container_4_Template, 5, 1, "ng-container", 19);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    const aff_r11 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngSwitch", aff_r11.lvl);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngSwitchCase", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngSwitchCase", 3);
  }
}
function MAffluence_ng_container_0_ng_container_1_ng_template_2_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, MAffluence_ng_container_0_ng_container_1_ng_template_2_ng_container_0_Template, 5, 3, "ng-container", 2);
  }
  if (rf & 2) {
    const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r5.affluence);
  }
}
function MAffluence_ng_container_0_ng_container_1_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, MAffluence_ng_container_0_ng_container_1_mat_list_1_Template, 11, 5, "mat-list", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, MAffluence_ng_container_0_ng_container_1_ng_template_2_Template, 1, 1, "ng-template", null, 4, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplateRefExtractor"]);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    const aff_r2 = ctx.$implicit;
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵreference"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", !aff_r2.lightMode)("ngIfElse", _r4);
  }
}
function MAffluence_ng_container_0_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0, 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, MAffluence_ng_container_0_ng_container_1_Template, 4, 2, "ng-container", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r0.affluence);
  }
}
/**
 * @component
 * @name MAffluence
 * @description
 * The `MAffluence` component displays the level of occupancy with a description.
 * This component uses Angular Material List and Icon to present the occupancy level.
 *
 * ### Usage example
 * ```html
 * <m-affluence [lvl]="1"></m-affluence>
 * ```
 *
 * @selector m-affluence
 * @standalone true
 * @module CommonModule, MatListModule, MatIconModule, MIcons
 */
class MAffluence {
  constructor() {
    /**
     * @private
     * List of available occupancy levels with their descriptions.
     */
    this.affluenceDescription = [{
      lvl: 1,
      name: 'Faible',
      description: 'C\'est calme, une place assise vous attend.',
      lightMode: false
    }, {
      lvl: 2,
      name: 'Modérée',
      description: 'Il reste sans doute quelques places assises.',
      lightMode: false
    }, {
      lvl: 3,
      name: 'Forte',
      description: 'Il n\'y aura probablement plus de places assises.',
      lightMode: false
    }];
    /**
     * Array used to display occupancy information based on the selected level (`lvl`).
     */
    this.affluence = [];
  }
  /**
   * Initializes the `affluence` array based on the provided occupancy level (`lvl`).
   * If no level is defined, displays all descriptions.
   */
  ngOnInit() {
    if (this.lvl === undefined || this.lvl === null) this.affluence = this.affluenceDescription;else {
      this.affluence.push(this.affluenceDescription.find(a => a.lvl === this.lvl));
      if (this.lightMode) this.affluence[0].lightMode = this.lightMode;
    }
  }
  static #_ = this.ɵfac = function MAffluence_Factory(t) {
    return new (t || MAffluence)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({
    type: MAffluence,
    selectors: [["m-affluence"]],
    inputs: {
      lvl: "lvl",
      lightMode: "lightMode"
    },
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵStandaloneFeature"]],
    decls: 1,
    vars: 1,
    consts: [["class", "m-theme important", 4, "ngIf"], [1, "m-theme", "important"], [4, "ngFor", "ngForOf"], ["class", "m-ui-affluence", 4, "ngIf", "ngIfElse"], ["lightMode", ""], [1, "m-ui-affluence"], [1, "dark-overlay-8", "light-overlay"], [1, "icon-content", "half-light", 3, "ngSwitch"], ["matListItemAvatar", "", "type", "group", "data-type", "moderee", 4, "ngSwitchCase"], ["matListItemAvatar", "", "type", "group", "data-type", "forte", 4, "ngSwitchCase"], ["matListItemAvatar", "", "type", "group", 4, "ngSwitchDefault"], [1, "desc-content"], ["matListItemTitle", ""], ["matListItemLine", ""], ["matListItemAvatar", "", "type", "group", "data-type", "moderee"], ["matListItemAvatar", "", "type", "group", "data-type", "forte"], ["matListItemAvatar", "", "type", "group"], [1, "affluenceLight", 3, "ngSwitch"], [4, "ngSwitchCase"], [4, "ngSwitchDefault"], [1, "text-secondary", "aff-legend"]],
    template: function MAffluence_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, MAffluence_ng_container_0_Template, 2, 1, "ng-container", 0);
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", ctx.affluence && ctx.affluence.length !== 0);
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule, _angular_common__WEBPACK_IMPORTED_MODULE_1__.NgForOf, _angular_common__WEBPACK_IMPORTED_MODULE_1__.NgIf, _angular_common__WEBPACK_IMPORTED_MODULE_1__.NgSwitch, _angular_common__WEBPACK_IMPORTED_MODULE_1__.NgSwitchCase, _angular_common__WEBPACK_IMPORTED_MODULE_1__.NgSwitchDefault, _angular_material_list__WEBPACK_IMPORTED_MODULE_2__.MatListModule, _angular_material_list__WEBPACK_IMPORTED_MODULE_2__.MatList, _angular_material_list__WEBPACK_IMPORTED_MODULE_2__.MatListItem, _angular_material_list__WEBPACK_IMPORTED_MODULE_2__.MatListItemAvatar, _angular_material_list__WEBPACK_IMPORTED_MODULE_2__.MatListItemLine, _angular_material_list__WEBPACK_IMPORTED_MODULE_2__.MatListItemTitle, _angular_material_icon__WEBPACK_IMPORTED_MODULE_3__.MatIconModule, _m_icons__WEBPACK_IMPORTED_MODULE_4__.MIcons],
    styles: [".affluenceLight[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n  text-align: center;\n  padding: 0 8px;\n  margin-right: 16px;\n  width: 100%;\n}\n.affluenceLight[_ngcontent-%COMP%]   .aff-legend[_ngcontent-%COMP%] {\n  font-size: 12px;\n  font-weight: 700;\n  line-height: 2rem;\n}\n\nmat-list.m-ui-affluence[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: row;\n  flex-wrap: wrap;\n  width: 100%;\n}\nmat-list.m-ui-affluence[_ngcontent-%COMP%]   mat-list-item[_ngcontent-%COMP%] {\n  padding: 0;\n  height: auto;\n}\nmat-list.m-ui-affluence[_ngcontent-%COMP%]   mat-list-item[_ngcontent-%COMP%]   .mdc-list-item__content[_ngcontent-%COMP%]    > span[_ngcontent-%COMP%] {\n  display: flex !important;\n}\nmat-list.m-ui-affluence[_ngcontent-%COMP%]   mat-list-item[_ngcontent-%COMP%]   .mdc-list-item__content[_ngcontent-%COMP%]   .icon-content[_ngcontent-%COMP%] {\n  padding: 16px !important;\n  border-radius: 8px 0 0 8px !important;\n}\nmat-list.m-ui-affluence[_ngcontent-%COMP%]   mat-list-item[_ngcontent-%COMP%]   .mdc-list-item__content[_ngcontent-%COMP%]   .desc-content[_ngcontent-%COMP%] {\n  padding: 0 16px;\n}\n@media screen and (min-width: 1079px) {\n  mat-list[_ngcontent-%COMP%]   h1.m-toolbar-title[_ngcontent-%COMP%] {\n    padding-left: 56px;\n  }\n}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8uL3Byb2plY3RzL20tdWkvc3JjL3RoZW1lL2dsb2JhbC9fdmFyaWFibGVzLnNjc3MiLCJ3ZWJwYWNrOi8vLi9wcm9qZWN0cy9tLXVpL3NyYy90aGVtZS9nbG9iYWwvX2hlbHBlcnMuc2NzcyIsIndlYnBhY2s6Ly8uL3Byb2plY3RzL20tdWkvc3JjL2xpYi9tLWFmZmx1ZW5jZS9tLWFmZmx1ZW5jZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFnQkEsa0JBQUE7QUFJQSxlQUFBO0FBR0Esb0JBQUE7QUFPQSxnQkFBQTtBQUdBLG9CQUFBO0FBR0EsaUJBQUE7QUFJQSxnQkFBQTtBQUdBLG9CQUFBO0FDcENBOzs7Ozs7R0FBQTtBQVdBOzs7Ozs7R0FBQTtBQTZCQTs7RUFBQTtBQVlBOztFQUFBO0FBc0NBOztHQUFBO0FDNUZBO0VBQ0MsYUFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FBMkJEO0FBekJDO0VBQ0MsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUEyQkY7O0FBdEJDO0VBQ0MsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUF5QkY7QUF2QkU7RUFDQyxVQUFBO0VBQ0EsWUFBQTtBQXlCSDtBQXRCSTtFQUNDLHdCQUFBO0FBd0JMO0FBckJJO0VBQ0Msd0JBQUE7RUFDQSxxQ0FBQTtBQXVCTDtBQXBCSTtFQUNDLGVBQUE7QUFzQkw7QUFoQkM7RUFDQztJQUNDLGtCQUFBO0VBa0JEO0FBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyIkYXNzZXRzLXBhdGg6IFwiXi4vYXNzZXRzXCIgIWRlZmF1bHQ7XHJcbiRzcGFjaW5nOiA4cHggIWRlZmF1bHQ7XHJcblxyXG4kZWxldmF0aW9uLWNvbG9yczogKFxyXG4gIDA6IDAlLFxyXG4gIDE6IDUlLFxyXG4gIDI6IDclLFxyXG4gIDM6IDglLFxyXG4gIDQ6IDklLFxyXG4gIDY6IDExJSxcclxuICA4OiAxMiUsXHJcbiAgMTI6IDE0JSxcclxuICAxNjogMTUlLFxyXG4gIDI0OiAxNiUsXHJcbikgIWRlZmF1bHQ7XHJcblxyXG4vKioqKiB0b29sYmFyICoqKiovXHJcbiR0b29sYmFyLWhlaWdodDogNTZweCAhZGVmYXVsdDtcclxuJHRvb2xiYXItbWFpbi1uYXYtaGVpZ2h0OiAkdG9vbGJhci1oZWlnaHQgKyAkc3BhY2luZyAhZGVmYXVsdDtcclxuXHJcbi8qKioqIGZvbnQgKioqKi9cclxuJGZvbnQtZmFtaWx5OiBSb2JvdG8sIFwiSGVsdmV0aWNhIE5ldWVcIiwgc2Fucy1zZXJpZjtcclxuXHJcbi8qKioqIGZvbnQgc2l6ZSAqKioqL1xyXG4kZGVmYXVsdC1mejogMS40cmVtICFkZWZhdWx0O1xyXG4kZGVmYXVsdC1mei1zbWFsbDogMS4ycmVtICFkZWZhdWx0O1xyXG4kZGVmYXVsdC1mei1iaWc6IDEuOHJlbSAhZGVmYXVsdDtcclxuJGhlYWRlci1mejogJGRlZmF1bHQtZnogKyAwLjJyZW0gIWRlZmF1bHQ7XHJcbiRoZWFkZXItZnotYmlnOiAkaGVhZGVyLWZ6ICsgMC40cmVtICFkZWZhdWx0O1xyXG5cclxuLyoqKiogSWNvbnMgKioqKi9cclxuJGljb24tbGFyZ2U6IDQwcHggIWRlZmF1bHQ7XHJcblxyXG4vKioqKiBDb250YWluZXIgKioqKi9cclxuJGVuZC1iZy1zaXplOiAxNDNweCArICRzcGFjaW5nICogMiAhZGVmYXVsdDtcclxuXHJcbi8qKioqIHNoYWRvdyAqKioqL1xyXG4kbGlnaHQtYm94LXNoYWRvdzogMCA0cHggOHB4IHJnYmEoYmxhY2ssIDAuNSkgIWRlZmF1bHQ7XHJcbiRkYXJrLWJveC1zaGFkb3c6IDAgNHB4IDhweCByZ2JhKGJsYWNrLCAwLjcpICFkZWZhdWx0O1xyXG5cclxuLyoqKiogU2hhcGUgKioqKi9cclxuJHNoYXBlLXJhZGl1czogOHB4ICFkZWZhdWx0O1xyXG5cclxuLyoqKiBCcmVha3BvaW50ICoqKiovXHJcbiRicmVha3BvaW50OiAxMDc5cHg7XHJcbiIsIkB1c2UgXCJzYXNzOm1hcFwiO1xyXG5AdXNlIFwic2FzczptZXRhXCI7XHJcbkB1c2UgXCJzYXNzOmxpc3RcIjtcclxuQHVzZSBcInNhc3M6bWF0aFwiO1xyXG5AdXNlIFwic2FzczpzZWxlY3RvclwiO1xyXG5AdXNlIFwidmFyaWFibGVzXCIgYXMgdmFyO1xyXG5cclxuLyoqXHJcbiAqIFNsaWdodGx5IGxpZ2h0ZW4gYSBjb2xvclxyXG4gKiBAYWNjZXNzIHB1YmxpY1xyXG4gKiBAcGFyYW0ge0NvbG9yfSAkY29sb3IgLSBjb2xvciB0byB0aW50XHJcbiAqIEBwYXJhbSB7TnVtYmVyfSAkcGVyY2VudGFnZSAtIHBlcmNlbnRhZ2Ugb2YgYCRjb2xvcmAgaW4gcmV0dXJuZWQgY29sb3JcclxuICogQHJldHVybiB7Q29sb3J9XHJcbiAqKi9cclxuQGZ1bmN0aW9uIHRpbnQoJGNvbG9yLCAkcGVyY2VudGFnZSkge1xyXG5cdEByZXR1cm4gbWl4KHdoaXRlLCAkY29sb3IsICRwZXJjZW50YWdlKTtcclxufVxyXG5cclxuLyoqXHJcbiAqIFNsaWdodGx5IGRhcmtlbiBhIGNvbG9yXHJcbiAqIEBhY2Nlc3MgcHVibGljXHJcbiAqIEBwYXJhbSB7Q29sb3J9ICRjb2xvciAtIGNvbG9yIHRvIHNoYWRlXHJcbiAqIEBwYXJhbSB7TnVtYmVyfSAkcGVyY2VudGFnZSAtIHBlcmNlbnRhZ2Ugb2YgYCRjb2xvcmAgaW4gcmV0dXJuZWQgY29sb3JcclxuICogQHJldHVybiB7Q29sb3J9XHJcbiAqKi9cclxuQGZ1bmN0aW9uIHNoYWRlKCRjb2xvciwgJHBlcmNlbnRhZ2UpIHtcclxuXHRAcmV0dXJuIG1peChibGFjaywgJGNvbG9yLCAkcGVyY2VudGFnZSk7XHJcbn1cclxuXHJcbkBtaXhpbiBmdWxsLWhlaWdodCgpIHtcclxuXHRoZWlnaHQ6IGNhbGMoMTAwdmggLSAje3Zhci4kdG9vbGJhci1tYWluLW5hdi1oZWlnaHR9KTtcclxufVxyXG5cclxuQG1peGluIGljb24tbWFyZ2luKCRkaXJlY3Rpb24pIHtcclxuXHRtYXJnaW4tI3skZGlyZWN0aW9ufTogdmFyLiRzcGFjaW5nO1xyXG5cclxuXHQmLWxhcmdlIHtcclxuXHRcdG1hcmdpbi0jeyRkaXJlY3Rpb259OiB2YXIuJHNwYWNpbmcgKiAyO1xyXG5cdH1cclxuXHQmLXZlcnktbGFyZ2Uge1xyXG5cdFx0bWFyZ2luLSN7JGRpcmVjdGlvbn06IHZhci4kc3BhY2luZyAqIDQ7XHJcblx0fVxyXG5cdCYtc21hbGwge1xyXG5cdFx0bWFyZ2luLSN7JGRpcmVjdGlvbn06IG1hdGguZGl2KHZhci4kc3BhY2luZywgMik7XHJcblx0fVxyXG59XHJcblxyXG4vKipcclxuICogTXVzdCBiZSB1c2VkIG9ubHkgaW5zaWRlIGEgb3ZlcnJpZGUtWCBtaXhpbi5cclxuICovXHJcbkBtaXhpbiBkcm9wLXNoYWRvdygpIHtcclxuXHRAYXQtcm9vdCAje3NlbGVjdG9yLnJlcGxhY2UoJiwgXCJib2R5XCIsIFwiYm9keS5kYXJrLXRoZW1lXCIpfSB7XHJcblx0XHRib3gtc2hhZG93OiB2YXIuJGRhcmstYm94LXNoYWRvdztcclxuXHR9XHJcblx0QGF0LXJvb3QgI3tzZWxlY3Rvci5yZXBsYWNlKCYsIFwiYm9keVwiLCBcImJvZHkubGlnaHQtdGhlbWVcIil9IHtcclxuXHRcdGJveC1zaGFkb3c6IHZhci4kbGlnaHQtYm94LXNoYWRvdztcclxuXHR9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBNdXN0IGJlIHVzZWQgb25seSBpbnNpZGUgYSBvdmVycmlkZS1YIG1peGluLlxyXG4gKi9cclxuQG1peGluIGJvdHRvbS1waWN0dXJlKCkge1xyXG5cdCY6OmFmdGVyIHtcclxuXHRcdGNvbnRlbnQ6IFwiXCI7XHJcblx0XHRkaXNwbGF5OiBibG9jaztcclxuXHRcdGhlaWdodDogdmFyLiRlbmQtYmctc2l6ZTtcclxuXHRcdGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcblx0XHRiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG5cdFx0QGF0LXJvb3QgI3tzZWxlY3Rvci5yZXBsYWNlKCYsIFwiYm9keVwiLCBcImJvZHkuZGFyay10aGVtZVwiKX0ge1xyXG5cdFx0XHRiYWNrZ3JvdW5kLWltYWdlOiB1cmwodmFyLiRhc3NldHMtcGF0aCArIFwiL2ltYWdlcy9ib3R0b21fcGljdHVyZV9tZXRyb21vYl9kYXJrL2JvdHRvbV9waWN0dXJlX21ldHJvbW9iX2RhcmsucG5nXCIpO1xyXG5cclxuXHRcdFx0QG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXJlc29sdXRpb246IDIwMGRwaSksIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiAxMDI0cHgpIHtcclxuXHRcdFx0XHRiYWNrZ3JvdW5kLWltYWdlOiB1cmwodmFyLiRhc3NldHMtcGF0aCArIFwiL2ltYWdlcy9ib3R0b21fcGljdHVyZV9tZXRyb21vYl9kYXJrL2JvdHRvbV9waWN0dXJlX21ldHJvbW9iX2RhcmtAMngucG5nXCIpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRAYXQtcm9vdCAje3NlbGVjdG9yLnJlcGxhY2UoJiwgXCJib2R5XCIsIFwiYm9keS5saWdodC10aGVtZVwiKX0ge1xyXG5cdFx0XHRiYWNrZ3JvdW5kLWltYWdlOiB1cmwodmFyLiRhc3NldHMtcGF0aCArIFwiL2ltYWdlcy9ib3R0b21fcGljdHVyZV9tZXRyb21vYl9saWdodC9ib3R0b21fcGljdHVyZV9tZXRyb21vYl9saWdodC5wbmdcIik7XHJcblxyXG5cdFx0XHRAbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4tcmVzb2x1dGlvbjogMjAwZHBpKSwgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDEwMjRweCkge1xyXG5cdFx0XHRcdGJhY2tncm91bmQtaW1hZ2U6IHVybCh2YXIuJGFzc2V0cy1wYXRoICsgXCIvaW1hZ2VzL2JvdHRvbV9waWN0dXJlX21ldHJvbW9iX2xpZ2h0L2JvdHRvbV9waWN0dXJlX21ldHJvbW9iX2xpZ2h0QDJ4LnBuZ1wiKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH1cclxufVxyXG5cclxuQGZ1bmN0aW9uIHN3aXRjaCgkdGVzdCwgJGFyZ3MuLi4pIHtcclxuXHQkY2FzZXM6IG1ldGEua2V5d29yZHMoJGFyZ3MpO1xyXG5cdEBpZiAobWFwLmhhcy1rZXkoJGNhc2VzLCAkdGVzdCkpIHtcclxuXHRcdEByZXR1cm4gbWFwLmdldCgkY2FzZXMsICR0ZXN0KTtcclxuXHR9IEBlbHNlIGlmIG1hcC5oYXMta2V5KCRjYXNlcywgXCJkZWZhdWx0XCIpIHtcclxuXHRcdEByZXR1cm4gbWFwLmdldCgkY2FzZXMsIFwiZGVmYXVsdFwiKTtcclxuXHR9IEBlbHNlIHtcclxuXHRcdEByZXR1cm4gbnVsbDtcclxuXHR9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBTYXNzIGRlYnVnIGZ1bmN0aW9uIGZyb20gaHR0cHM6Ly9jb2RlcGVuLmlvL0tpdHR5R2lyYXVkZWwvcGVuL3VueUJIXHJcbiAqKi9cclxuQGZ1bmN0aW9uIGRlYnVnLW1hcCgkbGlzdCwgJHByZTogdHJ1ZSwgJGxldmVsOiAxKSB7XHJcblx0JHRhYjogXCIgICAgXCI7XHJcblx0JGluZGVudDogXCJcIjtcclxuXHQkYnJlYWs6IGlmKCRwcmUsIFwiXFxBIFwiLCBcIlwiKTtcclxuXHJcblx0QGlmIGxlbmd0aCgkbGlzdCkgPT0gMCB7XHJcblx0XHRAcmV0dXJuIFwiKCApXCI7XHJcblx0fVxyXG5cclxuXHRAaWYgbGVuZ3RoKCRsaXN0KSA9PSAxIHtcclxuXHRcdEByZXR1cm4gaWYoJHByZSwgXCIoXCIgKyB0eXBlLW9mKCRsaXN0KSArIFwiKSBcIiwgXCJcIikgKyAkbGlzdDtcclxuXHR9XHJcblxyXG5cdEBmb3IgJGkgZnJvbSAxIHRvICRsZXZlbCB7XHJcblx0XHQkaW5kZW50OiAkaW5kZW50ICsgJHRhYjtcclxuXHR9XHJcblxyXG5cdCRyZXN1bHQ6IFwiW1wiICsgJGJyZWFrO1xyXG5cclxuXHRAZm9yICRpIGZyb20gMSB0aHJvdWdoIGxlbmd0aCgkbGlzdCkge1xyXG5cdFx0JGl0ZW06IG50aCgkbGlzdCwgJGkpO1xyXG5cdFx0JHJlc3VsdDogJHJlc3VsdCArIGlmKCRwcmUsICRpbmRlbnQgKyAkdGFiLCBcIiBcIik7XHJcblxyXG5cdFx0QGlmIGxlbmd0aCgkaXRlbSkgPiAxIHtcclxuXHRcdFx0JHJlc3VsdDogJHJlc3VsdCArIGlmKCRwcmUsIFwiKGxpc3Q6IFwiICsgbGVuZ3RoKCRpdGVtKSArIFwiKSBcIiwgXCJcIikgKyBkZWJ1Zy1tYXAoJGl0ZW0sICRwcmUsICRsZXZlbCArIDEpO1xyXG5cdFx0fSBAZWxzZSB7XHJcblx0XHRcdEBpZiAkcHJlIHtcclxuXHRcdFx0XHQkcmVzdWx0OiAkcmVzdWx0ICsgXCIoXCIgKyB0eXBlLW9mKCRpdGVtKSArIFwiKSBcIjtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0QGlmIGxlbmd0aCgkaXRlbSkgPT0gMCB7XHJcblx0XHRcdFx0JHJlc3VsdDogJHJlc3VsdCArIFwiKCApXCI7XHJcblx0XHRcdH0gQGVsc2UgaWYgdHlwZS1vZigkaXRlbSkgPT0gc3RyaW5nIHtcclxuXHRcdFx0XHQkcmVzdWx0OiAkcmVzdWx0ICsgcXVvdGUoJGl0ZW0pO1xyXG5cdFx0XHR9IEBlbHNlIGlmICRpdGVtID09IG51bGwge1xyXG5cdFx0XHRcdCRyZXN1bHQ6ICRyZXN1bHQgKyBcIm51bGxcIjtcclxuXHRcdFx0fSBAZWxzZSB7XHJcblx0XHRcdFx0JHJlc3VsdDogJHJlc3VsdCArICRpdGVtO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdFx0QGlmICRpICE9IGxlbmd0aCgkbGlzdCkge1xyXG5cdFx0XHQkcmVzdWx0OiAkcmVzdWx0ICsgXCIsXCIgKyAkYnJlYWs7XHJcblx0XHR9XHJcblx0fVxyXG5cclxuXHQkcmVzdWx0OiAkcmVzdWx0ICsgJGJyZWFrICsgaWYoJHByZSwgaWYoJGxldmVsID4gMSwgJGluZGVudCwgXCJcIiksIFwiIFwiKSArIFwiXVwiO1xyXG5cclxuXHRAcmV0dXJuIHF1b3RlKCRyZXN1bHQpO1xyXG59XHJcbiIsIkB1c2UgXCJzYXNzOm1hcFwiO1xyXG5AdXNlIFwic2FzczptYXRoXCI7XHJcbkB1c2UgXCJAYW5ndWxhci9tYXRlcmlhbFwiIGFzIG1hdDtcclxuQHVzZSBcIi4uLy4uL3RoZW1lL2dsb2JhbFwiIGFzIGdsb2JhbDtcclxuXHJcbi5hZmZsdWVuY2VMaWdodHtcclxuXHRkaXNwbGF5OiBmbGV4O1xyXG5cdGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdHBhZGRpbmc6IDAgOHB4O1xyXG5cdG1hcmdpbi1yaWdodDogMTZweDtcclxuXHR3aWR0aDogMTAwJTtcclxuXHJcblx0LmFmZi1sZWdlbmR7XHJcblx0XHRmb250LXNpemU6IDEycHg7XHJcblx0XHRmb250LXdlaWdodDogNzAwO1xyXG5cdFx0bGluZS1oZWlnaHQ6IDJyZW07XHJcblx0fVxyXG59XHJcblxyXG5tYXQtbGlzdCB7XHJcblx0Ji5tLXVpLWFmZmx1ZW5jZXtcclxuXHRcdGRpc3BsYXk6IGZsZXg7XHJcblx0XHRmbGV4LWRpcmVjdGlvbjogcm93O1xyXG5cdFx0ZmxleC13cmFwOiB3cmFwO1xyXG5cdFx0d2lkdGg6IDEwMCU7XHJcblxyXG5cdFx0bWF0LWxpc3QtaXRlbSB7XHJcblx0XHRcdHBhZGRpbmc6IDA7XHJcblx0XHRcdGhlaWdodDogYXV0bztcclxuXHJcblx0XHRcdC5tZGMtbGlzdC1pdGVtX19jb250ZW50IHtcclxuXHRcdFx0XHQmID4gc3BhbiB7XHJcblx0XHRcdFx0XHRkaXNwbGF5OiBmbGV4ICFpbXBvcnRhbnQ7XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHQuaWNvbi1jb250ZW50IHtcclxuXHRcdFx0XHRcdHBhZGRpbmc6IGdsb2JhbC4kc3BhY2luZyAqIDIgIWltcG9ydGFudDtcclxuXHRcdFx0XHRcdGJvcmRlci1yYWRpdXM6IGdsb2JhbC4kc3BhY2luZyAwIDAgZ2xvYmFsLiRzcGFjaW5nICAhaW1wb3J0YW50O1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0LmRlc2MtY29udGVudHtcclxuXHRcdFx0XHRcdHBhZGRpbmc6IDAgZ2xvYmFsLiRzcGFjaW5nICogMjtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdEBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IGdsb2JhbC4kYnJlYWtwb2ludCkge1xyXG5cdFx0aDEubS10b29sYmFyLXRpdGxlIHtcclxuXHRcdFx0cGFkZGluZy1sZWZ0OiBnbG9iYWwuJHNwYWNpbmcgKiA3O1xyXG5cdFx0fVxyXG5cdH1cclxuXHJcbn1cclxuXHJcblxyXG5AbWl4aW4gb3ZlcnJpZGUtY29sb3IoJHRoZW1lKSB7XHJcblx0JC10aGVtZTogbWFwLmdldCgkdGhlbWUsIHRoZW1lKTtcclxuXHQkY29sb3ItY29uZmlnOiBtYXQuZ2V0LWNvbG9yLWNvbmZpZygkdGhlbWUpO1xyXG5cdCRwcmltYXJ5LXBhbGV0dGU6IG1hcC5nZXQoJGNvbG9yLWNvbmZpZywgXCJwcmltYXJ5XCIpO1xyXG5cdCRmb3JlZ3JvdW5kOiBtYXAuZ2V0KCR0aGVtZSwgZm9yZWdyb3VuZCk7XHJcblxyXG5cdC5pY29uLWNvbnRlbnQsIC5hZmZsdWVuY2VMaWdodHtcclxuXHRcdCYuaGFsZi1saWdodHtcclxuXHRcdFx0YmFja2dyb3VuZC1jb2xvcjogcmdiYShtYXAuZ2V0KCRmb3JlZ3JvdW5kLCBiYXNlKSwgMC4xMikhaW1wb3J0YW50O1xyXG5cdFx0fVxyXG5cclxuXHRcdG1hdC1pY29uW3N2Z2ljb249XCJncm91cFwiXSB7XHJcblx0XHRcdCNpY19ncm91cF8yNHB4LFxyXG5cdFx0XHQjaWNfZ3JvdXBfMjRweC0yLFxyXG5cdFx0XHQjaWNfZ3JvdXBfMjRweC0zIHtcclxuXHRcdFx0XHRmaWxsOiBtYXAuZ2V0KCRwcmltYXJ5LXBhbGV0dGUsICdkZWZhdWx0JykgIWltcG9ydGFudDtcclxuXHRcdFx0XHRvcGFjaXR5OiAzMCU7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHJcblx0XHRtLWljb25ze1xyXG5cdFx0XHQmW2RhdGEtdHlwZT1cImZvcnRlXCJdICNpY19ncm91cF8yNHB4LFxyXG5cdFx0XHQmW2RhdGEtdHlwZT1cImZvcnRlXCJdICNpY19ncm91cF8yNHB4LTIsXHJcblx0XHRcdCZbZGF0YS10eXBlPVwibW9kZXJlZVwiXSAjaWNfZ3JvdXBfMjRweC0yLFxyXG5cdFx0XHQjaWNfZ3JvdXBfMjRweC0zIHtcclxuXHRcdFx0XHRvcGFjaXR5OiAxMDAlIWltcG9ydGFudDtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cclxuXHR9XHJcbn1cclxuIl0sInNvdXJjZVJvb3QiOiIifQ== */"]
  });
}

/***/ }),

/***/ 12764:
/*!**************************************************************************************!*\
  !*** ./src/app/docs/m-ui/m-affluence/m-affluence-demo/m-affluence-demo.component.ts ***!
  \**************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MAffluenceDemo": () => (/* binding */ MAffluenceDemo)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_material_expansion__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/expansion */ 17591);
/* harmony import */ var _metromobilite_m_ui_m_affluence__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @metromobilite/m-ui/m-affluence */ 79160);
/* harmony import */ var src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/features/preview/preview.component */ 55355);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 22560);






function MAffluenceDemo_ng_container_0_Template(rf, ctx) {
  if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementStart"](1, "app-preview", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelement"](2, "m-affluence", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵelementContainerEnd"]();
  }
  if (rf & 2) {
    const service_r1 = ctx.$implicit;
    const last_r3 = ctx.last;
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("title", service_r1.name)("nextElement", !last_r3);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("lvl", service_r1.lvl)("lightMode", service_r1.lightMode);
  }
}
class MAffluenceDemo {
  constructor() {
    this.services = [{
      lvl: null,
      name: 'Liste compléte des affluences'
    }, {
      lvl: 1,
      name: 'Affluence faible'
    }, {
      lvl: 2,
      name: 'Affluence modérée'
    }, {
      lvl: 3,
      name: 'Affluence forte'
    }, {
      lvl: 1,
      name: 'Affluence faible [lightMode]',
      lightMode: true
    }, {
      lvl: 2,
      name: 'Affluence modérée [lightMode]',
      lightMode: true
    }, {
      lvl: 3,
      name: 'Affluence forte [lightMode]',
      lightMode: true
    }];
  }
  static #_ = this.ɵfac = function MAffluenceDemo_Factory(t) {
    return new (t || MAffluenceDemo)();
  };
  static #_2 = this.ɵcmp = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵdefineComponent"]({
    type: MAffluenceDemo,
    selectors: [["app-m-affluence-demo"]],
    standalone: true,
    features: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵStandaloneFeature"]],
    decls: 1,
    vars: 1,
    consts: [[4, "ngFor", "ngForOf"], [3, "title", "nextElement"], [3, "lvl", "lightMode"]],
    template: function MAffluenceDemo_Template(rf, ctx) {
      if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵtemplate"](0, MAffluenceDemo_ng_container_0_Template, 3, 4, "ng-container", 0);
      }
      if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_1__["ɵɵproperty"]("ngForOf", ctx.services);
      }
    },
    dependencies: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, _angular_common__WEBPACK_IMPORTED_MODULE_2__.NgForOf, _metromobilite_m_ui_m_affluence__WEBPACK_IMPORTED_MODULE_3__.MAffluence, _angular_material_expansion__WEBPACK_IMPORTED_MODULE_4__.MatExpansionModule, src_app_features_preview_preview_component__WEBPACK_IMPORTED_MODULE_0__.Preview],
    styles: ["\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsInNvdXJjZVJvb3QiOiIifQ== */"]
  });
}

/***/ }),

/***/ 3474:
/*!**************************************************************!*\
  !*** ./src/app/docs/m-ui/m-affluence/ng-doc.dependencies.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _m_ui_m_affluence_m_affluence_demo_m_affluence_demo_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @m-ui/m-affluence/m-affluence-demo/m-affluence-demo.component */ 12764);
/* harmony import */ var _ng_doc_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ng-doc.module */ 51365);


const MAffluencePageDependencies = {
  module: _ng_doc_module__WEBPACK_IMPORTED_MODULE_1__.MAffluencePageModule,
  demo: {
    MAffluenceDemo: _m_ui_m_affluence_m_affluence_demo_m_affluence_demo_component__WEBPACK_IMPORTED_MODULE_0__.MAffluenceDemo
  }
};
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MAffluencePageDependencies);

/***/ }),

/***/ 51365:
/*!********************************************************!*\
  !*** ./src/app/docs/m-ui/m-affluence/ng-doc.module.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MAffluencePageModule": () => (/* binding */ MAffluencePageModule)
/* harmony export */ });
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ 94666);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 22560);


class MAffluencePageModule {
  static #_ = this.ɵfac = function MAffluencePageModule_Factory(t) {
    return new (t || MAffluencePageModule)();
  };
  static #_2 = this.ɵmod = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({
    type: MAffluencePageModule
  });
  static #_3 = this.ɵinj = /*@__PURE__*/_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
}
(function () {
  (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](MAffluencePageModule, {
    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_1__.CommonModule]
  });
})();

/***/ }),

/***/ 59990:
/*!*****************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-ui/m-affluence/assets/MAffluenceDemo/HTML/Asset1.html ***!
  \*****************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"html\" class=\"hljs language-html ngde\"><span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">ng-container</span>\n  *<span class=\"hljs-attr ngde\">ngFor</span>=<span class=\"hljs-string ngde\">\"let service of services; let index = index; let last = last\"</span>\n></span>\n  <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">app-preview</span> [<span class=\"hljs-attr ngde\">title</span>]=<span class=\"hljs-string ngde\">\"service.name\"</span> [<span class=\"hljs-attr ngde\">nextElement</span>]=<span class=\"hljs-string ngde\">\"!last\"</span>></span>\n    <span class=\"hljs-tag ngde\">&#x3C;<span class=\"hljs-name ngde\">m-affluence</span>\n      [<span class=\"hljs-attr ngde\">lvl</span>]=<span class=\"hljs-string ngde\">\"service.lvl\"</span>\n      [<span class=\"hljs-attr ngde\">lightMode</span>]=<span class=\"hljs-string ngde\">\"service.lightMode\"</span>\n    ></span><span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">m-affluence</span>></span>\n  <span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">app-preview</span>></span>\n<span class=\"hljs-tag ngde\">&#x3C;/<span class=\"hljs-name ngde\">ng-container</span>></span>\n</code></pre>");

/***/ }),

/***/ 54092:
/*!***********************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-ui/m-affluence/assets/MAffluenceDemo/TypeScript/Asset0.html ***!
  \***********************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<pre class=\"ngde hljs\"><code lang=\"typescript\" class=\"hljs language-typescript ngde\"><span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Component</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/core\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">CommonModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/common\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">MatExpansionModule</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@angular/material/expansion\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\"><a href=\"/api/m-ui/classes/MAffluence\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MAffluence</a></span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"@metromobilite/m-ui/m-affluence\"</span>;\n<span class=\"hljs-keyword ngde\">import</span> { <span class=\"hljs-title class_ ngde\">Preview</span> } <span class=\"hljs-keyword ngde\">from</span> <span class=\"hljs-string ngde\">\"src/app/features/preview/preview.component\"</span>;\n\n<span class=\"hljs-meta ngde\">@Component</span>({\n  <span class=\"hljs-attr ngde\">selector</span>: <span class=\"hljs-string ngde\">\"app-m-affluence-demo\"</span>,\n  <span class=\"hljs-attr ngde\">standalone</span>: <span class=\"hljs-literal ngde\">true</span>,\n  <span class=\"hljs-attr ngde\">imports</span>: [<span class=\"hljs-title class_ ngde\">CommonModule</span>, <span class=\"hljs-title class_ ngde\"><a href=\"/api/m-ui/classes/MAffluence\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MAffluence</a></span>, <span class=\"hljs-title class_ ngde\">MatExpansionModule</span>, <span class=\"hljs-title class_ ngde\">Preview</span>],\n  <span class=\"hljs-attr ngde\">templateUrl</span>: <span class=\"hljs-string ngde\">\"./m-affluence-demo.component.html\"</span>,\n  <span class=\"hljs-attr ngde\">styleUrls</span>: [<span class=\"hljs-string ngde\">\"./m-affluence-demo.component.scss\"</span>],\n})\n<span class=\"hljs-keyword ngde\">export</span> <span class=\"hljs-keyword ngde\">class</span> <span class=\"hljs-title class_ ngde\">MAffluenceDemo</span> {\n  <span class=\"hljs-keyword ngde\">public</span> services = [\n    { <span class=\"hljs-attr ngde\">lvl</span>: <span class=\"hljs-literal ngde\">null</span>, <span class=\"hljs-attr ngde\">name</span>: <span class=\"hljs-string ngde\">\"Liste compléte des affluences\"</span> },\n    { <span class=\"hljs-attr ngde\">lvl</span>: <span class=\"hljs-number ngde\">1</span>, <span class=\"hljs-attr ngde\">name</span>: <span class=\"hljs-string ngde\">\"<a href=\"/api/m-ui/interfaces/Affluence\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Interface\" class=\"ngde\">Affluence</a> faible\"</span> },\n    { <span class=\"hljs-attr ngde\">lvl</span>: <span class=\"hljs-number ngde\">2</span>, <span class=\"hljs-attr ngde\">name</span>: <span class=\"hljs-string ngde\">\"<a href=\"/api/m-ui/interfaces/Affluence\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Interface\" class=\"ngde\">Affluence</a> modérée\"</span> },\n    { <span class=\"hljs-attr ngde\">lvl</span>: <span class=\"hljs-number ngde\">3</span>, <span class=\"hljs-attr ngde\">name</span>: <span class=\"hljs-string ngde\">\"<a href=\"/api/m-ui/interfaces/Affluence\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Interface\" class=\"ngde\">Affluence</a> forte\"</span> },\n    { <span class=\"hljs-attr ngde\">lvl</span>: <span class=\"hljs-number ngde\">1</span>, <span class=\"hljs-attr ngde\">name</span>: <span class=\"hljs-string ngde\">\"<a href=\"/api/m-ui/interfaces/Affluence\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Interface\" class=\"ngde\">Affluence</a> faible [lightMode]\"</span>, <span class=\"hljs-attr ngde\">lightMode</span>: <span class=\"hljs-literal ngde\">true</span> },\n    { <span class=\"hljs-attr ngde\">lvl</span>: <span class=\"hljs-number ngde\">2</span>, <span class=\"hljs-attr ngde\">name</span>: <span class=\"hljs-string ngde\">\"<a href=\"/api/m-ui/interfaces/Affluence\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Interface\" class=\"ngde\">Affluence</a> modérée [lightMode]\"</span>, <span class=\"hljs-attr ngde\">lightMode</span>: <span class=\"hljs-literal ngde\">true</span> },\n    { <span class=\"hljs-attr ngde\">lvl</span>: <span class=\"hljs-number ngde\">3</span>, <span class=\"hljs-attr ngde\">name</span>: <span class=\"hljs-string ngde\">\"<a href=\"/api/m-ui/interfaces/Affluence\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Interface\" class=\"ngde\">Affluence</a> forte [lightMode]\"</span>, <span class=\"hljs-attr ngde\">lightMode</span>: <span class=\"hljs-literal ngde\">true</span> },\n  ];\n}\n</code></pre>");

/***/ }),

/***/ 59510:
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./.ng-doc/m-docs/guides/m-ui/m-affluence/index.html ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<h1 id=\"m-affluence\" class=\"ngde\">M-affluence<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/m-ui/m-affluence#m-affluence\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h1><h2 id=\"api\" class=\"ngde\">API<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/m-ui/m-affluence#api\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><p class=\"ngde\">Shows how to use the <code class=\"ngde ng-doc-code-with-link\" class=\"ngde\"><a href=\"/api/m-ui/classes/MAffluence\" class=\"ng-doc-code-anchor ngde\" data-link-type=\"Component\" class=\"ngde\">MAffluence</a></code> component.</p><h2 id=\"demo\" class=\"ngde\">Demo<a title=\"Link to heading\" class=\"ng-doc-header-link ngde\" href=\"/m-ui/m-affluence#demo\"><ng-doc-icon icon=\"link-2\" size=\"16\" class=\"ngde\"></ng-doc-icon></a></h2><ng-doc-demo componentname=\"MAffluenceDemo\" indexable=\"false\" class=\"ngde\"><div id=\"options\" class=\"ngde\">{}</div></ng-doc-demo>");

/***/ })

}]);
//# sourceMappingURL=_ng-doc_m-docs_guides_m-ui_m-affluence_module_ts.js.map