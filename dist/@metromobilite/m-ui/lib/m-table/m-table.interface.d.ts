import { ThemePalette } from '@angular/material/core';
import { MatSelectChange } from '@angular/material/select';
/**
 * Enumeration of the different types of headers supported in the table.
 */
export declare enum HeaderTypes {
    /** Text-based column. */
    TEXT = "text",
    /** Relative Date-based */
    RELATIVE_DATE = "relative_date",
    /** Date-based column. */
    DATE = "date",
    /** Numeric column. */
    NUMBER = "number",
    /** Boolean column. */
    BOOLEAN = "boolean",
    /** Column for action buttons. */
    ACTIONS = "actions",
    /** Dropdown selection in a row. */
    SELECT = "select",
    /** Checkbox for selecting rows. */
    SELECTION = "selection",
    /** Column for matching criteria. */
    MATCHING = "matching",
    /** Array data column. */
    ARRAY = "array",
    /** List data column. */
    LIST = "list",
    /** Column with anchor (`<a>`) links. */
    LINK = "link",
    /** Icon column.*/
    ICON = "icon",
    /** Transport line column. */
    LINE = "line"
}
/**
 * Defines the structure of each table header.
 */
export interface HeaderTable {
    /** The display title of the column */
    title: string;
    /** Unique key identifier for the column */
    key: string;
    /** Optional additional keys for complex data handling */
    otherKey?: string[];
    /** The type of the column header */
    type: HeaderTypes;
    /** Options for columns of type 'select' */
    selectOptions?: HeaderTableSelectOptions;
    /** Custom data associated with the header */
    data?: any;
    /** Additional options to configure the header */
    options?: HeaderTableOptions;
}
/**
 * Options for columns of type 'select'
 */
export interface HeaderTableSelectOptions {
    /** Method to handle selection changes */
    method: (evt: MatSelectChange, data: any) => void;
    /** Array of selectable options */
    options: any[];
}
/**
 * Additional options to configure the header
 */
export interface HeaderTableOptions {
    /** Optional format for displaying data */
    format?: string;
    /** Maximum allowed length of the column data */
    maxLength?: number;
    /** Method returning CSS classes for custom styling */
    class?: (row: any) => string[];
    /** If true, removes spaces from data */
    removeSpace?: boolean;
    /** Key used for matching data entries */
    matchingKey?: string;
}
/**
 * Custom configuration for table behavior and display.
 */
export interface ConfigTableCustom {
    /** Default sorting order for a column */
    defaultOrderColumn?: {
        /** Column name to sort by default */
        column: string;
        /** Sort order, ascending or descending */
        order: 'asc' | 'desc';
    };
    /** Array of possible page sizes for pagination */
    pageSize: number[];
    /** Enables or disables the filter option */
    showFilter: boolean;
    /** Allows multi-selection if set to true */
    multiSelect?: boolean;
}
/**
 * Defines configuration for row and column actions in the table.
 */
export interface ConfigActionTable {
    /** Array of actions available for columns */
    column?: ActionTable[];
    /** Function executed on row action */
    line?: (data: any, evt: Event) => void;
}
/**
 * Represents an action associated with a table.
 */
export interface ActionTable {
    /** Display title of the action */
    title: string;
    /** CSS class for styling the action */
    class: string;
    /** Icon name associated with the action */
    icon: string;
    /** The action to be performed */
    action: any;
    /** Determines if the action is part of a menu */
    isMenu: boolean;
    /** Optional color theme for the action */
    color?: ThemePalette;
    /** Disables the action based on specific criteria */
    disabled: (p: any) => boolean;
}
/**
 * Main configuration interface for the table component.
 */
export interface ConfigTable {
    /** Label for the table */
    label: string;
    /** Array of headers defining each column in the table */
    headers: HeaderTable[];
    /** Array of data entries for the table */
    data: any[];
    /** Type identifier for the table */
    type: string;
    /** Custom configuration options for the table */
    config: ConfigTableCustom;
    /** Array of custom button configurations */
    buttons?: ConfigButtonTable[];
    /** Configuration for table actions */
    actions: ConfigActionTable;
}
/**
 * Defines the structure for custom buttons in the table.
 */
export interface ConfigButtonTable {
    /** Unique identifier for the button */
    id: number | string;
    /** Display title of the button */
    title: string;
    /** Value associated with the button */
    value: any;
    /** Function executed when the button is clicked */
    action: (d: ConfigButtonTable, event: Event) => void;
}
/**
 * Defines data return by filter
 */
export interface FilterText {
    /** Search text */
    filter: string;
    /** Data found */
    data: any[];
}
