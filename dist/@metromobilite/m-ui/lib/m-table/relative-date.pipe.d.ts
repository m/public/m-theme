import { PipeTransform } from '@angular/core';
import * as i0 from "@angular/core";
export declare class RelativeDatePipe implements PipeTransform {
    transform(dateStamp: number): string;
    getDuration(timeAgoInSeconds: number): {
        interval: number;
        epoch: any;
    };
    static ɵfac: i0.ɵɵFactoryDeclaration<RelativeDatePipe, never>;
    static ɵpipe: i0.ɵɵPipeDeclaration<RelativeDatePipe, "relativeDate", true>;
}
