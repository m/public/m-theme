import { SelectionModel } from '@angular/cdk/collections';
import { AfterViewInit, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { ConfigActionTable, ConfigTableCustom, FilterText, HeaderTable, HeaderTypes } from './m-table.interface';
import * as i0 from "@angular/core";
/**
 * @component
 * @name MTable
 * @description
 * A configurable table component that extends Angular Material's mat-table.
 * It includes features like filtering, sorting, pagination, dynamic content, and row selection.
 *
 * ### Usage example
 * ```html
 * <m-table [data]="tableData" [headers]="tableHeaders" [config]="tableConfig"></m-table>
 * ```
 */
export declare class MTable implements AfterViewInit, OnChanges {
    /** Data to display in table */
    data: any[];
    /** Configuration for each table column header. */
    headers: HeaderTable[];
    /** Configuration settings for table behavior (e.g., pagination, filtering). */
    config: ConfigTableCustom;
    /** Actions configuration for rows or buttons. */
    actions: ConfigActionTable;
    /** Emits changes in row selection. */
    selectionChange: EventEmitter<any>;
    /** Emits changes filter */
    filter: EventEmitter<FilterText>;
    /** Reference to the MatTable instance. */
    table: MatTable<any>;
    /** Reference to the MatPaginator instance. */
    paginator: MatPaginator;
    /** Reference to the MatSort instance for sorting. */
    sort: MatSort;
    dataSource: MatTableDataSource<any>;
    header: string[];
    selection: SelectionModel<any>;
    protected readonly Object: ObjectConstructor;
    protected readonly HeaderTypes: typeof HeaderTypes;
    /** init dataSource */
    constructor();
    /**
     * @method ngAfterViewInit
     * @description
     * Initializes paginator and sorting after view is fully initialized.
     */
    ngAfterViewInit(): void;
    /**
     * @method ngOnChanges
     * @description
     * Detects changes in input data and updates the table if necessary.
     * @param {SimpleChanges} changes - Object containing changes in input properties.
     */
    ngOnChanges(changes: SimpleChanges): void;
    /**
     * Initializes table data and headers.
     */
    private initData;
    /**
     * Initializes the data source based on headers configuration.
     */
    private initDataSource;
    /**
     * Sorts data according to the default order in the configuration.
     * @param data Data array to sort.
     */
    private orderBy;
    /**
     * Retrieves cell data, applying any header configuration options.
     * @param row The data row.
     * @param data Header configuration.
     * @return Processed cell data.
     */
    private getCellData;
    /**
     * Retrieves a nested value from an object based on a list of keys.
     * @param obj The object to retrieve data from.
     * @param keyData Array of keys for nested object access.
     * @return The nested value.
     */
    private getNestedValue;
    /**
     * Initializes header keys based on the headers input.
     * @return void
     */
    private initHeader;
    /**
     * Applies a filter to the table data based on input text.
     * @param event Filter input event.
     */
    applyFilter(event: Event): void;
    /**
     * Toggles selection for all rows.
     * @param evt Event indicating the toggle state.
     */
    toggleAllRows(evt: any): void;
    /**
     * Checks if all rows are selected.
     * @return True if all rows are selected.
     */
    isAllSelected(): boolean;
    /**
     * Provides a label for the checkbox.
     * @param row Optional row data.
     * @return Checkbox label.
     */
    checkboxLabel(row?: any): string;
    /**
     * Builds CSS classes for a row based on header configuration.
     * @param data Header configuration.
     * @param row Row data.
     * @return Array of CSS classes.
     */
    buildClass(data: HeaderTable, row: any): string[];
    /**
     * Toggles selection for a specific row and emits the selection change.
     * @param evt The event triggering selection change.
     * @param row Row to toggle.
     */
    changeSelection(evt: any, row: any): void;
    /**
     * Checks if a specific row is selected.
     * @param row Row to check.
     * @return True if the row is selected.
     */
    isSelected(row: any): boolean;
    /**
     * Open link and stop propagation
     * @param link : string
     * @param target : string
     * @param evt : Event
     * @return void
     */
    openLink(link: string, target: string, evt: Event): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<MTable, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<MTable, "m-table", never, { "data": "data"; "headers": "headers"; "config": "config"; "actions": "actions"; }, { "selectionChange": "selectionChange"; "filter": "filter"; }, never, ["[left]", "[right]"], true, never>;
}
