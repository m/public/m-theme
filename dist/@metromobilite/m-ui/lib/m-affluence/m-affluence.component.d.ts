import { OnInit } from '@angular/core';
import * as i0 from "@angular/core";
export interface Affluence {
    /** Occupancy level that determines the description displayed. */
    lvl: number;
    /** Occupancy traduction lvl to text */
    name: string;
    /** Occupancy description */
    description: string;
    /** LightMode */
    lightMode: boolean;
}
/**
 * @component
 * @name MAffluence
 * @description
 * The `MAffluence` component displays the level of occupancy with a description.
 * This component uses Angular Material List and Icon to present the occupancy level.
 *
 * ### Usage example
 * ```html
 * <m-affluence [lvl]="1"></m-affluence>
 * ```
 *
 * @selector m-affluence
 * @standalone true
 * @module CommonModule, MatListModule, MatIconModule, MIcons
 */
export declare class MAffluence implements OnInit {
    /**
     * Occupancy level that determines the description displayed.
     * Possible values:
     * - 1 : Low
     * - 2 : Moderate
     * - 3 : High
     *
     * @default undefined (displays all descriptions)
     */
    lvl: number;
    /**
     * Enable/disable light mode.
     */
    lightMode: boolean;
    /**
     * @private
     * List of available occupancy levels with their descriptions.
     */
    private affluenceDescription;
    /**
     * Array used to display occupancy information based on the selected level (`lvl`).
     */
    affluence: any[];
    /**
     * Initializes the `affluence` array based on the provided occupancy level (`lvl`).
     * If no level is defined, displays all descriptions.
     */
    ngOnInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<MAffluence, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<MAffluence, "m-affluence", never, { "lvl": "lvl"; "lightMode": "lightMode"; }, {}, never, never, true, never>;
}
