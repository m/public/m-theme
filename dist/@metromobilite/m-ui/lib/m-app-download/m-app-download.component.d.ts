import { AfterViewInit, ElementRef, Renderer2 } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import * as i0 from "@angular/core";
export interface AppDownload {
    /** Logo de la plateforme de téléchargement (ex : logo Apple Store, Play Store...) */
    Logo: any;
    /** Lien de la page de téléchargement */
    Lien: string;
    /** Texte principal (nom de la plateforme de téléchargement) */
    TexteGras: string;
    /** Texte secondaire : pour contextualiser l'action */
    Texte: string;
}
/**
 * @component
 * @name MAppDownload
 * @description
 * Composant graphique pour afficher les liens de téléchargement d'une Appli
 * Exemple : liens pour télécharger l'Appli M sur Apple Store
 *
 * ### Usage example
 * ```html
 * <m-app-download [data]="App"></m-app-download>
 * ```
 *
 * @selector m-app-download
 * @standalone true
 * @module CommonModule, MatIconModule, MatButtonModule
 */
export declare class MAppDownload implements AfterViewInit {
    iconRegistry: MatIconRegistry;
    sanitizer: DomSanitizer;
    private renderer;
    /**
     * Logo + lien téléchargement + texte
     *
     */
    data: AppDownload;
    /**
     * Element HTML pour l'icon représentant la plateforme de téléchargement
     */
    iconPrincipal: ElementRef;
    text: ElementRef;
    constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, renderer: Renderer2);
    ngAfterViewInit(): void;
    static ɵfac: i0.ɵɵFactoryDeclaration<MAppDownload, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<MAppDownload, "m-app-download", never, { "data": "data"; }, {}, never, never, true, never>;
}
