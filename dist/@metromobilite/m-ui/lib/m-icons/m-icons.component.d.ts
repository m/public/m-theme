import { ThemePalette } from '@angular/material/core';
import { MIconsService } from './m-icons.service';
import * as i0 from "@angular/core";
/**
 * Represents the mapping of an icon used in the application.
 *
 * This interface defines the properties required to register an icon,
 * including the icon type and its file name or path.
 */
export interface IconMapping {
    /**
     * The unique type identifier for the icon.
     * This is the name used to register the icon in the `MatIconRegistry`
     * and to reference it throughout the application.
     *
     * @example
     * 'home', 'settings', 'user-profile'
     */
    type: string;
    /**
     * The name or path of the SVG file for the icon.
     * This is appended to the base icon path defined in `MIconsService`.
     *
     * @example
     * 'home.svg', 'settings.svg', 'user-profile.svg'
     */
    name?: string;
}
/**
 * Array of icon mappings used throughout the application.
 *
 * Each object in this array follows the `IconMapping` interface and represents a unique icon
 * that can be used by the application. Icons are registered in the application through the `MIconsService`
 * using this mapping. Each mapping defines a `type`, which serves as an identifier for the icon,
 * and optionally a `name`, which specifies the SVG file name in the icons directory.
 *
 * If `name` is not specified, the `type` may correspond to a default icon.
 *
 * Example usage:
 * ```
 * { type: 'accident', name: 'accident.svg' }
 * ```
 */
export declare const ICONS_MAPPING: IconMapping[];
/**
 * @component
 * @name MIcons
 * @description
 * Icon component that wraps Angular Material's icon component to provide a customized icon display.
 * The component utilizes the `ICONS_MAPPING` constant for registering a set of icons and relies on
 * `MIconsService` to manage icon registration.
 *
 * ### Usage example
 * ```html
 * <m-icons type="home"></m-icons>
 * ```
 */
export declare class MIcons {
    private mIconService;
    /**
     * Input property that defines the type of icon to display. This should match one of the `type` values
     * in `ICONS_MAPPING` to ensure the correct icon is displayed.
     */
    type?: string;
    /**
     * Input property that defines the color palette for the icon. This should match one of the `colors` values
     * in `ThemePalette` to ensure the correct color is applied to the icon.
     */
    color?: ThemePalette;
    /**
     * Initializes the component and registers a set of icons defined in the `ICONS_MAPPING` constant.
     * Calls `registerIcons` method of `MIconsService` to ensure that all necessary icons are available
     * for use within the application.
     *
     * @param mIconService The MIconsService instance to use for registering icons
     */
    constructor(mIconService: MIconsService);
    static ɵfac: i0.ɵɵFactoryDeclaration<MIcons, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<MIcons, "m-icons", never, { "type": "type"; "color": "color"; }, {}, never, ["*"], true, never>;
}
