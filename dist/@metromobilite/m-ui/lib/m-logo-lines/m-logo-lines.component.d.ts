import { EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import * as i0 from "@angular/core";
/**
 * @interface
 * @name Line
 * @description
 * Represents the properties of a transport line, including its colors, identifiers, and type.
 */
export interface Line {
    /** The color of the line. */
    color: string;
    /** A unique identifier for the line. */
    gtfsId: string;
    /** The line ID. */
    id: string;
    /** The full name of the line. */
    longName: string;
    /** The mode of transport, such as `TRAM`, `BUS`. */
    mode: string;
    /** The short name of the line. */
    shortName: string;
    /** The color for the text displayed on the line. */
    textColor: string;
    /** The line type, such as `PROXIMO`, `BUS`, `TEAM`, `CHRONO_PERI`. */
    type: string;
}
/**
 * @interface
 * @name disturbanceInfo
 * @description
 * Represents the properties of a disturbance, including the disturbance level and whether it is outer.
 */
export interface disturbanceInfo {
    /** If `true`, the line has a disturbance */
    hasDisturbance: boolean;
    /** The disturbance level. */
    nsv: number;
    /** If `true`, the disturbance is outer. */
    outer: boolean;
}
/**
 * @component
 * @name MLogoLines
 * @description
 * A component that displays a logo for a given transport line. It customizes the size, padding,
 * and shape of the logo based on the input properties, and calculates optimal font size
 * for the line's name display.
 *
 * ### Usage example
 * ```html
 * <m-logo-lines [ligne]="lineData" [height]="50" [width]="50"></m-logo-lines>
 * ```
 */
export declare class MLogoLines implements OnChanges {
    /** The transport line object that defines the logo's content and colors. */
    ligne: Line;
    /** Padding coefficient for the top of the logo. */
    paddingCoefTop: number;
    /** Padding coefficient for the left side of the logo. */
    paddingCoefLeft: number;
    /** The height of the logo. */
    height: number;
    /** The width of the logo. */
    width: number;
    /** If `true`, displays the logo in a circular shape */
    isCircle: boolean;
    /** If `true`, applies rounded corners to the logo. */
    isRounded: boolean;
    /** If `true`, displays the logo in a square shape. */
    isSquare: boolean;
    /** If `true` display mat-checkbox behind logo-lines */
    isCheckbox: boolean;
    /** If `true`, displays the line's description. */
    description: boolean;
    /** If `true`, selects the line. */
    checked: boolean;
    /** The opacity of the logo. */
    opacity: number;
    /** Add Logo Disturbance */
    disturbance: disturbanceInfo;
    /** Emits when the line is clicked. */
    lineChange: EventEmitter<{
        line: Line;
        checked: boolean;
    }>;
    /** The viewBox attribute for SVG scaling, updated based on width and height. */
    viewBox: string;
    /** The long name of the line, derived from the `ligne` input. */
    longName: string;
    /** The short name of the line, derived from the `ligne` input. */
    shortName: string;
    /** The calculated font size for the line's text display. */
    fontSize: string;
    /** The type of the line, used for determining line-specific display logic. */
    lineType: string;
    /**
     * @method onClick
     * @description
     * Emits the `lineClicked` event when the line is clicked.
     *
     * @returns {void}
     */
    clickLine(ligne: Line, evt: MatCheckboxChange): void;
    /**
     * @method ngOnChanges
     * @description
     * Updates the component when input properties change. Calculates viewBox, font size, and other
     * properties based on the input line data and dimensions.
     *
     * @param {SimpleChanges} changes - The changes in input properties.
     * @returns {void}
     */
    ngOnChanges(changes: SimpleChanges): void;
    /**
     * Attribute class for any disturbances
     * @returns {string}
     */
    getClass(): string;
    static ɵfac: i0.ɵɵFactoryDeclaration<MLogoLines, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<MLogoLines, "m-logo-lines", never, { "ligne": "ligne"; "paddingCoefTop": "paddingCoefTop"; "paddingCoefLeft": "paddingCoefLeft"; "height": "height"; "width": "width"; "isCircle": "isCircle"; "isRounded": "isRounded"; "isSquare": "isSquare"; "isCheckbox": "isCheckbox"; "description": "description"; "checked": "checked"; "opacity": "opacity"; "disturbance": "disturbance"; }, { "lineChange": "lineChange"; }, never, never, true, never>;
}
