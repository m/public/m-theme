import * as i0 from "@angular/core";
export declare class MDisturbanceDisplay {
    /** The disturbance to display: TYPE show -> [M-features] */
    disturbance: any;
    /** The disturbance collection: TYPE show -> [M-features] */
    disturbanceCollection: any[];
    /** The link for redirection */
    link: string;
    /**
     * Get the dynamic classes for the disturbance
     * @param disturbance any
     * @return string[]
     */
    getDynamicClasses(disturbance: any): string[];
    static ɵfac: i0.ɵɵFactoryDeclaration<MDisturbanceDisplay, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<MDisturbanceDisplay, "m-disturbance-display", never, { "disturbance": "disturbance"; "disturbanceCollection": "disturbanceCollection"; "link": "link"; }, {}, never, never, true, never>;
}
