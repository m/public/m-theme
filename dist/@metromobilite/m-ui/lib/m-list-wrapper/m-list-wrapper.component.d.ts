import * as i0 from "@angular/core";
export declare class MListWrapper {
    /**
     * Enable see more button
     */
    seeMoreEnable: boolean;
    /**
     * See more button state
     */
    seeMore: boolean;
    static ɵfac: i0.ɵɵFactoryDeclaration<MListWrapper, never>;
    static ɵcmp: i0.ɵɵComponentDeclaration<MListWrapper, "m-list-wrapper", ["MListWrapper"], { "seeMoreEnable": "seeMoreEnable"; }, {}, never, ["*"], true, never>;
}
