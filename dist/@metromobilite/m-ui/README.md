# M-UI

M-UI is a component library built on top of [Angular Material 15](https://v15.material.angular.io/), designed to extend its capabilities while leveraging M-Theme for styling consistency. It provides:

* Ready-to-use Angular components tailored for specific use cases.
* An intuitive API for developers, enabling seamless integration.
* Theming support out-of-the-box, powered by M-Theme.
---


### Motivation

While working on various projects, especially when building Angular applications, I noticed a common need for:

1. A unified theming system that simplifies the customization of Angular Material components.
2. A component library that goes beyond the basic Angular Material design system to meet specific business needs.

Other solutions often fell short:

- Writing custom themes manually was repetitive and prone to errors.
- Extending Angular Material components often required complex configurations.
- Integrating theming and custom components into a single cohesive package was challenging.

M-Theme and M-UI address these gaps:

- M-Theme simplifies theming by centralizing styles, reducing repetitive code, and offering pre-built palettes and functions.
- M-UI extends Angular Material with additional components, allowing developers to create rich, feature-complete applications faster.

### How does it work?

#### M-Theme

- Provides SCSS files for defining light and dark themes.
- Offers pre-configured variables and helper functions to make styling Angular Material components easier.
- Works seamlessly with Angular's build process, requiring only the addition of the necessary imports in your main SCSS file.

#### M-ui

- Built as a library of Angular components that extend Angular Material's functionality.
- Components are designed to be modular:
  - Import only the components you need or the entire library.
  - All components are styled consistently using M-Theme.
- Supports standalone usage and integration into existing Angular projects.


## Peer dependencies
M-UI depends on Angular Material and Angular CDK.
We need to install these dependencies before installing M-UI.

| Peer Dependency        | Version |
|------------------------|---------|
| @angular/common        | 15.2.0  |
| @angular/cdk           | 15.2.0  |
| @angular/core          | 15.2.0  |
| @angular/material      | 15.2.8  |


## Configuration/installation

### Installing the Library
1. Install via npm

Run the following command to install the latest version of the library:
```bash
npm i @metromobilite/m-ui@latest
```

### Configuration

#### M-Theme Setup
##### Step 1: Add stylePreprocessorOptions in angular.json
> **Note**
> The ``stylePreprocessorOptions`` in ``angular.json`` allows you to define custom paths for Sass to search for files during ``@use`` or ``@import``.
>
> Adding 'includePaths': ['node_modules'] lets Sass resolve imports from node_modules directly, enabling simplified imports for libraries.


```json
{
	"projects": {
		"project-name": {
			"architect": {
				"build": {
					"options": {
						"stylePreprocessorOptions": {
							"includePaths": ["node_modules"]
						}
					}
				}
			}
		}
	}	
}
```

---
##### Step 2: Add Assets in angular.json
To ensure that theme-related assets are properly loaded, include the following in the assets array of ``angular.json``:

```json
{
	"assets": [
		{
			"glob": "**/*",
			"input": "./node_modules/@metromobilite/m-ui/theme/assets/",
			"output": "./assets/"
		}
	]	
}
```
---
##### Step 3: Import Styles in styles.scss
In your main SCSS file (e.g., src/styles.scss), import the M-Theme styles.
You can choose to import all styles or only specific ones.

###### Option 1: Import All Styles

```scss
$assets-path: '/assets'; // Define the assets path
@use "@metromobilite/m-ui/theme/style";
```

###### Option 2: Import Specific Files
If you need more control over the imported styles, you can import them one by one:

```scss
@import 'reset';
@import 'variables';
@import 'font';
@import '@metromobilite/m-ui/theme/theme';
@import 'classes';
@import 'components/index';
```
---

#### M-UI Setup

##### Add Icon Assets in ``angular.json``

To use the icon library provided by M-UI, include the following configuration in ``angular.json`` under the assets array:

```json
{
	"assets": [
		{
			"glob": "**/*",
			"input": "node_modules/@metromobilite/m-ui/src/assets",
			"output": "/assets/"
		}
	]	
}
```


## Usage

### M-Theme

To enable light or dark themes, add the ``dark-theme`` or ``light-theme`` class to the ``<body>`` tag in your ``index.html``:


```html
<body class="dark-theme">
	<!-- Your app content -->
</body>
```

This theme provide custom palettes and provides the following variables:

* `$light-theme-background`
* `$light-theme-foreground`
* `$dark-theme-background`
* `$dark-theme-foreground`

Example:

```scss
.my-container {
  color: map-get($dark-theme-foreground, text);
}
```

Some helper functions are also provided:

* `dark-color-overlay($elevation)`
* `light-color-overlay($elevation)`

`$elevation` must be a value in `[0, 1, 2, 3, 4, 6, 8, 12, 16, 24]`

Example:

```scss
.my-container {
	background: dark-color-overlay(16);
}
```
---


##### Helper Functions and Variables

###### Variables
M-Theme provides pre-defined variables for customizing the look and feel of your app:

- ``$light-theme-background``
- ``$light-theme-foreground``
- ``$dark-theme-background``
- ``$dark-theme-foreground``

Example:
```scss
.my-container {
  color: map-get($dark-theme-foreground, text);
}
```
---
###### Helper Functions
Use helper functions for overlays and dynamic styles:

- ``dark-color-overlay($elevation)``
- ``light-color-overlay($elevation)``

> **Note**
> $elevation must be one of [0, 1, 2, 3, 4, 6, 8, 12, 16, 24].

Example:

```scss
.my-container {
  background: dark-color-overlay(16);
}
```

##### Testing the Installation
1. Ensure all configurations in angular.json are correctly applied.
2. Use the imported SCSS variables and functions in your components to verify that they work as expected.
3. If icons or assets aren't loading, double-check the assets array in angular.json.

---

### M-ui
All library components are prefixed with `m-` and are designed as standalone components.

```html
<m-icons type="accident" class="m-icon"></m-icons>
```

#### Default import
We can import the entire library in our Angular module.

```typescript
import {MIcons} from '@metromobilite/m-ui';

@NgModule({
  declarations: [AppComponent], 
  imports: [MIcons], 
  bootstrap: [AppComponent]
})
export class AppModule {}
```

#### Import unitary components

We can also import the components individually.

```typescript
import {MIcons} from '@metromobilite/m-ui/m-icons';
```

> **Note**
> Add this in your tsconfig.json
> ```json
> "compilerOptions": {
>   "paths": {
>     "@metromobilite/m-ui/*": [
>       "node_modules/@metromobilite/m-ui",
>       "node_modules/@metromobilite/m-ui/lib/*"
>     ]
>   } 
> }


### More docs

[show more informations](https://apptest.mobilites-m.fr)
