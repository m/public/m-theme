import { Pipe } from '@angular/core';
import * as i0 from "@angular/core";
// Epochs
const epochs = [
    ['an(s)', 31536000],
    ['mois', 2592000],
    ['j', 86400],
    ['h', 3600],
    ['mn', 60],
    ['s', 1]
];
export class RelativeDatePipe {
    transform(dateStamp) {
        const tempDate = new Date(dateStamp);
        if (tempDate.getFullYear() === 1970)
            return 'Jamais';
        else {
            let timeAgoInSeconds = Math.floor((new Date().getTime() - tempDate.getTime()) / 1000);
            let { interval, epoch } = this.getDuration(timeAgoInSeconds);
            let suffix = interval === 1 ? '' : ''; //pluriels supprimés car abbreviations
            return `il y a ${interval} ${epoch} ${suffix}`;
        }
    }
    getDuration(timeAgoInSeconds) {
        for (let [name, seconds] of epochs) {
            let interval = Math.floor(timeAgoInSeconds / seconds);
            if (interval >= 1) {
                return {
                    interval: interval,
                    epoch: name
                };
            }
        }
        return {
            interval: 0,
            epoch: 's'
        };
    }
    ;
}
RelativeDatePipe.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: RelativeDatePipe, deps: [], target: i0.ɵɵFactoryTarget.Pipe });
RelativeDatePipe.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "14.0.0", version: "15.2.10", ngImport: i0, type: RelativeDatePipe, isStandalone: true, name: "relativeDate" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: RelativeDatePipe, decorators: [{
            type: Pipe,
            args: [{
                    standalone: true,
                    name: 'relativeDate'
                }]
        }] });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVsYXRpdmUtZGF0ZS5waXBlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbS11aS9zcmMvbGliL20tdGFibGUvcmVsYXRpdmUtZGF0ZS5waXBlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxJQUFJLEVBQWdCLE1BQU0sZUFBZSxDQUFDOztBQUdsRCxTQUFTO0FBQ1QsTUFBTSxNQUFNLEdBQVE7SUFDbkIsQ0FBQyxPQUFPLEVBQUUsUUFBUSxDQUFDO0lBQ25CLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQztJQUNqQixDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUM7SUFDWixDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUM7SUFDWCxDQUFDLElBQUksRUFBRSxFQUFFLENBQUM7SUFDVixDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7Q0FDUixDQUFDO0FBT0YsTUFBTSxPQUFPLGdCQUFnQjtJQUU1QixTQUFTLENBQUMsU0FBaUI7UUFDMUIsTUFBTSxRQUFRLEdBQUcsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDckMsSUFBSSxRQUFRLENBQUMsV0FBVyxFQUFFLEtBQUssSUFBSTtZQUFFLE9BQU8sUUFBUSxDQUFDO2FBQ2hEO1lBQ0osSUFBSSxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxPQUFPLEVBQUUsR0FBRyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQztZQUN0RixJQUFJLEVBQUMsUUFBUSxFQUFFLEtBQUssRUFBQyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUMzRCxJQUFJLE1BQU0sR0FBRyxRQUFRLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFBLHNDQUFzQztZQUM1RSxPQUFPLFVBQVUsUUFBUSxJQUFJLEtBQUssSUFBSSxNQUFNLEVBQUUsQ0FBQztTQUMvQztJQUNGLENBQUM7SUFFRCxXQUFXLENBQUMsZ0JBQXdCO1FBQ25DLEtBQUssSUFBSSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsSUFBSSxNQUFNLEVBQUU7WUFDbkMsSUFBSSxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsR0FBRyxPQUFPLENBQUMsQ0FBQztZQUN0RCxJQUFJLFFBQVEsSUFBSSxDQUFDLEVBQUU7Z0JBQ2xCLE9BQU87b0JBQ04sUUFBUSxFQUFFLFFBQVE7b0JBQ2xCLEtBQUssRUFBRSxJQUFJO2lCQUNYLENBQUM7YUFDRjtTQUNEO1FBQ0QsT0FBTztZQUNOLFFBQVEsRUFBRSxDQUFDO1lBQ1gsS0FBSyxFQUFFLEdBQUc7U0FDVixDQUFDO0lBQ0gsQ0FBQztJQUFBLENBQUM7OzhHQTNCVSxnQkFBZ0I7NEdBQWhCLGdCQUFnQjs0RkFBaEIsZ0JBQWdCO2tCQUo1QixJQUFJO21CQUFDO29CQUNMLFVBQVUsRUFBRSxJQUFJO29CQUNoQixJQUFJLEVBQUUsY0FBYztpQkFDcEIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1BpcGUsIFBpcGVUcmFuc2Zvcm19IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuXHJcbi8vIEVwb2Noc1xyXG5jb25zdCBlcG9jaHM6IGFueSA9IFtcclxuXHRbJ2FuKHMpJywgMzE1MzYwMDBdLFxyXG5cdFsnbW9pcycsIDI1OTIwMDBdLFxyXG5cdFsnaicsIDg2NDAwXSxcclxuXHRbJ2gnLCAzNjAwXSxcclxuXHRbJ21uJywgNjBdLFxyXG5cdFsncycsIDFdXHJcbl07XHJcblxyXG5cclxuQFBpcGUoe1xyXG5cdHN0YW5kYWxvbmU6IHRydWUsXHJcblx0bmFtZTogJ3JlbGF0aXZlRGF0ZSdcclxufSlcclxuZXhwb3J0IGNsYXNzIFJlbGF0aXZlRGF0ZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuXHJcblx0dHJhbnNmb3JtKGRhdGVTdGFtcDogbnVtYmVyKTogc3RyaW5nIHtcclxuXHRcdGNvbnN0IHRlbXBEYXRlID0gbmV3IERhdGUoZGF0ZVN0YW1wKTtcclxuXHRcdGlmICh0ZW1wRGF0ZS5nZXRGdWxsWWVhcigpID09PSAxOTcwKSByZXR1cm4gJ0phbWFpcyc7XHJcblx0XHRlbHNlIHtcclxuXHRcdFx0bGV0IHRpbWVBZ29JblNlY29uZHMgPSBNYXRoLmZsb29yKChuZXcgRGF0ZSgpLmdldFRpbWUoKSAtIHRlbXBEYXRlLmdldFRpbWUoKSkgLyAxMDAwKTtcclxuXHRcdFx0bGV0IHtpbnRlcnZhbCwgZXBvY2h9ID0gdGhpcy5nZXREdXJhdGlvbih0aW1lQWdvSW5TZWNvbmRzKTtcclxuXHRcdFx0bGV0IHN1ZmZpeCA9IGludGVydmFsID09PSAxID8gJycgOiAnJzsvL3BsdXJpZWxzIHN1cHByaW3DqXMgY2FyIGFiYnJldmlhdGlvbnNcclxuXHRcdFx0cmV0dXJuIGBpbCB5IGEgJHtpbnRlcnZhbH0gJHtlcG9jaH0gJHtzdWZmaXh9YDtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdGdldER1cmF0aW9uKHRpbWVBZ29JblNlY29uZHM6IG51bWJlcikge1xyXG5cdFx0Zm9yIChsZXQgW25hbWUsIHNlY29uZHNdIG9mIGVwb2Nocykge1xyXG5cdFx0XHRsZXQgaW50ZXJ2YWwgPSBNYXRoLmZsb29yKHRpbWVBZ29JblNlY29uZHMgLyBzZWNvbmRzKTtcclxuXHRcdFx0aWYgKGludGVydmFsID49IDEpIHtcclxuXHRcdFx0XHRyZXR1cm4ge1xyXG5cdFx0XHRcdFx0aW50ZXJ2YWw6IGludGVydmFsLFxyXG5cdFx0XHRcdFx0ZXBvY2g6IG5hbWVcclxuXHRcdFx0XHR9O1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRyZXR1cm4ge1xyXG5cdFx0XHRpbnRlcnZhbDogMCxcclxuXHRcdFx0ZXBvY2g6ICdzJ1xyXG5cdFx0fTtcclxuXHR9O1xyXG5cclxufVxyXG4iXX0=