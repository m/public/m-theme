import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common";
import * as i2 from "@angular/material/button";
export class MListWrapper {
    constructor() {
        /**
         * Enable see more button
         */
        this.seeMoreEnable = false;
        /**
         * See more button state
         */
        this.seeMore = false;
    }
}
MListWrapper.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MListWrapper, deps: [], target: i0.ɵɵFactoryTarget.Component });
MListWrapper.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.10", type: MListWrapper, isStandalone: true, selector: "m-list-wrapper", inputs: { seeMoreEnable: "seeMoreEnable" }, exportAs: ["MListWrapper"], ngImport: i0, template: "<ng-content></ng-content>\n\n<button *ngIf=\"seeMoreEnable && !seeMore\" mat-button class=\"m-theme\" color=\"primary\" (click)=\"seeMore = true\">\n\t<span>Voir plus...</span>\n</button>\n", styles: [":host{display:flex;flex-direction:column}button{align-self:center}\n"], dependencies: [{ kind: "ngmodule", type: CommonModule }, { kind: "directive", type: i1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "ngmodule", type: MatButtonModule }, { kind: "component", type: i2.MatButton, selector: "    button[mat-button], button[mat-raised-button], button[mat-flat-button],    button[mat-stroked-button]  ", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MListWrapper, decorators: [{
            type: Component,
            args: [{ selector: 'm-list-wrapper', standalone: true, imports: [CommonModule, MatButtonModule], exportAs: 'MListWrapper', template: "<ng-content></ng-content>\n\n<button *ngIf=\"seeMoreEnable && !seeMore\" mat-button class=\"m-theme\" color=\"primary\" (click)=\"seeMore = true\">\n\t<span>Voir plus...</span>\n</button>\n", styles: [":host{display:flex;flex-direction:column}button{align-self:center}\n"] }]
        }], propDecorators: { seeMoreEnable: [{
                type: Input
            }] } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibS1saXN0LXdyYXBwZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbS11aS9zcmMvbGliL20tbGlzdC13cmFwcGVyL20tbGlzdC13cmFwcGVyLmNvbXBvbmVudC50cyIsIi4uLy4uLy4uLy4uLy4uLy4uL3Byb2plY3RzL20tdWkvc3JjL2xpYi9tLWxpc3Qtd3JhcHBlci9tLWxpc3Qtd3JhcHBlci5jb21wb25lbnQuaHRtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxPQUFPLEVBQUMsU0FBUyxFQUFFLEtBQUssRUFBQyxNQUFNLGVBQWUsQ0FBQztBQUMvQyxPQUFPLEVBQUMsWUFBWSxFQUFDLE1BQU0saUJBQWlCLENBQUM7QUFDN0MsT0FBTyxFQUFDLGVBQWUsRUFBQyxNQUFNLDBCQUEwQixDQUFDOzs7O0FBVXpELE1BQU0sT0FBTyxZQUFZO0lBUnpCO1FBVUM7O1dBRUc7UUFDTSxrQkFBYSxHQUFZLEtBQUssQ0FBQztRQUV4Qzs7V0FFRztRQUNJLFlBQU8sR0FBWSxLQUFLLENBQUM7S0FDaEM7OzBHQVhZLFlBQVk7OEZBQVosWUFBWSxrSkNaekIsK0xBS0EsNkhERVcsWUFBWSxrSUFBRSxlQUFlOzRGQUszQixZQUFZO2tCQVJ4QixTQUFTOytCQUNDLGdCQUFnQixjQUNkLElBQUksV0FDUCxDQUFDLFlBQVksRUFBRSxlQUFlLENBQUMsWUFHOUIsY0FBYzs4QkFPZixhQUFhO3NCQUFyQixLQUFLIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtDb21wb25lbnQsIElucHV0fSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7Q29tbW9uTW9kdWxlfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHtNYXRCdXR0b25Nb2R1bGV9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsL2J1dHRvbic7XG5cbkBDb21wb25lbnQoe1xuXHRzZWxlY3RvcjogJ20tbGlzdC13cmFwcGVyJyxcblx0c3RhbmRhbG9uZTogdHJ1ZSxcblx0aW1wb3J0czogW0NvbW1vbk1vZHVsZSwgTWF0QnV0dG9uTW9kdWxlXSxcblx0dGVtcGxhdGVVcmw6ICcuL20tbGlzdC13cmFwcGVyLmNvbXBvbmVudC5odG1sJyxcblx0c3R5bGVVcmxzOiBbJy4vbS1saXN0LXdyYXBwZXIuY29tcG9uZW50LnNjc3MnXSxcblx0ZXhwb3J0QXM6ICdNTGlzdFdyYXBwZXInXG59KVxuZXhwb3J0IGNsYXNzIE1MaXN0V3JhcHBlciB7XG5cblx0LyoqXG5cdCAqIEVuYWJsZSBzZWUgbW9yZSBidXR0b25cblx0ICovXG5cdEBJbnB1dCgpIHNlZU1vcmVFbmFibGU6IGJvb2xlYW4gPSBmYWxzZTtcblxuXHQvKipcblx0ICogU2VlIG1vcmUgYnV0dG9uIHN0YXRlXG5cdCAqL1xuXHRwdWJsaWMgc2VlTW9yZTogYm9vbGVhbiA9IGZhbHNlO1xufVxuIiwiPG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PlxuXG48YnV0dG9uICpuZ0lmPVwic2VlTW9yZUVuYWJsZSAmJiAhc2VlTW9yZVwiIG1hdC1idXR0b24gY2xhc3M9XCJtLXRoZW1lXCIgY29sb3I9XCJwcmltYXJ5XCIgKGNsaWNrKT1cInNlZU1vcmUgPSB0cnVlXCI+XG5cdDxzcGFuPlZvaXIgcGx1cy4uLjwvc3Bhbj5cbjwvYnV0dG9uPlxuIl19