import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/icon";
import * as i2 from "@angular/platform-browser";
/**
 * Service to register custom icons for use throughout the application.
 * This service leverages Angular Material's MatIconRegistry and DomSanitizer
 * to securely register and manage SVG icons, providing a centralized approach
 * to icon registration.
 */
export class MIconsService {
    /**
     * @constructor
     * @description
     * Initializes the MIconsService, injecting dependencies for managing SVG icons.
     * The constructor receives the Angular Material MatIconRegistry and Angular’s
     * DomSanitizer to securely handle SVG icon URLs.
     *
     * @param {MatIconRegistry} iconRegistry - Injected Angular Material's icon registry
     * used to register and manage SVG icons.
     * @param {DomSanitizer} sanitizer - Injected Angular DOM sanitizer to bypass
     * security restrictions on resource URLs for SVG icons.
     */
    constructor(iconRegistry, sanitizer) {
        this.iconRegistry = iconRegistry;
        this.sanitizer = sanitizer;
        /*
        * Base path to the directory where the SVG icons are stored.
        * This path is used as a prefix for all icon URLs in the application.
        */
        this.iconPath = 'assets/icons/';
    }
    /**
     * @method registerIcons
     * @description
     * Registers a list of icons with Angular Material's MatIconRegistry.
     * This method iterates over the provided icon mappings and registers each icon with
     * MatIconRegistry using DomSanitizer to securely handle icon paths.
     *
     * @param {IconMapping[]} icon - An array of IconMapping objects, each representing an icon
     * with a unique `type` and `name` that maps to an SVG file.
     *
     * @returns {void}
     * This method does not return a value but registers the icons for use throughout the application.
     *
     * @example
     * ```typescript
     * const icons: IconMapping[] = [
     *   { type: 'home', name: 'home.svg' },
     *   { type: 'settings', name: 'settings.svg' }
     * ];
     * mIconsService.registerIcons(icons);
     * ```
     */
    registerIcons(icon) {
        icon.forEach((i) => {
            this.iconRegistry.addSvgIcon(i.type, this.sanitizer.bypassSecurityTrustResourceUrl(`${this.iconPath}${i.name}`));
        });
    }
}
MIconsService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MIconsService, deps: [{ token: i1.MatIconRegistry }, { token: i2.DomSanitizer }], target: i0.ɵɵFactoryTarget.Injectable });
MIconsService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MIconsService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MIconsService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return [{ type: i1.MatIconRegistry }, { type: i2.DomSanitizer }]; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibS1pY29ucy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vLi4vLi4vLi4vcHJvamVjdHMvbS11aS9zcmMvbGliL20taWNvbnMvbS1pY29ucy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBQyxVQUFVLEVBQUMsTUFBTSxlQUFlLENBQUM7Ozs7QUFLekM7Ozs7O0dBS0c7QUFJSCxNQUFNLE9BQU8sYUFBYTtJQU96Qjs7Ozs7Ozs7Ozs7T0FXRztJQUNBLFlBQ1ksWUFBNkIsRUFDN0IsU0FBdUI7UUFEdkIsaUJBQVksR0FBWixZQUFZLENBQWlCO1FBQzdCLGNBQVMsR0FBVCxTQUFTLENBQWM7UUFwQnRDOzs7VUFHRTtRQUNTLGFBQVEsR0FBRyxlQUFlLENBQUM7SUFpQmhDLENBQUM7SUFFUDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O09BcUJHO0lBQ0EsYUFBYSxDQUFDLElBQW1CO1FBQzdCLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFjLEVBQUUsRUFBRTtZQUM1QixJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FDeEIsQ0FBQyxDQUFDLElBQUksRUFDTixJQUFJLENBQUMsU0FBUyxDQUFDLDhCQUE4QixDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FDN0UsQ0FBQztRQUNOLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7MkdBckRRLGFBQWE7K0dBQWIsYUFBYSxjQUZWLE1BQU07NEZBRVQsYUFBYTtrQkFIekIsVUFBVTttQkFBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge0luamVjdGFibGV9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge01hdEljb25SZWdpc3RyeX0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwvaWNvbic7XHJcbmltcG9ydCB7RG9tU2FuaXRpemVyfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcclxuaW1wb3J0IHtJY29uTWFwcGluZ30gZnJvbSAnLi4vbS1pY29ucyc7XHJcblxyXG4vKipcclxuICogU2VydmljZSB0byByZWdpc3RlciBjdXN0b20gaWNvbnMgZm9yIHVzZSB0aHJvdWdob3V0IHRoZSBhcHBsaWNhdGlvbi5cclxuICogVGhpcyBzZXJ2aWNlIGxldmVyYWdlcyBBbmd1bGFyIE1hdGVyaWFsJ3MgTWF0SWNvblJlZ2lzdHJ5IGFuZCBEb21TYW5pdGl6ZXJcclxuICogdG8gc2VjdXJlbHkgcmVnaXN0ZXIgYW5kIG1hbmFnZSBTVkcgaWNvbnMsIHByb3ZpZGluZyBhIGNlbnRyYWxpemVkIGFwcHJvYWNoXHJcbiAqIHRvIGljb24gcmVnaXN0cmF0aW9uLlxyXG4gKi9cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNSWNvbnNTZXJ2aWNlIHtcclxuXHQvKlxyXG5cdCogQmFzZSBwYXRoIHRvIHRoZSBkaXJlY3Rvcnkgd2hlcmUgdGhlIFNWRyBpY29ucyBhcmUgc3RvcmVkLlxyXG5cdCogVGhpcyBwYXRoIGlzIHVzZWQgYXMgYSBwcmVmaXggZm9yIGFsbCBpY29uIFVSTHMgaW4gdGhlIGFwcGxpY2F0aW9uLlxyXG5cdCovXHJcbiAgICBwcml2YXRlIGljb25QYXRoID0gJ2Fzc2V0cy9pY29ucy8nO1xyXG5cclxuXHQvKipcclxuXHQgKiBAY29uc3RydWN0b3JcclxuXHQgKiBAZGVzY3JpcHRpb25cclxuXHQgKiBJbml0aWFsaXplcyB0aGUgTUljb25zU2VydmljZSwgaW5qZWN0aW5nIGRlcGVuZGVuY2llcyBmb3IgbWFuYWdpbmcgU1ZHIGljb25zLlxyXG5cdCAqIFRoZSBjb25zdHJ1Y3RvciByZWNlaXZlcyB0aGUgQW5ndWxhciBNYXRlcmlhbCBNYXRJY29uUmVnaXN0cnkgYW5kIEFuZ3VsYXLigJlzXHJcblx0ICogRG9tU2FuaXRpemVyIHRvIHNlY3VyZWx5IGhhbmRsZSBTVkcgaWNvbiBVUkxzLlxyXG5cdCAqXHJcblx0ICogQHBhcmFtIHtNYXRJY29uUmVnaXN0cnl9IGljb25SZWdpc3RyeSAtIEluamVjdGVkIEFuZ3VsYXIgTWF0ZXJpYWwncyBpY29uIHJlZ2lzdHJ5XHJcblx0ICogdXNlZCB0byByZWdpc3RlciBhbmQgbWFuYWdlIFNWRyBpY29ucy5cclxuXHQgKiBAcGFyYW0ge0RvbVNhbml0aXplcn0gc2FuaXRpemVyIC0gSW5qZWN0ZWQgQW5ndWxhciBET00gc2FuaXRpemVyIHRvIGJ5cGFzc1xyXG5cdCAqIHNlY3VyaXR5IHJlc3RyaWN0aW9ucyBvbiByZXNvdXJjZSBVUkxzIGZvciBTVkcgaWNvbnMuXHJcblx0ICovXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIGljb25SZWdpc3RyeTogTWF0SWNvblJlZ2lzdHJ5LFxyXG4gICAgICAgIHByaXZhdGUgc2FuaXRpemVyOiBEb21TYW5pdGl6ZXJcclxuICAgICkge31cclxuXHJcblx0LyoqXHJcblx0ICogQG1ldGhvZCByZWdpc3Rlckljb25zXHJcblx0ICogQGRlc2NyaXB0aW9uXHJcblx0ICogUmVnaXN0ZXJzIGEgbGlzdCBvZiBpY29ucyB3aXRoIEFuZ3VsYXIgTWF0ZXJpYWwncyBNYXRJY29uUmVnaXN0cnkuXHJcblx0ICogVGhpcyBtZXRob2QgaXRlcmF0ZXMgb3ZlciB0aGUgcHJvdmlkZWQgaWNvbiBtYXBwaW5ncyBhbmQgcmVnaXN0ZXJzIGVhY2ggaWNvbiB3aXRoXHJcblx0ICogTWF0SWNvblJlZ2lzdHJ5IHVzaW5nIERvbVNhbml0aXplciB0byBzZWN1cmVseSBoYW5kbGUgaWNvbiBwYXRocy5cclxuXHQgKlxyXG5cdCAqIEBwYXJhbSB7SWNvbk1hcHBpbmdbXX0gaWNvbiAtIEFuIGFycmF5IG9mIEljb25NYXBwaW5nIG9iamVjdHMsIGVhY2ggcmVwcmVzZW50aW5nIGFuIGljb25cclxuXHQgKiB3aXRoIGEgdW5pcXVlIGB0eXBlYCBhbmQgYG5hbWVgIHRoYXQgbWFwcyB0byBhbiBTVkcgZmlsZS5cclxuXHQgKlxyXG5cdCAqIEByZXR1cm5zIHt2b2lkfVxyXG5cdCAqIFRoaXMgbWV0aG9kIGRvZXMgbm90IHJldHVybiBhIHZhbHVlIGJ1dCByZWdpc3RlcnMgdGhlIGljb25zIGZvciB1c2UgdGhyb3VnaG91dCB0aGUgYXBwbGljYXRpb24uXHJcblx0ICpcclxuXHQgKiBAZXhhbXBsZVxyXG5cdCAqIGBgYHR5cGVzY3JpcHRcclxuXHQgKiBjb25zdCBpY29uczogSWNvbk1hcHBpbmdbXSA9IFtcclxuXHQgKiAgIHsgdHlwZTogJ2hvbWUnLCBuYW1lOiAnaG9tZS5zdmcnIH0sXHJcblx0ICogICB7IHR5cGU6ICdzZXR0aW5ncycsIG5hbWU6ICdzZXR0aW5ncy5zdmcnIH1cclxuXHQgKiBdO1xyXG5cdCAqIG1JY29uc1NlcnZpY2UucmVnaXN0ZXJJY29ucyhpY29ucyk7XHJcblx0ICogYGBgXHJcblx0ICovXHJcbiAgICByZWdpc3Rlckljb25zKGljb246IEljb25NYXBwaW5nW10pOiB2b2lkIHtcclxuICAgICAgICBpY29uLmZvckVhY2goKGk6IEljb25NYXBwaW5nKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuaWNvblJlZ2lzdHJ5LmFkZFN2Z0ljb24oXHJcbiAgICAgICAgICAgICAgICBpLnR5cGUsXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNhbml0aXplci5ieXBhc3NTZWN1cml0eVRydXN0UmVzb3VyY2VVcmwoYCR7dGhpcy5pY29uUGF0aH0ke2kubmFtZX1gKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==