import * as i1$1 from '@angular/common';
import { CommonModule } from '@angular/common';
import * as i0 from '@angular/core';
import { Injectable, Component, Input, EventEmitter, ChangeDetectionStrategy, HostBinding, Output, Pipe, ViewChild, ElementRef } from '@angular/core';
import * as i1 from '@angular/material/icon';
import { MatIconModule } from '@angular/material/icon';
import * as i2 from '@angular/platform-browser';
import * as i2$1 from '@angular/material/list';
import { MatListModule } from '@angular/material/list';
import * as i2$2 from '@angular/material/checkbox';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { SelectionModel } from '@angular/cdk/collections';
import * as i11 from '@angular/forms';
import { FormsModule } from '@angular/forms';
import * as i2$4 from '@angular/material/button';
import { MatButtonModule } from '@angular/material/button';
import * as i10 from '@angular/material/core';
import { MatRippleModule } from '@angular/material/core';
import * as i12 from '@angular/material/input';
import { MatInputModule } from '@angular/material/input';
import * as i5 from '@angular/material/menu';
import { MatMenuModule } from '@angular/material/menu';
import * as i2$3 from '@angular/material/paginator';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import * as i9 from '@angular/material/select';
import { MatSelectModule } from '@angular/material/select';
import * as i13 from '@angular/material/sort';
import { MatSort, MatSortModule } from '@angular/material/sort';
import * as i3 from '@angular/material/table';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import * as i8 from '@angular/material/form-field';
import { RouterLink } from '@angular/router';

/**
 * Service to register custom icons for use throughout the application.
 * This service leverages Angular Material's MatIconRegistry and DomSanitizer
 * to securely register and manage SVG icons, providing a centralized approach
 * to icon registration.
 */
class MIconsService {
    /**
     * @constructor
     * @description
     * Initializes the MIconsService, injecting dependencies for managing SVG icons.
     * The constructor receives the Angular Material MatIconRegistry and Angular’s
     * DomSanitizer to securely handle SVG icon URLs.
     *
     * @param {MatIconRegistry} iconRegistry - Injected Angular Material's icon registry
     * used to register and manage SVG icons.
     * @param {DomSanitizer} sanitizer - Injected Angular DOM sanitizer to bypass
     * security restrictions on resource URLs for SVG icons.
     */
    constructor(iconRegistry, sanitizer) {
        this.iconRegistry = iconRegistry;
        this.sanitizer = sanitizer;
        /*
        * Base path to the directory where the SVG icons are stored.
        * This path is used as a prefix for all icon URLs in the application.
        */
        this.iconPath = 'assets/icons/';
    }
    /**
     * @method registerIcons
     * @description
     * Registers a list of icons with Angular Material's MatIconRegistry.
     * This method iterates over the provided icon mappings and registers each icon with
     * MatIconRegistry using DomSanitizer to securely handle icon paths.
     *
     * @param {IconMapping[]} icon - An array of IconMapping objects, each representing an icon
     * with a unique `type` and `name` that maps to an SVG file.
     *
     * @returns {void}
     * This method does not return a value but registers the icons for use throughout the application.
     *
     * @example
     * ```typescript
     * const icons: IconMapping[] = [
     *   { type: 'home', name: 'home.svg' },
     *   { type: 'settings', name: 'settings.svg' }
     * ];
     * mIconsService.registerIcons(icons);
     * ```
     */
    registerIcons(icon) {
        icon.forEach((i) => {
            this.iconRegistry.addSvgIcon(i.type, this.sanitizer.bypassSecurityTrustResourceUrl(`${this.iconPath}${i.name}`));
        });
    }
}
MIconsService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MIconsService, deps: [{ token: i1.MatIconRegistry }, { token: i2.DomSanitizer }], target: i0.ɵɵFactoryTarget.Injectable });
MIconsService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MIconsService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MIconsService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return [{ type: i1.MatIconRegistry }, { type: i2.DomSanitizer }]; } });

/**
 * Array of icon mappings used throughout the application.
 *
 * Each object in this array follows the `IconMapping` interface and represents a unique icon
 * that can be used by the application. Icons are registered in the application through the `MIconsService`
 * using this mapping. Each mapping defines a `type`, which serves as an identifier for the icon,
 * and optionally a `name`, which specifies the SVG file name in the icons directory.
 *
 * If `name` is not specified, the `type` may correspond to a default icon.
 *
 * Example usage:
 * ```
 * { type: 'accident', name: 'accident.svg' }
 * ```
 */
const ICONS_MAPPING = [
    { type: 'abnormalTraffic' },
    { type: 'accident', name: 'accident.svg' },
    { type: 'accidentInvolvingHeavyLorries' },
    { type: 'agenceM', name: 'agenceM.svg' },
    { type: 'animalPresenceObstruction' },
    { type: 'arret' },
    { type: 'ARE', name: 'ARE.svg' },
    { type: 'authorityOperation' },
    { type: 'autostop', name: 'autostop.svg' },
    { type: 'blastingWork' },
    { type: 'bouchon', name: 'bouchon.svg' },
    { type: 'brokenDownVehicle' },
    { type: 'BUS' },
    { type: 'C38', name: 'C38.svg' },
    { type: 'CAM' },
    { type: 'CABLE_CAR', name: 'funicular.svg' },
    { type: 'CAR' },
    { type: 'chantier', name: 'chantier.svg' },
    { type: 'citiz', name: 'citiz.svg' },
    { type: 'citizyea', name: 'citiz.svg' },
    { type: 'clearanceWork' },
    { type: 'clusters', name: 'clusters.svg' },
    { type: 'collision' },
    { type: 'constructionWork' },
    { type: 'constructionWorks' },
    { type: 'COV', name: 'covoiturage_2.svg' },
    { type: 'dat' },
    { type: 'depositaire', name: 'relaisvente.svg' },
    { type: 'disturbanceActivity' },
    { type: 'DIR', name: 'DIR.svg' },
    { type: 'dott', name: 'pony.svg' },
    { type: 'dott_bicycle', name: 'ic-dott-bicycle.svg' },
    { type: 'dott_scooter', name: 'ic-dott-scooter.svg' },
    { type: 'environmentalObstruction' },
    { type: 'equipmentOrSystemFault' },
    { type: 'flooding' },
    { type: 'generalInstructionOrMessageToRoadUsers' },
    { type: 'generalNetworkManagement' },
    { type: 'generalObstruction' },
    { type: 'gnv', name: 'gnv.svg' },
    { type: 'greve', name: 'greve.svg' },
    { type: 'group', name: 'group.svg' },
    { type: 'group_medium', name: 'group_medium.svg' },
    { type: 'group_small', name: 'group_small.svg' },
    { type: 'gpl', name: 'gpl.svg' },
    { type: 'hazardsOnTheRoad' },
    { type: 'hydrogene', name: 'hydrogene.svg' },
    { type: 'ic_bus', name: 'ic_bus.svg' },
    { type: 'ic_star', name: 'ic_star.svg' },
    { type: 'ic_velo', name: 'ic_velo.svg' },
    { type: 'ic_voiture', name: 'ic_voiture.svg' },
    { type: 'ic_trottinette', name: 'ic_trottinette.svg' },
    { type: 'ic_evt_exceptionnel', name: 'ic_evt_exceptionnel.svg' },
    { type: 'ic_mes_alertes', name: 'ic_mes_alertes.svg' },
    { type: 'IRVE_Chademo', name: 'IRVE_Chademo.svg' },
    { type: 'IRVE_Combo', name: 'IRVE_Combo.svg' },
    { type: 'IRVE_EF', name: 'IRVE_EF.svg' },
    { type: 'IRVE_Type_2', name: 'IRVE_Type_2.svg' },
    { type: 'IRVE_Type_3', name: 'IRVE_Type_3.svg' },
    { type: 'ic_smiley_service_legerement_perturbe', name: 'ic_smiley_service_legerement_perturbe.svg' },
    { type: 'ic_smiley_service_normal', name: 'ic_smiley_service_normal.svg' },
    { type: 'ic_smiley_service_tres_perturbe', name: 'ic_smiley_service_tres_perturbe.svg' },
    { type: 'ic_perturb_dark', name: 'ic_perturb_dark.svg' },
    { type: 'ic_perturb_light', name: 'ic_perturb_light.svg' },
    { type: 'ic_hors_service_dark', name: 'ic_hors_service_dark.svg' },
    { type: 'ic_hors_service_light', name: 'ic_hors_service_light.svg' },
    { type: 'incident' },
    { type: 'info' },
    { type: 'information', name: 'information.svg' },
    { type: 'infrastructureDamageObstruction' },
    { type: 'irve', name: 'irve.svg' },
    { type: 'iti' },
    { type: 'laneClosures' },
    { type: 'letaxi', name: 'letaxi.svg' },
    { type: 'lieux' },
    { type: 'maintenanceWork' },
    { type: 'maintenanceWorks' },
    { type: 'manifestation', name: 'manifestation.svg' },
    { type: 'meteo', name: 'meteo.svg' },
    { type: 'MVA', name: 'MV_agence.svg' },
    { type: 'MVC', name: 'MV_Consigne.svg' },
    { type: 'nonWeatherRelatedRoadConditions' },
    { type: 'objectOnTheRoad' },
    { type: 'obstacle', name: 'obstacle.svg' },
    { type: 'obstructionOnTheRoad' },
    { type: 'other' },
    { type: 'panne', name: 'panne.svg' },
    { type: 'parkingCov', name: 'parkingCov.svg' },
    { type: 'peopleOnRoadway' },
    { type: 'PKG', name: 'PKG_0.svg' },
    { type: 'pointCov', name: 'pointCov.svg' },
    { type: 'pointService', name: 'pointService.svg' },
    { type: 'pony', name: 'pony.svg' },
    { type: 'poorEnvironmentConditions' },
    { type: 'PAR', name: 'PR_0.svg' },
    { type: 'publicEvent' },
    { type: 'perturbation_low', name: 'ic_smiley_service_legerement_perturbe.svg' },
    { type: 'perturbation_normal', name: 'ic_smiley_service_normal.svg' },
    { type: 'perturbation_hight', name: 'ic_smiley_service_tres_perturbe.svg' },
    { type: 'perturbation_over', name: 'ic_smiley_hors_service.svg' },
    { type: 'RAIL', name: 'rail.svg' },
    { type: 'recharge', name: 'recharge.svg' },
    { type: 'reroutingManagement' },
    { type: 'resurfacingWork' },
    { type: 'restriction', name: 'restriction.svg' },
    { type: 'roadClosed' },
    { type: 'roadOperatorServiceDisruption' },
    { type: 'roadOrCarriagewayOrLaneManagement' },
    { type: 'roadsideAssistance' },
    { type: 'roadworks' },
    { type: 'rue' },
    { type: 'serviceDisruption' },
    { type: 'shedLoad' },
    { type: 'spillageOnTheRoad' },
    { type: 'speedManagement' },
    { type: 'stationstaxi', name: 'ic-taxi.svg' },
    { type: 'stops' },
    { type: 'strongWinds' },
    { type: 'SEM', name: 'SEM.svg' },
    { type: 'SMA', name: 'SMA.svg' },
    { type: 'SUBWAY' },
    { type: 'tad', name: 'tad.svg' },
    { type: 'tad_gsv', name: 'tad_gsv.svg' },
    { type: 'tier', name: 'tier.svg' },
    { type: 'transitInformation' },
    { type: 'TRAM' },
    { type: 'unprotectedAccidentArea' },
    { type: 'vehicleObstruction' },
    { type: 'vehicleOnWrongCarriageway' },
    { type: 'veloservice', name: 'veloservice.svg' },
    { type: 'visibilityReduced' },
    { type: 'WALK' },
    { type: 'weatherRelatedRoadConditions' },
    { type: 'winterDrivingManagement' }
];
/**
 * @component
 * @name MIcons
 * @description
 * Icon component that wraps Angular Material's icon component to provide a customized icon display.
 * The component utilizes the `ICONS_MAPPING` constant for registering a set of icons and relies on
 * `MIconsService` to manage icon registration.
 *
 * ### Usage example
 * ```html
 * <m-icons type="home"></m-icons>
 * ```
 */
class MIcons {
    /**
     * Initializes the component and registers a set of icons defined in the `ICONS_MAPPING` constant.
     * Calls `registerIcons` method of `MIconsService` to ensure that all necessary icons are available
     * for use within the application.
     *
     * @param mIconService The MIconsService instance to use for registering icons
     */
    constructor(mIconService) {
        this.mIconService = mIconService;
        /**
         * Input property that defines the type of icon to display. This should match one of the `type` values
         * in `ICONS_MAPPING` to ensure the correct icon is displayed.
         */
        this.type = '';
        this.mIconService.registerIcons(ICONS_MAPPING);
    }
}
MIcons.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MIcons, deps: [{ token: MIconsService }], target: i0.ɵɵFactoryTarget.Component });
MIcons.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.10", type: MIcons, isStandalone: true, selector: "m-icons", inputs: { type: "type", color: "color" }, ngImport: i0, template: "<ng-container>\r\n\t<ng-container *ngIf=\"!type; else iconType\">\r\n\t\t<mat-icon [color]=\"color\"><ng-content></ng-content></mat-icon>\r\n\t</ng-container>\r\n\r\n\t<ng-template #iconType>\r\n\t\t<ng-container [ngSwitch]=\"type\">\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'arret'\">directions_bus</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'clusters'\" svgIcon=\"clusters\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'stops'\">nature_people</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'rue'\">place</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'lieux'\">location_city</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'PAR'\" svgIcon=\"PAR\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'PKG'\" svgIcon=\"PKG\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'dat'\">local_grocery_store</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'agenceM'\" svgIcon=\"agenceM\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'CAM'\">videocam</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'iti'\">directions</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'WALK'\">directions_walk</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'BICYCLE'\">directions_bike</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'CAR'\">directions_car</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'TRAM'\">tram</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'BUS'\">directions_bus</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'RAIL'\" svgIcon=\"RAIL\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'SUBWAY'\">subway</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'recharge'\" svgIcon=\"recharge\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'autostop'\" svgIcon=\"autostop\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'citiz'\" svgIcon=\"citiz\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'citizyea'\" svgIcon=\"citizyea\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'parkingCov'\" svgIcon=\"parkingCov\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'pointCov'\" svgIcon=\"pointCov\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'pointService'\" svgIcon=\"pointService\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'depositaire'\" svgIcon=\"depositaire\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'MVA'\" svgIcon=\"MVA\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'MVC'\" svgIcon=\"MVC\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'gpl'\" svgIcon=\"gpl\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'gnv'\" svgIcon=\"gnv\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'hydrogene'\" svgIcon=\"hydrogene\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'COV'\" svgIcon=\"COV\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'accident'\" svgIcon=\"accident\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'bouchon'\" svgIcon=\"bouchon\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'chantier'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'greve'\" svgIcon=\"greve\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'info'\" svgIcon=\"information\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'information'\" svgIcon=\"information\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'manifestation'\" svgIcon=\"manifestation\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'meteo'\" svgIcon=\"meteo\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'obstacle'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'restriction'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'panne'\" svgIcon=\"panne\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'irve'\" svgIcon=\"recharge\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'pony'\" svgIcon=\"pony\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'tad'\" svgIcon=\"tad\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'tad_gsv'\" svgIcon=\"tad_gsv\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ponyVehicle'\" svgIcon=\"pony\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'tier'\" svgIcon=\"tier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'tierVehicle'\" svgIcon=\"tier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'veloservice'\" svgIcon=\"veloservice\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'dott'\" svgIcon=\"dott\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'dott_scooter'\" svgIcon=\"dott_scooter\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'dott_bicycle'\" svgIcon=\"dott_bicycle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'letaxi'\" svgIcon=\"letaxi\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'CABLE_CAR'\" svgIcon=\"CABLE_CAR\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'perturbation_low'\" svgIcon=\"perturbation_low\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'perturbation_normal'\" svgIcon=\"perturbation_normal\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'perturbation_hight'\" svgIcon=\"perturbation_hight\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'perturbation_over'\" svgIcon=\"perturbation_over\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'group'\" svgIcon=\"group\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'group_small'\" svgIcon=\"group_small\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'group_medium'\" svgIcon=\"group_medium\"></mat-icon>\r\n\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_bus'\" svgIcon=\"ic_bus\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_star'\" svgIcon=\"ic_star\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_velo'\" svgIcon=\"ic_velo\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_voiture'\" svgIcon=\"ic_voiture\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_trottinette'\" svgIcon=\"ic_trottinette\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_evt_exceptionnel'\" svgIcon=\"ic_evt_exceptionnel\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_mes_alertes'\" svgIcon=\"ic_mes_alertes\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'IRVE_Chademo'\" svgIcon=\"IRVE_Chademo\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'IRVE_Combo'\" svgIcon=\"IRVE_Combo\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'IRVE_EF'\" svgIcon=\"IRVE_EF\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'IRVE_Type_2'\" svgIcon=\"IRVE_Type_2\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'IRVE_Type_3'\" svgIcon=\"IRVE_Type_3\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'stationstaxi'\" svgIcon=\"stationstaxi\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_smiley_service_legerement_perturbe'\" svgIcon=\"ic_smiley_service_legerement_perturbe\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_smiley_service_normal'\" svgIcon=\"ic_smiley_service_normal\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_smiley_service_tres_perturbe'\" svgIcon=\"ic_smiley_service_tres_perturbe\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_hors_service_dark'\" svgIcon=\"ic_hors_service_dark\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_hors_service_light'\" svgIcon=\"ic_hors_service_light\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_perturb_dark'\" svgIcon=\"ic_perturb_dark\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_perturb_light'\" svgIcon=\"ic_perturb_light\"></mat-icon>\r\n\t\t\t<!-- New disturbance Type-->\r\n\t\t\t<!-- TODO give good icon for disturbances -->\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'abnormalTraffic'\" svgIcon=\"bouchon\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'accidentInvolvingHeavyLorries'\" svgIcon=\"accident\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'animalPresenceObstruction'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'authorityOperation'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'blastingWork'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'brokenDownVehicle'\" svgIcon=\"panne\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'clearanceWork'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'collision'\" svgIcon=\"accident\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'constructionWork'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'constructionWorks'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'disturbanceActivity'\" svgIcon=\"greve\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'environmentalObstruction'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'equipmentOrSystemFault'\" svgIcon=\"information\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'flooding'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'generalInstructionOrMessageToRoadUsers'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'generalNetworkManagement'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'generalObstruction'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'hazardsOnTheRoad'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'incident'\" svgIcon=\"accident\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'infrastructureDamageObstruction'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'laneClosures'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'maintenanceWork'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'maintenanceWorks'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'nonWeatherRelatedRoadConditions'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'objectOnTheRoad'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'obstructionOnTheRoad'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'other'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'peopleOnRoadway'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'poorEnvironmentConditions'\" svgIcon=\"meteo\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'publicEvent'\" svgIcon=\"manifestation\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'reroutingManagement'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'resurfacingWork'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'roadClosed'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'roadOperatorServiceDisruption'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'roadOrCarriagewayOrLaneManagement'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'roadsideAssistance'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'roadworks'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'serviceDisruption'\" svgIcon=\"information\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'shedLoad'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'spillageOnTheRoad'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'speedManagement'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'strongWinds'\" svgIcon=\"meteo\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'transitInformation'\" svgIcon=\"information\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'transitInformation'\" svgIcon=\"information\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'unprotectedAccidentArea'\" svgIcon=\"accident\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'vehicleObstruction'\" svgIcon=\"panne\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'vehicleOnWrongCarriageway'\" svgIcon=\"panne\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'visibilityReduced'\" svgIcon=\"meteo\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'weatherRelatedRoadConditions'\" svgIcon=\"meteo\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'winterDrivingManagement'\" svgIcon=\"meteo\"></mat-icon>\r\n\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'SMA'\" svgIcon=\"SMA\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'SEM'\" svgIcon=\"SEM\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'C38'\" svgIcon=\"C38\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ARE'\" svgIcon=\"ARE\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'DIR'\" svgIcon=\"DIR\"></mat-icon>\r\n\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchDefault>not_listed_location</mat-icon>\r\n\t\t</ng-container>\r\n\t</ng-template>\r\n</ng-container>\r\n", styles: [""], dependencies: [{ kind: "ngmodule", type: CommonModule }, { kind: "directive", type: i1$1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "directive", type: i1$1.NgSwitch, selector: "[ngSwitch]", inputs: ["ngSwitch"] }, { kind: "directive", type: i1$1.NgSwitchCase, selector: "[ngSwitchCase]", inputs: ["ngSwitchCase"] }, { kind: "directive", type: i1$1.NgSwitchDefault, selector: "[ngSwitchDefault]" }, { kind: "ngmodule", type: MatIconModule }, { kind: "component", type: i1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MIcons, decorators: [{
            type: Component,
            args: [{ selector: 'm-icons', standalone: true, imports: [CommonModule, MatIconModule], template: "<ng-container>\r\n\t<ng-container *ngIf=\"!type; else iconType\">\r\n\t\t<mat-icon [color]=\"color\"><ng-content></ng-content></mat-icon>\r\n\t</ng-container>\r\n\r\n\t<ng-template #iconType>\r\n\t\t<ng-container [ngSwitch]=\"type\">\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'arret'\">directions_bus</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'clusters'\" svgIcon=\"clusters\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'stops'\">nature_people</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'rue'\">place</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'lieux'\">location_city</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'PAR'\" svgIcon=\"PAR\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'PKG'\" svgIcon=\"PKG\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'dat'\">local_grocery_store</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'agenceM'\" svgIcon=\"agenceM\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'CAM'\">videocam</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'iti'\">directions</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'WALK'\">directions_walk</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'BICYCLE'\">directions_bike</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'CAR'\">directions_car</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'TRAM'\">tram</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'BUS'\">directions_bus</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'RAIL'\" svgIcon=\"RAIL\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'SUBWAY'\">subway</mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'recharge'\" svgIcon=\"recharge\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'autostop'\" svgIcon=\"autostop\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'citiz'\" svgIcon=\"citiz\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'citizyea'\" svgIcon=\"citizyea\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'parkingCov'\" svgIcon=\"parkingCov\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'pointCov'\" svgIcon=\"pointCov\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'pointService'\" svgIcon=\"pointService\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'depositaire'\" svgIcon=\"depositaire\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'MVA'\" svgIcon=\"MVA\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'MVC'\" svgIcon=\"MVC\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'gpl'\" svgIcon=\"gpl\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'gnv'\" svgIcon=\"gnv\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'hydrogene'\" svgIcon=\"hydrogene\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'COV'\" svgIcon=\"COV\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'accident'\" svgIcon=\"accident\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'bouchon'\" svgIcon=\"bouchon\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'chantier'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'greve'\" svgIcon=\"greve\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'info'\" svgIcon=\"information\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'information'\" svgIcon=\"information\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'manifestation'\" svgIcon=\"manifestation\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'meteo'\" svgIcon=\"meteo\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'obstacle'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'restriction'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'panne'\" svgIcon=\"panne\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'irve'\" svgIcon=\"recharge\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'pony'\" svgIcon=\"pony\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'tad'\" svgIcon=\"tad\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'tad_gsv'\" svgIcon=\"tad_gsv\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ponyVehicle'\" svgIcon=\"pony\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'tier'\" svgIcon=\"tier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'tierVehicle'\" svgIcon=\"tier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'veloservice'\" svgIcon=\"veloservice\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'dott'\" svgIcon=\"dott\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'dott_scooter'\" svgIcon=\"dott_scooter\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'dott_bicycle'\" svgIcon=\"dott_bicycle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'letaxi'\" svgIcon=\"letaxi\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'CABLE_CAR'\" svgIcon=\"CABLE_CAR\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'perturbation_low'\" svgIcon=\"perturbation_low\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'perturbation_normal'\" svgIcon=\"perturbation_normal\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'perturbation_hight'\" svgIcon=\"perturbation_hight\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'perturbation_over'\" svgIcon=\"perturbation_over\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'group'\" svgIcon=\"group\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'group_small'\" svgIcon=\"group_small\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'group_medium'\" svgIcon=\"group_medium\"></mat-icon>\r\n\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_bus'\" svgIcon=\"ic_bus\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_star'\" svgIcon=\"ic_star\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_velo'\" svgIcon=\"ic_velo\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_voiture'\" svgIcon=\"ic_voiture\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_trottinette'\" svgIcon=\"ic_trottinette\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_evt_exceptionnel'\" svgIcon=\"ic_evt_exceptionnel\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_mes_alertes'\" svgIcon=\"ic_mes_alertes\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'IRVE_Chademo'\" svgIcon=\"IRVE_Chademo\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'IRVE_Combo'\" svgIcon=\"IRVE_Combo\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'IRVE_EF'\" svgIcon=\"IRVE_EF\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'IRVE_Type_2'\" svgIcon=\"IRVE_Type_2\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'IRVE_Type_3'\" svgIcon=\"IRVE_Type_3\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'stationstaxi'\" svgIcon=\"stationstaxi\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_smiley_service_legerement_perturbe'\" svgIcon=\"ic_smiley_service_legerement_perturbe\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_smiley_service_normal'\" svgIcon=\"ic_smiley_service_normal\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_smiley_service_tres_perturbe'\" svgIcon=\"ic_smiley_service_tres_perturbe\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_hors_service_dark'\" svgIcon=\"ic_hors_service_dark\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_hors_service_light'\" svgIcon=\"ic_hors_service_light\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_perturb_dark'\" svgIcon=\"ic_perturb_dark\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ic_perturb_light'\" svgIcon=\"ic_perturb_light\"></mat-icon>\r\n\t\t\t<!-- New disturbance Type-->\r\n\t\t\t<!-- TODO give good icon for disturbances -->\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'abnormalTraffic'\" svgIcon=\"bouchon\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'accidentInvolvingHeavyLorries'\" svgIcon=\"accident\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'animalPresenceObstruction'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'authorityOperation'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'blastingWork'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'brokenDownVehicle'\" svgIcon=\"panne\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'clearanceWork'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'collision'\" svgIcon=\"accident\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'constructionWork'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'constructionWorks'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'disturbanceActivity'\" svgIcon=\"greve\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'environmentalObstruction'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'equipmentOrSystemFault'\" svgIcon=\"information\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'flooding'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'generalInstructionOrMessageToRoadUsers'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'generalNetworkManagement'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'generalObstruction'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'hazardsOnTheRoad'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'incident'\" svgIcon=\"accident\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'infrastructureDamageObstruction'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'laneClosures'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'maintenanceWork'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'maintenanceWorks'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'nonWeatherRelatedRoadConditions'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'objectOnTheRoad'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'obstructionOnTheRoad'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'other'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'peopleOnRoadway'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'poorEnvironmentConditions'\" svgIcon=\"meteo\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'publicEvent'\" svgIcon=\"manifestation\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'reroutingManagement'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'resurfacingWork'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'roadClosed'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'roadOperatorServiceDisruption'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'roadOrCarriagewayOrLaneManagement'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'roadsideAssistance'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'roadworks'\" svgIcon=\"chantier\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'serviceDisruption'\" svgIcon=\"information\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'shedLoad'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'spillageOnTheRoad'\" svgIcon=\"obstacle\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'speedManagement'\" svgIcon=\"restriction\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'strongWinds'\" svgIcon=\"meteo\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'transitInformation'\" svgIcon=\"information\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'transitInformation'\" svgIcon=\"information\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'unprotectedAccidentArea'\" svgIcon=\"accident\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'vehicleObstruction'\" svgIcon=\"panne\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'vehicleOnWrongCarriageway'\" svgIcon=\"panne\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'visibilityReduced'\" svgIcon=\"meteo\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'weatherRelatedRoadConditions'\" svgIcon=\"meteo\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'winterDrivingManagement'\" svgIcon=\"meteo\"></mat-icon>\r\n\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'SMA'\" svgIcon=\"SMA\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'SEM'\" svgIcon=\"SEM\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'C38'\" svgIcon=\"C38\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'ARE'\" svgIcon=\"ARE\"></mat-icon>\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchCase=\"'DIR'\" svgIcon=\"DIR\"></mat-icon>\r\n\r\n\t\t\t<mat-icon [color]=\"color\" *ngSwitchDefault>not_listed_location</mat-icon>\r\n\t\t</ng-container>\r\n\t</ng-template>\r\n</ng-container>\r\n" }]
        }], ctorParameters: function () { return [{ type: MIconsService }]; }, propDecorators: { type: [{
                type: Input
            }], color: [{
                type: Input
            }] } });

/**
 * @component
 * @name MAffluence
 * @description
 * The `MAffluence` component displays the level of occupancy with a description.
 * This component uses Angular Material List and Icon to present the occupancy level.
 *
 * ### Usage example
 * ```html
 * <m-affluence [lvl]="1"></m-affluence>
 * ```
 *
 * @selector m-affluence
 * @standalone true
 * @module CommonModule, MatListModule, MatIconModule, MIcons
 */
class MAffluence {
    constructor() {
        /**
         * @private
         * List of available occupancy levels with their descriptions.
         */
        this.affluenceDescription = [
            { lvl: 1, name: 'Faible', description: 'C\'est calme, une place assise vous attend.', lightMode: false },
            { lvl: 2, name: 'Modérée', description: 'Il reste sans doute quelques places assises.', lightMode: false },
            { lvl: 3, name: 'Forte', description: 'Il n\'y aura probablement plus de places assises.', lightMode: false }
        ];
        /**
         * Array used to display occupancy information based on the selected level (`lvl`).
         */
        this.affluence = [];
    }
    /**
     * Initializes the `affluence` array based on the provided occupancy level (`lvl`).
     * If no level is defined, displays all descriptions.
     */
    ngOnInit() {
        if (this.lvl === undefined || this.lvl === null)
            this.affluence = this.affluenceDescription;
        else {
            this.affluence.push(this.affluenceDescription.find(a => a.lvl === this.lvl));
            if (this.lightMode)
                this.affluence[0].lightMode = this.lightMode;
        }
    }
}
MAffluence.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MAffluence, deps: [], target: i0.ɵɵFactoryTarget.Component });
MAffluence.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.10", type: MAffluence, isStandalone: true, selector: "m-affluence", inputs: { lvl: "lvl", lightMode: "lightMode" }, ngImport: i0, template: "<ng-container *ngIf=\"affluence && affluence.length !== 0\" class=\"m-theme important\">\n\n\t<ng-container *ngFor=\"let aff of affluence\">\n\t\t<mat-list *ngIf=\"!aff.lightMode; else lightMode\" class=\"m-ui-affluence\">\n\t\t\t<mat-list-item class=\"dark-overlay-8 light-overlay\">\n\t\t\t\t<div class=\"icon-content half-light\" [ngSwitch]=\"aff.lvl\">\n\t\t\t\t\t<m-icons matListItemAvatar *ngSwitchCase=\"2\" type=\"group\" data-type=\"moderee\">group</m-icons>\n\t\t\t\t\t<m-icons matListItemAvatar *ngSwitchCase=\"3\" type=\"group\" data-type=\"forte\">group</m-icons>\n\t\t\t\t\t<m-icons matListItemAvatar *ngSwitchDefault type=\"group\">group</m-icons>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"desc-content\">\n\t\t\t\t\t<div matListItemTitle>{{aff.name}}</div>\n\t\t\t\t\t<div matListItemLine>{{aff.description}}</div>\n\t\t\t\t</div>\n\t\t\t</mat-list-item>\n\t\t</mat-list>\n\n\t\t<ng-template #lightMode>\n\t\t\t<ng-container *ngFor=\"let aff of affluence\">\n\t\t\t\t<span [ngSwitch]=\"aff.lvl\" class=\"affluenceLight\">\n\t\t\t\t\t<ng-container *ngSwitchCase=\"2\">\n\t\t\t\t\t\t<m-icons matListItemAvatar type=\"group\" data-type=\"moderee\">group</m-icons>\n\t\t\t\t\t\t<span class=\"text-secondary aff-legend\">{{aff.name}}</span>\n\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t<ng-container *ngSwitchCase=\"3\">\n\t\t\t\t\t\t<m-icons matListItemAvatar type=\"group\" data-type=\"forte\">group</m-icons>\n\t\t\t\t\t\t<span class=\"text-secondary aff-legend\">{{aff.name}}</span>\n\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t<ng-container *ngSwitchDefault>\n\t\t\t\t\t\t<m-icons matListItemAvatar type=\"group\">group</m-icons>\n\t\t\t\t\t\t<span class=\"text-secondary aff-legend\">{{aff.name}}</span>\n\t\t\t\t\t</ng-container>\n\n\t\t\t\t</span>\n\t\t\t</ng-container>\n\t\t</ng-template>\n\n\t</ng-container>\n</ng-container>\n", styles: [".affluenceLight{display:flex;flex-direction:column;text-align:center;padding:0 8px;margin-right:16px;width:100%}.affluenceLight .aff-legend{font-size:12px;font-weight:700;line-height:2rem}mat-list.m-ui-affluence{display:flex;flex-direction:row;flex-wrap:wrap;width:100%}mat-list.m-ui-affluence mat-list-item{padding:0;height:auto}mat-list.m-ui-affluence mat-list-item .mdc-list-item__content>span{display:flex!important}mat-list.m-ui-affluence mat-list-item .mdc-list-item__content .icon-content{padding:16px!important;border-radius:8px 0 0 8px!important}mat-list.m-ui-affluence mat-list-item .mdc-list-item__content .desc-content{padding:0 16px}@media screen and (min-width: 1079px){mat-list h1.m-toolbar-title{padding-left:56px}}\n"], dependencies: [{ kind: "ngmodule", type: CommonModule }, { kind: "directive", type: i1$1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: i1$1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "directive", type: i1$1.NgSwitch, selector: "[ngSwitch]", inputs: ["ngSwitch"] }, { kind: "directive", type: i1$1.NgSwitchCase, selector: "[ngSwitchCase]", inputs: ["ngSwitchCase"] }, { kind: "directive", type: i1$1.NgSwitchDefault, selector: "[ngSwitchDefault]" }, { kind: "ngmodule", type: MatListModule }, { kind: "component", type: i2$1.MatList, selector: "mat-list", exportAs: ["matList"] }, { kind: "component", type: i2$1.MatListItem, selector: "mat-list-item, a[mat-list-item], button[mat-list-item]", inputs: ["activated"], exportAs: ["matListItem"] }, { kind: "directive", type: i2$1.MatListItemAvatar, selector: "[matListItemAvatar]" }, { kind: "directive", type: i2$1.MatListItemLine, selector: "[matListItemLine]" }, { kind: "directive", type: i2$1.MatListItemTitle, selector: "[matListItemTitle]" }, { kind: "ngmodule", type: MatIconModule }, { kind: "component", type: MIcons, selector: "m-icons", inputs: ["type", "color"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MAffluence, decorators: [{
            type: Component,
            args: [{ selector: 'm-affluence', standalone: true, imports: [CommonModule, MatListModule, MatIconModule, MIcons], template: "<ng-container *ngIf=\"affluence && affluence.length !== 0\" class=\"m-theme important\">\n\n\t<ng-container *ngFor=\"let aff of affluence\">\n\t\t<mat-list *ngIf=\"!aff.lightMode; else lightMode\" class=\"m-ui-affluence\">\n\t\t\t<mat-list-item class=\"dark-overlay-8 light-overlay\">\n\t\t\t\t<div class=\"icon-content half-light\" [ngSwitch]=\"aff.lvl\">\n\t\t\t\t\t<m-icons matListItemAvatar *ngSwitchCase=\"2\" type=\"group\" data-type=\"moderee\">group</m-icons>\n\t\t\t\t\t<m-icons matListItemAvatar *ngSwitchCase=\"3\" type=\"group\" data-type=\"forte\">group</m-icons>\n\t\t\t\t\t<m-icons matListItemAvatar *ngSwitchDefault type=\"group\">group</m-icons>\n\t\t\t\t</div>\n\n\t\t\t\t<div class=\"desc-content\">\n\t\t\t\t\t<div matListItemTitle>{{aff.name}}</div>\n\t\t\t\t\t<div matListItemLine>{{aff.description}}</div>\n\t\t\t\t</div>\n\t\t\t</mat-list-item>\n\t\t</mat-list>\n\n\t\t<ng-template #lightMode>\n\t\t\t<ng-container *ngFor=\"let aff of affluence\">\n\t\t\t\t<span [ngSwitch]=\"aff.lvl\" class=\"affluenceLight\">\n\t\t\t\t\t<ng-container *ngSwitchCase=\"2\">\n\t\t\t\t\t\t<m-icons matListItemAvatar type=\"group\" data-type=\"moderee\">group</m-icons>\n\t\t\t\t\t\t<span class=\"text-secondary aff-legend\">{{aff.name}}</span>\n\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t<ng-container *ngSwitchCase=\"3\">\n\t\t\t\t\t\t<m-icons matListItemAvatar type=\"group\" data-type=\"forte\">group</m-icons>\n\t\t\t\t\t\t<span class=\"text-secondary aff-legend\">{{aff.name}}</span>\n\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t<ng-container *ngSwitchDefault>\n\t\t\t\t\t\t<m-icons matListItemAvatar type=\"group\">group</m-icons>\n\t\t\t\t\t\t<span class=\"text-secondary aff-legend\">{{aff.name}}</span>\n\t\t\t\t\t</ng-container>\n\n\t\t\t\t</span>\n\t\t\t</ng-container>\n\t\t</ng-template>\n\n\t</ng-container>\n</ng-container>\n", styles: [".affluenceLight{display:flex;flex-direction:column;text-align:center;padding:0 8px;margin-right:16px;width:100%}.affluenceLight .aff-legend{font-size:12px;font-weight:700;line-height:2rem}mat-list.m-ui-affluence{display:flex;flex-direction:row;flex-wrap:wrap;width:100%}mat-list.m-ui-affluence mat-list-item{padding:0;height:auto}mat-list.m-ui-affluence mat-list-item .mdc-list-item__content>span{display:flex!important}mat-list.m-ui-affluence mat-list-item .mdc-list-item__content .icon-content{padding:16px!important;border-radius:8px 0 0 8px!important}mat-list.m-ui-affluence mat-list-item .mdc-list-item__content .desc-content{padding:0 16px}@media screen and (min-width: 1079px){mat-list h1.m-toolbar-title{padding-left:56px}}\n"] }]
        }], propDecorators: { lvl: [{
                type: Input
            }], lightMode: [{
                type: Input
            }] } });

/**
 * @component
 * @name MLogoLines
 * @description
 * A component that displays a logo for a given transport line. It customizes the size, padding,
 * and shape of the logo based on the input properties, and calculates optimal font size
 * for the line's name display.
 *
 * ### Usage example
 * ```html
 * <m-logo-lines [ligne]="lineData" [height]="50" [width]="50"></m-logo-lines>
 * ```
 */
class MLogoLines {
    constructor() {
        /** Padding coefficient for the top of the logo. */
        this.paddingCoefTop = 0.23;
        /** Padding coefficient for the left side of the logo. */
        this.paddingCoefLeft = 0.23;
        /** If `true`, displays the logo in a circular shape */
        this.isCircle = false;
        /** If `true`, applies rounded corners to the logo. */
        this.isRounded = false;
        /** If `true`, displays the logo in a square shape. */
        this.isSquare = false;
        /** If `true` display mat-checkbox behind logo-lines */
        this.isCheckbox = false;
        /** If `true`, displays the line's description. */
        this.description = false;
        /** If `true`, selects the line. */
        this.checked = false;
        /** The opacity of the logo. */
        this.opacity = 1;
        /** Emits when the line is clicked. */
        this.lineChange = new EventEmitter();
    }
    /**
     * @method onClick
     * @description
     * Emits the `lineClicked` event when the line is clicked.
     *
     * @returns {void}
     */
    clickLine(ligne, evt) {
        this.lineChange.emit({ line: ligne, checked: evt.checked });
    }
    /**
     * @method ngOnChanges
     * @description
     * Updates the component when input properties change. Calculates viewBox, font size, and other
     * properties based on the input line data and dimensions.
     *
     * @param {SimpleChanges} changes - The changes in input properties.
     * @returns {void}
     */
    ngOnChanges(changes) {
        if (!changes.ligne)
            return;
        this.shortName = this.ligne.shortName;
        this.longName = this.ligne.longName;
        this.lineType = this.ligne.type;
        if (this.ligne.type === 'SNC')
            this.shortName = 'TER';
        if (this.ligne.type === 'NAVETTE')
            this.shortName = this.ligne.shortName.substring(this.ligne.shortName.length - 1);
        if (changes.width || changes.height) {
            this.viewBox = `0 0 ${this.width} ${this.height}`;
            const fontHeight = this.height - (this.height * this.paddingCoefTop);
            const maxTextWidth = this.width - (this.width * this.paddingCoefLeft);
            const fzh = (0.8 / 16) * fontHeight;
            const fzw = (0.8 / 8) * (maxTextWidth / this.shortName.length);
            this.fontSize = `${Math.min(fzw, fzh)}em`;
        }
    }
    /**
     * Attribute class for any disturbances
     * @returns {string}
     */
    getClass() {
        if (this.disturbance && this.disturbance.hasDisturbance) {
            const hasOuter = this.disturbance.outer ? 'outer' : '';
            const hasDisturbance = this.disturbance.hasDisturbance ? 'has-disturbance' : '';
            let nsv = '';
            if (this.disturbance.nsv) {
                nsv = `disturbance-${this.disturbance.nsv}`;
            }
            return `${hasOuter} ${hasDisturbance} ${nsv}`;
        }
    }
}
MLogoLines.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MLogoLines, deps: [], target: i0.ɵɵFactoryTarget.Component });
MLogoLines.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.10", type: MLogoLines, isStandalone: true, selector: "m-logo-lines", inputs: { ligne: "ligne", paddingCoefTop: "paddingCoefTop", paddingCoefLeft: "paddingCoefLeft", height: "height", width: "width", isCircle: "isCircle", isRounded: "isRounded", isSquare: "isSquare", isCheckbox: "isCheckbox", description: "description", checked: "checked", opacity: "opacity", disturbance: "disturbance" }, outputs: { lineChange: "lineChange" }, host: { properties: { "style.height.px": "this.height", "style.width.px": "this.width" } }, usesOnChanges: true, ngImport: i0, template: "<ng-container>\r\n\t<ng-container *ngIf=\"!isCheckbox else logoCheckbox\">\r\n\t\t<ng-container *ngTemplateOutlet=\"logolines\"></ng-container>\r\n\t</ng-container>\r\n\r\n\t<ng-template #logoCheckbox>\r\n\t\t<mat-checkbox labelPosition=\"before\" type=\"checkbox\"\r\n\t\t\t\t\t  [attr.id]=\"ligne.id\"\r\n\t\t\t\t\t  [class.description]=\"description\"\r\n\t\t\t\t\t  [class.hideInput]=\"!description\"\r\n\t\t\t\t\t  [checked]=\"checked\"\r\n\t\t\t\t\t  (change)=\"clickLine(ligne, $event)\"\r\n\t\t\t\t\t  class=\"m-logo-line-checkbox\"\r\n\t\t>\r\n\t\t\t<ng-container *ngTemplateOutlet=\"logolines\"></ng-container>\r\n\t\t</mat-checkbox>\r\n\t</ng-template>\r\n\r\n</ng-container>\r\n\r\n<ng-template #logolines>\r\n\t<div class=\"m-logo-line-icons\">\r\n\t\t<svg\r\n\t\t\tclass=\"m-logo-line\"\r\n\t\t\t[ngStyle]=\"{\r\n\t\t\t\t'width.px': width,\r\n\t\t\t\t'height.px': height,\r\n\t\t\t\t'background': '#'+(ligne?.color ||'999999'),\r\n\t\t\t\t'font-size': fontSize,\r\n\t\t\t\t'opacity': opacity,\r\n\t\t\t\t'checkbox': disturbance?.hasDisturbance\r\n\t\t\t}\"\r\n\t\t\t[ngClass]=\"getClass()\"\r\n\t\t\t[attr.viewBox]=\"viewBox\"\r\n\t\t\t[class.circle]=\"['TRAM', 'CHRONO', 'NAVETTE', 'CHRONO_PERI'].includes(lineType) || isCircle\"\r\n\t\t\t[class.rounded]=\"!['CHRONO', 'NAVETTE', 'TRAM', 'CHRONO_PERI'].includes(lineType) || isRounded\"\r\n\t\t\t[class.square]=\"isSquare\"\r\n\t\t>\r\n\t\t\t<title>{{longName}}</title>\r\n\t\t\t<text x=\"51%\" y=\"50%\" text-anchor=\"middle\" dy=\"0.34em\" [attr.fill]=\"'#'+(ligne?.textColor||'ffffff')\">{{shortName}}</text>\r\n\t\t</svg>\r\n\t\t<svg [ngStyle]=\"{\r\n\t\t\t\t'width.px': width,\r\n\t\t\t\t'height.px': height,\r\n\t\t\t\t'opacity': opacity\r\n\t\t\t}\" viewBox=\"0 0 59.3 40.1\" *ngIf=\"['NAVETTE'].includes(lineType)\" class='relais'>\r\n\t\t\t<defs>\r\n\t\t\t\t<style>\r\n\t\t\t\t\t.relais-cls-1 {fill: #fff; stroke: #ea541e; stroke-miterlimit: 10;}\r\n\t\t\t\t\t.relais-cls-2 {fill: #ea541e; stroke-width: 0;}\r\n\t\t\t\t</style>\r\n\t\t\t</defs>\r\n\t\t\t<g transform=\"translate(0 16)\">\r\n\t\t\t\t<rect class=\"relais-cls-1\" x=\"3\" y=\"7.9\" width=\"53.8\" height=\"31.5\" rx=\"1.7\" ry=\"1.7\" transform=\"translate(-1.8 0) rotate(-5.5)\"/>\r\n\t\t\t\t<path class=\"relais-cls-2\" d=\"M17.5,19.6c.9,0,1.7,0,2.5.2.8.2,1.3.6,1.8,1.2s.9,1.1,1.2,1.9.4,1.5.7,2.2c0,.8.1,1.5,0,2.1,0,.8-.3,1.4-.7,1.9s-1.1,1.3-1.8,1.8-1.4.7-2.4.8-1.7,0-2.5-.2-1.4-.8-2.1-1.5h-.2s0,.2,0,.3v.8c.1.3,0,.5,0,.8l-.4.5c-.3.2-.6,0-.9,0-.6,0-1.1-.4-1.4-.8-.3-.4-.5-.9-.7-1.3-.2-.4-.4-.9-.4-1.5s-.2-.9-.3-1l-.2-2.6c0-.8,0-1.7,0-2.4,0-.8,0-1.5.2-2.5s.3-1.7.4-2.6c0-.8.3-1.4.4-2,.2-.6.3-1.2.7-1.7.2-.6.7-1.1,1.1-1.6s.8-1,1.5-1.5c.6-.5,1.3-.7,2.1-.8s1.5,0,2.2.4c.6.2,1.3.8,1.7,1.4s.6,1.3.5,2.1c0,.6-.2,1.2-.4,1.7-.1.5-.4,1.1-.6,1.6s-.5,1-.8,1.4c-.3.2-.6.5-.8.8M20.8,26.7c.2-.8.3-1.4,0-2.1,0-.8-.3-1.3-.6-1.9s-.7-1-1.2-1.3c-.5-.3-1-.5-1.3-.5-.5-.1-1.1,0-1.4.1-.5,0-.7.2-1.2.4l-.9.5c-.1.2-.4.3-.4.6-.1.3-.2.8-.3,1.2s0,.9,0,1.5l.9-.5c.3-.2.6-.2.9-.4.3,0,.6-.2.9,0,.3,0,.5.1.5.1q.2.1,0,.3c-.1.2-.3.2-.6.4s-.6.4-.8.7c-.3.3-.5.7-.6,1.3-.1.5,0,.8.1,1.1s.4.6.8.8c.5.1.9.2,1.4.2s1-.3,1.5-.4c.4-.2.9-.5,1.3-.9.6-.4.8-.7,1-1.2M17.3,12.9c-.3-.1-.6,0-.9.2-.3.3-.7.7-1,1.2-.3.5-.5,1.1-.7,1.7-.2.6-.5,1.3-.6,1.7s-.2.9-.2,1.4,0,.5.2.4c.6,0,1.2-.4,1.6-.8.4-.3.8-.8,1.2-1.3.3-.5.5-1,.8-1.6.1-.5.2-1.1.3-1.5v-1.2c-.3,0-.4-.3-.7-.2\"/>\r\n\t\t\t\t<path class=\"relais-cls-2\" d=\"M28.2,18.7v1.1c.1.5.2,1.1.3,1.7l.2,2c0,.6.3,1.2.3,1.8.2.6.4,1,.6,1.3.2.3.5.4.8.4.5,0,.9-.2,1.2-.6.3-.3.5-.8.8-1.4.2-.6.3-1.2.4-1.9,0-.6.2-1.4.1-2,0-.6,0-1.2,0-1.8s-.1-1.1,0-1.4c0-.3,0-.6.2-.8.1-.2.4-.3.7-.4s.6,0,.9,0l.9.4c.3.1.5.4.8.5.2.3.3.4.4.7v1.4c.2.6,0,1.2.2,2l.2,2.3c0,.8.1,1.5.4,2.1s.4,1.2.6,1.6c.3.4.7.7,1.1.8s.6.2.8.4.2.3,0,.5-.3.3-.6.4c-.3.2-.8,0-1.4.1-.9,0-1.7-.3-2.4-.8s-1-1-1.3-1.4c-.4-.6-.6-1.3-.7-2.1-.2.8-.5,1.6-.9,2.2-.2.6-.8,1.1-1.2,1.5-.6.5-1.3.6-2.1.7-.9,0-1.5-.3-2.2-.6-.6-.4-1.1-.8-1.5-1.4-.4-.6-.7-1.1-.9-1.9s-.4-1.3-.5-2.1-.3-1.2-.3-1.6l-.2-1.7c0-.5,0-.9,0-1.2.1-.3,0-.6.2-.8.1-.2.4-.2.7-.4.4-.2.7-.2,1.2-.3s.8,0,1.1,0c.6,0,.8.2.8.7\"/>\r\n\t\t\t\t<path class=\"relais-cls-2\" d=\"M47.5,13.8c.2.1,0,.3,0,.5-.1.2-.3.2-.4.3s-.4.2-.6.4c-.3.2-.4.3-.7.7-.3.3-.4.6-.5,1-.1.3,0,.8,0,1.4s.3,1.2.6,1.6c.4.6.7,1,1.2,1.6.5.4,1,1,1.5,1.4.5.4,1,1,1.5,1.4.5.4.9,1,1.1,1.6.4.6.4,1,.5,1.6,0,.6-.4,1.1-.8,1.6-.6.5-1.1.9-1.9,1.1s-1.6.5-2.5.5-1.8.2-2.6,0c-.8,0-1.5-.3-2.2-.6s-1-.8-1.2-1.3c-.4-.9-.4-1.5-.4-2.1,0-.6.2-1.1.3-1.5.3-.5.4-.8.8-1,.3-.2.6-.5.7-.5.4-.3.7-.4.9,0s0,.8-.2,1.2c-.1.3,0,.6,0,.9.2.3.4.7.7,1s.8.5,1.3.6,1.1.2,1.7.1.8,0,1-.3c.4-.2.7-.4.9-.5.3-.2.4-.5.4-.8s0-.6-.4-.9-.8-.7-1.5-1.2-1.5-.9-2.1-1.6c-.8-.5-1.5-1.2-2-1.9s-.9-1.4-1-2.2c-.1-1.5,0-2.6.4-3.2.4-.8,1.3-1.2,2.4-1.6.6-.2,1-.3,1.3-.3s.6,0,.9,0c.2.1.3.1.5.3,0,.6.2.7.2.9\"/>\r\n\t\t\t</g>\r\n\t\t</svg>\r\n\t</div>\r\n</ng-template>\r\n", styles: [":host{position:relative}:host div.m-logo-line-icons{position:relative;display:flex}:host div.m-logo-line-icons svg{box-sizing:border-box;font-family:Arial,serif;font-weight:700;-webkit-user-select:none;-moz-user-select:none;user-select:none}:host div.m-logo-line-icons svg a.relais{border:none!important}:host div.m-logo-line-icons svg.checkbox{border:4px solid transparent;transition:all .25s}:host div.m-logo-line-icons svg.relais{position:absolute;top:4px;left:0}:host div.m-logo-line-icons svg.square{border-radius:unset}\n"], dependencies: [{ kind: "ngmodule", type: CommonModule }, { kind: "directive", type: i1$1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { kind: "directive", type: i1$1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "directive", type: i1$1.NgTemplateOutlet, selector: "[ngTemplateOutlet]", inputs: ["ngTemplateOutletContext", "ngTemplateOutlet", "ngTemplateOutletInjector"] }, { kind: "directive", type: i1$1.NgStyle, selector: "[ngStyle]", inputs: ["ngStyle"] }, { kind: "ngmodule", type: MatCheckboxModule }, { kind: "component", type: i2$2.MatCheckbox, selector: "mat-checkbox", inputs: ["disableRipple", "color", "tabIndex"], exportAs: ["matCheckbox"] }], changeDetection: i0.ChangeDetectionStrategy.OnPush });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MLogoLines, decorators: [{
            type: Component,
            args: [{ selector: 'm-logo-lines', standalone: true, imports: [CommonModule, MatCheckboxModule], changeDetection: ChangeDetectionStrategy.OnPush, template: "<ng-container>\r\n\t<ng-container *ngIf=\"!isCheckbox else logoCheckbox\">\r\n\t\t<ng-container *ngTemplateOutlet=\"logolines\"></ng-container>\r\n\t</ng-container>\r\n\r\n\t<ng-template #logoCheckbox>\r\n\t\t<mat-checkbox labelPosition=\"before\" type=\"checkbox\"\r\n\t\t\t\t\t  [attr.id]=\"ligne.id\"\r\n\t\t\t\t\t  [class.description]=\"description\"\r\n\t\t\t\t\t  [class.hideInput]=\"!description\"\r\n\t\t\t\t\t  [checked]=\"checked\"\r\n\t\t\t\t\t  (change)=\"clickLine(ligne, $event)\"\r\n\t\t\t\t\t  class=\"m-logo-line-checkbox\"\r\n\t\t>\r\n\t\t\t<ng-container *ngTemplateOutlet=\"logolines\"></ng-container>\r\n\t\t</mat-checkbox>\r\n\t</ng-template>\r\n\r\n</ng-container>\r\n\r\n<ng-template #logolines>\r\n\t<div class=\"m-logo-line-icons\">\r\n\t\t<svg\r\n\t\t\tclass=\"m-logo-line\"\r\n\t\t\t[ngStyle]=\"{\r\n\t\t\t\t'width.px': width,\r\n\t\t\t\t'height.px': height,\r\n\t\t\t\t'background': '#'+(ligne?.color ||'999999'),\r\n\t\t\t\t'font-size': fontSize,\r\n\t\t\t\t'opacity': opacity,\r\n\t\t\t\t'checkbox': disturbance?.hasDisturbance\r\n\t\t\t}\"\r\n\t\t\t[ngClass]=\"getClass()\"\r\n\t\t\t[attr.viewBox]=\"viewBox\"\r\n\t\t\t[class.circle]=\"['TRAM', 'CHRONO', 'NAVETTE', 'CHRONO_PERI'].includes(lineType) || isCircle\"\r\n\t\t\t[class.rounded]=\"!['CHRONO', 'NAVETTE', 'TRAM', 'CHRONO_PERI'].includes(lineType) || isRounded\"\r\n\t\t\t[class.square]=\"isSquare\"\r\n\t\t>\r\n\t\t\t<title>{{longName}}</title>\r\n\t\t\t<text x=\"51%\" y=\"50%\" text-anchor=\"middle\" dy=\"0.34em\" [attr.fill]=\"'#'+(ligne?.textColor||'ffffff')\">{{shortName}}</text>\r\n\t\t</svg>\r\n\t\t<svg [ngStyle]=\"{\r\n\t\t\t\t'width.px': width,\r\n\t\t\t\t'height.px': height,\r\n\t\t\t\t'opacity': opacity\r\n\t\t\t}\" viewBox=\"0 0 59.3 40.1\" *ngIf=\"['NAVETTE'].includes(lineType)\" class='relais'>\r\n\t\t\t<defs>\r\n\t\t\t\t<style>\r\n\t\t\t\t\t.relais-cls-1 {fill: #fff; stroke: #ea541e; stroke-miterlimit: 10;}\r\n\t\t\t\t\t.relais-cls-2 {fill: #ea541e; stroke-width: 0;}\r\n\t\t\t\t</style>\r\n\t\t\t</defs>\r\n\t\t\t<g transform=\"translate(0 16)\">\r\n\t\t\t\t<rect class=\"relais-cls-1\" x=\"3\" y=\"7.9\" width=\"53.8\" height=\"31.5\" rx=\"1.7\" ry=\"1.7\" transform=\"translate(-1.8 0) rotate(-5.5)\"/>\r\n\t\t\t\t<path class=\"relais-cls-2\" d=\"M17.5,19.6c.9,0,1.7,0,2.5.2.8.2,1.3.6,1.8,1.2s.9,1.1,1.2,1.9.4,1.5.7,2.2c0,.8.1,1.5,0,2.1,0,.8-.3,1.4-.7,1.9s-1.1,1.3-1.8,1.8-1.4.7-2.4.8-1.7,0-2.5-.2-1.4-.8-2.1-1.5h-.2s0,.2,0,.3v.8c.1.3,0,.5,0,.8l-.4.5c-.3.2-.6,0-.9,0-.6,0-1.1-.4-1.4-.8-.3-.4-.5-.9-.7-1.3-.2-.4-.4-.9-.4-1.5s-.2-.9-.3-1l-.2-2.6c0-.8,0-1.7,0-2.4,0-.8,0-1.5.2-2.5s.3-1.7.4-2.6c0-.8.3-1.4.4-2,.2-.6.3-1.2.7-1.7.2-.6.7-1.1,1.1-1.6s.8-1,1.5-1.5c.6-.5,1.3-.7,2.1-.8s1.5,0,2.2.4c.6.2,1.3.8,1.7,1.4s.6,1.3.5,2.1c0,.6-.2,1.2-.4,1.7-.1.5-.4,1.1-.6,1.6s-.5,1-.8,1.4c-.3.2-.6.5-.8.8M20.8,26.7c.2-.8.3-1.4,0-2.1,0-.8-.3-1.3-.6-1.9s-.7-1-1.2-1.3c-.5-.3-1-.5-1.3-.5-.5-.1-1.1,0-1.4.1-.5,0-.7.2-1.2.4l-.9.5c-.1.2-.4.3-.4.6-.1.3-.2.8-.3,1.2s0,.9,0,1.5l.9-.5c.3-.2.6-.2.9-.4.3,0,.6-.2.9,0,.3,0,.5.1.5.1q.2.1,0,.3c-.1.2-.3.2-.6.4s-.6.4-.8.7c-.3.3-.5.7-.6,1.3-.1.5,0,.8.1,1.1s.4.6.8.8c.5.1.9.2,1.4.2s1-.3,1.5-.4c.4-.2.9-.5,1.3-.9.6-.4.8-.7,1-1.2M17.3,12.9c-.3-.1-.6,0-.9.2-.3.3-.7.7-1,1.2-.3.5-.5,1.1-.7,1.7-.2.6-.5,1.3-.6,1.7s-.2.9-.2,1.4,0,.5.2.4c.6,0,1.2-.4,1.6-.8.4-.3.8-.8,1.2-1.3.3-.5.5-1,.8-1.6.1-.5.2-1.1.3-1.5v-1.2c-.3,0-.4-.3-.7-.2\"/>\r\n\t\t\t\t<path class=\"relais-cls-2\" d=\"M28.2,18.7v1.1c.1.5.2,1.1.3,1.7l.2,2c0,.6.3,1.2.3,1.8.2.6.4,1,.6,1.3.2.3.5.4.8.4.5,0,.9-.2,1.2-.6.3-.3.5-.8.8-1.4.2-.6.3-1.2.4-1.9,0-.6.2-1.4.1-2,0-.6,0-1.2,0-1.8s-.1-1.1,0-1.4c0-.3,0-.6.2-.8.1-.2.4-.3.7-.4s.6,0,.9,0l.9.4c.3.1.5.4.8.5.2.3.3.4.4.7v1.4c.2.6,0,1.2.2,2l.2,2.3c0,.8.1,1.5.4,2.1s.4,1.2.6,1.6c.3.4.7.7,1.1.8s.6.2.8.4.2.3,0,.5-.3.3-.6.4c-.3.2-.8,0-1.4.1-.9,0-1.7-.3-2.4-.8s-1-1-1.3-1.4c-.4-.6-.6-1.3-.7-2.1-.2.8-.5,1.6-.9,2.2-.2.6-.8,1.1-1.2,1.5-.6.5-1.3.6-2.1.7-.9,0-1.5-.3-2.2-.6-.6-.4-1.1-.8-1.5-1.4-.4-.6-.7-1.1-.9-1.9s-.4-1.3-.5-2.1-.3-1.2-.3-1.6l-.2-1.7c0-.5,0-.9,0-1.2.1-.3,0-.6.2-.8.1-.2.4-.2.7-.4.4-.2.7-.2,1.2-.3s.8,0,1.1,0c.6,0,.8.2.8.7\"/>\r\n\t\t\t\t<path class=\"relais-cls-2\" d=\"M47.5,13.8c.2.1,0,.3,0,.5-.1.2-.3.2-.4.3s-.4.2-.6.4c-.3.2-.4.3-.7.7-.3.3-.4.6-.5,1-.1.3,0,.8,0,1.4s.3,1.2.6,1.6c.4.6.7,1,1.2,1.6.5.4,1,1,1.5,1.4.5.4,1,1,1.5,1.4.5.4.9,1,1.1,1.6.4.6.4,1,.5,1.6,0,.6-.4,1.1-.8,1.6-.6.5-1.1.9-1.9,1.1s-1.6.5-2.5.5-1.8.2-2.6,0c-.8,0-1.5-.3-2.2-.6s-1-.8-1.2-1.3c-.4-.9-.4-1.5-.4-2.1,0-.6.2-1.1.3-1.5.3-.5.4-.8.8-1,.3-.2.6-.5.7-.5.4-.3.7-.4.9,0s0,.8-.2,1.2c-.1.3,0,.6,0,.9.2.3.4.7.7,1s.8.5,1.3.6,1.1.2,1.7.1.8,0,1-.3c.4-.2.7-.4.9-.5.3-.2.4-.5.4-.8s0-.6-.4-.9-.8-.7-1.5-1.2-1.5-.9-2.1-1.6c-.8-.5-1.5-1.2-2-1.9s-.9-1.4-1-2.2c-.1-1.5,0-2.6.4-3.2.4-.8,1.3-1.2,2.4-1.6.6-.2,1-.3,1.3-.3s.6,0,.9,0c.2.1.3.1.5.3,0,.6.2.7.2.9\"/>\r\n\t\t\t</g>\r\n\t\t</svg>\r\n\t</div>\r\n</ng-template>\r\n", styles: [":host{position:relative}:host div.m-logo-line-icons{position:relative;display:flex}:host div.m-logo-line-icons svg{box-sizing:border-box;font-family:Arial,serif;font-weight:700;-webkit-user-select:none;-moz-user-select:none;user-select:none}:host div.m-logo-line-icons svg a.relais{border:none!important}:host div.m-logo-line-icons svg.checkbox{border:4px solid transparent;transition:all .25s}:host div.m-logo-line-icons svg.relais{position:absolute;top:4px;left:0}:host div.m-logo-line-icons svg.square{border-radius:unset}\n"] }]
        }], propDecorators: { ligne: [{
                type: Input
            }], paddingCoefTop: [{
                type: Input
            }], paddingCoefLeft: [{
                type: Input
            }], height: [{
                type: Input
            }, {
                type: HostBinding,
                args: ['style.height.px']
            }], width: [{
                type: Input
            }, {
                type: HostBinding,
                args: ['style.width.px']
            }], isCircle: [{
                type: Input
            }], isRounded: [{
                type: Input
            }], isSquare: [{
                type: Input
            }], isCheckbox: [{
                type: Input
            }], description: [{
                type: Input
            }], checked: [{
                type: Input
            }], opacity: [{
                type: Input
            }], disturbance: [{
                type: Input
            }], lineChange: [{
                type: Output
            }] } });

// Epochs
const epochs = [
    ['an(s)', 31536000],
    ['mois', 2592000],
    ['j', 86400],
    ['h', 3600],
    ['mn', 60],
    ['s', 1]
];
class RelativeDatePipe {
    transform(dateStamp) {
        const tempDate = new Date(dateStamp);
        if (tempDate.getFullYear() === 1970)
            return 'Jamais';
        else {
            let timeAgoInSeconds = Math.floor((new Date().getTime() - tempDate.getTime()) / 1000);
            let { interval, epoch } = this.getDuration(timeAgoInSeconds);
            let suffix = interval === 1 ? '' : ''; //pluriels supprimés car abbreviations
            return `il y a ${interval} ${epoch} ${suffix}`;
        }
    }
    getDuration(timeAgoInSeconds) {
        for (let [name, seconds] of epochs) {
            let interval = Math.floor(timeAgoInSeconds / seconds);
            if (interval >= 1) {
                return {
                    interval: interval,
                    epoch: name
                };
            }
        }
        return {
            interval: 0,
            epoch: 's'
        };
    }
    ;
}
RelativeDatePipe.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: RelativeDatePipe, deps: [], target: i0.ɵɵFactoryTarget.Pipe });
RelativeDatePipe.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "14.0.0", version: "15.2.10", ngImport: i0, type: RelativeDatePipe, isStandalone: true, name: "relativeDate" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: RelativeDatePipe, decorators: [{
            type: Pipe,
            args: [{
                    standalone: true,
                    name: 'relativeDate'
                }]
        }] });

/**
 * Enumeration of the different types of headers supported in the table.
 */
var HeaderTypes;
(function (HeaderTypes) {
    /** Text-based column. */
    HeaderTypes["TEXT"] = "text";
    /** Relative Date-based */
    HeaderTypes["RELATIVE_DATE"] = "relative_date";
    /** Date-based column. */
    HeaderTypes["DATE"] = "date";
    /** Numeric column. */
    HeaderTypes["NUMBER"] = "number";
    /** Boolean column. */
    HeaderTypes["BOOLEAN"] = "boolean";
    /** Column for action buttons. */
    HeaderTypes["ACTIONS"] = "actions";
    /** Dropdown selection in a row. */
    HeaderTypes["SELECT"] = "select";
    /** Checkbox for selecting rows. */
    HeaderTypes["SELECTION"] = "selection";
    /** Column for matching criteria. */
    HeaderTypes["MATCHING"] = "matching";
    /** Array data column. */
    HeaderTypes["ARRAY"] = "array";
    /** List data column. */
    HeaderTypes["LIST"] = "list";
    /** Column with anchor (`<a>`) links. */
    HeaderTypes["LINK"] = "link";
    /** Icon column.*/
    HeaderTypes["ICON"] = "icon";
    /** Transport line column. */
    HeaderTypes["LINE"] = "line";
})(HeaderTypes || (HeaderTypes = {}));

/**
 * @component
 * @name MTable
 * @description
 * A configurable table component that extends Angular Material's mat-table.
 * It includes features like filtering, sorting, pagination, dynamic content, and row selection.
 *
 * ### Usage example
 * ```html
 * <m-table [data]="tableData" [headers]="tableHeaders" [config]="tableConfig"></m-table>
 * ```
 */
class MTable {
    /** init dataSource */
    constructor() {
        /** Actions configuration for rows or buttons. */
        this.actions = {};
        /** Emits changes in row selection. */
        this.selectionChange = new EventEmitter();
        /** Emits changes filter */
        this.filter = new EventEmitter();
        this.header = [];
        this.selection = new SelectionModel(true, []);
        this.Object = Object;
        this.HeaderTypes = HeaderTypes;
        this.dataSource = new MatTableDataSource([]);
    }
    /**
     * @method ngAfterViewInit
     * @description
     * Initializes paginator and sorting after view is fully initialized.
     */
    ngAfterViewInit() {
        if (this.config.pageSize.length > 0)
            this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }
    /**
     * @method ngOnChanges
     * @description
     * Detects changes in input data and updates the table if necessary.
     * @param {SimpleChanges} changes - Object containing changes in input properties.
     */
    ngOnChanges(changes) {
        // Detect changes on data and headers
        const data = changes.data;
        const headers = changes.headers;
        if (data && data.previousValue === undefined || headers) {
            this.initData();
        }
        else if (JSON.stringify(data.currentValue) !== JSON.stringify(data.previousValue)) {
            // Check if data send is different from previous data
            // Add new element to dataSource
            if (data.currentValue.length !== this.dataSource.data.length)
                this.initData();
            else {
                const newData = [...this.dataSource.data];
                if (typeof data === 'undefined')
                    return;
                if (data.currentValue.length !== this.dataSource.data.length)
                    this.initData();
                data.currentValue.forEach((oneCurr, index) => {
                    if (JSON.stringify(oneCurr) !== JSON.stringify(this.dataSource.data[index]))
                        newData[index] = oneCurr;
                });
                this.orderBy(newData);
                this.dataSource.data = newData;
            }
        }
    }
    /**
     * Initializes table data and headers.
     */
    initData() {
        this.initHeader();
        this.initDataSource();
    }
    /**
     * Initializes the data source based on headers configuration.
     */
    initDataSource() {
        if (this.data && this.headers) {
            // Map again the data to transform it if other keys found
            const newData = this.data.map(row => {
                // Add old Key to new key
                const transformedRow = { ...row };
                this.headers.forEach((header) => {
                    if (header.type !== HeaderTypes.SELECTION)
                        transformedRow[header.key] = this.getCellData(row, header);
                });
                return transformedRow;
            });
            this.orderBy(newData);
            this.dataSource.data = newData;
        }
    }
    /**
     * Sorts data according to the default order in the configuration.
     * @param data Data array to sort.
     */
    orderBy(data) {
        if (this.config && this.config.defaultOrderColumn && this.config.defaultOrderColumn.column) {
            const column = this.config.defaultOrderColumn.column;
            const order = this.config.defaultOrderColumn.order;
            if (column && order) {
                const value = order === 'asc' ? -1 : 1;
                data.sort((a, b) => a[column] > b[column] ? value : -value);
            }
        }
    }
    /**
     * Retrieves cell data, applying any header configuration options.
     * @param row The data row.
     * @param data Header configuration.
     * @return Processed cell data.
     */
    getCellData(row, data) {
        let result = null;
        if (row) {
            result = data.otherKey && data.otherKey.length > 0 ? this.getNestedValue(row, data.otherKey) : row[data.key];
            if (data.options && data.options.maxLength && result)
                return result.length > data.options.maxLength ? result.substring(0, data.options.maxLength) + '...' : result;
            if (data.options && Object.keys(data.options).length > 0) {
                if (data.options.removeSpace)
                    result = result.replace(/\s/g, '');
            }
        }
        return result;
    }
    /**
     * Retrieves a nested value from an object based on a list of keys.
     * @param obj The object to retrieve data from.
     * @param keyData Array of keys for nested object access.
     * @return The nested value.
     */
    getNestedValue(obj, keyData) {
        return keyData.reduce((nestedObj, currentKey) => {
            return nestedObj && nestedObj[currentKey];
        }, obj);
    }
    /**
     * Initializes header keys based on the headers input.
     * @return void
     */
    initHeader() {
        if (this.headers)
            this.header = this.headers.map((item) => item.key);
    }
    /**
     * Applies a filter to the table data based on input text.
     * @param event Filter input event.
     */
    applyFilter(event) {
        const filterValue = event.target.value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
        this.filter.emit({ filter: filterValue, data: this.dataSource.filteredData });
        if (this.config.pageSize.length > 0 && this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }
    /**
     * Toggles selection for all rows.
     * @param evt Event indicating the toggle state.
     */
    toggleAllRows(evt) {
        const select = !!evt;
        if (select && this.isAllSelected()) {
            this.selection.clear();
            this.selectionChange.emit(this.selection.selected);
            return;
        }
        this.selection.select(...this.dataSource.data);
        this.selectionChange.emit(this.selection.selected);
        return;
    }
    /**
     * Checks if all rows are selected.
     * @return True if all rows are selected.
     */
    isAllSelected() {
        const numSelected = this.selection.selected.length;
        const numRows = this.dataSource.data.length;
        return numSelected === numRows;
    }
    /**
     * Provides a label for the checkbox.
     * @param row Optional row data.
     * @return Checkbox label.
     */
    checkboxLabel(row) {
        if (!row)
            return `${this.isAllSelected() ? 'désélectionner' : 'sélectionner'} tous les éléments`;
        return `${this.selection.isSelected(row) ? 'désélectionner' : 'sélectionner'} la ligne ${row.position + 1}`;
    }
    /**
     * Builds CSS classes for a row based on header configuration.
     * @param data Header configuration.
     * @param row Row data.
     * @return Array of CSS classes.
     */
    buildClass(data, row) {
        if (data.options && typeof data.options.class !== 'undefined') {
            return data.options.class(row);
        }
        return [];
    }
    /**
     * Toggles selection for a specific row and emits the selection change.
     * @param evt The event triggering selection change.
     * @param row Row to toggle.
     */
    changeSelection(evt, row) {
        const select = evt ? this.selection.toggle(row) : null;
        this.selectionChange.emit(this.selection.selected);
    }
    /**
     * Checks if a specific row is selected.
     * @param row Row to check.
     * @return True if the row is selected.
     */
    isSelected(row) {
        return this.selection.isSelected(row);
    }
    /**
     * Open link and stop propagation
     * @param link : string
     * @param target : string
     * @param evt : Event
     * @return void
     */
    openLink(link, target, evt) {
        window.open(link, target);
        evt.stopImmediatePropagation();
    }
}
MTable.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MTable, deps: [], target: i0.ɵɵFactoryTarget.Component });
MTable.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.10", type: MTable, isStandalone: true, selector: "m-table", inputs: { data: "data", headers: "headers", config: "config", actions: "actions" }, outputs: { selectionChange: "selectionChange", filter: "filter" }, viewQueries: [{ propertyName: "table", first: true, predicate: ["table"], descendants: true }, { propertyName: "paginator", first: true, predicate: MatPaginator, descendants: true }, { propertyName: "sort", first: true, predicate: MatSort, descendants: true }], usesOnChanges: true, ngImport: i0, template: "<div class=\"m-table\">\n\t<div class=\"m-table-header dark-overlay-2 light-overlay-inner\" *ngIf=\"config.showFilter\">\n\t\t<div class=\"m-table-filter-content\">\n\t\t\t<ng-content select=\"[left]\"></ng-content>\n\t\t</div>\n\n\t\t<div class=\"m-table-filter\">\n\t\t\t<mat-form-field appearance=\"outline\">\n\t\t\t\t<mat-label>Filter</mat-label>\n\t\t\t\t<span matPrefix>&nbsp;<mat-icon>search</mat-icon>&nbsp;</span>\n\t\t\t\t<input (keyup)=\"applyFilter($event)\" matInput placeholder=\"Recherche\">\n\t\t\t</mat-form-field>\n\t\t</div>\n\t\t<div class=\"m-table-filter-content\">\n\t\t\t<ng-content select=\"[right]\"></ng-content>\n\t\t</div>\n\t</div>\n\n\t<div class=\"m-table-content\">\n\t\t<table [dataSource]=\"dataSource\" class=\"m-theme\" mat-table matSort #table>\n\t\t\t<ng-container *ngFor=\"let data of headers; let i = index\">\n\t\t\t\t<ng-container [matColumnDef]=\"data.key\">\n\t\t\t\t\t<!-- Colonne name -->\n\t\t\t\t\t<ng-container *ngIf=\"data.type !== HeaderTypes.SELECTION; else select\">\n\t\t\t\t\t\t<th *matHeaderCellDef class=\"title\" mat-header-cell mat-sort-header>{{ data.title }}</th>\n\t\t\t\t\t</ng-container>\n\t\t\t\t\t<ng-template #select>\n\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef>\n\t\t\t\t\t\t\t<mat-checkbox (change)=\"toggleAllRows($event)\"\n\t\t\t\t\t\t\t\t\t\t  [checked]=\"selection.hasValue() && isAllSelected()\"\n\t\t\t\t\t\t\t\t\t\t  [indeterminate]=\"selection.hasValue() && !isAllSelected()\"\n\t\t\t\t\t\t\t\t\t\t  [aria-label]=\"checkboxLabel()\">\n\t\t\t\t\t\t\t</mat-checkbox>\n\t\t\t\t\t\t</th>\n\t\t\t\t\t</ng-template>\n\n\t\t\t\t\t<td *matCellDef=\"let row\" mat-cell [ngClass]=\"buildClass(data, row)\">\n\n\t\t\t\t\t\t<ng-container [ngSwitch]=\"data.type\">\n\n\t\t\t\t\t\t\t<!-- RELATIVE DATE -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.RELATIVE_DATE\">\n\t\t\t\t\t\t\t\t<ng-container *ngIf=\"row[data.key]\">\n\t\t\t\t\t\t\t\t\t{{ row[data.key] | relativeDate }}\n\t\t\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- DATE -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.DATE\">\n\t\t\t\t\t\t\t\t<ng-container *ngIf=\"row[data.key]\">\n\t\t\t\t\t\t\t\t\t{{ row[data.key] | date: data.options && data.options.format ? data.options.format : 'dd/MM/yyyy H:mm' }}\n\t\t\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- SELECT -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.SELECT\">\n\t\t\t\t\t\t\t\t<ng-container *ngIf=\"data.selectOptions && data.selectOptions.method\">\n\t\t\t\t\t\t\t\t\t<mat-select [ngClass]=\"buildClass(data, row)\"\n\t\t\t\t\t\t\t\t\t\t\t\t(selectionChange)=\"data.selectOptions.method($event, row)\"\n\t\t\t\t\t\t\t\t\t\t\t\t[ngModel]=\"row[data.key]\">\n\t\t\t\t\t\t\t\t\t\t<mat-option *ngFor=\"let i of data.selectOptions.options\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t[value]=\"i\">{{ i }}\n\t\t\t\t\t\t\t\t\t\t</mat-option>\n\t\t\t\t\t\t\t\t\t</mat-select>\n\t\t\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- SELECTION -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.SELECTION\">\n\t\t\t\t\t\t\t\t<mat-checkbox (click)=\"$event.stopPropagation()\"\n\t\t\t\t\t\t\t\t\t\t\t  (change)=\"changeSelection($event, row)\"\n\t\t\t\t\t\t\t\t\t\t\t  [checked]=\"isSelected(row)\"\n\t\t\t\t\t\t\t\t\t\t\t  [aria-label]=\"checkboxLabel(row)\">\n\t\t\t\t\t\t\t\t</mat-checkbox>\n\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- BOOLEAN -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.BOOLEAN\">\n\t\t\t\t\t\t\t\t<mat-icon>{{ row[data.key] ? 'check' : 'close' }}</mat-icon>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- MATCHING -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.MATCHING\">\n\t\t\t\t\t\t\t\t<ng-container *ngIf=\"data.data && data.data[row[data.key]]\">\n\t\t\t\t\t\t\t\t\t{{ data.data[row[data.key]]}}\n\t\t\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- ICON -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.ICON\">\n\t\t\t\t\t\t\t\t<m-icons [type]=\"row[data.key]\"></m-icons>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- LINE -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.LINE\">\n\t\t\t\t\t\t\t\t<div class=\"lines\">\n\t\t\t\t\t\t\t\t\t<ul *ngIf=\"row[data.key] && Object.keys(row[data.key]).length !== 0\">\n\t\t\t\t\t\t\t\t\t\t<li *ngFor=\"let line of Object.keys(row[data.key])\">\n\t\t\t\t\t\t\t\t\t\t\t<m-logo-lines\n\t\t\t\t\t\t\t\t\t\t\t\t[ligne]=\"row[data.key][line]\"\n\t\t\t\t\t\t\t\t\t\t\t\t[height]=\"30\"\n\t\t\t\t\t\t\t\t\t\t\t\t[width]=\"row[data.key][line].id.includes('SEM') ? 30 : 50\"\n\t\t\t\t\t\t\t\t\t\t\t\t[isRounded]=\"!['TRAM', 'CHRONO'].includes(row[data.key][line].type)\"\n\t\t\t\t\t\t\t\t\t\t\t></m-logo-lines>\n\t\t\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- LIST -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.LIST\">\n\t\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t\t<li *ngFor=\"let item of row[data.key]; let isFirst = first; let isLast = last\"\n\t\t\t\t\t\t\t\t\t\t[class.first]=\"isFirst\"\n\t\t\t\t\t\t\t\t\t\t[class.last]=\"isLast\"\n\t\t\t\t\t\t\t\t\t\t[class.valid]=\"item == 'Valide'\"\n\t\t\t\t\t\t\t\t\t\t[class.invalid]=\"item == 'Invalide'\"\n\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t\t{{ item }}\n\t\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- LINK -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.LINK\">\n\t\t\t\t\t\t\t\t<div class=\"links\">\n\t\t\t\t\t\t\t\t\t<button mat-icon-button *ngIf=\"row[data.key]; else noLink\" (click)=\"openLink(row[data.key], '_blank', $event)\" color=\"primary\">\n\t\t\t\t\t\t\t\t\t\t<mat-icon>link</mat-icon>\n\t\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t\t\t<ng-template #noLink></ng-template>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- ARRAY -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.ARRAY\">\n\t\t\t\t\t\t\t\t<ng-container *ngIf=\"row[data.key] && row[data.key].length > 0\">\n\t\t\t\t\t\t\t\t\t<ng-container *ngFor=\"let item of row[data.key]; let last = last\">\n\t\t\t\t\t\t\t\t\t\t<span>{{ item }}</span>\n\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"!last\">, </span>\n\t\t\t\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- TEXT, NUMBER -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchDefault>\n\t\t\t\t\t\t\t\t<span>{{ row[data.key] }}</span>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- ACTIONS -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.ACTIONS\">\n\t\t\t\t\t\t\t\t<ng-container *ngIf=\"actions && actions.column && actions.column.length > 0\">\n\t\t\t\t\t\t\t\t\t<div class=\"actions\">\n\t\t\t\t\t\t\t\t\t\t<ng-container *ngFor=\"let a of actions.column\">\n\t\t\t\t\t\t\t\t\t\t\t<ng-container *ngIf=\"!a.isMenu; else btnMenu\">\n\t\t\t\t\t\t\t\t\t\t\t\t<button\n\t\t\t\t\t\t\t\t\t\t\t\t\t[disabled]=\"a.disabled(row)\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t(click)=\"a.action(row, $event)\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t[color]=\"a.color\"\n\t\t\t\t\t\t\t\t\t\t\t\t\tmat-icon-button\n\t\t\t\t\t\t\t\t\t\t\t\t\ttitle=\"{{a.title}}\"\n\t\t\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-icon>{{ a.icon }}</mat-icon>\n\t\t\t\t\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t\t\t\t\t\t<ng-template #btnMenu>\n\t\t\t\t\t\t\t\t\t\t\t\t<button mat-icon-button [matMenuTriggerFor]=\"menu\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t\taria-label=\"Actions\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-icon>more_vert</mat-icon>\n\t\t\t\t\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t\t\t\t\t\t<mat-menu class=\"actions-menu\" #menu=\"matMenu\" [xPosition]=\"'before'\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<button\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t[disabled]=\"a.disabled(row)\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t(click)=\"a.action(row, $event)\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tmat-menu-item\n\t\t\t\t\t\t\t\t\t\t\t\t\t\ttitle=\"{{a.title}}\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-icon [color]=\"a.color\">{{ a.icon }}</mat-icon>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span>{{a.title}}</span>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t\t\t\t\t\t</mat-menu>\n\t\t\t\t\t\t\t\t\t\t\t</ng-template>\n\t\t\t\t\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t</td>\n\n\t\t\t\t</ng-container>\n\n\t\t\t</ng-container>\n\n\t\t\t<tr *matHeaderRowDef=\"header; sticky: true\" mat-header-row></tr>\n\t\t\t<tr mat-row (click)=\"actions && actions.line && actions.line(row, $event)\"\n\t\t\t\t*matRowDef=\"let row; columns: header;\"\n\t\t\t\t[class.pointer]=\"!!actions.line\"\n\t\t\t>\n\t\t\t</tr>\n\n\t\t\t<!-- Row shown when there is no matching data. -->\n\t\t\t<tr *matNoDataRow class=\"mat-row empty\">\n\t\t\t\t<td class=\"mat-cell\" colspan=\"4\">No data matching the filter</td>\n\t\t\t</tr>\n\n\t\t</table>\n\t</div>\n\n\t<ng-container *ngIf=\"config.pageSize.length > 0\">\n\t\t<mat-paginator [pageSizeOptions]=\"config.pageSize\" aria-label=\"Select page\"></mat-paginator>\n\t</ng-container>\n\n</div>\n", styles: [".m-table .m-table-header{width:100%;display:flex;padding:8px}.m-table .m-table-header .m-table-filter{flex:1 1 auto;padding:8px}.m-table .m-table-header .m-table-filter mat-form-field{width:100%}.m-table .m-table-header .m-table-filter-content{display:flex;padding:8px}.m-table .m-table-content{max-height:calc(100vh - 221px);overflow:auto}.m-table .m-table-content .pointer{cursor:pointer}.m-table .m-table-content div.lines ul{list-style:none;display:flex;flex-direction:row;flex-wrap:wrap;margin:0;padding:0}.m-table .m-table-content div.lines ul li{margin:4px 4px 4px 0}.m-table .actions{display:flex}.cdk-overlay-pane,.mat-mdc-select-panel-above{width:100%!important}\n"], dependencies: [{ kind: "ngmodule", type: CommonModule }, { kind: "directive", type: i1$1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { kind: "directive", type: i1$1.NgForOf, selector: "[ngFor][ngForOf]", inputs: ["ngForOf", "ngForTrackBy", "ngForTemplate"] }, { kind: "directive", type: i1$1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "directive", type: i1$1.NgSwitch, selector: "[ngSwitch]", inputs: ["ngSwitch"] }, { kind: "directive", type: i1$1.NgSwitchCase, selector: "[ngSwitchCase]", inputs: ["ngSwitchCase"] }, { kind: "directive", type: i1$1.NgSwitchDefault, selector: "[ngSwitchDefault]" }, { kind: "pipe", type: i1$1.DatePipe, name: "date" }, { kind: "ngmodule", type: MatPaginatorModule }, { kind: "component", type: i2$3.MatPaginator, selector: "mat-paginator", inputs: ["disabled"], exportAs: ["matPaginator"] }, { kind: "ngmodule", type: MatTableModule }, { kind: "component", type: i3.MatTable, selector: "mat-table, table[mat-table]", exportAs: ["matTable"] }, { kind: "directive", type: i3.MatHeaderCellDef, selector: "[matHeaderCellDef]" }, { kind: "directive", type: i3.MatHeaderRowDef, selector: "[matHeaderRowDef]", inputs: ["matHeaderRowDef", "matHeaderRowDefSticky"] }, { kind: "directive", type: i3.MatColumnDef, selector: "[matColumnDef]", inputs: ["sticky", "matColumnDef"] }, { kind: "directive", type: i3.MatCellDef, selector: "[matCellDef]" }, { kind: "directive", type: i3.MatRowDef, selector: "[matRowDef]", inputs: ["matRowDefColumns", "matRowDefWhen"] }, { kind: "directive", type: i3.MatHeaderCell, selector: "mat-header-cell, th[mat-header-cell]" }, { kind: "directive", type: i3.MatCell, selector: "mat-cell, td[mat-cell]" }, { kind: "component", type: i3.MatHeaderRow, selector: "mat-header-row, tr[mat-header-row]", exportAs: ["matHeaderRow"] }, { kind: "component", type: i3.MatRow, selector: "mat-row, tr[mat-row]", exportAs: ["matRow"] }, { kind: "directive", type: i3.MatNoDataRow, selector: "ng-template[matNoDataRow]" }, { kind: "ngmodule", type: MatButtonModule }, { kind: "component", type: i2$4.MatIconButton, selector: "button[mat-icon-button]", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }, { kind: "ngmodule", type: MatMenuModule }, { kind: "component", type: i5.MatMenu, selector: "mat-menu", exportAs: ["matMenu"] }, { kind: "component", type: i5.MatMenuItem, selector: "[mat-menu-item]", inputs: ["disabled", "disableRipple", "role"], exportAs: ["matMenuItem"] }, { kind: "directive", type: i5.MatMenuTrigger, selector: "[mat-menu-trigger-for], [matMenuTriggerFor]", exportAs: ["matMenuTrigger"] }, { kind: "ngmodule", type: MatIconModule }, { kind: "component", type: i1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { kind: "ngmodule", type: MatCheckboxModule }, { kind: "component", type: i2$2.MatCheckbox, selector: "mat-checkbox", inputs: ["disableRipple", "color", "tabIndex"], exportAs: ["matCheckbox"] }, { kind: "ngmodule", type: MatSelectModule }, { kind: "component", type: i8.MatFormField, selector: "mat-form-field", inputs: ["hideRequiredMarker", "color", "floatLabel", "appearance", "subscriptSizing", "hintLabel"], exportAs: ["matFormField"] }, { kind: "directive", type: i8.MatLabel, selector: "mat-label" }, { kind: "directive", type: i8.MatPrefix, selector: "[matPrefix], [matIconPrefix], [matTextPrefix]", inputs: ["matTextPrefix"] }, { kind: "component", type: i9.MatSelect, selector: "mat-select", inputs: ["disabled", "disableRipple", "tabIndex", "hideSingleSelectionIndicator"], exportAs: ["matSelect"] }, { kind: "component", type: i10.MatOption, selector: "mat-option", exportAs: ["matOption"] }, { kind: "ngmodule", type: FormsModule }, { kind: "directive", type: i11.NgControlStatus, selector: "[formControlName],[ngModel],[formControl]" }, { kind: "directive", type: i11.NgModel, selector: "[ngModel]:not([formControlName]):not([formControl])", inputs: ["name", "disabled", "ngModel", "ngModelOptions"], outputs: ["ngModelChange"], exportAs: ["ngModel"] }, { kind: "ngmodule", type: MatInputModule }, { kind: "directive", type: i12.MatInput, selector: "input[matInput], textarea[matInput], select[matNativeControl],      input[matNativeControl], textarea[matNativeControl]", inputs: ["disabled", "id", "placeholder", "name", "required", "type", "errorStateMatcher", "aria-describedby", "value", "readonly"], exportAs: ["matInput"] }, { kind: "ngmodule", type: MatSortModule }, { kind: "directive", type: i13.MatSort, selector: "[matSort]", inputs: ["matSortDisabled", "matSortActive", "matSortStart", "matSortDirection", "matSortDisableClear"], outputs: ["matSortChange"], exportAs: ["matSort"] }, { kind: "component", type: i13.MatSortHeader, selector: "[mat-sort-header]", inputs: ["disabled", "mat-sort-header", "arrowPosition", "start", "sortActionDescription", "disableClear"], exportAs: ["matSortHeader"] }, { kind: "component", type: MIcons, selector: "m-icons", inputs: ["type", "color"] }, { kind: "component", type: MLogoLines, selector: "m-logo-lines", inputs: ["ligne", "paddingCoefTop", "paddingCoefLeft", "height", "width", "isCircle", "isRounded", "isSquare", "isCheckbox", "description", "checked", "opacity", "disturbance"], outputs: ["lineChange"] }, { kind: "ngmodule", type: MatRippleModule }, { kind: "pipe", type: RelativeDatePipe, name: "relativeDate" }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MTable, decorators: [{
            type: Component,
            args: [{ selector: 'm-table', standalone: true, imports: [
                        CommonModule, MatPaginatorModule, MatTableModule, MatButtonModule, MatMenuModule,
                        MatIconModule, MatCheckboxModule, MatSelectModule, FormsModule, MatInputModule, MatSortModule, MIcons, MLogoLines, MatRippleModule, RelativeDatePipe
                    ], template: "<div class=\"m-table\">\n\t<div class=\"m-table-header dark-overlay-2 light-overlay-inner\" *ngIf=\"config.showFilter\">\n\t\t<div class=\"m-table-filter-content\">\n\t\t\t<ng-content select=\"[left]\"></ng-content>\n\t\t</div>\n\n\t\t<div class=\"m-table-filter\">\n\t\t\t<mat-form-field appearance=\"outline\">\n\t\t\t\t<mat-label>Filter</mat-label>\n\t\t\t\t<span matPrefix>&nbsp;<mat-icon>search</mat-icon>&nbsp;</span>\n\t\t\t\t<input (keyup)=\"applyFilter($event)\" matInput placeholder=\"Recherche\">\n\t\t\t</mat-form-field>\n\t\t</div>\n\t\t<div class=\"m-table-filter-content\">\n\t\t\t<ng-content select=\"[right]\"></ng-content>\n\t\t</div>\n\t</div>\n\n\t<div class=\"m-table-content\">\n\t\t<table [dataSource]=\"dataSource\" class=\"m-theme\" mat-table matSort #table>\n\t\t\t<ng-container *ngFor=\"let data of headers; let i = index\">\n\t\t\t\t<ng-container [matColumnDef]=\"data.key\">\n\t\t\t\t\t<!-- Colonne name -->\n\t\t\t\t\t<ng-container *ngIf=\"data.type !== HeaderTypes.SELECTION; else select\">\n\t\t\t\t\t\t<th *matHeaderCellDef class=\"title\" mat-header-cell mat-sort-header>{{ data.title }}</th>\n\t\t\t\t\t</ng-container>\n\t\t\t\t\t<ng-template #select>\n\t\t\t\t\t\t<th mat-header-cell *matHeaderCellDef>\n\t\t\t\t\t\t\t<mat-checkbox (change)=\"toggleAllRows($event)\"\n\t\t\t\t\t\t\t\t\t\t  [checked]=\"selection.hasValue() && isAllSelected()\"\n\t\t\t\t\t\t\t\t\t\t  [indeterminate]=\"selection.hasValue() && !isAllSelected()\"\n\t\t\t\t\t\t\t\t\t\t  [aria-label]=\"checkboxLabel()\">\n\t\t\t\t\t\t\t</mat-checkbox>\n\t\t\t\t\t\t</th>\n\t\t\t\t\t</ng-template>\n\n\t\t\t\t\t<td *matCellDef=\"let row\" mat-cell [ngClass]=\"buildClass(data, row)\">\n\n\t\t\t\t\t\t<ng-container [ngSwitch]=\"data.type\">\n\n\t\t\t\t\t\t\t<!-- RELATIVE DATE -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.RELATIVE_DATE\">\n\t\t\t\t\t\t\t\t<ng-container *ngIf=\"row[data.key]\">\n\t\t\t\t\t\t\t\t\t{{ row[data.key] | relativeDate }}\n\t\t\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- DATE -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.DATE\">\n\t\t\t\t\t\t\t\t<ng-container *ngIf=\"row[data.key]\">\n\t\t\t\t\t\t\t\t\t{{ row[data.key] | date: data.options && data.options.format ? data.options.format : 'dd/MM/yyyy H:mm' }}\n\t\t\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- SELECT -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.SELECT\">\n\t\t\t\t\t\t\t\t<ng-container *ngIf=\"data.selectOptions && data.selectOptions.method\">\n\t\t\t\t\t\t\t\t\t<mat-select [ngClass]=\"buildClass(data, row)\"\n\t\t\t\t\t\t\t\t\t\t\t\t(selectionChange)=\"data.selectOptions.method($event, row)\"\n\t\t\t\t\t\t\t\t\t\t\t\t[ngModel]=\"row[data.key]\">\n\t\t\t\t\t\t\t\t\t\t<mat-option *ngFor=\"let i of data.selectOptions.options\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t[value]=\"i\">{{ i }}\n\t\t\t\t\t\t\t\t\t\t</mat-option>\n\t\t\t\t\t\t\t\t\t</mat-select>\n\t\t\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- SELECTION -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.SELECTION\">\n\t\t\t\t\t\t\t\t<mat-checkbox (click)=\"$event.stopPropagation()\"\n\t\t\t\t\t\t\t\t\t\t\t  (change)=\"changeSelection($event, row)\"\n\t\t\t\t\t\t\t\t\t\t\t  [checked]=\"isSelected(row)\"\n\t\t\t\t\t\t\t\t\t\t\t  [aria-label]=\"checkboxLabel(row)\">\n\t\t\t\t\t\t\t\t</mat-checkbox>\n\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- BOOLEAN -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.BOOLEAN\">\n\t\t\t\t\t\t\t\t<mat-icon>{{ row[data.key] ? 'check' : 'close' }}</mat-icon>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- MATCHING -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.MATCHING\">\n\t\t\t\t\t\t\t\t<ng-container *ngIf=\"data.data && data.data[row[data.key]]\">\n\t\t\t\t\t\t\t\t\t{{ data.data[row[data.key]]}}\n\t\t\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- ICON -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.ICON\">\n\t\t\t\t\t\t\t\t<m-icons [type]=\"row[data.key]\"></m-icons>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- LINE -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.LINE\">\n\t\t\t\t\t\t\t\t<div class=\"lines\">\n\t\t\t\t\t\t\t\t\t<ul *ngIf=\"row[data.key] && Object.keys(row[data.key]).length !== 0\">\n\t\t\t\t\t\t\t\t\t\t<li *ngFor=\"let line of Object.keys(row[data.key])\">\n\t\t\t\t\t\t\t\t\t\t\t<m-logo-lines\n\t\t\t\t\t\t\t\t\t\t\t\t[ligne]=\"row[data.key][line]\"\n\t\t\t\t\t\t\t\t\t\t\t\t[height]=\"30\"\n\t\t\t\t\t\t\t\t\t\t\t\t[width]=\"row[data.key][line].id.includes('SEM') ? 30 : 50\"\n\t\t\t\t\t\t\t\t\t\t\t\t[isRounded]=\"!['TRAM', 'CHRONO'].includes(row[data.key][line].type)\"\n\t\t\t\t\t\t\t\t\t\t\t></m-logo-lines>\n\t\t\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- LIST -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.LIST\">\n\t\t\t\t\t\t\t\t<ul>\n\t\t\t\t\t\t\t\t\t<li *ngFor=\"let item of row[data.key]; let isFirst = first; let isLast = last\"\n\t\t\t\t\t\t\t\t\t\t[class.first]=\"isFirst\"\n\t\t\t\t\t\t\t\t\t\t[class.last]=\"isLast\"\n\t\t\t\t\t\t\t\t\t\t[class.valid]=\"item == 'Valide'\"\n\t\t\t\t\t\t\t\t\t\t[class.invalid]=\"item == 'Invalide'\"\n\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t\t{{ item }}\n\t\t\t\t\t\t\t\t\t</li>\n\t\t\t\t\t\t\t\t</ul>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- LINK -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.LINK\">\n\t\t\t\t\t\t\t\t<div class=\"links\">\n\t\t\t\t\t\t\t\t\t<button mat-icon-button *ngIf=\"row[data.key]; else noLink\" (click)=\"openLink(row[data.key], '_blank', $event)\" color=\"primary\">\n\t\t\t\t\t\t\t\t\t\t<mat-icon>link</mat-icon>\n\t\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t\t\t<ng-template #noLink></ng-template>\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- ARRAY -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.ARRAY\">\n\t\t\t\t\t\t\t\t<ng-container *ngIf=\"row[data.key] && row[data.key].length > 0\">\n\t\t\t\t\t\t\t\t\t<ng-container *ngFor=\"let item of row[data.key]; let last = last\">\n\t\t\t\t\t\t\t\t\t\t<span>{{ item }}</span>\n\t\t\t\t\t\t\t\t\t\t<span *ngIf=\"!last\">, </span>\n\t\t\t\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- TEXT, NUMBER -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchDefault>\n\t\t\t\t\t\t\t\t<span>{{ row[data.key] }}</span>\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t<!-- ACTIONS -->\n\t\t\t\t\t\t\t<ng-container *ngSwitchCase=\"HeaderTypes.ACTIONS\">\n\t\t\t\t\t\t\t\t<ng-container *ngIf=\"actions && actions.column && actions.column.length > 0\">\n\t\t\t\t\t\t\t\t\t<div class=\"actions\">\n\t\t\t\t\t\t\t\t\t\t<ng-container *ngFor=\"let a of actions.column\">\n\t\t\t\t\t\t\t\t\t\t\t<ng-container *ngIf=\"!a.isMenu; else btnMenu\">\n\t\t\t\t\t\t\t\t\t\t\t\t<button\n\t\t\t\t\t\t\t\t\t\t\t\t\t[disabled]=\"a.disabled(row)\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t(click)=\"a.action(row, $event)\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t[color]=\"a.color\"\n\t\t\t\t\t\t\t\t\t\t\t\t\tmat-icon-button\n\t\t\t\t\t\t\t\t\t\t\t\t\ttitle=\"{{a.title}}\"\n\t\t\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-icon>{{ a.icon }}</mat-icon>\n\t\t\t\t\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t\t\t\t\t\t<ng-template #btnMenu>\n\t\t\t\t\t\t\t\t\t\t\t\t<button mat-icon-button [matMenuTriggerFor]=\"menu\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t\taria-label=\"Actions\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-icon>more_vert</mat-icon>\n\t\t\t\t\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t\t\t\t\t\t<mat-menu class=\"actions-menu\" #menu=\"matMenu\" [xPosition]=\"'before'\">\n\t\t\t\t\t\t\t\t\t\t\t\t\t<button\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t[disabled]=\"a.disabled(row)\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t(click)=\"a.action(row, $event)\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t\tmat-menu-item\n\t\t\t\t\t\t\t\t\t\t\t\t\t\ttitle=\"{{a.title}}\"\n\t\t\t\t\t\t\t\t\t\t\t\t\t>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<mat-icon [color]=\"a.color\">{{ a.icon }}</mat-icon>\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t<span>{{a.title}}</span>\n\t\t\t\t\t\t\t\t\t\t\t\t\t</button>\n\t\t\t\t\t\t\t\t\t\t\t\t</mat-menu>\n\t\t\t\t\t\t\t\t\t\t\t</ng-template>\n\t\t\t\t\t\t\t\t\t\t</ng-container>\n\t\t\t\t\t\t\t\t\t</div>\n\n\t\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t\t</ng-container>\n\n\t\t\t\t\t</td>\n\n\t\t\t\t</ng-container>\n\n\t\t\t</ng-container>\n\n\t\t\t<tr *matHeaderRowDef=\"header; sticky: true\" mat-header-row></tr>\n\t\t\t<tr mat-row (click)=\"actions && actions.line && actions.line(row, $event)\"\n\t\t\t\t*matRowDef=\"let row; columns: header;\"\n\t\t\t\t[class.pointer]=\"!!actions.line\"\n\t\t\t>\n\t\t\t</tr>\n\n\t\t\t<!-- Row shown when there is no matching data. -->\n\t\t\t<tr *matNoDataRow class=\"mat-row empty\">\n\t\t\t\t<td class=\"mat-cell\" colspan=\"4\">No data matching the filter</td>\n\t\t\t</tr>\n\n\t\t</table>\n\t</div>\n\n\t<ng-container *ngIf=\"config.pageSize.length > 0\">\n\t\t<mat-paginator [pageSizeOptions]=\"config.pageSize\" aria-label=\"Select page\"></mat-paginator>\n\t</ng-container>\n\n</div>\n", styles: [".m-table .m-table-header{width:100%;display:flex;padding:8px}.m-table .m-table-header .m-table-filter{flex:1 1 auto;padding:8px}.m-table .m-table-header .m-table-filter mat-form-field{width:100%}.m-table .m-table-header .m-table-filter-content{display:flex;padding:8px}.m-table .m-table-content{max-height:calc(100vh - 221px);overflow:auto}.m-table .m-table-content .pointer{cursor:pointer}.m-table .m-table-content div.lines ul{list-style:none;display:flex;flex-direction:row;flex-wrap:wrap;margin:0;padding:0}.m-table .m-table-content div.lines ul li{margin:4px 4px 4px 0}.m-table .actions{display:flex}.cdk-overlay-pane,.mat-mdc-select-panel-above{width:100%!important}\n"] }]
        }], ctorParameters: function () { return []; }, propDecorators: { data: [{
                type: Input
            }], headers: [{
                type: Input
            }], config: [{
                type: Input
            }], actions: [{
                type: Input
            }], selectionChange: [{
                type: Output
            }], filter: [{
                type: Output
            }], table: [{
                type: ViewChild,
                args: ['table', { static: false }]
            }], paginator: [{
                type: ViewChild,
                args: [MatPaginator]
            }], sort: [{
                type: ViewChild,
                args: [MatSort]
            }] } });

/**
 * @component
 * @name MAppDownload
 * @description
 * Composant graphique pour afficher les liens de téléchargement d'une Appli
 * Exemple : liens pour télécharger l'Appli M sur Apple Store
 *
 * ### Usage example
 * ```html
 * <m-app-download [data]="App"></m-app-download>
 * ```
 *
 * @selector m-app-download
 * @standalone true
 * @module CommonModule, MatIconModule, MatButtonModule
 */
class MAppDownload {
    constructor(iconRegistry, sanitizer, renderer) {
        this.iconRegistry = iconRegistry;
        this.sanitizer = sanitizer;
        this.renderer = renderer;
    }
    ngAfterViewInit() {
        this.iconRegistry.getSvgIconFromUrl(this.sanitizer.bypassSecurityTrustResourceUrl(this.data.Logo.url)).subscribe((icon) => {
            icon.setAttribute('fill', '');
            //   let colortext = document.defaultView?.getComputedStyle(document.getElementById('textBase')!)['color']; 
            let colortext = this.text.nativeElement.style.color;
            let childrens = Array.from(icon.children);
            childrens.forEach(element => {
                this.renderer.setAttribute(element, 'fill', colortext);
                Array.from(element.children).forEach(element2 => {
                    this.renderer.setAttribute(element2, 'fill', `${colortext} !important`);
                });
            });
            this.renderer.appendChild(this.iconPrincipal.nativeElement, icon);
        });
    }
}
MAppDownload.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MAppDownload, deps: [{ token: i1.MatIconRegistry }, { token: i2.DomSanitizer }, { token: i0.Renderer2 }], target: i0.ɵɵFactoryTarget.Component });
MAppDownload.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.10", type: MAppDownload, isStandalone: true, selector: "m-app-download", inputs: { data: "data" }, viewQueries: [{ propertyName: "iconPrincipal", first: true, predicate: ["iconPrincipal"], descendants: true, read: ElementRef }, { propertyName: "text", first: true, predicate: ["text"], descendants: true, read: ElementRef }], ngImport: i0, template: "<a [href]=\"data.Lien\" class=\"buttonDownload\" id=\"button\" mat-stroked-button color=\"primary\" target=\"_blank\">\r\n    <div id=\"icon2\"></div>\r\n    <mat-icon #iconPrincipal iconPositionStart class=\"icon iconPrincipal\" id=\"icon\"></mat-icon>\r\n    <span class=\"text\">\r\n        <span aria-hidden=\"true\" #text id=\"textBase\" class=\"text-transparent\">{{data.Texte}}</span>\r\n        <span class=\"textInApp\">\r\n            <span aria-hidden=\"true\" class=\"textGras\">{{data.TexteGras}}</span>\r\n            <mat-icon aria-hidden=\"true\" iconPositionEnd class=\"external arrow-outward\">arrow_outward</mat-icon>\r\n        </span>\r\n    </span>\r\n    <span class=\"cdk-visually-hidden\">{{data.Texte}} {{data.TexteGras}}</span>\r\n</a>", styles: [".buttonDownload{width:160px;display:flex;justify-content:flex-start;padding:8px 12px;height:auto!important;border-radius:8px!important;border-style:solid;border-width:2px;text-transform:none!important;color:inherit!important;border-color:inherit!important;background-color:inherit}.buttonDownload:hover .textGras{text-decoration:underline}.buttonDownload .iconPrincipal{width:32px;height:32px;font-size:32px}.buttonDownload .arrow-outward{font-size:16px;width:16px;height:16px}.buttonDownload .text-transparent{display:flex;justify-content:flex-start;font-size:11px;font-weight:300;line-height:16px;letter-spacing:.5px}.buttonDownload .textInApp{display:flex;gap:8px;align-items:center;font-size:11px;font-weight:500;line-height:16px;letter-spacing:.5px}.buttonDownload .external{font-size:1.2em;display:flex;align-items:center}.buttonDownload .cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;white-space:nowrap;outline:0;-webkit-appearance:none;-moz-appearance:none;left:0}\n"], dependencies: [{ kind: "ngmodule", type: CommonModule }, { kind: "ngmodule", type: MatIconModule }, { kind: "component", type: i1.MatIcon, selector: "mat-icon", inputs: ["color", "inline", "svgIcon", "fontSet", "fontIcon"], exportAs: ["matIcon"] }, { kind: "ngmodule", type: MatButtonModule }, { kind: "component", type: i2$4.MatAnchor, selector: "a[mat-button], a[mat-raised-button], a[mat-flat-button], a[mat-stroked-button]", inputs: ["disabled", "disableRipple", "color", "tabIndex"], exportAs: ["matButton", "matAnchor"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MAppDownload, decorators: [{
            type: Component,
            args: [{ selector: 'm-app-download', standalone: true, imports: [CommonModule, MatIconModule, MatButtonModule], template: "<a [href]=\"data.Lien\" class=\"buttonDownload\" id=\"button\" mat-stroked-button color=\"primary\" target=\"_blank\">\r\n    <div id=\"icon2\"></div>\r\n    <mat-icon #iconPrincipal iconPositionStart class=\"icon iconPrincipal\" id=\"icon\"></mat-icon>\r\n    <span class=\"text\">\r\n        <span aria-hidden=\"true\" #text id=\"textBase\" class=\"text-transparent\">{{data.Texte}}</span>\r\n        <span class=\"textInApp\">\r\n            <span aria-hidden=\"true\" class=\"textGras\">{{data.TexteGras}}</span>\r\n            <mat-icon aria-hidden=\"true\" iconPositionEnd class=\"external arrow-outward\">arrow_outward</mat-icon>\r\n        </span>\r\n    </span>\r\n    <span class=\"cdk-visually-hidden\">{{data.Texte}} {{data.TexteGras}}</span>\r\n</a>", styles: [".buttonDownload{width:160px;display:flex;justify-content:flex-start;padding:8px 12px;height:auto!important;border-radius:8px!important;border-style:solid;border-width:2px;text-transform:none!important;color:inherit!important;border-color:inherit!important;background-color:inherit}.buttonDownload:hover .textGras{text-decoration:underline}.buttonDownload .iconPrincipal{width:32px;height:32px;font-size:32px}.buttonDownload .arrow-outward{font-size:16px;width:16px;height:16px}.buttonDownload .text-transparent{display:flex;justify-content:flex-start;font-size:11px;font-weight:300;line-height:16px;letter-spacing:.5px}.buttonDownload .textInApp{display:flex;gap:8px;align-items:center;font-size:11px;font-weight:500;line-height:16px;letter-spacing:.5px}.buttonDownload .external{font-size:1.2em;display:flex;align-items:center}.buttonDownload .cdk-visually-hidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px;white-space:nowrap;outline:0;-webkit-appearance:none;-moz-appearance:none;left:0}\n"] }]
        }], ctorParameters: function () { return [{ type: i1.MatIconRegistry }, { type: i2.DomSanitizer }, { type: i0.Renderer2 }]; }, propDecorators: { data: [{
                type: Input
            }], iconPrincipal: [{
                type: ViewChild,
                args: ['iconPrincipal', { read: ElementRef }]
            }], text: [{
                type: ViewChild,
                args: ['text', { read: ElementRef }]
            }] } });

class MListWrapper {
    constructor() {
        /**
         * Enable see more button
         */
        this.seeMoreEnable = false;
        /**
         * See more button state
         */
        this.seeMore = false;
    }
}
MListWrapper.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MListWrapper, deps: [], target: i0.ɵɵFactoryTarget.Component });
MListWrapper.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.10", type: MListWrapper, isStandalone: true, selector: "m-list-wrapper", inputs: { seeMoreEnable: "seeMoreEnable" }, exportAs: ["MListWrapper"], ngImport: i0, template: "<ng-content></ng-content>\n\n<button *ngIf=\"seeMoreEnable && !seeMore\" mat-button class=\"m-theme\" color=\"primary\" (click)=\"seeMore = true\">\n\t<span>Voir plus...</span>\n</button>\n", styles: [":host{display:flex;flex-direction:column}button{align-self:center}\n"], dependencies: [{ kind: "ngmodule", type: CommonModule }, { kind: "directive", type: i1$1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "ngmodule", type: MatButtonModule }, { kind: "component", type: i2$4.MatButton, selector: "    button[mat-button], button[mat-raised-button], button[mat-flat-button],    button[mat-stroked-button]  ", inputs: ["disabled", "disableRipple", "color"], exportAs: ["matButton"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MListWrapper, decorators: [{
            type: Component,
            args: [{ selector: 'm-list-wrapper', standalone: true, imports: [CommonModule, MatButtonModule], exportAs: 'MListWrapper', template: "<ng-content></ng-content>\n\n<button *ngIf=\"seeMoreEnable && !seeMore\" mat-button class=\"m-theme\" color=\"primary\" (click)=\"seeMore = true\">\n\t<span>Voir plus...</span>\n</button>\n", styles: [":host{display:flex;flex-direction:column}button{align-self:center}\n"] }]
        }], propDecorators: { seeMoreEnable: [{
                type: Input
            }] } });

class MDisturbanceDisplay {
    /**
     * Get the dynamic classes for the disturbance
     * @param disturbance any
     * @return string[]
     */
    getDynamicClasses(disturbance) {
        let classes = ['line-nsv', 'm-disturbance-display'];
        if (disturbance && disturbance.nsv && disturbance.nsv != 0 && disturbance.nsv != 1 && disturbance.nsv != 5) {
            classes.push(`has-disturbance disturbance-${disturbance.nsv}`);
        }
        return classes;
    }
}
MDisturbanceDisplay.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MDisturbanceDisplay, deps: [], target: i0.ɵɵFactoryTarget.Component });
MDisturbanceDisplay.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "14.0.0", version: "15.2.10", type: MDisturbanceDisplay, isStandalone: true, selector: "m-disturbance-display", inputs: { disturbance: "disturbance", disturbanceCollection: "disturbanceCollection", link: "link" }, ngImport: i0, template: "<ng-container *ngIf=\"disturbance\">\n\t<p *ngIf=\"!disturbance.hasDisturbance && disturbanceCollection.length === 0; else hasDisturbances\" [ngClass]=\"getDynamicClasses(disturbance)\">\n\t\t<ng-container *ngTemplateOutlet=\"disturbanceContentTpl; context:{$implicit: disturbance, isLink: false}\"></ng-container>\n\t</p>\n\n\t<ng-template #hasDisturbances>\n\t\t<a mat-button [routerLink]=\"link\" [ngClass]=\"getDynamicClasses(disturbance)\">\n\t\t\t<ng-container *ngTemplateOutlet=\"disturbanceContentTpl; context:{$implicit: disturbance, isLink: true}\"></ng-container>\n\t\t</a>\n\t</ng-template>\n</ng-container>\n\n<ng-template #disturbanceContentTpl let-disturbance let-isLink=\"isLink\">\n\t<ng-container [ngSwitch]=\"disturbance.nsv\">\n\t\t<div *ngSwitchCase=\"2\">\n\t\t\t<m-icons class=\"icon-left-large\" [ngClass]=\"[disturbance.calculatedClasses]\" type=\"perturbation_low\"></m-icons>\n\t\t\t<span class=\"layout column\">\n\t\t\t\t<span>Service l\u00E9g\u00E8rement perturb\u00E9</span>\n\t\t\t\t<ng-container *ngTemplateOutlet=\"disturbancesCountTpl; context:{$implicit: disturbanceCollection.length}\"></ng-container>\n\t\t\t</span>\n\t\t</div>\n\t\t<div *ngSwitchCase=\"3\">\n\t\t\t<m-icons class=\"icon-left-large\" [ngClass]=\"[disturbance.calculatedClasses]\" type=\"perturbation_hight\"></m-icons>\n\t\t\t<span class=\"layout column\">\n\t\t\t\t<span>Service tr\u00E8s perturb\u00E9</span>\n\t\t\t\t<ng-container *ngTemplateOutlet=\"disturbancesCountTpl; context:{$implicit: disturbanceCollection.length}\"></ng-container>\n\t\t\t</span>\n\t\t</div>\n\t\t<div *ngSwitchCase=\"4\">\n\t\t\t<m-icons class=\"icon-left-large\" [ngClass]=\"[disturbance.calculatedClasses]\" type=\"perturbation_over\"></m-icons>\n\t\t\t<span class=\"layout column\">\n\t\t\t\t<span>Hors service</span>\n\t\t\t\t<ng-container *ngTemplateOutlet=\"disturbancesCountTpl; context:{$implicit: disturbanceCollection.length}\"></ng-container>\n\t\t\t</span>\n\t\t</div>\n\t\t<div *ngSwitchDefault class=\"default-display\">\n\t\t\t<m-icons class=\"icon-left-large\" type=\"perturbation_low\"></m-icons>\n\t\t\t<span class=\"layout column\">\n\t\t\t\t<span>Service normal</span>\n\t\t\t\t<ng-container *ngTemplateOutlet=\"disturbancesCountTpl; context:{$implicit: disturbanceCollection.length}\"></ng-container>\n\t\t\t</span>\n\t\t</div>\n\t\t<m-icons *ngIf=\"isLink\" class=\"icon-right\">chevron_right</m-icons>\n\t</ng-container>\n</ng-template>\n\n<ng-template #disturbancesCountTpl>\n\t<span [ngPlural]=\"disturbanceCollection.length\" class=\"m-disturbance-display-text-secondary\">\n\t\t<ng-template ngPluralCase=\"=0\">Aucun \u00E9v\u00E9nement</ng-template>\n\t\t<ng-template ngPluralCase=\"=1\">1 \u00E9v\u00E9nement</ng-template>\n\t\t<ng-template ngPluralCase=\"other\">{{ disturbanceCollection.length }} \u00E9v\u00E9nements</ng-template>\n\t</span>\n</ng-template>\n", styles: [".has-disturbance:after{top:-4px;right:-4px}\n"], dependencies: [{ kind: "ngmodule", type: CommonModule }, { kind: "directive", type: i1$1.NgClass, selector: "[ngClass]", inputs: ["class", "ngClass"] }, { kind: "directive", type: i1$1.NgIf, selector: "[ngIf]", inputs: ["ngIf", "ngIfThen", "ngIfElse"] }, { kind: "directive", type: i1$1.NgTemplateOutlet, selector: "[ngTemplateOutlet]", inputs: ["ngTemplateOutletContext", "ngTemplateOutlet", "ngTemplateOutletInjector"] }, { kind: "directive", type: i1$1.NgSwitch, selector: "[ngSwitch]", inputs: ["ngSwitch"] }, { kind: "directive", type: i1$1.NgSwitchCase, selector: "[ngSwitchCase]", inputs: ["ngSwitchCase"] }, { kind: "directive", type: i1$1.NgSwitchDefault, selector: "[ngSwitchDefault]" }, { kind: "directive", type: i1$1.NgPlural, selector: "[ngPlural]", inputs: ["ngPlural"] }, { kind: "directive", type: i1$1.NgPluralCase, selector: "[ngPluralCase]" }, { kind: "component", type: MIcons, selector: "m-icons", inputs: ["type", "color"] }, { kind: "directive", type: RouterLink, selector: "[routerLink]", inputs: ["target", "queryParams", "fragment", "queryParamsHandling", "state", "relativeTo", "preserveFragment", "skipLocationChange", "replaceUrl", "routerLink"] }, { kind: "ngmodule", type: MatButtonModule }, { kind: "component", type: i2$4.MatAnchor, selector: "a[mat-button], a[mat-raised-button], a[mat-flat-button], a[mat-stroked-button]", inputs: ["disabled", "disableRipple", "color", "tabIndex"], exportAs: ["matButton", "matAnchor"] }] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "15.2.10", ngImport: i0, type: MDisturbanceDisplay, decorators: [{
            type: Component,
            args: [{ selector: 'm-disturbance-display', standalone: true, imports: [CommonModule, MIcons, RouterLink, MatButtonModule], template: "<ng-container *ngIf=\"disturbance\">\n\t<p *ngIf=\"!disturbance.hasDisturbance && disturbanceCollection.length === 0; else hasDisturbances\" [ngClass]=\"getDynamicClasses(disturbance)\">\n\t\t<ng-container *ngTemplateOutlet=\"disturbanceContentTpl; context:{$implicit: disturbance, isLink: false}\"></ng-container>\n\t</p>\n\n\t<ng-template #hasDisturbances>\n\t\t<a mat-button [routerLink]=\"link\" [ngClass]=\"getDynamicClasses(disturbance)\">\n\t\t\t<ng-container *ngTemplateOutlet=\"disturbanceContentTpl; context:{$implicit: disturbance, isLink: true}\"></ng-container>\n\t\t</a>\n\t</ng-template>\n</ng-container>\n\n<ng-template #disturbanceContentTpl let-disturbance let-isLink=\"isLink\">\n\t<ng-container [ngSwitch]=\"disturbance.nsv\">\n\t\t<div *ngSwitchCase=\"2\">\n\t\t\t<m-icons class=\"icon-left-large\" [ngClass]=\"[disturbance.calculatedClasses]\" type=\"perturbation_low\"></m-icons>\n\t\t\t<span class=\"layout column\">\n\t\t\t\t<span>Service l\u00E9g\u00E8rement perturb\u00E9</span>\n\t\t\t\t<ng-container *ngTemplateOutlet=\"disturbancesCountTpl; context:{$implicit: disturbanceCollection.length}\"></ng-container>\n\t\t\t</span>\n\t\t</div>\n\t\t<div *ngSwitchCase=\"3\">\n\t\t\t<m-icons class=\"icon-left-large\" [ngClass]=\"[disturbance.calculatedClasses]\" type=\"perturbation_hight\"></m-icons>\n\t\t\t<span class=\"layout column\">\n\t\t\t\t<span>Service tr\u00E8s perturb\u00E9</span>\n\t\t\t\t<ng-container *ngTemplateOutlet=\"disturbancesCountTpl; context:{$implicit: disturbanceCollection.length}\"></ng-container>\n\t\t\t</span>\n\t\t</div>\n\t\t<div *ngSwitchCase=\"4\">\n\t\t\t<m-icons class=\"icon-left-large\" [ngClass]=\"[disturbance.calculatedClasses]\" type=\"perturbation_over\"></m-icons>\n\t\t\t<span class=\"layout column\">\n\t\t\t\t<span>Hors service</span>\n\t\t\t\t<ng-container *ngTemplateOutlet=\"disturbancesCountTpl; context:{$implicit: disturbanceCollection.length}\"></ng-container>\n\t\t\t</span>\n\t\t</div>\n\t\t<div *ngSwitchDefault class=\"default-display\">\n\t\t\t<m-icons class=\"icon-left-large\" type=\"perturbation_low\"></m-icons>\n\t\t\t<span class=\"layout column\">\n\t\t\t\t<span>Service normal</span>\n\t\t\t\t<ng-container *ngTemplateOutlet=\"disturbancesCountTpl; context:{$implicit: disturbanceCollection.length}\"></ng-container>\n\t\t\t</span>\n\t\t</div>\n\t\t<m-icons *ngIf=\"isLink\" class=\"icon-right\">chevron_right</m-icons>\n\t</ng-container>\n</ng-template>\n\n<ng-template #disturbancesCountTpl>\n\t<span [ngPlural]=\"disturbanceCollection.length\" class=\"m-disturbance-display-text-secondary\">\n\t\t<ng-template ngPluralCase=\"=0\">Aucun \u00E9v\u00E9nement</ng-template>\n\t\t<ng-template ngPluralCase=\"=1\">1 \u00E9v\u00E9nement</ng-template>\n\t\t<ng-template ngPluralCase=\"other\">{{ disturbanceCollection.length }} \u00E9v\u00E9nements</ng-template>\n\t</span>\n</ng-template>\n", styles: [".has-disturbance:after{top:-4px;right:-4px}\n"] }]
        }], propDecorators: { disturbance: [{
                type: Input
            }], disturbanceCollection: [{
                type: Input
            }], link: [{
                type: Input
            }] } });

/*
 * Public API Surface of m-ui
 */

/**
 * Generated bundle index. Do not edit.
 */

export { HeaderTypes, ICONS_MAPPING, MAffluence, MAppDownload, MDisturbanceDisplay, MIcons, MIconsService, MListWrapper, MLogoLines, MTable, RelativeDatePipe };
//# sourceMappingURL=metromobilite-m-ui.mjs.map
