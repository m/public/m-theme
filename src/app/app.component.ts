import {Component, Renderer2, RendererFactory2} from '@angular/core';
import {NgDocThemeService} from '@ng-doc/app';
import {ThemingService} from 'src/app/services/theming.service';

export enum THEME {
	DARK = 'dark-theme',
	LIGHT = 'light-theme'
}

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {

	static readonly THEME_KEY: string = 'm-ui:theme';
	public THEME: typeof THEME= THEME;
	public theming: THEME = THEME.DARK;

	private renderer: Renderer2;

	constructor(private rendererFactory: RendererFactory2,
				protected readonly themeService: NgDocThemeService,
				private themingService: ThemingService) {
		this.renderer = this.rendererFactory.createRenderer(null, null);
		this.initializeTheme();
	}

	/**
	 * Toggles between DARK and LIGHT themes
	 */
	switchTheme(): void {
		const newTheme = this.theming === THEME.DARK ? THEME.LIGHT : THEME.DARK;
		this.updateTheme(newTheme);
	}

	/**
	 * Initializes the theme based on user preferences or localStorage
	 */
	private initializeTheme(): void {
		const savedTheme = localStorage.getItem(AppComponent.THEME_KEY) as THEME;
		const systemPreference: THEME = this.getSystemTheme();
		const defaultTheme: THEME = savedTheme || systemPreference || THEME.DARK;

		this.updateTheme(defaultTheme);

		// Listen to system preference changes.
		this.listenToSystemThemeChanges();
	}

	/**
	 * Updates the current theme
	 * @param theme - The new theme to apply
	 */
	private updateTheme(theme: THEME): void {

		// Update the body class
		this.renderer.removeClass(document.body, this.theming);
		this.renderer.addClass(document.body, theme);

		// Save to localStorage and synchronize services
		localStorage.setItem(AppComponent.THEME_KEY, theme);
		this.themingService.theming = theme;
		this.themeService.set(theme).then();

		this.theming = theme;
	}


	/**
	 * Detects the system theme (light/dark) using prefers-color-scheme
	 * @returns The detected system theme
	 */
	private getSystemTheme(): THEME {
		return window.matchMedia('(prefers-color-scheme: dark)').matches ? THEME.DARK : THEME.LIGHT;
	}


	/**
	 * Listens for system theme changes and updates the theme if no preference is saved
	 */
	private listenToSystemThemeChanges(): void {
		window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', (event: MediaQueryListEvent) => {
			const systemTheme: THEME = event.matches ? THEME.DARK : THEME.LIGHT;
			if (!localStorage.getItem(AppComponent.THEME_KEY)) this.updateTheme(systemTheme);
		});
	}

}
