import {NgIf} from '@angular/common';
import {Component, Input} from '@angular/core';
import {MatListModule} from '@angular/material/list';

@Component({
	selector: 'app-preview',
	templateUrl: './preview.component.html',
	styleUrls: ['./preview.component.scss'],
	standalone: true,
	imports: [
		MatListModule,
		NgIf
	]
})
export class Preview {

	@Input() title?: string;
	@Input() nextElement: boolean = true;
	@Input() fullWidth: boolean = false;
	@Input() oneLine: boolean = false;

}
