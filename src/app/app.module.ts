import {A11yModule} from '@angular/cdk/a11y';
import {registerLocaleData} from '@angular/common';
import {provideHttpClient, withInterceptorsFromDi} from '@angular/common/http';
import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RouterModule} from '@angular/router';
import {NgDocDefaultSearchEngine, NgDocModule, provideSearchEngine} from '@ng-doc/app';
import {NgDocNavbarModule} from '@ng-doc/app/components/navbar';
import {NgDocSidebarModule} from '@ng-doc/app/components/sidebar';
import {NG_DOC_ROUTING, NgDocGeneratedModule} from '@ng-doc/generated';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent, THEME} from './app.component';

// Angular
import localeFr from '@angular/common/locales/fr';

registerLocaleData(localeFr, 'fr');


@NgModule({
	declarations: [
		AppComponent
	],
	imports: [
		MatNativeDateModule,
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		A11yModule,
		ReactiveFormsModule,
		RouterModule.forRoot(NG_DOC_ROUTING, {scrollPositionRestoration: 'enabled', anchorScrolling: 'enabled', scrollOffset: [0, 70]}),
		NgDocNavbarModule,
		NgDocSidebarModule,
		NgDocModule.forRoot({
			defaultThemeId: THEME.DARK,
			themes: [
				{
					id: THEME.DARK,
					path: 'assets/themes/dark-theme.css'
				},
				{
					id: THEME.LIGHT,
					path: 'assets/themes/light-theme.css'
				}
			]
		}),
		NgDocGeneratedModule.forRoot()
	],
	providers: [
		provideHttpClient(withInterceptorsFromDi()),
		provideSearchEngine(NgDocDefaultSearchEngine),
	],
	bootstrap: [AppComponent]
})
export class AppModule {
}
