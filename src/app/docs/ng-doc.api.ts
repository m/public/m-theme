import {NgDocApi} from '@ng-doc/core';

const Api: NgDocApi = {
	title: 'API',
	scopes: [
		{
			name: 'm-ui',
			route: 'm-ui',
			include: 'projects/m-ui/src/lib/**/*.ts',
		}
	]
};

export default Api;
