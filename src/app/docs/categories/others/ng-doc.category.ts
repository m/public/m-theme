import {NgDocCategory} from '@ng-doc/core';

const OthersCategory: NgDocCategory = {
	title: 'Others',
};

export default OthersCategory;
