import {NgDocCategory} from '@ng-doc/core';

const MThemeCategory: NgDocCategory = {
	title: 'M-theme',
	order: 2
};

export default MThemeCategory;
