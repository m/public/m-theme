import {NgDocCategory} from '@ng-doc/core';

const MUiCategory: NgDocCategory = {
	title: 'M-ui',
	order: 3
};

export default MUiCategory;
