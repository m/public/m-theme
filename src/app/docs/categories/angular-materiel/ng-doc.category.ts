import {NgDocCategory} from '@ng-doc/core';

const AngularMaterielCategory: NgDocCategory = {
	title: 'Angular-materiel',
	order: 4
};

export default AngularMaterielCategory;
