import {NgDocCategory} from '@ng-doc/core';

const GettingStartedCategory: NgDocCategory = {
	title: 'Getting Started',
	expanded: true,
	order: 1
};

export default GettingStartedCategory;
