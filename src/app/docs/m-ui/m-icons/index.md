# {{ NgDocPage.title }}

## API
Shows how to use the `MIcons` component.

## Installation
* Import the `MIconsModule` and `HttpClientModule` in your application module.
  * MIconsModule use the icons in the application.
  * HttpClientModule is required to load the icons from the server.
* Add icons path in angular.json files.

```json
    {
	"assets": [
		{
			"glob": "**/*",
			"input": "node_modules/@metromobilite/m-ui/assets",
			"output": "/assets/"
		}
	]
}
```

## Add new icons

* Add the new icon in the `project/m-ui/src/assets/icons` folder.
* Add the icon definition in component MIcons
```typescript
	const ICONS_MAPPING = [
		{type: 'newIcon', name: 'newIcon.svg'}
	];
```
* Add mat-icon in the html file
```angular2html
	<mat-icon *ngSwitchCase="'newIcon'"  svgIcon="newIcon"></mat-icon>
```
* Rebuild the project for share the new icon.
* Use the new icon in the application.

```angular2html
	<mat-icon svgIcon="newIcon"></mat-icon>
```

## Icons List
{{ NgDocActions.demo("MIconsDemo") }}
