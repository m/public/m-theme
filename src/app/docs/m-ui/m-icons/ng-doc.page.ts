import MUiCategory from '@categories/m-ui/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MIconsPage: NgDocPage = {
	title: `M-icons`,
	mdFile: './index.md',
	category: MUiCategory
};

export default MIconsPage;
