import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatInputModule} from '@angular/material/input';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {IconMapping, ICONS_MAPPING, MIcons} from '@metromobilite/m-ui/m-icons';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-icon-demo',
	standalone: true,
	imports: [CommonModule, Preview, MIcons, MatTooltipModule, MatInputModule],
	templateUrl: './icon-demo.component.html',
	styleUrls: ['./icon-demo.component.scss'],
	providers: [MatSnackBar]
})
export class MIconsDemo {

	public iconList: string[] = ICONS_MAPPING.map((icon) => icon.type);

	constructor(private snackBar: MatSnackBar) {}

	applyFilter(event: Event): void {
		const filterValue = (event.target as HTMLInputElement).value;
		this.iconList = ICONS_MAPPING.map((icon: IconMapping) => icon.type).filter((icon: string) => icon.toLowerCase().includes(filterValue.toLowerCase()));
	}

	copyToClipboard(text: string): void {
		navigator.clipboard.writeText(text).then(() => {
			this.snackBar.open(`Texte copié : ${text}`, 'Fermer', {duration: 2000});
		}).catch(err => {
			console.error('Erreur lors de la copie :', err);
		});
	}
}
