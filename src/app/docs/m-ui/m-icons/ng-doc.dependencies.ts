import {NgDocDependencies} from '@ng-doc/core';
import {MIconsDemo} from 'src/app/docs/m-ui/m-icons/icon-demo/icon-demo.component';
import {MIconsPageModule} from 'src/app/docs/m-ui/m-icons/ng-doc.module';

const MIconsPageDependencies: NgDocDependencies = {
	module: MIconsPageModule,
	// Add your demos that you are going to use in the page here
	demo: {MIconsDemo},
};

export default MIconsPageDependencies;
