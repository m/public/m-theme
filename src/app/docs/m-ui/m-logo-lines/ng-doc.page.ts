import MUiCategory from '@categories/m-ui/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MLogoLinesPage: NgDocPage = {
	title: `M-logo-lines`,
	mdFile: './index.md',
	category: MUiCategory
};

export default MLogoLinesPage;
