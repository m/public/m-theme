# {{ NgDocPage.title }}

## API
Shows how to use the `MLogoLines` component.

## Demo
{{ NgDocActions.demo("LogoLinesDemo") }}
