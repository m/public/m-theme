import {CommonModule} from '@angular/common';
import {HttpClient} from '@angular/common/http';
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {Line, MLogoLines} from '@metromobilite/m-ui/m-logo-lines';
import {Subject, takeUntil} from 'rxjs';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-logo-lines-demo',
	standalone: true,
	imports: [CommonModule, Preview, MLogoLines],
	templateUrl: './logo-lines-demo.component.html',
	styleUrls: ['./logo-lines-demo.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class LogoLinesDemo implements OnDestroy {
	public lines: any = [
		// {'id': 'SEM:E', 'gtfsId': 'SEM:E', 'shortName': 'E', 'longName': 'FONTANIL-CORNILLON Palluel / GRENOBLE Louise Michel', 'color': '533786', 'textColor': 'FFFFFF', 'mode': 'TRAM', 'type': 'TRAM'},
		// {'id': 'SEM:C13', 'gtfsId': 'SEM:13', 'shortName': 'C13', 'longName': 'VIZILLE Chantefeuille / GRENOBLE Gare Routière / VOREPPE Gare SNCF', 'color': 'EF7C00', 'textColor': 'FFFFFF', 'mode': 'BUS', 'type': 'CHRONO_PERI'},
		// {'id': 'SEM:25', 'gtfsId': 'SEM:25', 'shortName': '25', 'longName': 'GRENOBLE Colonel Dumont / LE GUA Les Saillants', 'color': '1E71B8', 'textColor': 'FFFFFF', 'mode': 'BUS', 'type': 'PROXIMO'}
	];
	private destroy$: Subject<boolean> = new Subject<boolean>();

	constructor(private httpCLient: HttpClient, private cdRef: ChangeDetectorRef) {
		// Load all lines from the API
		this.httpCLient.get('https://data-pp.mobilites-m.fr/api/routers/default/index/routes')
			.pipe(takeUntil(this.destroy$))
			.subscribe((data: any) => {
				this.lines = [...data];
				this.cdRef.markForCheck();
			});
	}


	/**
	 * Check or uncheck a line
	 * @param {{line: Line, checked: boolean}} evt
	 * @returns void
	 */
	changeLine(evt: {line: Line, checked: boolean}): void {
		console.log(evt);
	}


	ngOnDestroy(): void {
		this.destroy$.next(true);
		this.destroy$.complete();
	}
}
