import {NgDocDependencies} from '@ng-doc/core';
import {LogoLinesDemo} from 'src/app/docs/m-ui/m-logo-lines/logo-lines-demo/logo-lines-demo.component';
import {MLogoLinesPageModule} from 'src/app/docs/m-ui/m-logo-lines/ng-doc.module';

const MLogoLinesPageDependencies: NgDocDependencies = {
	module: MLogoLinesPageModule,
	// Add your demos that you are going to use in the page here
	demo: {LogoLinesDemo},
};

export default MLogoLinesPageDependencies;
