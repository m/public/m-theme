import {MListWrapperDemo} from '@m-ui/m-list-wrapper/m-list-wrapper-demo/m-list-wrapper-demo.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MListWrapperPageModule} from './ng-doc.module';

const MListWrapperPageDependencies: NgDocDependencies = {
	module: MListWrapperPageModule,
	// Add your demos that you are going to use in the page here
	demo: {MListWrapperDemo},
};

export default MListWrapperPageDependencies;
