import {Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatListModule} from '@angular/material/list';
import {MListWrapper} from '@metromobilite/m-ui/lib';


@Component({
	selector: 'app-m-list-wrapper-demo',
	standalone: true,
	imports: [CommonModule, MListWrapper, MatListModule],
	templateUrl: './m-list-wrapper-demo.component.html',
	styleUrls: ['./m-list-wrapper-demo.component.scss']
})
export class MListWrapperDemo {

	public items: string[] = ['Item 1', 'Item 2', 'Item 3', 'Item 4', 'Item 5', 'Item 6', 'Item 7', 'Item 8', 'Item 9', 'Item 10'];

}
