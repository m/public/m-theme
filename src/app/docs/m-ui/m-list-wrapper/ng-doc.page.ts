import MUiCategory from '@categories/m-ui/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MListWrapperPage: NgDocPage = {
	title: `M-list-wrapper`,
	mdFile: './index.md',
	category: MUiCategory
};

export default MListWrapperPage;
