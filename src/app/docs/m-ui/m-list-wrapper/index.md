# {{ NgDocPage.title }}


## API
Shows how to use the `MListWrapper` component.

## Demo
{{ NgDocActions.demo("MListWrapperDemo") }}
