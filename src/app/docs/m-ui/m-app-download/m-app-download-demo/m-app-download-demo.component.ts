import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MAppDownload} from '@metromobilite/m-ui/lib/m-app-download';

@Component({
	selector: 'appm-app-download-demo-demo',
	standalone: true,
	imports: [CommonModule, MAppDownload],
	templateUrl: './m-app-download-demo.component.html',
	styleUrls: ['./m-app-download-demo.component.scss']
})
export class MAppDownloadDemo {

	public data = {
		'Lien': 'destination',
		'Logo': {
			'url': 'https://data-pp.mobilites-m.fr/uploads/computer_line_546bba1226.svg'
		},
		'TexteGras': 'App Store',
		'Texte': 'Télécharger en'
	};
}
