import MUiCategory from '@categories/m-ui/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MAppDownloadPage: NgDocPage = {
	title: `M-app-download`,
	mdFile: './index.md',
	category: MUiCategory
};

export default MAppDownloadPage;
