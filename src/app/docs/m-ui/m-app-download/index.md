# {{ NgDocPage.title }}

## API
Shows how to use the `MAppDownload` component.

## Demo
{{ NgDocActions.demo("MAppDownloadDemo") }}
