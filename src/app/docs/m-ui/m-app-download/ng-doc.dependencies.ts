import {MAffluenceDemo} from '@m-ui/m-affluence/m-affluence-demo/m-affluence-demo.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MAppDownloadPageModule} from './ng-doc.module';
import { MAppDownloadDemo } from './m-app-download-demo/m-app-download-demo.component';

const MAppDownloadPageDependencies: NgDocDependencies = {
	module: MAppDownloadPageModule,
	demo: {MAppDownloadDemo},
};

export default MAppDownloadPageDependencies;
