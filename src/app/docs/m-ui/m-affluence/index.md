# {{ NgDocPage.title }}

## API
Shows how to use the `MAffluence` component.

## Demo
{{ NgDocActions.demo("MAffluenceDemo") }}
