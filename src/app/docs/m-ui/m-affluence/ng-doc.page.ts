import MUiCategory from '@categories/m-ui/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MAffluencePage: NgDocPage = {
	title: `M-affluence`,
	mdFile: './index.md',
	category: MUiCategory
};

export default MAffluencePage;
