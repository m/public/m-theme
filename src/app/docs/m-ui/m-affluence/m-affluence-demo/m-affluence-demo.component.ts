import {Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatExpansionModule} from '@angular/material/expansion';
import {MAffluence} from '@metromobilite/m-ui/m-affluence';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-m-affluence-demo',
	standalone: true,
	imports: [CommonModule, MAffluence, MatExpansionModule, Preview],
	templateUrl: './m-affluence-demo.component.html',
	styleUrls: ['./m-affluence-demo.component.scss']
})
export class MAffluenceDemo {

	public services = [
		{lvl: null, name: 'Liste compléte des affluences'},
		{lvl: 1, name: 'Affluence faible'},
		{lvl: 2, name: 'Affluence modérée'},
		{lvl: 3, name: 'Affluence forte'},
		{lvl: 1, name: 'Affluence faible [lightMode]', lightMode: true},
		{lvl: 2, name: 'Affluence modérée [lightMode]', lightMode: true},
		{lvl: 3, name: 'Affluence forte [lightMode]', lightMode: true}
	];
}
