import {MAffluenceDemo} from '@m-ui/m-affluence/m-affluence-demo/m-affluence-demo.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MAffluencePageModule} from './ng-doc.module';

const MAffluencePageDependencies: NgDocDependencies = {
	module: MAffluencePageModule,
	demo: {MAffluenceDemo},
};

export default MAffluencePageDependencies;
