import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MDisturbanceDisplay} from '@metromobilite/m-ui/lib/m-disturbance-display';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-disturbance-display-demo',
	standalone: true,
	imports: [CommonModule, MDisturbanceDisplay, Preview],
	templateUrl: './m-disturbance-display-demo.component.html',
	styleUrls: ['./m-disturbance-display-demo.component.scss']
})
export class MDisturbanceDisplayDemo {

	public disturbance = {
		activateHostbinding: true,
		calculatedClasses: "",
		disturbanceOptions: {lineOrNsv: 1},
		hasDisturbance: false,
		hostClass: "",
		nsv: 1,
		outer: true
	};

	public disturbanceCollection: any = {
		disturbances: [
			{
				'type': 'restriction_ltc',
				'code': 'SEM_1396148590774867315',
				'dateDebut': '08/01/2025 09:00',
				'dateFin': '08/01/2025 16:30',
				'heureDebut': '00:00:00',
				'heureFin': '00:00:00',
				'latitude': -1,
				'longitude': -1,
				'weekEnd': '2',
				'texte': 'A : travaux|jusqu\'au 08/01/2025 16:30|La ligne A ne circule pas entre les stations Polesud - Alpexpo et Malherbe en raison de travaux.\nDes bus relais effectuent la liaison entre ces stations. \n \nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.',
				'titre': 'travaux',
				'description': 'jusqu\'au 08/01/2025 16:30\r\nLa ligne A ne circule pas entre les stations Polesud - Alpexpo et Malherbe en raison de travaux.\nDes bus relais effectuent la liaison entre ces stations. \n \nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.',
				'plan': 'https://www.tag.fr/include/downloadInfoCompPerturbation.php?f=Info coupure A _Malherbe Pole Sud.pdf',
				'visibleTC': true,
				'visibleVoiture': false,
				'visibleVelo': false,
				'visibleVenteTitres': false,
				'visibleBandeauSite': false,
				'visibleBandeauAppli': false,
				'listeLigne': 'SEM_A',
				'listeLigneArret': 'SEM_A',
				'nsv_id': 2,
				'computed': {
					'title': 'travaux',
					'content': 'jusqu\'au 08/01/2025 16:30<br>La ligne A ne circule pas entre les stations Polesud - Alpexpo et Malherbe en raison de travaux.\nDes bus relais effectuent la liaison entre ces stations. \n \nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.',
					'line': {
						'id': 'SEM:A',
						'gtfsId': 'SEM:A',
						'shortName': 'A',
						'longName': 'FONTAINE La Poya / PONT DE CLAIX L\'Etoile',
						'color': '3376B8',
						'textColor': 'FFFFFF',
						'mode': 'TRAM',
						'type': 'TRAM',
						'pdf': 'https://data-pp.mobilites-m.fr/api/planligne/pdf?route=SEM:A'
					}
				}
			},
			{
				'type': 'restriction_ltc',
				'code': 'SEM_5828443584937855085',
				'dateDebut': '09/01/2025 22:00',
				'dateFin': '10/01/2025 03:00',
				'heureDebut': '22:00',
				'heureFin': '02:00',
				'latitude': -1,
				'longitude': -1,
				'weekEnd': '0',
				'texte': 'A : travaux de nuit Grand\'Place|à partir du 09/01/2025 22:00|jusqu\'au 10/01/2025 03:00|La ligne A ne circule pas entre les stations L\'Etoile et Chavant en raison de travaux de nuit secteur Grand\'Place.\nDes bus relais effectuent la liaison entre ces stations.\n \nÀ noter que la station Echirolles Gare n\'est pas desservie par les bus relais.\n \nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.',
				'titre': 'travaux de nuit Grand\'Place',
				'description': 'à partir du 09/01/2025 22:00|jusqu\'au 10/01/2025 03:00\r\nLa ligne A ne circule pas entre les stations L\'Etoile et Chavant en raison de travaux de nuit secteur Grand\'Place.\nDes bus relais effectuent la liaison entre ces stations.\n \nÀ noter que la station Echirolles Gare n\'est pas desservie par les bus relais.\n \nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.',
				'plan': 'https://www.tag.fr/include/downloadInfoCompPerturbation.php?f=Info Coupure A LEtoile-Chavant.pdf',
				'visibleTC': true,
				'visibleVoiture': false,
				'visibleVelo': false,
				'visibleVenteTitres': false,
				'visibleBandeauSite': false,
				'visibleBandeauAppli': false,
				'listeLigne': 'SEM_A',
				'listeLigneArret': 'SEM_A',
				'listeArret': 'SEM:2207,SEM:2208',
				'nsv_id': 2,
				'computed': {
					'title': 'travaux de nuit Grand\'Place',
					'content': 'à partir du 09/01/2025 22:00<br>jusqu\'au 10/01/2025 03:00<br>La ligne A ne circule pas entre les stations L\'Etoile et Chavant en raison de travaux de nuit secteur Grand\'Place.\nDes bus relais effectuent la liaison entre ces stations.\n \nÀ noter que la station Echirolles Gare n\'est pas desservie par les bus relais.\n \nLes bus relais sont accessibles aux personnes à mobilité réduite sauf aux arrêts indiqués sur le plan. \nRetrouvez le plan détaillé des bus relais et les arrêts de report en téléchargement sur reso-m.fr/trafic.',
					'line': {
						'id': 'SEM:A',
						'gtfsId': 'SEM:A',
						'shortName': 'A',
						'longName': 'FONTAINE La Poya / PONT DE CLAIX L\'Etoile',
						'color': '3376B8',
						'textColor': 'FFFFFF',
						'mode': 'TRAM',
						'type': 'TRAM',
						'pdf': 'https://data-pp.mobilites-m.fr/api/planligne/pdf?route=SEM:A'
					}
				}
			}
		],
		dis: {
			hasDisturbance: true,
			nsv: 2,
			calculatedClasses: 'has-disturbance',
		}
	};


	updateDisturbance(nsv: number): any {
		const disturbance = JSON.parse(JSON.stringify(this.disturbance));
		disturbance.nsv = nsv;
		disturbance.disturbanceOptions.lineOrNsv = nsv;
		return  disturbance;
	}

}
