import MUiCategory from '@categories/m-ui/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MDisturbanceDisplayPage: NgDocPage = {
	title: `M-disturbance-display`,
	mdFile: './index.md',
	category: MUiCategory
};

export default MDisturbanceDisplayPage;
