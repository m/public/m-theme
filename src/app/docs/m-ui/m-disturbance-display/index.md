# {{ NgDocPage.title }}

## API
Shows how to use the `MDisturbanceDisplay` component.

## Default table

{{ NgDocActions.demo("MDisturbanceDisplayDemo") }}
