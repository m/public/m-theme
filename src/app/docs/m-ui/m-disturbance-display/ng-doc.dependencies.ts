import {MDisturbanceDisplayDemo} from '@m-ui/m-disturbance-display/m-disturbance-display-demo/m-disturbance-display-demo.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MDisturbanceDisplayPageModule} from './ng-doc.module';

const MDisturbanceDisplayPageDependencies: NgDocDependencies = {
	module: MDisturbanceDisplayPageModule,
	// Add your demos that you are going to use in the page here
	demo: {MDisturbanceDisplayDemo},
};

export default MDisturbanceDisplayPageDependencies;
