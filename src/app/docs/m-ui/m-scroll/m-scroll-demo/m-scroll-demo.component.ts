import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-m-scroll-demo',
	standalone: true,
	imports: [CommonModule, Preview],
	templateUrl: './m-scroll-demo.component.html',
	styleUrls: ['./m-scroll-demo.component.scss']
})
export class MScrollDemo {

}
