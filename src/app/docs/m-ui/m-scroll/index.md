# {{ NgDocPage.title }}

The scrollbar is customized natively (no additional classes are needed). However, Firefox does not allow for as much precise customization of the scrollbar as other browsers do.
[More infomation](https://developer.mozilla.org/fr/docs/Web/CSS/::-webkit-scrollbar)

{{ NgDocActions.demo("MScrollDemo") }}
