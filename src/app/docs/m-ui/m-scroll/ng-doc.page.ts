import MUiCategory from '@categories/m-ui/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MScrollPage: NgDocPage = {
	title: `M-scroll`,
	mdFile: './index.md',
	category: MUiCategory
};

export default MScrollPage;
