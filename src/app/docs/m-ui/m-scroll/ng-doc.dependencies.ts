import {MScrollDemo} from '@m-ui/m-scroll/m-scroll-demo/m-scroll-demo.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MScrollPageModule} from './ng-doc.module';

const MScrollPageDependencies: NgDocDependencies = {
	module: MScrollPageModule,
	// Add your demos that you are going to use in the page here
	demo: {MScrollDemo}
};

export default MScrollPageDependencies;
