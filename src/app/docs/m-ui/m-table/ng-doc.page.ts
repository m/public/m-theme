import MUiCategory from '@categories/m-ui/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MTablePage: NgDocPage = {
	title: `M-table`,
	mdFile: './index.md',
	category: MUiCategory
};

export default MTablePage;
