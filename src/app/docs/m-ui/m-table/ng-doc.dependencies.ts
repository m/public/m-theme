import {MTableDemo} from '@m-ui/m-table/table-demo/m-table-demo.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MTablePageModule} from './ng-doc.module';

const MTablePageDependencies: NgDocDependencies = {
	module: MTablePageModule,
	// Add your demos that you are going to use in the page here
	demo: {MTableDemo}
};

export default MTablePageDependencies;
