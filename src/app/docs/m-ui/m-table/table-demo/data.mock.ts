export const dataMock = [
	{
		"type": "restriction_ltc",
		"code": "SEM_1234567890123456789",
		"dateDebut": "11/15/2024 23:30",
		"dateFin": "11/15/2024 23:30",
		"latitude": 5.12345678,
		"titre": "maintenance de nuit",
		"plan": "https://www.tag.fr/include/downloadInfoCompPerturbation.php?f=Info_Coupure_LaBastille-Chavant.pdf",
		"visibleTC": false,
		"isVisible": {
			"TC": false,
			"ROUTIER": true
		},
		"testSelect": "demo_2",
		'role': 'adm',
		'icon': 'serviceDisruption',
		'line': {
			'SEM:E': {'id': 'SEM:E', 'gtfsId': 'SEM:E', 'shortName': 'E', 'longName': 'FONTANIL-CORNILLON Palluel / GRENOBLE Louise Michel', 'color': '533786', 'textColor': 'FFFFFF', 'mode': 'TRAM', 'type': 'TRAM'}
		}
	},
	{
		"type": "restriction_ltc",
		"code": "SEM_9876543210987654321",
		"dateDebut": "11/16/2024 21:00",
		"dateFin": "1733123015413",
		"latitude": 5.87654321,
		"titre": "changement de voies",
		"plan": "https://www.tag.fr/include/downloadInfoCompPerturbation.php?f=Info_Coupure_LesSablons-Chavant.pdf",
		"visibleTC": true,
		"isVisible": {
			"TC": true,
			"ROUTIER": false
		},
		"testSelect": "demo_3",
		'role': 'usr',
		'icon': 'peopleOnRoadway',
		'line': {
			"SEM:C13": {'id': 'SEM:C13', 'gtfsId': 'SEM:13', 'shortName': 'C13', 'longName': 'VIZILLE Chantefeuille / GRENOBLE Gare Routière / VOREPPE Gare SNCF', 'color': 'EF7C00', 'textColor': 'FFFFFF', 'mode': 'BUS', 'type': 'CHRONO_PERI'}
		}
	},
	{
		"type": "restriction_ltc",
		"code": "SEM_1112131415161718192",
		"dateDebut": "11/17/2024 22:15",
		"latitude": 5.65432109,
		"titre": "entretien du réseau",
		"plan": "https://www.tag.fr/include/downloadInfoCompPerturbation.php?f=Info_Coupure_LeVillage-Chavant.pdf",
		"visibleTC": false,
		"isVisible": {
			"TC": true,
			"ROUTIER": true
		},
		"testSelect": "demo_4",
		'role': 'adm',
		'icon': 'flooding',
		'line': {
			"C38_150837_T73": {
				"id": "C38:T73",
				"gtfsId": "C38:T73",
				"shortName": "T73",
				"longName": "LES DEUX ALPES-BOURG D'OISANS-GRENOBLE",
				"color": "f29549",
				"textColor": "FFFFFF",
				"mode": "BUS",
				"type": "C38_AUTRE"
			}
		}
	},
	{
		"type": "restriction_ltc",
		"code": "SEM_9876123456789101112",
		"dateDebut": "11/18/2024 20:45",
		"latitude": 5.43219876,
		"titre": "fermeture temporaire",
		"plan": "https://www.tag.fr/include/downloadInfoCompPerturbation.php?f=Info_Coupure_LePont-Chavant.pdf",
		"visibleTC": true,
		"isVisible": {
			"TC": false,
			"ROUTIER": true
		},
		"testSelect": "demo_5",
		'role': 'test',
		'icon': 'citiz',
		'line': {
			"C38_150837_VIZ03": {
				"id": "C38:VIZ03",
				"gtfsId": "C38:VIZ03",
				"shortName": "VIZ03",
				"longName": "BOURG D'OISANS-VIZILLE",
				"color": "37333a",
				"textColor": "ffffff",
				"mode": "BUS",
				"type": "SCOL"
			},
			"C38_150837_T70": {
				"id": "C38:T70",
				"gtfsId": "C38:T70",
				"shortName": "T70",
				"longName": "BOURG D'OISANS-ALLEMONT-VAUJANY (TAD)",
				"color": "65b363",
				"textColor": "FFFFFF",
				"mode": "BUS",
				"type": "C38_AUTRE"
			},
			"C38_150837_T77": {
				"id": "C38:T77",
				"gtfsId": "C38:T77",
				"shortName": "T77",
				"longName": "BOURG D'OISANS-LA BERARDE",
				"color": "65b363",
				"textColor": "FFFFFF",
				"mode": "BUS",
				"type": "C38_AUTRE"
			},
			"C38_150837_T75": {
				"id": "C38:T75",
				"gtfsId": "C38:T75",
				"shortName": "T75",
				"longName": "BOURG D'OISANS-VIZILLE-GRENOBLE",
				"color": "f29549",
				"textColor": "FFFFFF",
				"mode": "BUS",
				"type": "C38_AUTRE"
			},
			"C38_150837_T76": {
				"id": "C38:T76",
				"gtfsId": "C38:T76",
				"shortName": "T76",
				"longName": "L'ALPE D'HUEZ-BOURG D'OISANS-GRENOBLE",
				"color": "f29549",
				"textColor": "FFFFFF",
				"mode": "BUS",
				"type": "C38_AUTRE"
			},
			"C38_150837_T73": {
				"id": "C38:T73",
				"gtfsId": "C38:T73",
				"shortName": "T73",
				"longName": "LES DEUX ALPES-BOURG D'OISANS-GRENOBLE",
				"color": "f29549",
				"textColor": "FFFFFF",
				"mode": "BUS",
				"type": "C38_AUTRE"
			}
		}
	}
]
