import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {dataMock} from '@m-ui/m-table/table-demo/data.mock';
import {MTable} from '@metromobilite/m-ui/m-table';
import {ConfigTable, HeaderTypes} from '@metromobilite/m-ui/m-table/m-table.interface';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-m-table-demo',
	standalone: true,
	imports: [CommonModule, Preview, MTable, MatButtonModule],
	templateUrl: './m-table-demo.component.html',
	styleUrls: ['./m-table-demo.component.scss']
})
export class MTableDemo {

	public roleLabels = {adm: 'admin', usr: 'user', test: 'testeur'};

	public dataTable: ConfigTable = {
		data: [...dataMock, ...dataMock, ...dataMock, ...dataMock, ...dataMock, ...dataMock, ...dataMock, ...dataMock, ...dataMock],
		headers: [
			{title: '', key: '0', type: HeaderTypes.SELECTION},
			{title: HeaderTypes.MATCHING, type: HeaderTypes.MATCHING, key: 'role', data: this.roleLabels, options: {class: (row: any) => ['demo']}},
			{title: HeaderTypes.TEXT, type: HeaderTypes.TEXT, key: 'titre'},
			{title: HeaderTypes.DATE, type: HeaderTypes.DATE, key: 'dateDebut'},
			{title: HeaderTypes.RELATIVE_DATE, type: HeaderTypes.RELATIVE_DATE, key: 'dateFin'},
			{title: HeaderTypes.NUMBER, type: HeaderTypes.NUMBER, key: 'latitude'},
			{title: HeaderTypes.LINK, type: HeaderTypes.LINK, key: 'plan'},
			{title: HeaderTypes.ICON, type: HeaderTypes.ICON, key: 'icon'},
			{title: HeaderTypes.LINE, type: HeaderTypes.LINE, key: 'line'},
			{title: HeaderTypes.BOOLEAN, type: HeaderTypes.BOOLEAN, key: 'visibleTC'},
			{title: `${HeaderTypes.BOOLEAN} + Other keys`, type: HeaderTypes.BOOLEAN, key: 'isVisibleROUTIER', otherKey: ['isVisible', 'ROUTIER']},
			{title: HeaderTypes.SELECT, type: HeaderTypes.SELECT, key: 'testSelect', selectOptions: {
				method: (evt: any, data: any) => {
					this.change(evt, data)
				},
				options: ['demo_1', 'demo_2', 'demo_3']
			}},
			{title: 'Actions', key: 'action', type: HeaderTypes.ACTIONS}
		],
		label: 'Demo',
		config: {
			pageSize: [5, 10, 20, 60, 120],
			defaultOrderColumn: {
				column: 'creationTime',
				order: 'asc'
			},
			showFilter: true
		},
		actions: {
			line: (row) => console.log(row),
			column: [
				{title: 'log', class: '', icon: 'install_desktop', disabled: () => false, action: (p: any, event: Event) => console.log('action -> log'), isMenu: false}
			]
		},
		type: 'demoType'
	}

	change(evt: any, data: any) {
		console.log(evt, data);
	}

	selection(evt: any) {
		console.log(evt);
	}

}
