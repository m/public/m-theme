# {{ NgDocPage.title }}

## API
Shows how to use the `MTable` component.

## Default table

{{ NgDocActions.demo("MTableDemo") }}
