# {{ NgDocPage.title }} M-UI | M-THEME

M-UI is a component library built on top of [Angular Material 15](https://v15.material.angular.io/), designed to extend its capabilities while leveraging M-Theme for styling consistency. It provides:

* Ready-to-use Angular components tailored for specific use cases.
* An intuitive API for developers, enabling seamless integration.
* Theming support out-of-the-box, powered by M-Theme.
---


## Motivation

While working on various projects, especially when building Angular applications, I noticed a common need for:

1. A unified theming system that simplifies the customization of Angular Material components.
2. A component library that goes beyond the basic Angular Material design system to meet specific business needs.

Other solutions often fell short:

- Writing custom themes manually was repetitive and prone to errors.
- Extending Angular Material components often required complex configurations.
- Integrating theming and custom components into a single cohesive package was challenging.

M-Theme and M-UI address these gaps:

- M-Theme simplifies theming by centralizing styles, reducing repetitive code, and offering pre-built palettes and functions.
- M-UI extends Angular Material with additional components, allowing developers to create rich, feature-complete applications faster.

## How does it work?

### M-Theme

- Provides SCSS files for defining light and dark themes.
- Offers pre-configured variables and helper functions to make styling Angular Material components easier.
- Works seamlessly with Angular's build process, requiring only the addition of the necessary imports in your main SCSS file.

### M-ui

- Built as a library of Angular components that extend Angular Material's functionality.
- Components are designed to be modular:
  - Import only the components you need or the entire library.
  - All components are styled consistently using M-Theme.
- Supports standalone usage and integration into existing Angular projects.
