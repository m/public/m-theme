import GettingStartedCategory from '@categories/getting-started/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const WhatIsPage: NgDocPage = {
	title: `What is`,
	mdFile: './index.md',
	order: 1,
	keyword: 'what_is',
	category: GettingStartedCategory
};

export default WhatIsPage;
