import GettingStartedCategory from '@categories/getting-started/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const PeerDependencyPage: NgDocPage = {
	title: `Peer dependency`,
	mdFile: './index.md',
	category: GettingStartedCategory,
  	order: 2
};

export default PeerDependencyPage;
