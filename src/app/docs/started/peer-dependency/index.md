M-UI depends on Angular Material and Angular CDK.
We need to install these dependencies before installing M-UI.

| Peer Dependency        | Version |
|------------------------|---------|
| @angular/common        | 15.2.0  |
| @angular/cdk           | 15.2.0  |
| @angular/core          | 15.2.0  |
| @angular/material      | 15.2.8  |
