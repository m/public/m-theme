import GettingStartedCategory from '@categories/getting-started/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const DocsPage: NgDocPage = {
	title: `Docs`,
	mdFile: './index.md',
	category: GettingStartedCategory,
	order: 5
};

export default DocsPage;
