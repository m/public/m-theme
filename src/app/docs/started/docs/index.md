# Creating a New Component
## Generate the Component

* Use the following command to create a new component in the projects\m-ui\src\lib folder:

```bash
ng g c m-{component-name}
```

* Add an Index File
  In the projects\m-ui\src\lib folder, create an index.ts file and export the new component:

```typescript
export * from './m-{component-name}.component';
```

**note**: Remember to document all component parts to ensure they appear in the auto-generated documentation.

## Create a Documentation Page

* Use the following command to generate a new documentation page (with the --demo flag to create a demo page) in the src\app\docs folder:

```bash
ng g @ng-doc/builder:page m-{component-name} --demo
```

* Update index.ts for Documentation Display
  In the documentation index, include relevant sections to correctly display documentation and the menu. Add the following structure:

```markdown
## API
Shows how to use the `M{Component}` component.

## Demo
{{ NgDocActions.demo("DemoComponent") }}

```

* Add Demo Component to `ng-doc.dependencies.ts`
  Register the demo component:

```typescript
demo: {DemoComponent}
```

* Add Component Category in `ng-doc.config.ts`
  Assign a category to the documentation:

```
category: MUiCategory
```

* Create a Demo Component
  To demonstrate the component’s usage, create a demo component in the `src\app\docs\m-{component-name}` folder:

```bash
ng g c {component-name}-demo
```

## API DOCS

see more at [API DOCS](/api) or [ngDoc](https://github.com/ng-doc/ng-doc/tree/v15.13.0)
