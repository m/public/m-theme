# {{ NgDocPage.title }}
## Installing the Library
1. Install via npm

Run the following command to install the latest version of the library:
```bash
npm i @metromobilite/m-ui@latest
```

## Configuration

### M-Theme Setup
#### Step 1: Add stylePreprocessorOptions in angular.json
> **Note**
> The ``stylePreprocessorOptions`` in ``angular.json`` allows you to define custom paths for Sass to search for files during ``@use`` or ``@import``.
>
> Adding 'includePaths': ['node_modules'] lets Sass resolve imports from node_modules directly, enabling simplified imports for libraries.


```json
{
	"projects": {
		"project-name": {
			"architect": {
				"build": {
					"options": {
						"stylePreprocessorOptions": {
							"includePaths": ["node_modules"]
						}
					}
				}
			}
		}
	}	
}
```

---
#### Step 2: Add Assets in angular.json
To ensure that theme-related assets are properly loaded, include the following in the assets array of ``angular.json``:

```json
{
	"assets": [
		{
			"glob": "**/*",
			"input": "./node_modules/@metromobilite/m-ui/theme/assets/",
			"output": "./assets/"
		}
	]	
}
```
---
#### Step 3: Import Styles in styles.scss
In your main SCSS file (e.g., src/styles.scss), import the M-Theme styles.
You can choose to import all styles or only specific ones.

##### Option 1: Import All Styles

```scss
$assets-path: '/assets'; // Define the assets path
@use "@metromobilite/m-ui/theme/style";
```

##### Option 2: Import Specific Files
If you need more control over the imported styles, you can import them one by one:

```scss
@import 'reset';
@import 'variables';
@import 'font';
@import '@metromobilite/m-ui/theme/theme';
@import 'classes';
@import 'components/index';
```
---

### M-UI Setup

#### Add Icon Assets in ``angular.json``

To use the icon library provided by M-UI, include the following configuration in ``angular.json`` under the assets array:

```json
{
	"assets": [
		{
			"glob": "**/*",
			"input": "node_modules/@metromobilite/m-ui/src/assets",
			"output": "/assets/"
		}
	]	
}
```
