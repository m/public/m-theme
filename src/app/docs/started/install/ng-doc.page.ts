import GettingStartedCategory from '@categories/getting-started/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const InstallPage: NgDocPage = {
	title: `Install`,
	mdFile: './index.md',
	category: GettingStartedCategory,
	order: 3,
	keyword: 'm_ui_install'
};

export default InstallPage;
