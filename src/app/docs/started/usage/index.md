# {{ NgDocPage.title }}

## M-Theme

To enable light or dark themes, add the ``dark-theme`` or ``light-theme`` class to the ``<body>`` tag in your ``index.html``:


```html
<body class="dark-theme">
	<!-- Your app content -->
</body>
```

This theme provide custom palettes and provides the following variables:

* `$light-theme-background`
* `$light-theme-foreground`
* `$dark-theme-background`
* `$dark-theme-foreground`

Example:

```scss
.my-container {
  color: map-get($dark-theme-foreground, text);
}
```

Some helper functions are also provided:

* `dark-color-overlay($elevation)`
* `light-color-overlay($elevation)`

`$elevation` must be a value in `[0, 1, 2, 3, 4, 6, 8, 12, 16, 24]`

Example:

```scss
.my-container {
	background: dark-color-overlay(16);
}
```
---


#### Helper Functions and Variables

##### Variables
M-Theme provides pre-defined variables for customizing the look and feel of your app:

- ``$light-theme-background``
- ``$light-theme-foreground``
- ``$dark-theme-background``
- ``$dark-theme-foreground``

Example:
```scss
.my-container {
  color: map-get($dark-theme-foreground, text);
}
```
---
##### Helper Functions
Use helper functions for overlays and dynamic styles:

- ``dark-color-overlay($elevation)``
- ``light-color-overlay($elevation)``

> **Note**
> $elevation must be one of [0, 1, 2, 3, 4, 6, 8, 12, 16, 24].

Example:

```scss
.my-container {
  background: dark-color-overlay(16);
}
```

#### Testing the Installation
1. Ensure all configurations in angular.json are correctly applied.
2. Use the imported SCSS variables and functions in your components to verify that they work as expected.
3. If icons or assets aren't loading, double-check the assets array in angular.json.

---

## M-ui
All library components are prefixed with `m-` and are designed as standalone components.

```html
<m-icons type="accident" class="m-icon"></m-icons>
```

### Default import
We can import the entire library in our Angular module.

```typescript
import {MIcons} from '@metromobilite/m-ui';

@NgModule({
  declarations: [AppComponent], 
  imports: [MIcons], 
  bootstrap: [AppComponent]
})
export class AppModule {}
```

### Import unitary components

We can also import the components individually.

```typescript
import {MIcons} from '@metromobilite/m-ui/m-icons';
```

> **Note**
> Add this in your tsconfig.json
> ```json
> "compilerOptions": {
>   "paths": {
>     "@metromobilite/m-ui/*": [
>       "node_modules/@metromobilite/m-ui",
>       "node_modules/@metromobilite/m-ui/lib/*"
>     ]
>   } 
> }
