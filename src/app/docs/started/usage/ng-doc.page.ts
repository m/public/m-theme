import GettingStartedCategory from '@categories/getting-started/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const UsagePage: NgDocPage = {
	title: `Usage`,
	mdFile: './index.md',
	keyword: 'usage',
	category: GettingStartedCategory,
	order: 4
};

export default UsagePage;
