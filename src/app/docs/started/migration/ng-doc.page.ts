import GettingStartedCategory from '@categories/getting-started/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MigrationPage: NgDocPage = {
	title: `Migration`,
	mdFile: './index.md',
	category: GettingStartedCategory,
	order: 6
};

export default MigrationPage;
