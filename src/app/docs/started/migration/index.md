# {{ NgDocPage.title }}

## M-theme (3.1.28) to M-ui (15.0.1)

1. Remove ``M-theme`` from ``package.json``
2. Delete the ``package.lock`` file and the ``node_modules`` folder
3. Add m-ui (`*m_ui_install`)

>**Notes**
> If M-theme is already installed, remember to update the imports in the .scss files and in angular.json.
