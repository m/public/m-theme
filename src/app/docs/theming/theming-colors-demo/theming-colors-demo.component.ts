import {NgForOf} from '@angular/common';
import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {THEME} from 'src/app/app.component';
import {ThemingService} from 'src/app/services/theming.service';

export interface PaletteColor {
	[palette: string]: { name: string; color: string; contrast: string; usage: string }[];
}

@Component({
	selector: 'app-theming-colors-demo',
	templateUrl: './theming-colors-demo.component.html',
	styleUrls: ['./theming-colors-demo.component.scss'],
	imports: [
		NgForOf
	],
	standalone: true
})
export class ThemingColorsDemo implements OnInit {
	colors: PaletteColor = {};
	theme: THEME = this.themingService.theming;
	palettes = ['primary', 'accent', 'warn'];

	constructor(private themingService: ThemingService,
				private cdRef: ChangeDetectorRef) {
		this.themingService.themingObservable().subscribe(theme => {
			if (theme !== this.theme) {
				this.theme = theme;
				this.loadColors(this.theme);
				this.cdRef.detectChanges();
			}
		});
	}

	ngOnInit(): void {
		// Appeler `getComputedStyle` après le chargement de la vue
		this.loadColors(this.theme);
	}

	loadColors(theme: THEME) {
		console.log(theme);
		this.colors = {};

		// Obtenir les variables CSS de la palette
		const styles = getComputedStyle(document.documentElement);

		// Nom des palettes et nuances
		const palettesVar =  {
			primary: '$primary-palette',
			accent: '$accent-palette',
			warn: '$warn-palette'
		}
		this.palettes.forEach(palette => this.colors[palette] = []);

		const shades = ['50', '100', '200', '300', '400', '500', '600', '700', '800', '900', 'A100', 'A200', 'A400', 'A700'];
		const additionalKeys = ['default', 'lighter', 'darker', 'text']; // Pour les autres couleurs

		// Récupérer chaque couleur avec ses contrastes associés
		this.palettes.forEach(palette => {
			// Boucle pour chaque nuance standard
			shades.forEach(shade => {
				const colorVar = `--${theme}--${palette}-${shade}`;
				const contrastVar = `${colorVar}-contrast`;

				const colorValue = styles.getPropertyValue(colorVar).trim();
				const contrastValue = styles.getPropertyValue(contrastVar).trim();

				if (colorValue) {
					this.colors[palette].push({
						name: `${palette} ${shade}`,
						color: colorValue,
						contrast: contrastValue || 'N/A',
						usage: `map.get(${palettesVar[palette]}, ${JSON.stringify(shade)}))`
					});
				}
			});

			// Boucle pour les autres clés dans chaque palette (default, lighter, darker, etc.)
			additionalKeys.forEach(key => {
				const colorVar = `--${theme}--${palette}-${key}`;
				const contrastVar = `${colorVar}-contrast`;

				const colorValue = styles.getPropertyValue(colorVar).trim();
				const contrastValue = styles.getPropertyValue(contrastVar).trim();

				if (colorValue) {
					this.colors[palette].push({
						name: `${palette} ${key}`,
						color: colorValue,
						contrast: contrastValue || 'N/A',
						usage: `map.get(${palettesVar[palette]}, ${JSON.stringify(key)}))`
					});
				}
			});
		});
	}
}
