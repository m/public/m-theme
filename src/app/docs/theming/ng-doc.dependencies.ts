import {NgDocDependencies} from '@ng-doc/core';
import {ThemingPageModule} from 'src/app/docs/theming/ng-doc.module';
import {ThemingColorsDemo} from 'src/app/docs/theming/theming-colors-demo/theming-colors-demo.component';

const ThemingPageDependencies: NgDocDependencies = {
	module: ThemingPageModule,
	// Add your demos that you are going to use in the page here
	demo: {ThemingColorsDemo}
};

export default ThemingPageDependencies;
