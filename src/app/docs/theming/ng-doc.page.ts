import {NgDocPage} from '@ng-doc/core';
import MThemeCategory from '@categories/m-theme/ng-doc.category';

const ThemingPage: NgDocPage = {
	title: `Colors`,
	mdFile: './index.md',
	keyword: 'theming',
	category: MThemeCategory
};

export default ThemingPage;
