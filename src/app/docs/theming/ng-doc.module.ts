import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';

@NgModule({
	imports: [CommonModule],
	// Declare you demo components here
	declarations: []
})
export class ThemingPageModule {
}
