# {{ NgDocPage.title }}

## Default

{{ NgDocActions.demo("DatepickerDemoBasic") }}

## Date range picker forms integration

{{ NgDocActions.demo("DatepickerDemoRangeForm") }}
