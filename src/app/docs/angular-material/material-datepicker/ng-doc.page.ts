import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialDatepickerPage: NgDocPage = {
	title: `Datepicker`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialDatepickerPage;
