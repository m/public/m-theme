import {DatepickerDemoBasic} from '@material-docs/material-datepicker/datepicker-basic/datepicker-basic.component';
import {DatepickerDemoRangeForm} from '@material-docs/material-datepicker/datepicker-demo-range-form/datepicker-demo-range-form.component';
import {MaterialDatepickerPageModule} from '@material-docs/material-datepicker/ng-doc.module';
import {NgDocDependencies} from '@ng-doc/core';

const MaterialDatepickerPageDependencies: NgDocDependencies = {
	module: MaterialDatepickerPageModule,
	// Add your demos that you are going to use in the page here
	demo: {DatepickerDemoBasic, DatepickerDemoRangeForm}
};

export default MaterialDatepickerPageDependencies;
