import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {MatNativeDateModule} from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-datepicker-demo-range-form',
	standalone: true,
	imports: [CommonModule, Preview, MatFormFieldModule, MatDatepickerModule, ReactiveFormsModule, MatNativeDateModule],
	templateUrl: './datepicker-demo-range-form.component.html',
	styleUrls: ['./datepicker-demo-range-form.component.scss']
})
export class DatepickerDemoRangeForm {
	range = new FormGroup({
		start: new FormControl<Date | null>(null),
		end: new FormControl<Date | null>(null)
	});
}
