import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatNativeDateModule} from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-datepicker-basic',
  standalone: true,
	imports: [CommonModule, Preview, MatInputModule, MatDatepickerModule, MatNativeDateModule],
  templateUrl: './datepicker-basic.component.html',
  styleUrls: ['./datepicker-basic.component.scss']
})
export class DatepickerDemoBasic {

}
