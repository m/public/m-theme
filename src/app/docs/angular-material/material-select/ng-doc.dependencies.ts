import {SelectDemoBasic} from '@material-docs/material-select/select-demo-basic/select-demo-basic.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialSelectPageModule} from './ng-doc.module';

const MaterialSelectPageDependencies: NgDocDependencies = {
	module: MaterialSelectPageModule,
	// Add your demos that you are going to use in the page here
	demo: {SelectDemoBasic}
};

export default MaterialSelectPageDependencies;
