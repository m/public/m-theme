import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {FormControl, ReactiveFormsModule} from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-select-demo-basic',
	standalone: true,
	imports: [CommonModule, Preview, MatCheckboxModule, ReactiveFormsModule, MatInputModule, MatSelectModule],
	templateUrl: './select-demo-basic.component.html',
	styleUrls: ['./select-demo-basic.component.scss']
})
export class SelectDemoBasic {
	disableSelect = new FormControl(false);
}
