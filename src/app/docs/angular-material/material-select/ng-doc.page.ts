import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialSelectPage: NgDocPage = {
	title: `Select`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialSelectPage;
