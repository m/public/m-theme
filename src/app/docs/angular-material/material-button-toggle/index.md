# {{ NgDocPage.title }}

## Button toggle appearance

{{ NgDocActions.demo("ButtonToggleAppearanceDemo") }}

## Exclusive selection

{{ NgDocActions.demo("ButtonToggleExclusiveSelectionDemo") }}

## Button toggle with forms

{{ NgDocActions.demo("ButtonToggleButtonFormDemo") }}

## Button toggle selection mode

{{ NgDocActions.demo("ButtonToggleSelectionModeDemo") }}
