import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialButtonTogglePage: NgDocPage = {
	title: `Button-toggle`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialButtonTogglePage;
