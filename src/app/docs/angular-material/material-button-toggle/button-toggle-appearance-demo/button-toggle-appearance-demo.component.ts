import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-button-toggle-appearance-demo',
  standalone: true,
	imports: [CommonModule, Preview, MatButtonToggleModule],
  templateUrl: './button-toggle-appearance-demo.component.html',
  styleUrls: ['./button-toggle-appearance-demo.component.scss']
})
export class ButtonToggleAppearanceDemo {

}
