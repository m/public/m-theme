import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormControl, FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-button-toggle-button-form-demo',
  standalone: true,
	imports: [CommonModule, Preview, MatButtonToggleModule, FormsModule, ReactiveFormsModule],
  templateUrl: './button-toggle-button-form-demo.component.html',
  styleUrls: ['./button-toggle-button-form-demo.component.scss']
})
export class ButtonToggleButtonFormDemo {
	fontStyleControl = new FormControl('');
	fontStyle?: string;
}
