import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-button-toggle-selection-mode-demo',
  standalone: true,
	imports: [CommonModule, Preview, MatButtonToggleModule],
  templateUrl: './button-toggle-selection-mode-demo.component.html',
  styleUrls: ['./button-toggle-selection-mode-demo.component.scss']
})
export class ButtonToggleSelectionModeDemo {

}
