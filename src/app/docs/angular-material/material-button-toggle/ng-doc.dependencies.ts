import {ButtonToggleAppearanceDemo} from '@material-docs/material-button-toggle/button-toggle-appearance-demo/button-toggle-appearance-demo.component';
import {ButtonToggleButtonFormDemo} from '@material-docs/material-button-toggle/button-toggle-button-form-demo/button-toggle-button-form-demo.component';
import {ButtonToggleExclusiveSelectionDemo} from '@material-docs/material-button-toggle/button-toggle-exclusive-selection-demo/button-toggle-exclusive-selection-demo.component';
import {ButtonToggleSelectionModeDemo} from '@material-docs/material-button-toggle/button-toggle-selection-mode-demo/button-toggle-selection-mode-demo.component';
import {MaterialButtonTogglePageModule} from '@material-docs/material-button-toggle/ng-doc.module';
import {NgDocDependencies} from '@ng-doc/core';

const MaterialButtonTogglePageDependencies: NgDocDependencies = {
	module: MaterialButtonTogglePageModule,
	demo: {ButtonToggleAppearanceDemo, ButtonToggleButtonFormDemo, ButtonToggleExclusiveSelectionDemo, ButtonToggleSelectionModeDemo},
};

export default MaterialButtonTogglePageDependencies;
