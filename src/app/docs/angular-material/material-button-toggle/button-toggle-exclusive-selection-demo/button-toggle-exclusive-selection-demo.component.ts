import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatIconModule} from '@angular/material/icon';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-button-toggle-exclusive-selection-demo',
  standalone: true,
	imports: [CommonModule, MatButtonToggleModule, MatIconModule, Preview],
  templateUrl: './button-toggle-exclusive-selection-demo.component.html',
  styleUrls: ['./button-toggle-exclusive-selection-demo.component.scss']
})
export class ButtonToggleExclusiveSelectionDemo {

}
