import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';

@NgModule({
	imports: [CommonModule, MatBottomSheetModule],
	// Declare you demo components here
	declarations: [],
})
export class MaterialBottomSheetPageModule {}
