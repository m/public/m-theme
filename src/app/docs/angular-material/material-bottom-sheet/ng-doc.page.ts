import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialBottomSheetPage: NgDocPage = {
	title: `Bottom-sheet`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialBottomSheetPage;
