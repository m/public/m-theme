import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {MatButtonModule} from '@angular/material/button';
import {MatLineModule} from '@angular/material/core';
import {MatListModule} from '@angular/material/list';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-bottom-sheet-demo',
  standalone: true,
	imports: [CommonModule, Preview, MatButtonModule],
  templateUrl: './bottom-sheet-demo.component.html',
  styleUrls: ['./bottom-sheet-demo.component.scss']
})
export class BottomSheetDemo {
	constructor(private _bottomSheet: MatBottomSheet) {}

	openBottomSheet(): void {
		this._bottomSheet.open(BottomSheetOverviewExampleSheet);
	}
}

@Component({
	selector: 'bottom-sheet-overview-example-sheet',
	templateUrl: 'bottom-sheet-overview-example-sheet.html',
	imports: [
		MatListModule,
		MatLineModule
	],
	standalone: true
})
export class BottomSheetOverviewExampleSheet {
	constructor(private _bottomSheetRef: MatBottomSheetRef<BottomSheetOverviewExampleSheet>) {}

	openLink(event: any): void {
		this._bottomSheetRef.dismiss();
		event.preventDefault();
	}
}
