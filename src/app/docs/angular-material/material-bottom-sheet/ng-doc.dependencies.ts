import {BottomSheetDemo} from '@material-docs/material-bottom-sheet/bottom-sheet-demo/bottom-sheet-demo.component';
import {MaterialBottomSheetPageModule} from '@material-docs/material-bottom-sheet/ng-doc.module';
import {NgDocDependencies} from '@ng-doc/core';

const MaterialBottomSheetPageDependencies: NgDocDependencies = {
	module: MaterialBottomSheetPageModule,
	demo: {BottomSheetDemo}
};

export default MaterialBottomSheetPageDependencies;
