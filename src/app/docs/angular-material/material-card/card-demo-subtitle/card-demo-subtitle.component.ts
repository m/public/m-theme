import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-card-demo-subtitle',
  standalone: true,
	imports: [CommonModule, Preview, MatCardModule, MatButtonModule],
  templateUrl: './card-demo-subtitle.component.html',
  styleUrls: ['./card-demo-subtitle.component.scss']
})
export class CardDemoSubtitle {
	longText = `The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog
				  from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was
				  originally bred for hunting.`;
}
