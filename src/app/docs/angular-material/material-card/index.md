# {{ NgDocPage.title }}

## Card with actions alignment option

{{ NgDocActions.demo("CardDemoAction") }}

## Card with multiple sections

{{ NgDocActions.demo("CardDemoMultiple") }}

## Card with footer

{{ NgDocActions.demo("CardDemoFooter") }}

## Card with media size

{{ NgDocActions.demo("CardDemoMedia") }}

## Basic cards

{{ NgDocActions.demo("CardDemoBasic") }}

## Card with sub-title

{{ NgDocActions.demo("CardDemoSubtitle") }}
