import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatCardModule} from '@angular/material/card';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-card-demo-media',
	standalone: true,
	imports: [CommonModule, Preview, MatCardModule],
	templateUrl: './card-demo-media.component.html',
	styleUrls: ['./card-demo-media.component.scss']
})
export class CardDemoMedia {
	longText = `The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog
				  from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was
				  originally bred for hunting.`;

}
