import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatCardModule} from '@angular/material/card';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-card-demo-basic',
  standalone: true,
	imports: [CommonModule, Preview, MatCardModule],
  templateUrl: './card-demo-basic.component.html',
  styleUrls: ['./card-demo-basic.component.scss']
})
export class CardDemoBasic {

}
