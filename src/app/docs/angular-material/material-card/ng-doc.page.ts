import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialCardPage: NgDocPage = {
	title: `Card`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialCardPage;
