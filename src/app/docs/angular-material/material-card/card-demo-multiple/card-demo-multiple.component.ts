import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-card-demo-multiple',
  standalone: true,
	imports: [CommonModule, Preview, MatCardModule, MatButtonModule],
  templateUrl: './card-demo-multiple.component.html',
  styleUrls: ['./card-demo-multiple.component.scss']
})
export class CardDemoMultiple {

}
