import {CardDemoAction} from '@material-docs/material-card/card-demo-action/card-demo-action.component';
import {CardDemoBasic} from '@material-docs/material-card/card-demo-basic/card-demo-basic.component';
import {CardDemoFooter} from '@material-docs/material-card/card-demo-footer/card-demo-footer.component';
import {CardDemoMedia} from '@material-docs/material-card/card-demo-media/card-demo-media.component';
import {CardDemoMultiple} from '@material-docs/material-card/card-demo-multiple/card-demo-multiple.component';
import {CardDemoSubtitle} from '@material-docs/material-card/card-demo-subtitle/card-demo-subtitle.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialCardPageModule} from 'src/app/docs/angular-material/material-card/ng-doc.module';

const MaterialCardPageDependencies: NgDocDependencies = {
	module: MaterialCardPageModule,
	// Add your demos that you are going to use in the page here
	demo: {CardDemoAction, CardDemoBasic, CardDemoFooter, CardDemoMedia, CardDemoMultiple, CardDemoSubtitle}
};

export default MaterialCardPageDependencies;
