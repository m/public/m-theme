import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatListModule} from '@angular/material/list';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-card-demo-footer',
	standalone: true,
	imports: [CommonModule, Preview, MatProgressBarModule, MatCardModule, MatButtonModule, MatListModule],
	templateUrl: './card-demo-footer.component.html',
	styleUrls: ['./card-demo-footer.component.scss']
})
export class CardDemoFooter {
	longText = `The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog
				  from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was
				  originally bred for hunting.`;
}
