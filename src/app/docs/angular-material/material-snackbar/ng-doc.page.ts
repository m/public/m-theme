import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialSnackbarPage: NgDocPage = {
	title: `Snackbar`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialSnackbarPage;
