import {SnackbarDemoBasic} from '@material-docs/material-snackbar/snackbar-demo-basic/snackbar-demo-basic.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialSnackbarPageModule} from './ng-doc.module';

const MaterialSnackbarPageDependencies: NgDocDependencies = {
	module: MaterialSnackbarPageModule,
	// Add your demos that you are going to use in the page here
	demo: {SnackbarDemoBasic}
};

export default MaterialSnackbarPageDependencies;
