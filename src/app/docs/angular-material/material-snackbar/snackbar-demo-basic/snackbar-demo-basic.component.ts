import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition} from '@angular/material/snack-bar';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-snackbar-demo-basic',
	standalone: true,
	imports: [CommonModule, MatFormFieldModule, Preview, MatSelectModule, MatButtonModule],
	templateUrl: './snackbar-demo-basic.component.html',
	styleUrls: ['./snackbar-demo-basic.component.scss']
})
export class SnackbarDemoBasic {
	horizontalPosition: MatSnackBarHorizontalPosition = 'start';
	verticalPosition: MatSnackBarVerticalPosition = 'bottom';

	constructor(private _snackBar: MatSnackBar) {}

	openSnackBar() {
		this._snackBar.open('Cannonball!!', 'Splash', {
			horizontalPosition: this.horizontalPosition,
			verticalPosition: this.verticalPosition
		});
	}
}
