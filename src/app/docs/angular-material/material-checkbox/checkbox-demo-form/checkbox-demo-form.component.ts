import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {FormBuilder, ReactiveFormsModule} from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-checkbox-demo-form',
	standalone: true,
	imports: [CommonModule, Preview, MatCheckboxModule, ReactiveFormsModule],
	templateUrl: './checkbox-demo-form.component.html',
	styleUrls: ['./checkbox-demo-form.component.scss']
})
export class CheckboxDemoForm {
	toppings = this._formBuilder.group({
		pepperoni: false,
		extracheese: false,
		mushroom: false
	});

	constructor(private _formBuilder: FormBuilder) {}
}
