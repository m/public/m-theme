import {CheckboxDemoBasic} from '@material-docs/material-checkbox/checkbox-demo-basic/checkbox-demo-basic.component';
import {CheckboxDemoForm} from '@material-docs/material-checkbox/checkbox-demo-form/checkbox-demo-form.component';
import {NgDocDependencies} from '@ng-doc/core';
import {CheckboxDemo} from 'src/app/docs/angular-material/material-checkbox/checkbox-demo/checkbox-demo.component';
import {MaterialCheckboxPageModule} from 'src/app/docs/angular-material/material-checkbox/ng-doc.module';

const MaterialCheckboxPageDependencies: NgDocDependencies = {
	module: MaterialCheckboxPageModule,
	// Add your demos that you are going to use in the page here
	demo: {CheckboxDemo, CheckboxDemoBasic, CheckboxDemoForm}
};

export default MaterialCheckboxPageDependencies;
