import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-checkbox-demo',
  standalone: true,
	imports: [CommonModule, MatRadioModule, FormsModule, MatCheckboxModule, MatCardModule, Preview],
  templateUrl: './checkbox-demo.component.html',
  styleUrls: ['./checkbox-demo.component.scss']
})
export class CheckboxDemo {
	checked = false;
	indeterminate = false;
	labelPosition: 'before' | 'after' = 'after';
	disabled = false;
}
