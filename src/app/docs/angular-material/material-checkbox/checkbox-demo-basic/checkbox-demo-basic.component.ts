import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {ThemePalette} from '@angular/material/core';
import {Preview} from 'src/app/features/preview/preview.component';

export interface Task {
	name: string;
	completed: boolean;
	color: ThemePalette;
	subtasks?: Task[];
}

@Component({
	selector: 'app-checkbox-demo-basic',
	standalone: true,
	imports: [CommonModule, Preview, MatCheckboxModule, FormsModule],
	templateUrl: './checkbox-demo-basic.component.html',
	styleUrls: ['./checkbox-demo-basic.component.scss']
})
export class CheckboxDemoBasic {
	task: Task = {
		name: 'Indeterminate',
		completed: false,
		color: 'primary',
		subtasks: [
			{name: 'Primary', completed: false, color: 'primary'},
			{name: 'Accent', completed: false, color: 'accent'},
			{name: 'Warn', completed: false, color: 'warn'}
		]
	};

	allComplete: boolean = false;

	updateAllComplete() {
		this.allComplete = this.task.subtasks != null && this.task.subtasks.every(t => t.completed);
	}

	someComplete(): boolean {
		if (this.task.subtasks == null) {
			return false;
		}
		return this.task.subtasks.filter(t => t.completed).length > 0 && !this.allComplete;
	}

	setAll(completed: boolean) {
		this.allComplete = completed;
		if (this.task.subtasks == null) {
			return;
		}
		this.task.subtasks.forEach(t => (t.completed = completed));
	}
}
