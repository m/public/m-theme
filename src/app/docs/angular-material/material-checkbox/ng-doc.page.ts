import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialCheckboxPage: NgDocPage = {
	title: `Checkbox`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialCheckboxPage;
