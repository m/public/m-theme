# {{ NgDocPage.title }}

## Configurable checkbox

{{ NgDocActions.demo("CheckboxDemo") }}

## Basic checkboxes

{{ NgDocActions.demo("CheckboxDemoBasic") }}

## Checkboxes with reactive forms

{{ NgDocActions.demo("CheckboxDemoForm") }}
