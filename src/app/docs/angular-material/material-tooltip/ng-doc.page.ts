import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialTooltipPage: NgDocPage = {
	title: `Tooltip`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialTooltipPage;
