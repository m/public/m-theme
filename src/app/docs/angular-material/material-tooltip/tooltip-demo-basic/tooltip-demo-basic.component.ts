import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {FormControl, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatTooltipModule, TooltipPosition} from '@angular/material/tooltip';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-tooltip-demo-basic',
	standalone: true,
	imports: [CommonModule, Preview, MatInputModule, MatSelectModule, ReactiveFormsModule, MatButtonModule, MatTooltipModule],
	templateUrl: './tooltip-demo-basic.component.html',
	styleUrls: ['./tooltip-demo-basic.component.scss']
})
export class TooltipDemoBasic {
	positionOptions: TooltipPosition[] = ['after', 'before', 'above', 'below', 'left', 'right'];
	position = new FormControl(this.positionOptions[0]);
}
