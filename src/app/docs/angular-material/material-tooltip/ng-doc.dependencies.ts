import {TooltipDemoBasic} from '@material-docs/material-tooltip/tooltip-demo-basic/tooltip-demo-basic.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialTooltipPageModule} from './ng-doc.module';

const MaterialTooltipPageDependencies: NgDocDependencies = {
	module: MaterialTooltipPageModule,
	// Add your demos that you are going to use in the page here
	demo: {TooltipDemoBasic}
};

export default MaterialTooltipPageDependencies;
