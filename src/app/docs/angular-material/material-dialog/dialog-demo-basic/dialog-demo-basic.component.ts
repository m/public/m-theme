import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatDialog, MatDialogModule} from '@angular/material/dialog';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-dialog-demo-basic',
	standalone: true,
	imports: [CommonModule, Preview, MatButtonModule, MatDialogModule],
	templateUrl: './dialog-demo-basic.component.html',
	styleUrls: ['./dialog-demo-basic.component.scss']
})
export class DialogDemoBasic {
	constructor(public dialog: MatDialog) {}

	openDialog() {
		const dialogRef = this.dialog.open(DialogContentExampleDialog);

		dialogRef.afterClosed().subscribe(result => {
			console.log(`Dialog result: ${result}`);
		});
	}
}

@Component({
	selector: 'dialog-content-example-dialog',
	templateUrl: './dialog-content-example-dialog.html',
	imports: [
		MatDialogModule,
		MatButtonModule
	],
	standalone: true
})
export class DialogContentExampleDialog {
}
