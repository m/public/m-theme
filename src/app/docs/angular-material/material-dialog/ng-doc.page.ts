import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialDialogPage: NgDocPage = {
	title: `Dialog`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialDialogPage;
