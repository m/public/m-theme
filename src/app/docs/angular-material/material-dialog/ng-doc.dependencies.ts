import {DialogDemoBasic} from '@material-docs/material-dialog/dialog-demo-basic/dialog-demo-basic.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialDialogPageModule} from 'src/app/docs/angular-material/material-dialog/ng-doc.module';

const MaterialDialogPageDependencies: NgDocDependencies = {
	module: MaterialDialogPageModule,
	// Add your demos that you are going to use in the page here
	demo: {DialogDemoBasic},
};

export default MaterialDialogPageDependencies;
