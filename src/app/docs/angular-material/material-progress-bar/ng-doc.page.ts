import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialProgressBarDemoPage: NgDocPage = {
	title: `Progress bar`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialProgressBarDemoPage;
