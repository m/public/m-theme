import {ProgressBarDemoBasic} from '@material-docs/material-progress-bar/progress-bar-demo-basic/progress-bar-demo-basic.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialProgressBarPageModule} from './ng-doc.module';

const MaterialProgressBarDemoPageDependencies: NgDocDependencies = {
	module: MaterialProgressBarPageModule,
	demo: {ProgressBarDemoBasic}
};

export default MaterialProgressBarDemoPageDependencies;
