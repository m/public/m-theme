import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {ThemePalette} from '@angular/material/core';
import {MatProgressBarModule, ProgressBarMode} from '@angular/material/progress-bar';
import {MatRadioModule} from '@angular/material/radio';
import {MatSliderModule} from '@angular/material/slider';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-progress-bar-demo-basic',
	standalone: true,
	imports: [CommonModule, Preview, MatCardModule, MatRadioModule, FormsModule, MatSliderModule, MatProgressBarModule],
	templateUrl: './progress-bar-demo-basic.component.html',
	styleUrls: ['./progress-bar-demo-basic.component.scss']
})
export class ProgressBarDemoBasic {
	color: ThemePalette = 'primary';
	mode: ProgressBarMode = 'determinate';
	value = 50;
	bufferValue = 75;
}
