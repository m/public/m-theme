import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-toolbar-demo-basic',
	standalone: true,
	imports: [CommonModule, Preview, MatToolbarModule, MatButtonModule, MatIconModule],
	templateUrl: './toolbar-demo-basic.component.html',
	styleUrls: ['./toolbar-demo-basic.component.scss']
})
export class ToolbarDemoBasic {

}
