import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialToolbarPage: NgDocPage = {
	title: `Toolbar`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialToolbarPage;
