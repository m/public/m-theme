import {ToolbarDemoBasic} from '@material-docs/material-toolbar/toolbar-demo-basic/toolbar-demo-basic.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialToolbarPageModule} from './ng-doc.module';

const MaterialToolbarPageDependencies: NgDocDependencies = {
	module: MaterialToolbarPageModule,
	// Add your demos that you are going to use in the page here
	demo: {ToolbarDemoBasic}
};

export default MaterialToolbarPageDependencies;
