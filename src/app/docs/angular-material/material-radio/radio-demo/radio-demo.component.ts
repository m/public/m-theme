import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatRadioModule} from '@angular/material/radio';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-radio-demo',
	standalone: true,
	imports: [CommonModule, MatRadioModule, Preview, FormsModule],
	templateUrl: './radio-demo.component.html',
	styleUrls: ['./radio-demo.component.scss']
})
export class RadioDemo {
	favoriteSeason: string;
	seasons: string[] = ['Winter', 'Spring', 'Summer', 'Autumn'];
}
