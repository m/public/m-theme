import {NgDocDependencies} from '@ng-doc/core';
import {MaterialRadioPageModule} from 'src/app/docs/angular-material/material-radio/ng-doc.module';
import {RadioDemo} from 'src/app/docs/angular-material/material-radio/radio-demo/radio-demo.component';


const MaterialRadioDependencies: NgDocDependencies = {
	module: MaterialRadioPageModule,
	// Add your demos that you are going to use in the page here
	demo: {RadioDemo},
};

export default MaterialRadioDependencies;
