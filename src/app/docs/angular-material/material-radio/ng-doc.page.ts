import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialRadioPage: NgDocPage = {
	title: `Radio`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialRadioPage;
