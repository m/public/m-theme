import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatLineModule, MatRippleModule} from '@angular/material/core';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-list-demo',
	standalone: true,
	imports: [CommonModule, Preview, MatListModule, MatRippleModule, MatIconModule, MatButtonModule, MatLineModule],
	templateUrl: './list-demo.component.html',
	styleUrls: ['./list-demo.component.scss']
})
export class ListDemo {

}
