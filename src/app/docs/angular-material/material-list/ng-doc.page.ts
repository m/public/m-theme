import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialListPage: NgDocPage = {
	title: `List`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialListPage;
