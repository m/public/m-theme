import {NgDocDependencies} from '@ng-doc/core';
import {MaterialListPageModule} from 'src/app/docs/angular-material/material-list/ng-doc.module';
import {ListDemo} from 'src/app/docs/angular-material/material-list/list-demo/list-demo.component';

const MaterialListPageDependencies: NgDocDependencies = {
	module: MaterialListPageModule,
	// Add your demos that you are going to use in the page here
	demo: {ListDemo}
};

export default MaterialListPageDependencies;
