import {SlideToggleDemoBasic} from '@material-docs/material-slide-toggle/slide-toggle-demo-basic/slide-toggle-demo-basic.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialSlideTogglePageModule} from './ng-doc.module';

const MaterialSlideTogglePageDependencies: NgDocDependencies = {
	module: MaterialSlideTogglePageModule,
	// Add your demos that you are going to use in the page here
	demo: {SlideToggleDemoBasic}
};

export default MaterialSlideTogglePageDependencies;
