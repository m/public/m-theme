import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialSlideTogglePage: NgDocPage = {
	title: `Slide toggle`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialSlideTogglePage;
