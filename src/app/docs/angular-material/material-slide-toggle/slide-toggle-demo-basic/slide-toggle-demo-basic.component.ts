import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {ThemePalette} from '@angular/material/core';
import {MatRadioModule} from '@angular/material/radio';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-slide-toggle-demo-basic',
	standalone: true,
	imports: [CommonModule, Preview, MatSlideToggleModule, MatCardModule, MatCheckboxModule, FormsModule, MatRadioModule],
	templateUrl: './slide-toggle-demo-basic.component.html',
	styleUrls: ['./slide-toggle-demo-basic.component.scss']
})
export class SlideToggleDemoBasic {
	color: ThemePalette = 'accent';
	checked = false;
	disabled = false;
}
