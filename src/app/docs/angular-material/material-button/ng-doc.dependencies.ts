import {ButtonBasicDemo} from '@material-docs/material-button/button-basic-demo/button-basic-demo.component';
import {ButtonVarietiesDemo} from '@material-docs/material-button/button-varieties-demo/button-varieties-demo.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialButtonPageModule} from 'src/app/docs/angular-material/material-button/ng-doc.module';

const MaterialButtonPageDependencies: NgDocDependencies = {
	module: MaterialButtonPageModule,
	// Add your demos that you are going to use in the page here
	demo: {ButtonBasicDemo, ButtonVarietiesDemo}
};

export default MaterialButtonPageDependencies;
