import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialButtonPage: NgDocPage = {
	title: `Button`,
	mdFile: './index.md',
	category: AngularMaterielCategory

};

export default MaterialButtonPage;
