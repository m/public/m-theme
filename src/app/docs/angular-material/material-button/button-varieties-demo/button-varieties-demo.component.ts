import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {RouterLink} from '@angular/router';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-button-varieties-demo',
	standalone: true,
	imports: [CommonModule, Preview, MatButtonModule, RouterLink, MatTooltipModule, MatIconModule],
	templateUrl: './button-varieties-demo.component.html',
	styleUrls: ['./button-varieties-demo.component.scss']
})
export class ButtonVarietiesDemo {

}
