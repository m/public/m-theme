# {{ NgDocPage.title }}

## Basic Button
{{ NgDocActions.demo("ButtonBasicDemo") }}

## Varieties Buttons
{{ NgDocActions.demo("ButtonVarietiesDemo") }}
