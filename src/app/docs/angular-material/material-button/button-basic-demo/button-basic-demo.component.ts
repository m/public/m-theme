import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {MatIconModule} from '@angular/material/icon';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-button-basic-demo',
  standalone: true,
	imports: [CommonModule, MatButtonModule, MatDividerModule, MatIconModule, Preview],
  templateUrl: './button-basic-demo.component.html',
  styleUrls: ['./button-basic-demo.component.scss']
})
export class ButtonBasicDemo {

}
