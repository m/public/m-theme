import {NgDocDependencies} from '@ng-doc/core';
import {ExpansionPanelOneDemo} from 'src/app/docs/angular-material/material-expansion-panel/expansion-panel-one-demo/expansion-panel-one-demo.component';
import {ExpansionPanelThreeDemo} from 'src/app/docs/angular-material/material-expansion-panel/expansion-panel-three-demo/expansion-panel-three-demo.component';
import {ExpansionPanelTwoDemo} from 'src/app/docs/angular-material/material-expansion-panel/expansion-panel-two-demo/expansion-panel-two-demo.component';
import {MaterialExpansionPanelPageModule} from 'src/app/docs/angular-material/material-expansion-panel/ng-doc.module';

const MaterialExpansionPanelPageDependencies: NgDocDependencies = {
	module: MaterialExpansionPanelPageModule,
	// Add your demos that you are going to use in the page here
	demo: {ExpansionPanelOneDemo, ExpansionPanelTwoDemo, ExpansionPanelThreeDemo},
};

export default MaterialExpansionPanelPageDependencies;
