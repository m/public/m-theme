import {A11yModule} from '@angular/cdk/a11y';
import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatIconModule} from '@angular/material/icon';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-expansion-panel-two-demo',
	standalone: true,
	imports: [CommonModule, A11yModule, MatButtonModule, MatExpansionModule, MatIconModule, Preview],
	templateUrl: './expansion-panel-two-demo.component.html',
	styleUrls: ['./expansion-panel-two-demo.component.scss']
})
export class ExpansionPanelTwoDemo {

}
