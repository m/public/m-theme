import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialExpansionPanelPage: NgDocPage = {
	title: `Expansion Panel`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialExpansionPanelPage;
