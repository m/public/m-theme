# {{ NgDocPage.title }}

## Demo

### Basic
{{ NgDocActions.demo("ExpansionPanelOneDemo") }}

### Mat-list style
{{ NgDocActions.demo("ExpansionPanelTwoDemo") }}

### Mat-list style + stroke
{{ NgDocActions.demo("ExpansionPanelThreeDemo") }}
