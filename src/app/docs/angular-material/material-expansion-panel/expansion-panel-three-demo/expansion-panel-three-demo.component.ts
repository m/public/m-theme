import {A11yModule} from '@angular/cdk/a11y';
import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatExpansionModule} from '@angular/material/expansion';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-expansion-panel-three-demo',
	standalone: true,
	imports: [CommonModule, A11yModule, MatExpansionModule, Preview],
	templateUrl: './expansion-panel-three-demo.component.html',
	styleUrls: ['./expansion-panel-three-demo.component.scss']
})
export class ExpansionPanelThreeDemo {

}
