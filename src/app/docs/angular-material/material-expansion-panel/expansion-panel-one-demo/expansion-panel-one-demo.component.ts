import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatExpansionModule} from '@angular/material/expansion';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-expansion-panel-one-demo',
  standalone: true,
    imports: [CommonModule, MatExpansionModule, Preview],
  templateUrl: './expansion-panel-one-demo.component.html',
  styleUrls: ['./expansion-panel-one-demo.component.scss']
})
export class ExpansionPanelOneDemo {

}
