import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialSidenavPage: NgDocPage = {
	title: `Sidenav`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialSidenavPage;
