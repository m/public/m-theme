import {SidenavDemoBasic} from '@material-docs/material-sidenav/sidenav-demo-basic/sidenav-demo-basic.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialSidenavPageModule} from './ng-doc.module';

const MaterialSidenavPageDependencies: NgDocDependencies = {
	module: MaterialSidenavPageModule,
	// Add your demos that you are going to use in the page here
	demo: {SidenavDemoBasic}
};

export default MaterialSidenavPageDependencies;
