import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-sidenav-demo-basic',
	standalone: true,
	imports: [CommonModule, Preview, MatSidenavModule, MatInputModule, MatSelectModule, MatButtonModule],
	templateUrl: './sidenav-demo-basic.component.html',
	styleUrls: ['./sidenav-demo-basic.component.scss']
})
export class SidenavDemoBasic {

}
