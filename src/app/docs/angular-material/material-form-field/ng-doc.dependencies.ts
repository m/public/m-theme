import {NgDocDependencies} from '@ng-doc/core';
import {FormFieldDemo} from 'src/app/docs/angular-material/material-form-field/form-field-demo/form-field-demo.component';
import {MaterialFormFieldPageModule} from 'src/app/docs/angular-material/material-form-field/ng-doc.module';

const MaterialFormFieldPageDependencies: NgDocDependencies = {
	module: MaterialFormFieldPageModule,
	// Add your demos that you are going to use in the page here
	demo: {FormFieldDemo},
};

export default MaterialFormFieldPageDependencies;
