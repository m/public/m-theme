import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {FormBuilder, FormControl, ReactiveFormsModule} from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {FloatLabelType} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';

@Component({
	selector: 'app-form-field-demo',
	standalone: true,
	imports: [CommonModule, MatInputModule, MatSelectModule, MatIconModule, MatRadioModule, MatCheckboxModule, ReactiveFormsModule],
	templateUrl: './form-field-demo.component.html',
	styleUrls: ['./form-field-demo.component.scss']
})
export class FormFieldDemo {
	hideRequiredControl = new FormControl(false);
	floatLabelControl = new FormControl('auto' as FloatLabelType);
	options = this._formBuilder.group({
		hideRequired: this.hideRequiredControl,
		floatLabel: this.floatLabelControl
	});

	constructor(private _formBuilder: FormBuilder) {}

	getFloatLabelValue(): FloatLabelType {
		return this.floatLabelControl.value || 'auto';
	}
}
