import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialFormFieldPage: NgDocPage = {
	title: `Form field`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialFormFieldPage;
