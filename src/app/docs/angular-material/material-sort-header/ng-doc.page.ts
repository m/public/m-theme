import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialSortHeaderPage: NgDocPage = {
	title: `Sort header`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialSortHeaderPage;
