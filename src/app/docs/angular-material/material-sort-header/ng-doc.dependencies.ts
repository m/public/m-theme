import {SortHeaderDemoBasic} from '@material-docs/material-sort-header/sort-header-demo-basic/sort-header-demo-basic.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialSortHeaderPageModule} from './ng-doc.module';

const MaterialSortHeaderPageDependencies: NgDocDependencies = {
	module: MaterialSortHeaderPageModule,
	// Add your demos that you are going to use in the page here
	demo: {SortHeaderDemoBasic}
};

export default MaterialSortHeaderPageDependencies;
