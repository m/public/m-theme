import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialRipplesPage: NgDocPage = {
	title: `Ripples`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialRipplesPage;
