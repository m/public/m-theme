import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRippleModule} from '@angular/material/core';
import {MatInputModule} from '@angular/material/input';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-ripples-demo-basic',
	standalone: true,
	imports: [CommonModule, Preview, MatCheckboxModule, FormsModule, MatInputModule, MatRippleModule],
	templateUrl: './ripples-demo-basic.component.html',
	styleUrls: ['./ripples-demo-basic.component.scss']
})
export class RipplesDemoBasic {
	centered = false;
	disabled = false;
	unbounded = false;

	radius: number;
	color: string;
}
