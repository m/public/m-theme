import {RipplesDemoBasic} from '@material-docs/material-ripples/ripples-demo-basic/ripples-demo-basic.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialRipplesPageModule} from './ng-doc.module';

const MaterialRipplesPageDependencies: NgDocDependencies = {
	module: MaterialRipplesPageModule,
	// Add your demos that you are going to use in the page here
	demo: {RipplesDemoBasic}
};

export default MaterialRipplesPageDependencies;
