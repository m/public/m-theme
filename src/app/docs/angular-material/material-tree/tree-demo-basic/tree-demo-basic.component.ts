import {NestedTreeControl} from '@angular/cdk/tree';
import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatTreeModule, MatTreeNestedDataSource} from '@angular/material/tree';
import {Preview} from 'src/app/features/preview/preview.component';


/**
 * Food data with nested structure.
 * Each node has a name and an optional list of children.
 */
interface FoodNode {
	name: string;
	children?: FoodNode[];
}

const TREE_DATA: FoodNode[] = [
	{
		name: 'Fruit',
		children: [{name: 'Apple'}, {name: 'Banana'}, {name: 'Fruit loops'}]
	},
	{
		name: 'Vegetables',
		children: [
			{
				name: 'Green',
				children: [{name: 'Broccoli'}, {name: 'Brussels sprouts'}]
			},
			{
				name: 'Orange',
				children: [{name: 'Pumpkins'}, {name: 'Carrots'}]
			}
		]
	}
];

@Component({
	selector: 'app-tree-demo-basic',
	standalone: true,
	imports: [CommonModule, Preview, MatTreeModule, MatButtonModule, MatIconModule],
	templateUrl: './tree-demo-basic.component.html',
	styleUrls: ['./tree-demo-basic.component.scss']
})
export class TreeDemoBasic {
	treeControl = new NestedTreeControl<FoodNode>(node => node.children);
	dataSource = new MatTreeNestedDataSource<FoodNode>();

	constructor() {
		this.dataSource.data = TREE_DATA;
	}

	hasChild = (_: number, node: FoodNode) => !!node.children && node.children.length > 0;
}
