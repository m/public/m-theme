import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialTreePage: NgDocPage = {
	title: `Tree`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialTreePage;
