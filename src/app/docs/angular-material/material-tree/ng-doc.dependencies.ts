import {TreeDemoBasic} from '@material-docs/material-tree/tree-demo-basic/tree-demo-basic.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialTreePageModule} from './ng-doc.module';

const MaterialTreePageDependencies: NgDocDependencies = {
	module: MaterialTreePageModule,
	// Add your demos that you are going to use in the page here
	demo: {TreeDemoBasic}
};

export default MaterialTreePageDependencies;
