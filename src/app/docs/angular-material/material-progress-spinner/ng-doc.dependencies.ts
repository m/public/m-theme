import {ProgressSpinnerDemoBasic} from '@material-docs/material-progress-spinner/progress-spinner-demo-basic/progress-spinner-demo-basic.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialProgressSplinnerPageModule} from './ng-doc.module';

const MaterialProgressSplinnerPageDependencies: NgDocDependencies = {
	module: MaterialProgressSplinnerPageModule,
	// Add your demos that you are going to use in the page here
	demo: {ProgressSpinnerDemoBasic}
};

export default MaterialProgressSplinnerPageDependencies;
