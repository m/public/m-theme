import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialProgressSpinnerPage: NgDocPage = {
	title: `Progress spinner`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialProgressSpinnerPage;
