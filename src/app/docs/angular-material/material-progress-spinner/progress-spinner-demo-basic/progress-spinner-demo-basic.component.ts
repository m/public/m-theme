import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {ThemePalette} from '@angular/material/core';
import {MatProgressSpinnerModule, ProgressSpinnerMode} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatSliderModule} from '@angular/material/slider';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-progress-spinner-demo-basic',
	standalone: true,
	imports: [CommonModule, Preview, MatCardModule, MatRadioModule, FormsModule, MatSliderModule, MatProgressSpinnerModule],
	templateUrl: './progress-spinner-demo-basic.component.html',
	styleUrls: ['./progress-spinner-demo-basic.component.scss']
})
export class ProgressSpinnerDemoBasic {
	color: ThemePalette = 'primary';
	mode: ProgressSpinnerMode = 'determinate';
	value = 50;
}
