import {AutoCompleteDemo} from '@material-docs/material-autocomplete/auto-complete-demo/auto-complete-demo.component';
import {MaterialAutocompletePageModule} from '@material-docs/material-autocomplete/ng-doc.module';
import {NgDocDependencies} from '@ng-doc/core';

const MaterialAutocompletePageDependencies: NgDocDependencies = {
	module: MaterialAutocompletePageModule,
	demo: {AutoCompleteDemo}
};

export default MaterialAutocompletePageDependencies;
