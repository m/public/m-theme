import {Component, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormControl, ReactiveFormsModule} from '@angular/forms';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import {map, Observable, startWith} from 'rxjs';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-auto-complete-demo',
  standalone: true,
	imports: [CommonModule, MatInputModule, ReactiveFormsModule, MatAutocompleteModule, Preview],
  templateUrl: './auto-complete-demo.component.html',
  styleUrls: ['./auto-complete-demo.component.scss']
})
export class AutoCompleteDemo implements OnInit{
	myControl = new FormControl('');
	options: string[] = ['One', 'Two', 'Three'];
	filteredOptions: Observable<string[]>;

	ngOnInit() {
		this.filteredOptions = this.myControl.valueChanges.pipe(
			startWith(''),
			map(value => this._filter(value || '')),
		);
	}

	private _filter(value: string): string[] {
		const filterValue = value.toLowerCase();

		return this.options.filter(option => option.toLowerCase().includes(filterValue));
	}
}
