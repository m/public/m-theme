import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialAutocompletePage: NgDocPage = {
	title: `Autocomplete`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialAutocompletePage;
