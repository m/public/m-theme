import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatTabsModule} from '@angular/material/tabs';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-tabs-demo-basic',
	standalone: true,
	imports: [CommonModule, Preview, MatTabsModule],
	templateUrl: './tabs-demo-basic.component.html',
	styleUrls: ['./tabs-demo-basic.component.scss']
})
export class TabsDemoBasic {

}
