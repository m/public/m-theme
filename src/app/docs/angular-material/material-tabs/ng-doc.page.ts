import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialTabsPage: NgDocPage = {
	title: `Tabs`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialTabsPage;
