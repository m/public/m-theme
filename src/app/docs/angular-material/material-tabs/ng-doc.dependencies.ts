import {TabsDemoBasic} from '@material-docs/material-tabs/tabs-demo-basic/tabs-demo-basic.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialTabsPageModule} from './ng-doc.module';

const MaterialTabsPageDependencies: NgDocDependencies = {
	module: MaterialTabsPageModule,
	// Add your demos that you are going to use in the page here
	demo: {TabsDemoBasic}
};

export default MaterialTabsPageDependencies;
