import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatSliderModule} from '@angular/material/slider';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-slider-demo-range',
  standalone: true,
	imports: [CommonModule, Preview, MatSliderModule],
  templateUrl: './slider-demo-range.component.html',
  styleUrls: ['./slider-demo-range.component.scss']
})
export class SliderDemoRange {

}
