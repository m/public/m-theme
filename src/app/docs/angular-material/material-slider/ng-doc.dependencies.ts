import {SliderDemoBasic} from '@material-docs/material-slider/slider-demo-basic/slider-demo-basic.component';
import {SliderDemoLabel} from '@material-docs/material-slider/slider-demo-label/slider-demo-label.component';
import {SliderDemoRange} from '@material-docs/material-slider/slider-demo-range/slider-demo-range.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialSliderPageModule} from './ng-doc.module';

const MaterialSliderPageDependencies: NgDocDependencies = {
	module: MaterialSliderPageModule,
	// Add your demos that you are going to use in the page here
	demo: {SliderDemoBasic, SliderDemoLabel, SliderDemoRange}
};

export default MaterialSliderPageDependencies;
