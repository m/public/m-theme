import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialSliderPage: NgDocPage = {
	title: `Slider`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialSliderPage;
