# {{ NgDocPage.title }}

## Configurable slider

{{ NgDocActions.demo("SliderDemoBasic") }}

## Slider with custom thumb label formatting.

{{ NgDocActions.demo("SliderDemoLabel") }}

## Slider range

{{ NgDocActions.demo("SliderDemoRange") }}

