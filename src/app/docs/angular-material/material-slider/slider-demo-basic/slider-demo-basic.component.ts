import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatInputModule} from '@angular/material/input';
import {MatSliderModule} from '@angular/material/slider';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-slider-demo-basic',
	standalone: true,
	imports: [CommonModule, Preview, MatSliderModule, FormsModule, MatCardModule, MatCheckboxModule, MatInputModule],
	templateUrl: './slider-demo-basic.component.html',
	styleUrls: ['./slider-demo-basic.component.scss']
})
export class SliderDemoBasic {
	disabled = false;
	max = 100;
	min = 0;
	showTicks = false;
	step = 1;
	thumbLabel = false;
	value = 0;
}
