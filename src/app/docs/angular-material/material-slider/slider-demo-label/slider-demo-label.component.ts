import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatSliderModule} from '@angular/material/slider';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-slider-demo-label',
	standalone: true,
	imports: [CommonModule, Preview, MatSliderModule],
	templateUrl: './slider-demo-label.component.html',
	styleUrls: ['./slider-demo-label.component.scss']
})
export class SliderDemoLabel {
	formatLabel(value: number): string {
		if (value >= 1000) {
			return Math.round(value / 1000) + 'k';
		}

		return `${value}`;
	}
}
