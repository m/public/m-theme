# {{ NgDocPage.title }}

## Basic Inputs

{{ NgDocActions.demo("InputDemoBasic") }}

## Inputs with a hint

{{ NgDocActions.demo("InputDemoHint") }}

## Input with error messages

{{ NgDocActions.demo("InputDemoError") }}

