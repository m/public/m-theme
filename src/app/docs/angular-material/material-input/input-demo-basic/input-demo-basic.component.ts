import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatInputModule} from '@angular/material/input';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-input-demo-basic',
  standalone: true,
	imports: [CommonModule, Preview, MatInputModule],
  templateUrl: './input-demo-basic.component.html',
  styleUrls: ['./input-demo-basic.component.scss']
})
export class InputDemoBasic {

}
