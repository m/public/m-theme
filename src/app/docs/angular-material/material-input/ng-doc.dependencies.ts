import {InputDemoBasic} from '@material-docs/material-input/input-demo-basic/input-demo-basic.component';
import {InputDemoError} from '@material-docs/material-input/input-demo-error/input-demo-error.component';
import {InputDemoHint} from '@material-docs/material-input/input-demo-hint/input-demo-hint.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialInputPageModule} from 'src/app/docs/angular-material/material-input/ng-doc.module';

const MaterialInputPageDependencies: NgDocDependencies = {
	module: MaterialInputPageModule,
	// Add your demos that you are going to use in the page here
	demo: {InputDemoBasic, InputDemoError, InputDemoHint},
};

export default MaterialInputPageDependencies;
