import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialInputPage: NgDocPage = {
	title: `Input`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialInputPage;
