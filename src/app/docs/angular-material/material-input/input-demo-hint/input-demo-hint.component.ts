import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatInputModule} from '@angular/material/input';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-input-demo-hint',
  standalone: true,
	imports: [CommonModule, Preview, MatInputModule],
  templateUrl: './input-demo-hint.component.html',
  styleUrls: ['./input-demo-hint.component.scss']
})
export class InputDemoHint {

}
