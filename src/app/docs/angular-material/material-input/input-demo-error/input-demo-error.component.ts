import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {FormControl, ReactiveFormsModule, Validators} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-input-demo-error',
	standalone: true,
	imports: [CommonModule, Preview, MatInputModule, ReactiveFormsModule],
	templateUrl: './input-demo-error.component.html',
	styleUrls: ['./input-demo-error.component.scss']
})
export class InputDemoError {
	emailFormControl = new FormControl('', [Validators.required, Validators.email]);
}
