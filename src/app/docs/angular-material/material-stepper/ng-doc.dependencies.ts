import {StepperDemoBasic} from '@material-docs/material-stepper/stepper-demo-basic/stepper-demo-basic.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialStepperPageModule} from './ng-doc.module';

const MaterialStepperPageDependencies: NgDocDependencies = {
	module: MaterialStepperPageModule,
	// Add your demos that you are going to use in the page here
	demo: {StepperDemoBasic}
};

export default MaterialStepperPageDependencies;
