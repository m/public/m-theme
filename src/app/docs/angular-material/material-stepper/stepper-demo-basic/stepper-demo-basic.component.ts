import {STEPPER_GLOBAL_OPTIONS} from '@angular/cdk/stepper';
import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {FormBuilder, ReactiveFormsModule, Validators} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatStepperModule} from '@angular/material/stepper';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-stepper-demo-basic',
	standalone: true,
	imports: [CommonModule, Preview, MatStepperModule, MatButtonModule, ReactiveFormsModule, MatInputModule],
	templateUrl: './stepper-demo-basic.component.html',
	styleUrls: ['./stepper-demo-basic.component.scss'],
	providers: [
		{
			provide: STEPPER_GLOBAL_OPTIONS,
			useValue: {showError: true}
		}
	]
})
export class StepperDemoBasic {
	firstFormGroup = this._formBuilder.group({
		firstCtrl: ['', Validators.required]
	});
	secondFormGroup = this._formBuilder.group({
		secondCtrl: ['', Validators.required]
	});

	constructor(private _formBuilder: FormBuilder) {}
}
