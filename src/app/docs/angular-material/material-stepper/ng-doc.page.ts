import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialStepperPage: NgDocPage = {
	title: `Stepper`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialStepperPage;
