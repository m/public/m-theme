import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialTablePage: NgDocPage = {
	title: `Table`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialTablePage;
