import {TableDemoBasic} from '@material-docs/material-table/table-demo-basic/table-demo-basic.component';
import {NgDocDependencies} from '@ng-doc/core';
import {MaterialTablePageModule} from './ng-doc.module';

const MaterialTablePageDependencies: NgDocDependencies = {
	module: MaterialTablePageModule,
	// Add your demos that you are going to use in the page here
	demo: {TableDemoBasic}
};

export default MaterialTablePageDependencies;
