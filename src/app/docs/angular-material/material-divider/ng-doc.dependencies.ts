import {DividerDemoBasic} from '@material-docs/material-divider/divider-demo-basic/divider-demo-basic.component';
import {MaterialDividerPageModule} from '@material-docs/material-divider/ng-doc.module';
import {NgDocDependencies} from '@ng-doc/core';

const MaterialDividerPageDependencies: NgDocDependencies = {
	module: MaterialDividerPageModule,
	demo: {DividerDemoBasic}
};

export default MaterialDividerPageDependencies;
