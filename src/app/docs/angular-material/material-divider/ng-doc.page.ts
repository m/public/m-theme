import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialDividerPage: NgDocPage = {
	title: `Divider`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialDividerPage;
