import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatListModule} from '@angular/material/list';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-divider-demo-basic',
	standalone: true,
    imports: [CommonModule, MatListModule, Preview],
	templateUrl: './divider-demo-basic.component.html',
	styleUrls: ['./divider-demo-basic.component.scss']
})
export class DividerDemoBasic {

}
