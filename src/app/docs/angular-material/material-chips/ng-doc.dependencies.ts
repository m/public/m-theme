import {ChipsDemoAutocomplete} from '@material-docs/material-chips/chips-demo-autocomplete/chips-demo-autocomplete.component';
import {ChipsDemoAvatar} from '@material-docs/material-chips/chips-demo-avatar/chips-demo-avatar.component';
import {ChipsDemoBasic} from '@material-docs/material-chips/chips-demo-basic/chips-demo-basic.component';
import {ChipsDemoDragDrop} from '@material-docs/material-chips/chips-demo-drag-drop/chips-demo-drag-drop.component';
import {ChipsDemoForm} from '@material-docs/material-chips/chips-demo-form/chips-demo-form.component';
import {ChipsDemoInput} from '@material-docs/material-chips/chips-demo-input/chips-demo-input.component';
import {ChipsDemoStacked} from '@material-docs/material-chips/chips-demo-stacked/chips-demo-stacked.component';
import {MaterialChipsPageModule} from '@material-docs/material-chips/ng-doc.module';
import {NgDocDependencies} from '@ng-doc/core';

const MaterialChipsPageDependencies: NgDocDependencies = {
	module: MaterialChipsPageModule,
	// Add your demos that you are going to use in the page here
	demo: {ChipsDemoAutocomplete, ChipsDemoAvatar, ChipsDemoBasic, ChipsDemoDragDrop, ChipsDemoForm, ChipsDemoInput, ChipsDemoStacked}
};

export default MaterialChipsPageDependencies;
