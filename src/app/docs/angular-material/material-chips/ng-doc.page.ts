import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialChipsPage: NgDocPage = {
	title: `Chips`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialChipsPage;
