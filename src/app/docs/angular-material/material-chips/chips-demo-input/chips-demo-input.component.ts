import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatChipEditedEvent, MatChipInputEvent, MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {Preview} from 'src/app/features/preview/preview.component';

export interface Fruit {
	name: string;
}

@Component({
	selector: 'app-chips-demo-input',
	standalone: true,
	imports: [CommonModule, Preview, MatChipsModule, MatIconModule, MatInputModule],
	templateUrl: './chips-demo-input.component.html',
	styleUrls: ['./chips-demo-input.component.scss']
})
export class ChipsDemoInput {
	addOnBlur = true;
	readonly separatorKeysCodes = [ENTER, COMMA] as const;
	fruits: Fruit[] = [{name: 'Lemon'}, {name: 'Lime'}, {name: 'Apple'}];

	add(event: MatChipInputEvent): void {
		const value = (event.value || '').trim();

		// Add our fruit
		if (value) {
			this.fruits.push({name: value});
		}

		// Clear the input value
		event.chipInput!.clear();
	}

	remove(fruit: Fruit): void {
		const index = this.fruits.indexOf(fruit);

		if (index >= 0) {
			this.fruits.splice(index, 1);
		}
	}

	edit(fruit: Fruit, event: MatChipEditedEvent) {
		const value = event.value.trim();

		// Remove fruit if it no longer has a name
		if (!value) {
			this.remove(fruit);
			return;
		}

		// Edit existing fruit
		const index = this.fruits.indexOf(fruit);
		if (index >= 0) {
			this.fruits[index].name = value;
		}
	}
}
