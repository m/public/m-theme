import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {FormControl, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatChipInputEvent, MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-chips-demo-form',
	standalone: true,
	imports: [CommonModule, Preview, MatChipsModule, MatIconModule, MatInputModule, MatButtonModule, ReactiveFormsModule],
	templateUrl: './chips-demo-form.component.html',
	styleUrls: ['./chips-demo-form.component.scss']
})
export class ChipsDemoForm {
	keywords = ['angular', 'how-to', 'tutorial', 'accessibility'];
	formControl = new FormControl(['angular']);

	removeKeyword(keyword: string) {
		const index = this.keywords.indexOf(keyword);
		if (index >= 0) {
			this.keywords.splice(index, 1);
		}
	}

	add(event: MatChipInputEvent): void {
		const value = (event.value || '').trim();

		// Add our keyword
		if (value) {
			this.keywords.push(value);
		}

		// Clear the input value
		event.chipInput!.clear();
	}
}
