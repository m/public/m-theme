import {CdkDrag, CdkDragDrop, CdkDropList, moveItemInArray} from '@angular/cdk/drag-drop';
import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatChipsModule} from '@angular/material/chips';
import {Preview} from 'src/app/features/preview/preview.component';

export interface Vegetable {
	name: string;
}

@Component({
	selector: 'app-chips-demo-drag-drop',
	standalone: true,
	imports: [CommonModule, Preview, CdkDropList, MatChipsModule, CdkDrag],
	templateUrl: './chips-demo-drag-drop.component.html',
	styleUrls: ['./chips-demo-drag-drop.component.scss']
})
export class ChipsDemoDragDrop {
	vegetables: Vegetable[] = [
		{name: 'apple'},
		{name: 'banana'},
		{name: 'strawberry'},
		{name: 'orange'},
		{name: 'kiwi'},
		{name: 'cherry'}
	];

	drop(event: CdkDragDrop<Vegetable[]>) {
		moveItemInArray(this.vegetables, event.previousIndex, event.currentIndex);
	}
}
