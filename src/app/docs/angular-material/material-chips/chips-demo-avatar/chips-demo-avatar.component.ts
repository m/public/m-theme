import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatChipsModule} from '@angular/material/chips';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-chips-demo-avatar',
  standalone: true,
	imports: [CommonModule, Preview, MatChipsModule],
  templateUrl: './chips-demo-avatar.component.html',
  styleUrls: ['./chips-demo-avatar.component.scss']
})
export class ChipsDemoAvatar {

}
