import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {CommonModule} from '@angular/common';
import {Component, ElementRef, ViewChild} from '@angular/core';
import {FormControl, ReactiveFormsModule} from '@angular/forms';
import {MatAutocompleteModule, MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MatChipInputEvent, MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {map, Observable, startWith} from 'rxjs';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-chips-demo-autocomplete',
	standalone: true,
	imports: [CommonModule, Preview, MatAutocompleteModule, MatChipsModule, ReactiveFormsModule, MatIconModule, MatInputModule],
	templateUrl: './chips-demo-autocomplete.component.html',
	styleUrls: ['./chips-demo-autocomplete.component.scss']
})
export class ChipsDemoAutocomplete {
	separatorKeysCodes: number[] = [ENTER, COMMA];
	fruitCtrl = new FormControl('');
	filteredFruits: Observable<string[]>;
	fruits: string[] = ['Lemon'];
	allFruits: string[] = ['Apple', 'Lemon', 'Lime', 'Orange', 'Strawberry'];

	@ViewChild('fruitInput') fruitInput: ElementRef<HTMLInputElement>;

	constructor() {
		this.filteredFruits = this.fruitCtrl.valueChanges.pipe(
			startWith(null),
			map((fruit: string | null) => (fruit ? this._filter(fruit) : this.allFruits.slice()))
		);
	}

	add(event: MatChipInputEvent): void {
		const value = (event.value || '').trim();

		// Add our fruit
		if (value) {
			this.fruits.push(value);
		}

		// Clear the input value
		event.chipInput!.clear();

		this.fruitCtrl.setValue(null);
	}

	remove(fruit: string): void {
		const index = this.fruits.indexOf(fruit);

		if (index >= 0) {
			this.fruits.splice(index, 1);
		}
	}

	selected(event: MatAutocompleteSelectedEvent): void {
		this.fruits.push(event.option.viewValue);
		this.fruitInput.nativeElement.value = '';
		this.fruitCtrl.setValue(null);
	}

	private _filter(value: string): string[] {
		const filterValue = value.toLowerCase();

		return this.allFruits.filter(fruit => fruit.toLowerCase().includes(filterValue));
	}
}
