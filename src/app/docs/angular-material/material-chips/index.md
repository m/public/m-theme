# {{ NgDocPage.title }}

## Chips Autocomplete

{{ NgDocActions.demo("ChipsDemoAutocomplete") }}

## Chips avatar

{{ NgDocActions.demo("ChipsDemoAvatar") }}

## Chips Drag and Drop

{{ NgDocActions.demo("ChipsDemoDragDrop") }}

## Chips Form

{{ NgDocActions.demo("ChipsDemoForm") }}

## Chips Input

{{ NgDocActions.demo("ChipsDemoInput") }}

## Chips Stacked

{{ NgDocActions.demo("ChipsDemoStacked") }}
