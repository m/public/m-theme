import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatChipsModule} from '@angular/material/chips';
import {ThemePalette} from '@angular/material/core';
import {Preview} from 'src/app/features/preview/preview.component';

export interface ChipColor {
	name: string;
	color: ThemePalette;
}

@Component({
	selector: 'app-chips-demo-stacked',
	standalone: true,
	imports: [CommonModule, Preview, MatChipsModule],
	templateUrl: './chips-demo-stacked.component.html',
	styleUrls: ['./chips-demo-stacked.component.scss']
})
export class ChipsDemoStacked {
	availableColors: ChipColor[] = [
		{name: 'none', color: undefined},
		{name: 'Primary', color: 'primary'},
		{name: 'Accent', color: 'accent'},
		{name: 'Warn', color: 'warn'}
	];
}
