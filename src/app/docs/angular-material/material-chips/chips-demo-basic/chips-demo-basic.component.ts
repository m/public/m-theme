import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatChipsModule} from '@angular/material/chips';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
  selector: 'app-chips-demo-basic',
  standalone: true,
	imports: [CommonModule, Preview, MatChipsModule],
  templateUrl: './chips-demo-basic.component.html',
  styleUrls: ['./chips-demo-basic.component.scss']
})
export class ChipsDemoBasic {

}
