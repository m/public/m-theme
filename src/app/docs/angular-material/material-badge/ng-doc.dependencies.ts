import {BadgeDemo} from '@material-docs/material-badge/badge-demo/badge-demo.component';
import {MaterialBadgePageModule} from '@material-docs/material-badge/ng-doc.module';
import {NgDocDependencies} from '@ng-doc/core';

const MaterialBadgePageDependencies: NgDocDependencies = {
	module: MaterialBadgePageModule,
	demo: {BadgeDemo}
};

export default MaterialBadgePageDependencies;
