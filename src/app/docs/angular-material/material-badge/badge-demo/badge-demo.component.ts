import {CommonModule} from '@angular/common';
import {Component} from '@angular/core';
import {MatBadgeModule} from '@angular/material/badge';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {Preview} from 'src/app/features/preview/preview.component';

@Component({
	selector: 'app-material-badge-demo',
	standalone: true,
	imports: [CommonModule, MatBadgeModule, MatButtonModule, MatIconModule, Preview],
	templateUrl: './badge-demo.component.html',
	styleUrls: ['./badge-demo.component.scss']
})
export class BadgeDemo {
	hidden = false;

	toggleBadgeVisibility() {
		this.hidden = !this.hidden;
	}

}
