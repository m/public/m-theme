import AngularMaterielCategory from '@categories/angular-materiel/ng-doc.category';
import {NgDocPage} from '@ng-doc/core';

const MaterialBadgePage: NgDocPage = {
	title: `Badge`,
	mdFile: './index.md',
	category: AngularMaterielCategory
};

export default MaterialBadgePage;
