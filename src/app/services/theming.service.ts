import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {THEME} from 'src/app/app.component';

@Injectable({
	providedIn: 'root'
})
export class ThemingService {
	private _theming: BehaviorSubject<THEME> = new BehaviorSubject<THEME>(null);

	get theming(): THEME {
		return this._theming.value;
	}

	set theming(value: THEME) {
		this._theming.next(value);
	}

	public themingObservable(): Observable<THEME> {
		return this._theming.asObservable();
	}

}
