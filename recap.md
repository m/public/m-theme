# M-UI
@include #whatIs

## Peer dependencies
@include #peerDependencies

## Configuration/installation

@include #installLib

## Usage
@include #usage

### More docs

[show more informations](https://apptest.mobilites-m.fr)
