import {Pipe, PipeTransform} from '@angular/core';


// Epochs
const epochs: any = [
	['an(s)', 31536000],
	['mois', 2592000],
	['j', 86400],
	['h', 3600],
	['mn', 60],
	['s', 1]
];


@Pipe({
	standalone: true,
	name: 'relativeDate'
})
export class RelativeDatePipe implements PipeTransform {

	transform(dateStamp: number): string {
		const tempDate = new Date(dateStamp);
		if (tempDate.getFullYear() === 1970) return 'Jamais';
		else {
			let timeAgoInSeconds = Math.floor((new Date().getTime() - tempDate.getTime()) / 1000);
			let {interval, epoch} = this.getDuration(timeAgoInSeconds);
			let suffix = interval === 1 ? '' : '';//pluriels supprimés car abbreviations
			return `il y a ${interval} ${epoch} ${suffix}`;
		}
	}

	getDuration(timeAgoInSeconds: number) {
		for (let [name, seconds] of epochs) {
			let interval = Math.floor(timeAgoInSeconds / seconds);
			if (interval >= 1) {
				return {
					interval: interval,
					epoch: name
				};
			}
		}
		return {
			interval: 0,
			epoch: 's'
		};
	};

}
