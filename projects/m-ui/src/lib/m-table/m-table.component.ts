import {SelectionModel} from '@angular/cdk/collections';
import {CommonModule} from '@angular/common';
import {AfterViewInit, Component, EventEmitter, Input, OnChanges, Output, SimpleChange, SimpleChanges, ViewChild} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRippleModule} from '@angular/material/core';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatMenuModule} from '@angular/material/menu';
import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import {MatSelectModule} from '@angular/material/select';
import {MatSort, MatSortModule} from '@angular/material/sort';
import {MatTable, MatTableDataSource, MatTableModule} from '@angular/material/table';
import {RelativeDatePipe} from './relative-date.pipe';
import {MIcons} from '../m-icons';
import {MLogoLines} from '../m-logo-lines';
import {ConfigActionTable, ConfigTableCustom, FilterText, HeaderTable, HeaderTypes} from './m-table.interface';

/**
 * @component
 * @name MTable
 * @description
 * A configurable table component that extends Angular Material's mat-table.
 * It includes features like filtering, sorting, pagination, dynamic content, and row selection.
 *
 * ### Usage example
 * ```html
 * <m-table [data]="tableData" [headers]="tableHeaders" [config]="tableConfig"></m-table>
 * ```
 */
@Component({
	selector: 'm-table',
	standalone: true,
	imports: [
		CommonModule, MatPaginatorModule, MatTableModule, MatButtonModule, MatMenuModule,
		MatIconModule, MatCheckboxModule, MatSelectModule, FormsModule, MatInputModule, MatSortModule, MIcons, MLogoLines, MatRippleModule, RelativeDatePipe
	],
	templateUrl: './m-table.component.html',
	styleUrls: ['./m-table.component.scss']
})
export class MTable implements AfterViewInit, OnChanges {
	/** Data to display in table */
	@Input() data: any[];
	/** Configuration for each table column header. */
	@Input() headers: HeaderTable[];
	/** Configuration settings for table behavior (e.g., pagination, filtering). */
	@Input() config: ConfigTableCustom;
	/** Actions configuration for rows or buttons. */
	@Input() actions: ConfigActionTable = {};
	/** Emits changes in row selection. */
	@Output() selectionChange = new EventEmitter();
	/** Emits changes filter */
	@Output() filter: EventEmitter<FilterText> = new EventEmitter();
	/** Reference to the MatTable instance. */
	@ViewChild('table', {static: false}) table: MatTable<any>;
	/** Reference to the MatPaginator instance. */
	@ViewChild(MatPaginator) paginator: MatPaginator;
	/** Reference to the MatSort instance for sorting. */
	@ViewChild(MatSort) sort: MatSort;

	public dataSource: MatTableDataSource<any>;
	public header: string[] = [];
	public selection = new SelectionModel<any>(true, []);

	protected readonly Object = Object;
	protected readonly HeaderTypes = HeaderTypes;

	/** init dataSource */
	constructor() {
		this.dataSource = new MatTableDataSource<any>([]);
	}

	/**
	 * @method ngAfterViewInit
	 * @description
	 * Initializes paginator and sorting after view is fully initialized.
	 */
	ngAfterViewInit(): void {
		if (this.config.pageSize.length > 0) this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
	}

	/**
	 * @method ngOnChanges
	 * @description
	 * Detects changes in input data and updates the table if necessary.
	 * @param {SimpleChanges} changes - Object containing changes in input properties.
	 */
	ngOnChanges(changes: SimpleChanges): void {
		// Detect changes on data and headers
		const data: SimpleChange = changes.data;
		const headers: SimpleChange = changes.headers;

		if (data && data.previousValue === undefined || headers) {
			this.initData();
		} else if (JSON.stringify(data.currentValue) !== JSON.stringify(data.previousValue)) {
			// Check if data send is different from previous data
			// Add new element to dataSource
			if (data.currentValue.length !== this.dataSource.data.length) this.initData();
			else {
				const newData: any[] = [...this.dataSource.data];
				if (typeof data === 'undefined') return;
				if (data.currentValue.length !== this.dataSource.data.length) this.initData();
				data.currentValue.forEach((oneCurr: any, index: string | number) => {
					if (JSON.stringify(oneCurr) !== JSON.stringify(this.dataSource.data[index])) newData[index] = oneCurr;
				});
				this.orderBy(newData);
				this.dataSource.data = newData;
			}
		}
	}


	/**
	 * Initializes table data and headers.
	 */
	private initData() {
		this.initHeader();
		this.initDataSource();
	}

	/**
	 * Initializes the data source based on headers configuration.
	 */
	private initDataSource(): void {
		if (this.data && this.headers) {
			// Map again the data to transform it if other keys found
			const newData = this.data.map(row => {
				// Add old Key to new key
				const transformedRow: any = {...row};
				this.headers.forEach((header: HeaderTable) => {
					if (header.type !== HeaderTypes.SELECTION) transformedRow[header.key] = this.getCellData(row, header);
				});
				return transformedRow;
			});

			this.orderBy(newData);
			this.dataSource.data = newData;
		}
	}

	/**
	 * Sorts data according to the default order in the configuration.
	 * @param data Data array to sort.
	 */
	private orderBy(data: any[]): void {
		if (this.config && this.config.defaultOrderColumn && this.config.defaultOrderColumn.column) {
			const column = this.config.defaultOrderColumn.column;
			const order: 'asc' | 'desc' = this.config.defaultOrderColumn.order;
			if (column && order) {
				const value = order === 'asc' ? -1 : 1;
				data.sort((a, b) => a[column] > b[column] ? value : -value);
			}
		}
	}

	/**
	 * Retrieves cell data, applying any header configuration options.
	 * @param row The data row.
	 * @param data Header configuration.
	 * @return Processed cell data.
	 */
	private getCellData(row: any, data: HeaderTable): any {
		let result = null;
		if (row) {
			result = data.otherKey && data.otherKey.length > 0 ? this.getNestedValue(row, data.otherKey) : row[data.key];
			if (data.options && data.options.maxLength && result) return result.length > data.options.maxLength ? result.substring(0, data.options.maxLength) + '...' : result;
			if (data.options && Object.keys(data.options).length > 0) {
				if (data.options.removeSpace) result = result.replace(/\s/g, '');
			}

		}
		return result;
	}

	/**
	 * Retrieves a nested value from an object based on a list of keys.
	 * @param obj The object to retrieve data from.
	 * @param keyData Array of keys for nested object access.
	 * @return The nested value.
	 */
	private getNestedValue(obj: any, keyData: string[]): any {
		return keyData.reduce((nestedObj, currentKey) => {
			return nestedObj && nestedObj[currentKey];
		}, obj);
	}

	/**
	 * Initializes header keys based on the headers input.
	 * @return void
	 */
	private initHeader(): void {
		if (this.headers) this.header = this.headers.map((item: HeaderTable) => item.key);
	}

	/**
	 * Applies a filter to the table data based on input text.
	 * @param event Filter input event.
	 */
	applyFilter(event: Event): void {
		const filterValue: string = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();
		this.filter.emit({filter: filterValue, data: this.dataSource.filteredData});

		if (this.config.pageSize.length > 0 && this.dataSource.paginator) {
			this.dataSource.paginator.firstPage();
		}
	}

	/**
	 * Toggles selection for all rows.
	 * @param evt Event indicating the toggle state.
	 */
	toggleAllRows(evt: any) {
		const select = !!evt;

		if (select && this.isAllSelected()) {
			this.selection.clear();
			this.selectionChange.emit(this.selection.selected);
			return;
		}
		this.selection.select(...this.dataSource.data);
		this.selectionChange.emit(this.selection.selected);
		return;
	}

	/**
	 * Checks if all rows are selected.
	 * @return True if all rows are selected.
	 */
	isAllSelected(): boolean {
		const numSelected: number = this.selection.selected.length;
		const numRows: number = this.dataSource.data.length;
		return numSelected === numRows;
	}

	/**
	 * Provides a label for the checkbox.
	 * @param row Optional row data.
	 * @return Checkbox label.
	 */
	checkboxLabel(row?: any): string {
		if (!row) return `${this.isAllSelected() ? 'désélectionner' : 'sélectionner'} tous les éléments`;
		return `${this.selection.isSelected(row) ? 'désélectionner' : 'sélectionner'} la ligne ${row.position + 1}`;
	}

	/**
	 * Builds CSS classes for a row based on header configuration.
	 * @param data Header configuration.
	 * @param row Row data.
	 * @return Array of CSS classes.
	 */
	buildClass(data: HeaderTable, row: any): string[] {
		if (data.options && typeof data.options.class !== 'undefined') {
			return data.options.class(row);
		}
		return [];
	}

	/**
	 * Toggles selection for a specific row and emits the selection change.
	 * @param evt The event triggering selection change.
	 * @param row Row to toggle.
	 */
	changeSelection(evt: any, row: any): void {
		const select = evt ? this.selection.toggle(row) : null;
		this.selectionChange.emit(this.selection.selected);
	}

	/**
	 * Checks if a specific row is selected.
	 * @param row Row to check.
	 * @return True if the row is selected.
	 */
	isSelected(row: any): boolean {
		return this.selection.isSelected(row);
	}

	/**
	 * Open link and stop propagation
	 * @param link : string
	 * @param target : string
	 * @param evt : Event
	 * @return void
	 */
	openLink(link: string, target: string, evt: Event): void {
		window.open(link, target);
		evt.stopImmediatePropagation();
	}

}
