import {Component, Input, OnInit} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MIcons} from '../m-icons';

export interface Affluence {
	/** Occupancy level that determines the description displayed. */
	lvl: number;
	/** Occupancy traduction lvl to text */
	name: string;
	/** Occupancy description */
	description: string;
	/** LightMode */
	lightMode: boolean;
}

/**
 * @component
 * @name MAffluence
 * @description
 * The `MAffluence` component displays the level of occupancy with a description.
 * This component uses Angular Material List and Icon to present the occupancy level.
 *
 * ### Usage example
 * ```html
 * <m-affluence [lvl]="1"></m-affluence>
 * ```
 *
 * @selector m-affluence
 * @standalone true
 * @module CommonModule, MatListModule, MatIconModule, MIcons
 */
@Component({
	selector: 'm-affluence',
	standalone: true,
	imports: [CommonModule, MatListModule, MatIconModule, MIcons],
	templateUrl: './m-affluence.component.html',
	styleUrls: ['./m-affluence.component.scss']
})
export class MAffluence implements OnInit {
	/**
	 * Occupancy level that determines the description displayed.
	 * Possible values:
	 * - 1 : Low
	 * - 2 : Moderate
	 * - 3 : High
	 *
	 * @default undefined (displays all descriptions)
	 */
	@Input() lvl: number;

	/**
	 * Enable/disable light mode.
	 */
	@Input() lightMode: boolean;

	/**
	 * @private
	 * List of available occupancy levels with their descriptions.
	 */
	private affluenceDescription: Affluence[] = [
		{lvl: 1, name: 'Faible', description: 'C\'est calme, une place assise vous attend.', lightMode: false},
		{lvl: 2, name: 'Modérée', description: 'Il reste sans doute quelques places assises.', lightMode: false},
		{lvl: 3, name: 'Forte', description: 'Il n\'y aura probablement plus de places assises.', lightMode: false}
	];

	/**
	 * Array used to display occupancy information based on the selected level (`lvl`).
	 */
	public affluence: any[] = [];

	/**
	 * Initializes the `affluence` array based on the provided occupancy level (`lvl`).
	 * If no level is defined, displays all descriptions.
	 */
	ngOnInit(): void {
		if (this.lvl === undefined || this.lvl === null) this.affluence = this.affluenceDescription;
		else {
			this.affluence.push(this.affluenceDescription.find(a => a.lvl === this.lvl));
			if (this.lightMode) this.affluence[0].lightMode = this.lightMode;
		}
	}

}
