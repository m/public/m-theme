import {CommonModule} from '@angular/common';
import {Component, Input} from '@angular/core';
import {ThemePalette} from '@angular/material/core';
import {MatIconModule} from '@angular/material/icon';
import {MIconsService} from './m-icons.service';
 
/**
 * Represents the mapping of an icon used in the application.
 *
 * This interface defines the properties required to register an icon,
 * including the icon type and its file name or path.
 */
export interface IconMapping {
	/**
	 * The unique type identifier for the icon.
	 * This is the name used to register the icon in the `MatIconRegistry`
	 * and to reference it throughout the application.
	 *
	 * @example
	 * 'home', 'settings', 'user-profile'
	 */
	type: string;
	/**
	 * The name or path of the SVG file for the icon.
	 * This is appended to the base icon path defined in `MIconsService`.
	 *
	 * @example
	 * 'home.svg', 'settings.svg', 'user-profile.svg'
	 */
	name?: string;
}

/**
 * Array of icon mappings used throughout the application.
 *
 * Each object in this array follows the `IconMapping` interface and represents a unique icon
 * that can be used by the application. Icons are registered in the application through the `MIconsService`
 * using this mapping. Each mapping defines a `type`, which serves as an identifier for the icon,
 * and optionally a `name`, which specifies the SVG file name in the icons directory.
 *
 * If `name` is not specified, the `type` may correspond to a default icon.
 *
 * Example usage:
 * ```
 * { type: 'accident', name: 'accident.svg' }
 * ```
 */
export const ICONS_MAPPING: IconMapping[] = [
	{ type: 'abnormalTraffic' },
	{ type: 'accident', name: 'accident.svg' },
	{ type: 'accidentInvolvingHeavyLorries' },
	{ type: 'agenceM', name: 'agenceM.svg' },
	{ type: 'animalPresenceObstruction' },
	{ type: 'arret' },
	{ type: 'ARE', name: 'ARE.svg' },
	{ type: 'authorityOperation' },
	{ type: 'autostop', name: 'autostop.svg' },
	{ type: 'blastingWork' },
	{ type: 'bouchon', name: 'bouchon.svg' },
	{ type: 'brokenDownVehicle' },
	{ type: 'BUS' },
	{ type: 'C38', name: 'C38.svg' },
	{ type: 'CAM' },
	{ type: 'CABLE_CAR', name: 'funicular.svg' },
	{ type: 'CAR' },
	{ type: 'chantier', name: 'chantier.svg' },
	{ type: 'citiz', name: 'citiz.svg' },
	{ type: 'citizyea', name: 'citiz.svg' },
	{ type: 'clearanceWork' },
	{ type: 'clusters', name: 'clusters.svg' },
	{ type: 'collision' },
	{ type: 'constructionWork' },
	{ type: 'constructionWorks' },
	{ type: 'COV', name: 'covoiturage_2.svg' },
	{ type: 'dat' },
	{ type: 'depositaire', name: 'relaisvente.svg' },
	{ type: 'disturbanceActivity' },
	{ type: 'DIR', name: 'DIR.svg' },
	{ type: 'dott', name: 'pony.svg' },
	{ type: 'dott_bicycle', name: 'ic-dott-bicycle.svg' },
	{ type: 'dott_scooter', name: 'ic-dott-scooter.svg' },
	{ type: 'environmentalObstruction' },
	{ type: 'equipmentOrSystemFault' },
	{ type: 'flooding' },
	{ type: 'generalInstructionOrMessageToRoadUsers' },
	{ type: 'generalNetworkManagement' },
	{ type: 'generalObstruction' },
	{ type: 'gnv', name: 'gnv.svg' },
	{ type: 'greve', name: 'greve.svg' },
	{ type: 'group', name: 'group.svg'},
	{ type: 'group_medium', name: 'group_medium.svg'},
	{ type: 'group_small', name: 'group_small.svg'},
	{ type: 'gpl', name: 'gpl.svg' },
	{ type: 'hazardsOnTheRoad' },
	{ type: 'hydrogene', name: 'hydrogene.svg' },
	{ type: 'ic_bus', name: 'ic_bus.svg' },
	{ type: 'ic_star', name: 'ic_star.svg' },
	{ type: 'ic_velo', name: 'ic_velo.svg' },
	{ type: 'ic_voiture', name: 'ic_voiture.svg' },
	{ type: 'ic_trottinette', name: 'ic_trottinette.svg' },
	{ type: 'ic_evt_exceptionnel', name: 'ic_evt_exceptionnel.svg' },
	{ type: 'ic_mes_alertes', name: 'ic_mes_alertes.svg' },
	{ type: 'IRVE_Chademo', name: 'IRVE_Chademo.svg' },
	{ type: 'IRVE_Combo', name: 'IRVE_Combo.svg' },
	{ type: 'IRVE_EF', name: 'IRVE_EF.svg' },
	{ type: 'IRVE_Type_2', name: 'IRVE_Type_2.svg' },
	{ type: 'IRVE_Type_3', name: 'IRVE_Type_3.svg' },
	{ type: 'ic_smiley_service_legerement_perturbe', name: 'ic_smiley_service_legerement_perturbe.svg' },
	{ type: 'ic_smiley_service_normal', name: 'ic_smiley_service_normal.svg' },
	{ type: 'ic_smiley_service_tres_perturbe', name: 'ic_smiley_service_tres_perturbe.svg' },
	{ type: 'ic_perturb_dark', name: 'ic_perturb_dark.svg' },
	{ type: 'ic_perturb_light', name: 'ic_perturb_light.svg' },
	{ type: 'ic_hors_service_dark', name: 'ic_hors_service_dark.svg' },
	{ type: 'ic_hors_service_light', name: 'ic_hors_service_light.svg' },
	{ type: 'incident' },
	{ type: 'info' },
	{ type: 'information', name: 'information.svg' },
	{ type: 'infrastructureDamageObstruction' },
	{ type: 'irve', name: 'irve.svg' },
	{ type: 'iti' },
	{ type: 'laneClosures' },
	{ type: 'letaxi', name: 'letaxi.svg' },
	{ type: 'lieux' },
	{ type: 'maintenanceWork' },
	{ type: 'maintenanceWorks' },
	{ type: 'manifestation', name: 'manifestation.svg' },
	{ type: 'meteo', name: 'meteo.svg' },
	{ type: 'MVA', name: 'MV_agence.svg' },
	{ type: 'MVC', name: 'MV_Consigne.svg' },
	{ type: 'nonWeatherRelatedRoadConditions' },
	{ type: 'objectOnTheRoad' },
	{ type: 'obstacle', name: 'obstacle.svg' },
	{ type: 'obstructionOnTheRoad' },
	{ type: 'other' },
	{ type: 'panne', name: 'panne.svg' },
	{ type: 'parkingCov', name: 'parkingCov.svg' },
	{ type: 'peopleOnRoadway' },
	{ type: 'PKG', name: 'PKG_0.svg' },
	{ type: 'pointCov', name: 'pointCov.svg' },
	{ type: 'pointService', name: 'pointService.svg' },
	{ type: 'pony', name: 'pony.svg' },
	{ type: 'poorEnvironmentConditions' },
	{ type: 'PAR', name: 'PR_0.svg' },
	{ type: 'publicEvent' },
	{ type: 'perturbation_low', name: 'ic_smiley_service_legerement_perturbe.svg' },
	{ type: 'perturbation_normal', name: 'ic_smiley_service_normal.svg' },
	{ type: 'perturbation_hight', name: 'ic_smiley_service_tres_perturbe.svg' },
	{ type: 'perturbation_over', name: 'ic_smiley_hors_service.svg' },
	{ type: 'RAIL', name: 'rail.svg' },
	{ type: 'recharge', name: 'recharge.svg' },
	{ type: 'reroutingManagement' },
	{ type: 'resurfacingWork' },
	{ type: 'restriction', name: 'restriction.svg' },
	{ type: 'roadClosed' },
	{ type: 'roadOperatorServiceDisruption' },
	{ type: 'roadOrCarriagewayOrLaneManagement' },
	{ type: 'roadsideAssistance' },
	{ type: 'roadworks' },
	{ type: 'rue' },
	{ type: 'serviceDisruption' },
	{ type: 'shedLoad' },
	{ type: 'spillageOnTheRoad' },
	{ type: 'speedManagement' },
	{ type: 'stationstaxi', name: 'ic-taxi.svg' },
	{ type: 'stops' },
	{ type: 'strongWinds' },
	{ type: 'SEM', name: 'SEM.svg' },
	{ type: 'SMA', name: 'SMA.svg' },
	{ type: 'SUBWAY' },
	{ type: 'tad', name: 'tad.svg' },
	{ type: 'tad_gsv', name: 'tad_gsv.svg' },
	{ type: 'tier', name: 'tier.svg' },
	{ type: 'transitInformation' },
	{ type: 'TRAM' },
	{ type: 'unprotectedAccidentArea' },
	{ type: 'vehicleObstruction' },
	{ type: 'vehicleOnWrongCarriageway' },
	{ type: 'veloservice', name: 'veloservice.svg' },
	{ type: 'visibilityReduced' },
	{ type: 'WALK' },
	{ type: 'weatherRelatedRoadConditions' },
	{ type: 'winterDrivingManagement' }
];

/**
 * @component
 * @name MIcons
 * @description
 * Icon component that wraps Angular Material's icon component to provide a customized icon display.
 * The component utilizes the `ICONS_MAPPING` constant for registering a set of icons and relies on
 * `MIconsService` to manage icon registration.
 *
 * ### Usage example
 * ```html
 * <m-icons type="home"></m-icons>
 * ```
 */
@Component({
	selector: 'm-icons',
	standalone: true,
	imports: [CommonModule, MatIconModule],
	templateUrl: './m-icons.component.html',
	styleUrls: ['./m-icons.component.scss'],
})
export class MIcons {
	/**
	 * Input property that defines the type of icon to display. This should match one of the `type` values
	 * in `ICONS_MAPPING` to ensure the correct icon is displayed.
	 */
	@Input() type?: string = '';

	/**
	 * Input property that defines the color palette for the icon. This should match one of the `colors` values
	 * in `ThemePalette` to ensure the correct color is applied to the icon.
	 */
	@Input() color?: ThemePalette;

	/**
	 * Initializes the component and registers a set of icons defined in the `ICONS_MAPPING` constant.
	 * Calls `registerIcons` method of `MIconsService` to ensure that all necessary icons are available
	 * for use within the application.
	 *
	 * @param mIconService The MIconsService instance to use for registering icons
	 */
	constructor(private mIconService: MIconsService) {
		this.mIconService.registerIcons(ICONS_MAPPING);
	}
}
