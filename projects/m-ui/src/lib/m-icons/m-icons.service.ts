import {Injectable} from '@angular/core';
import {MatIconRegistry} from '@angular/material/icon';
import {DomSanitizer} from '@angular/platform-browser';
import {IconMapping} from '../m-icons';

/**
 * Service to register custom icons for use throughout the application.
 * This service leverages Angular Material's MatIconRegistry and DomSanitizer
 * to securely register and manage SVG icons, providing a centralized approach
 * to icon registration.
 */
@Injectable({
    providedIn: 'root'
})
export class MIconsService {
	/*
	* Base path to the directory where the SVG icons are stored.
	* This path is used as a prefix for all icon URLs in the application.
	*/
    private iconPath = 'assets/icons/';

	/**
	 * @constructor
	 * @description
	 * Initializes the MIconsService, injecting dependencies for managing SVG icons.
	 * The constructor receives the Angular Material MatIconRegistry and Angular’s
	 * DomSanitizer to securely handle SVG icon URLs.
	 *
	 * @param {MatIconRegistry} iconRegistry - Injected Angular Material's icon registry
	 * used to register and manage SVG icons.
	 * @param {DomSanitizer} sanitizer - Injected Angular DOM sanitizer to bypass
	 * security restrictions on resource URLs for SVG icons.
	 */
    constructor(
        private iconRegistry: MatIconRegistry,
        private sanitizer: DomSanitizer
    ) {}

	/**
	 * @method registerIcons
	 * @description
	 * Registers a list of icons with Angular Material's MatIconRegistry.
	 * This method iterates over the provided icon mappings and registers each icon with
	 * MatIconRegistry using DomSanitizer to securely handle icon paths.
	 *
	 * @param {IconMapping[]} icon - An array of IconMapping objects, each representing an icon
	 * with a unique `type` and `name` that maps to an SVG file.
	 *
	 * @returns {void}
	 * This method does not return a value but registers the icons for use throughout the application.
	 *
	 * @example
	 * ```typescript
	 * const icons: IconMapping[] = [
	 *   { type: 'home', name: 'home.svg' },
	 *   { type: 'settings', name: 'settings.svg' }
	 * ];
	 * mIconsService.registerIcons(icons);
	 * ```
	 */
    registerIcons(icon: IconMapping[]): void {
        icon.forEach((i: IconMapping) => {
            this.iconRegistry.addSvgIcon(
                i.type,
                this.sanitizer.bypassSecurityTrustResourceUrl(`${this.iconPath}${i.name}`)
            );
        });
    }
}
