import {AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2, ViewChild} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatIconModule, MatIconRegistry} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MIcons} from '../m-icons';
import { MatButtonModule } from '@angular/material/button';
import { DomSanitizer } from '@angular/platform-browser';

export interface AppDownload {
	/** Logo de la plateforme de téléchargement (ex : logo Apple Store, Play Store...) */
	Logo: any;
	/** Lien de la page de téléchargement */
	Lien: string;
	/** Texte principal (nom de la plateforme de téléchargement) */
	TexteGras: string;

	/** Texte secondaire : pour contextualiser l'action */
	Texte: string;
}

/**
 * @component
 * @name MAppDownload
 * @description
 * Composant graphique pour afficher les liens de téléchargement d'une Appli
 * Exemple : liens pour télécharger l'Appli M sur Apple Store
 *
 * ### Usage example
 * ```html
 * <m-app-download [data]="App"></m-app-download>
 * ```
 *
 * @selector m-app-download
 * @standalone true
 * @module CommonModule, MatIconModule, MatButtonModule
 */
@Component({
	selector: 'm-app-download',
	standalone: true,
	imports: [CommonModule, MatIconModule, MatButtonModule],
	templateUrl: './m-app-download.component.html',
	styleUrls: ['./m-app-download.component.scss']
})
export class MAppDownload implements AfterViewInit {
	/**
	 * Logo + lien téléchargement + texte
	 *
	 */
	@Input() data: AppDownload;

	/**
	 * Element HTML pour l'icon représentant la plateforme de téléchargement
	 */
	@ViewChild('iconPrincipal', { read: ElementRef }) iconPrincipal!: ElementRef;
	@ViewChild('text', { read: ElementRef }) text!: ElementRef;

	constructor(public iconRegistry: MatIconRegistry, public sanitizer: DomSanitizer, private renderer : Renderer2){

	}

  ngAfterViewInit(): void {
    this.iconRegistry.getSvgIconFromUrl(this.sanitizer.bypassSecurityTrustResourceUrl(this.data.Logo.url)).subscribe((icon : SVGElement) => { 
		icon.setAttribute('fill', '');  
		
		//   let colortext = document.defaultView?.getComputedStyle(document.getElementById('textBase')!)['color']; 
		let colortext = this.text.nativeElement.style.color;  
				
		let childrens = Array.from(icon.children);
			childrens.forEach(element => {
			this.renderer.setAttribute(element, 'fill', colortext!);
			Array.from(element.children).forEach(element2 => {
				this.renderer.setAttribute(element2, 'fill', `${colortext!} !important`);
			});
		});
		
		this.renderer.appendChild(this.iconPrincipal.nativeElement, icon);
             
    });
  }

}
