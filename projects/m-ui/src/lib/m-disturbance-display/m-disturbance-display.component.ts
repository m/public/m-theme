import {CommonModule} from '@angular/common';
import {Component, Input} from '@angular/core';
import {MatButtonModule} from '@angular/material/button';
import {RouterLink} from '@angular/router';
import {MIcons} from '../m-icons/m-icons.component';

@Component({
	selector: 'm-disturbance-display',
	standalone: true,
	imports: [CommonModule, MIcons, RouterLink, MatButtonModule],
	templateUrl: './m-disturbance-display.component.html',
	styleUrls: ['./m-disturbance-display.component.scss']
})
export class MDisturbanceDisplay {

	/** The disturbance to display: TYPE show -> [M-features] */
	@Input() disturbance: any;

	/** The disturbance collection: TYPE show -> [M-features] */
	@Input() disturbanceCollection: any[];

	/** The link for redirection */
	@Input() link: string;

	/**
	 * Get the dynamic classes for the disturbance
	 * @param disturbance any
	 * @return string[]
	 */
	public getDynamicClasses(disturbance: any): string[] {
		let classes = ['line-nsv', 'm-disturbance-display'];
		if (disturbance && disturbance.nsv && disturbance.nsv != 0 && disturbance.nsv != 1 && disturbance.nsv != 5) {
			classes.push(`has-disturbance disturbance-${disturbance.nsv}`);
		}
		return classes;
	}
}
