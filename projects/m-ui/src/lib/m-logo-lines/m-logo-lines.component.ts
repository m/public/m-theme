import {CommonModule} from '@angular/common';
import {ChangeDetectionStrategy, Component, EventEmitter, HostBinding, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {MatCheckboxChange, MatCheckboxModule} from '@angular/material/checkbox';

/**
 * @interface
 * @name Line
 * @description
 * Represents the properties of a transport line, including its colors, identifiers, and type.
 */
export interface Line {
	/** The color of the line. */
	color: string;
	/** A unique identifier for the line. */
	gtfsId: string;
	/** The line ID. */
	id: string;
	/** The full name of the line. */
	longName: string;
	/** The mode of transport, such as `TRAM`, `BUS`. */
	mode: string;
	/** The short name of the line. */
	shortName: string;
	/** The color for the text displayed on the line. */
	textColor: string;
	/** The line type, such as `PROXIMO`, `BUS`, `TEAM`, `CHRONO_PERI`. */
	type: string;
}

/**
 * @interface
 * @name disturbanceInfo
 * @description
 * Represents the properties of a disturbance, including the disturbance level and whether it is outer.
 */
export interface disturbanceInfo {
	/** If `true`, the line has a disturbance */
	hasDisturbance: boolean;
	/** The disturbance level. */
	nsv: number;
	/** If `true`, the disturbance is outer. */
	outer: boolean;
}

/**
 * @component
 * @name MLogoLines
 * @description
 * A component that displays a logo for a given transport line. It customizes the size, padding,
 * and shape of the logo based on the input properties, and calculates optimal font size
 * for the line's name display.
 *
 * ### Usage example
 * ```html
 * <m-logo-lines [ligne]="lineData" [height]="50" [width]="50"></m-logo-lines>
 * ```
 */
@Component({
	selector: 'm-logo-lines',
	standalone: true,
	imports: [CommonModule, MatCheckboxModule],
	templateUrl: './m-logo-lines.component.html',
	styleUrls: ['./m-logo-lines.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class MLogoLines implements OnChanges {
	/** The transport line object that defines the logo's content and colors. */
	@Input() ligne: Line;
	/** Padding coefficient for the top of the logo. */
	@Input() paddingCoefTop = 0.23;
	/** Padding coefficient for the left side of the logo. */
	@Input() paddingCoefLeft = 0.23;
	/** The height of the logo. */
	@Input() @HostBinding('style.height.px') height: number;
	/** The width of the logo. */
	@Input() @HostBinding('style.width.px') width: number;
	/** If `true`, displays the logo in a circular shape */
	@Input() isCircle = false;
	/** If `true`, applies rounded corners to the logo. */
	@Input() isRounded = false;
	/** If `true`, displays the logo in a square shape. */
	@Input() isSquare = false;
	/** If `true` display mat-checkbox behind logo-lines */
	@Input() isCheckbox = false;
	/** If `true`, displays the line's description. */
	@Input() description: boolean = false;
	/** If `true`, selects the line. */
	@Input() checked: boolean = false;
	/** The opacity of the logo. */
	@Input() opacity: number = 1;
	/** Add Logo Disturbance */
	@Input() disturbance: disturbanceInfo;
	/** Emits when the line is clicked. */
	@Output() lineChange: EventEmitter<{ line: Line, checked: boolean }> = new EventEmitter<{ line: Line, checked: boolean }>();

	/** The viewBox attribute for SVG scaling, updated based on width and height. */
	viewBox: string;
	/** The long name of the line, derived from the `ligne` input. */
	longName: string;
	/** The short name of the line, derived from the `ligne` input. */
	shortName: string;
	/** The calculated font size for the line's text display. */
	fontSize: string;
	/** The type of the line, used for determining line-specific display logic. */
	lineType: string;

	/**
	 * @method onClick
	 * @description
	 * Emits the `lineClicked` event when the line is clicked.
	 *
	 * @returns {void}
	 */
	clickLine(ligne: Line, evt: MatCheckboxChange): void {
		this.lineChange.emit({line: ligne, checked: evt.checked});
	}

	/**
	 * @method ngOnChanges
	 * @description
	 * Updates the component when input properties change. Calculates viewBox, font size, and other
	 * properties based on the input line data and dimensions.
	 *
	 * @param {SimpleChanges} changes - The changes in input properties.
	 * @returns {void}
	 */
	ngOnChanges(changes: SimpleChanges): void {
		if (!changes.ligne) return;

		this.shortName = this.ligne.shortName;
		this.longName = this.ligne.longName;
		this.lineType = this.ligne.type;

		if (this.ligne.type === 'SNC') this.shortName = 'TER';
		if (this.ligne.type === 'NAVETTE') this.shortName = this.ligne.shortName.substring(this.ligne.shortName.length - 1);

		if (changes.width || changes.height) {
			this.viewBox = `0 0 ${this.width} ${this.height}`;
			const fontHeight = this.height - (this.height * this.paddingCoefTop);
			const maxTextWidth = this.width - (this.width * this.paddingCoefLeft);
			const fzh = (0.8 / 16) * fontHeight;
			const fzw = (0.8 / 8) * (maxTextWidth / this.shortName.length);
			this.fontSize = `${Math.min(fzw, fzh)}em`;
		}
	}

	/**
	 * Attribute class for any disturbances
	 * @returns {string}
	 */
	getClass(): string {
		if (this.disturbance && this.disturbance.hasDisturbance) {
			const hasOuter = this.disturbance.outer ? 'outer' : '';
			const hasDisturbance = this.disturbance.hasDisturbance ? 'has-disturbance' : '';
			let nsv = '';

			if (this.disturbance.nsv) {
				nsv = `disturbance-${this.disturbance.nsv}`;
			}

			return `${hasOuter} ${hasDisturbance} ${nsv}`;
		}
	}
}
