import {Component, Input} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';

@Component({
	selector: 'm-list-wrapper',
	standalone: true,
	imports: [CommonModule, MatButtonModule],
	templateUrl: './m-list-wrapper.component.html',
	styleUrls: ['./m-list-wrapper.component.scss'],
	exportAs: 'MListWrapper'
})
export class MListWrapper {

	/**
	 * Enable see more button
	 */
	@Input() seeMoreEnable: boolean = false;

	/**
	 * See more button state
	 */
	public seeMore: boolean = false;
}
